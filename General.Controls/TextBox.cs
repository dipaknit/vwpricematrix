using System;
using System.Drawing;
using System.Configuration;
using System.Windows.Forms;
using System.Text.RegularExpressions; 

/// <Page Name>TextBox</Page Name> 
/// <Description>Properties Of TextBox</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date>16-Sep-2006</Date>
/// <Author>Darshan</Author>
/// <Comments>"AllowAllCharactes" property added.</Comments>		
/// </Revision History>

namespace General.Controls
{
    public delegate void ValidateTextHandler(object sender,ref Boolean bFlag);

	/// <summary>
	/// Summary description for TextBox.
	/// </summary>
	public class TextBox : System.Windows.Forms.TextBox, iRoot
	{
        #region Declaration
        bool _blnApplyMySettings, _blnConvertEnterToTab , _bVerify = true, _bAllowSpace = true;
        bool _blnAllowAllChars = true;
        bool _blAllowCurrency = false;
               
        
        ucInfoMessage _ctlInfoMessage;
        string _strInfoMessage = string.Empty;
        
        public event ValidateTextHandler ValidateText;
        #endregion
		
        #region Public Properties
		/// <summary>
		/// ILink General property to access default property of the control
		/// </summary>
		public string CtrlProperty
		{
			get
			{
                return base.Text.Trim();
			}   
            set
            {
                if (this.Multiline)
                {
                        if ((_blnAllowAllChars) || (IsValid(value)))
                            base.Text = (value == null) ? string.Empty : value.Replace("\r\n", "\n").Replace("\n", "\r\n");
                }
                else
                    if ((_blnAllowAllChars) || (IsValid(value)))
                         base.Text = (value == null) ? string.Empty : value.Trim();
            }
		}

        /// <summary>
        /// ILink General property to specify Currency 
        /// </summary>
        public bool AllowCurrency
        {
            get
            {
                return _blAllowCurrency;
            }
            set
            {
                _blAllowCurrency = value;
            }
        }


        /// <summary>
        /// Override Control Text Property to Formate Text and set CtrlProperty
        /// </summary>
        public override string Text
        {
            get
            {
                return CtrlProperty;
            }
            set
            {
                CtrlProperty = (value == null) ? string.Empty : value.Trim();
            }
        }
        /// <summary>
        /// ILink General property to specify whether convert Enter Key to Tab Key or not
        /// </summary>
        public Boolean ConvertEnterToTab
        {
            get
            {
                return _blnConvertEnterToTab;
            }
            set
            {
                _blnConvertEnterToTab = value;
            }
        }

        /// <summary>
        /// ILink General property to specify whether convert Enter Key to Tab Key or not
        /// </summary>
        public Boolean Verify
        {
            get
            {
                return _bVerify; 
            }
            set
            {
                _bVerify = value;
            }
        }

        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return _blnApplyMySettings;
            }
            set
            {
                _blnApplyMySettings = value;
            }
        }
		
        /// <summary>
        /// ILink General property to decide in which infoMessage bar the message should display for the textbox
        /// </summary>
        public ucInfoMessage ctlInfoMessage
        {
            get
            {
                return _ctlInfoMessage;
            }
            set
            {
                _ctlInfoMessage = value;
            }
        }

        /// <summary>
        /// ILink General property to decide which messsage should display for the Textbox in InfoMessage control
        /// </summary>
        public string InfoMessage
        {
            get
            {
                return _strInfoMessage;
            }
            set
            {
                _strInfoMessage = value;
            }
        }

        /// <summary>
        /// ILink General property to decide whether space is allowed for particular textbox or not
        /// </summary>
        public bool AllowSpace
        {
            get
            {
                return _bAllowSpace;
            }
            set
            {
                _bAllowSpace = value;
            }
        }
        
        /// <summary>
        /// ILink General property to decide whether allow all chracters or only certain chracters.
        /// </summary>
        public bool AllowAllCharacters
        {
            get
            {
                return _blnAllowAllChars;
            }
            set
            {
                _blnAllowAllChars = value;
            }
        }
        
        #endregion

        # region Control Event
              
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && _blnApplyMySettings)
            {               
                 
                //Set back color for current control
                this.BackColor = clsGeneralInfo.txtBackColorCode;
                //Set font for current control
                this.Font = clsGeneralInfo.txtFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode; 
                this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.Verify = true;
            }
        }

        /// <summary>
        /// Set InfoMessage bar Message on control gotfocus
        /// </summary>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            if (ctlInfoMessage != null)
                ctlInfoMessage.InfoMessage = InfoMessage;   
        }

        /// <summary>
        /// Set InfoMessage bar Message emplt on control lostfocus
        /// </summary>
        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            if (ctlInfoMessage != null)
                ctlInfoMessage.InfoMessage = string.Empty;   
        }

        /// <summary>
        /// Send {Tab} Key on Enter Key Down on control KeyDown Event
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (!e.Control && this!=null && e.KeyCode == Keys.Enter && e.KeyCode != Keys.F5)
            { 
                Boolean bFlag=new Boolean();
                bFlag = true;
                if (this.ValidateText !=null)
                    ValidateText(this,ref bFlag);
                if (bFlag && ConvertEnterToTab  && (!this.Multiline) )
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }
       
        /// <summary>
        /// Handel Key press if AllowSpace=false on Control Key Press Event 
        /// </summary>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            // assemble new text with new KeyStroke
            //	and pass to validation routine.

            // NOTES;
            //	1) Delete key is NOT passed here
            //	2) control passed here after ProcessCmdKey() is run

            char c = e.KeyChar;
            if (!Char.IsControl(c))	// not sure about this?? nothing in docs about what is Control char??
            {
                // prevent spaces
                if (!AllowSpace)
                {
                    if (c.ToString() == " ")
                    {
                        e.Handled = true;
                        return;
                    }
                }

                string newText = base.Text.Substring(0, base.SelectionStart)
                    + c.ToString() + base.Text.Substring(base.SelectionStart + base.SelectionLength);

                if (!_blnAllowAllChars)
                {
                    if (!IsValid(newText))
                        e.Handled = true;
                }
            }
            else if (e.KeyChar==22) // This code is for Ctrl+V mean paste 
            {
                // prevent spaces
                if (!AllowSpace)
                { 
                    if (Clipboard.GetText().Contains(" "))
                    {
                        e.Handled = true;
                        return;
                    }
                }

                string newText = base.Text.Substring(0, base.SelectionStart)
                    + Clipboard.GetText() + base.Text.Substring(base.SelectionStart + base.SelectionLength);

                if (!_blnAllowAllChars)
                {
                    if (!IsValid(newText))
                        e.Handled = true;
                }                
            }
            base.OnKeyPress(e);
        }
       
        #endregion

        #region Validation Function
        /// <summary>
        /// Verify value in textbox must be alphanumeric
        /// </summary>
        /// <param name="val"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool IsValid(string val)
        {
            string Allowed_Chars;

            if(!_blAllowCurrency)
                Allowed_Chars = @"[^a-zA-Z_0-9.,\s]";
            else
                Allowed_Chars = @"[^a-zA-Z_0-9�$.,\s]";
            
            return !Regex.IsMatch(val, Allowed_Chars);
        }
        #endregion
    }
}