using System;
using System.Drawing;
using System.Configuration;


/// <Page Name>TabControl</Page Name> 
/// <Description>Properties Of TabControl</Description>
/// <Author>Dipak</Author>
/// <Created Date>24/7/2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{   
    public partial class TabControl : System.Windows.Forms.TabControl, iRoot
    {
        #region Declaration           
           
            bool blnApplyMySettings;
            private string strCtrPrp;
        #endregion

		#region Public Prioperties
		/// <summary>
		/// ILink General property to access default property of the control
		/// </summary>
		public string CtrlProperty
		{
			get
			{
                return  strCtrPrp;
			}
			set
			{
                strCtrPrp = (value == null) ? string.Empty : value.Trim();
			}
  
		}
 
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion

        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {
                ////Set back color for current control
                //this.BackColor = Color.Transparent;
                
                ////Set fore color for current control
                //this.ForeColor = clsGeneralInfo.btnFontColorCode; 

                //set Tab Draw Mode.
                this.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            }
        }
        #endregion


        #region Override Event/Method

        //protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
        //{
        //    base.OnDrawItem(e);

        //    //Brush theBrush = new SolidBrush(this.ForeColor);
        //    //StringFormat sf = new StringFormat();
            
        //    //sf.Alignment = StringAlignment.Center;
        //    //e.Graphics.FillRectangle(new SolidBrush(clsGeneralInfo.frmBackColorCode), e.Bounds);
        //    //e.Graphics.DrawString(this.TabPages[e.Index].Text,clsGeneralInfo.tbctlFont, theBrush, e.Bounds, sf);               
        //}

        protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
        {
            base.OnDrawItem(e);
            
            Brush theBrush = new SolidBrush(this.ForeColor);
            StringFormat sf = new StringFormat();
            sf.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Show;
            sf.Alignment = StringAlignment.Center;
            e.Graphics.FillRectangle(new SolidBrush(clsGeneralInfo.frmBackColorCode), e.Bounds);
            e.Graphics.DrawString(this.TabPages[e.Index].Text, clsGeneralInfo.tbctlFont, theBrush, e.Bounds, sf);               

        }
        protected override bool ProcessMnemonic(char charCode)
        {
            foreach (System.Windows.Forms.TabPage p in this.TabPages)
            {
                if (System.Windows.Forms.Control.IsMnemonic(charCode, p.Text))
                {
                    this.SelectedTab = p;
                    this.Focus();
                    return true;
                }
            }
            return false;
        }

        #endregion
    }
}