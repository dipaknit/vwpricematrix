using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

/// <Page Name>NumericTextBox</Page Name> 
/// <Description>Property Of Numeric TextBox</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
	/// <summary>
	/// Numeric data entry control
	/// </summary>
	public class NumericTextBox : General.Controls.TextBox
	{
		#region Declaration

		private NumEditType m_inpType;		

		public enum NumEditType
		{			
			Decimal,
			Single,
			Double,
			SmallInteger,
			Integer,
			LargeInteger
		}

		#endregion

		#region Public Properties

		#region InputType
		[Description("Sets the numeric type allowed"), Category("Behavior")]
		public NumEditType InputType
		{
			get{return m_inpType;}
			set
			{
				m_inpType = value;			
			}
		}

		#endregion

		#region Text
		public override string Text
		{
			get{return base.Text.Replace(",","");}
			set
			{
    			base.Text = FormatText(value);
			}
		}

		#endregion

		#region Precision
		int _intPrecision = 2;
		public int Precision
		{
			get
			{
				return _intPrecision;
			}
			set
			{
				_intPrecision = value;
			}
		}
		#endregion

        #region Max Value
        [Description("Sets the Maximum value allowed"), Category("Behavior")]
        double _dblMaxValue=9999999999999;
        public double MaxValue
        {
            get
            {
                return _dblMaxValue;
            }
            set
            {
                _dblMaxValue = value;
            }
        }
        #endregion

		#endregion

		#region Constructor
		public NumericTextBox()
		{
            // set default input type
			this.InputType = NumEditType.Integer;

			// NOTE: Existing context menu allows Paste command, with no known
			//	method to intercept. Only option is to reset to empty.
			//	(setting to Null doesn't work)
			this.ContextMenu = new ContextMenu();
		}
		#endregion

        #region Set Max length
        protected override void OnCreateControl()
        {
            //Call base event
            base.OnCreateControl();

            //Run this only in execution mode
            if (!this.DesignMode)
            {
                //Set Max Length for numeric controls
                if(this.InputType == NumEditType.Integer && this.MaxLength == 32767)
                    this.MaxLength = 4;
                else if (this.InputType == NumEditType.Decimal && this.MaxLength == 32767)
                    this.MaxLength = 10;

                //Set textalign property
                this.TextAlign = HorizontalAlignment.Right;
            }
        }
        #endregion

        #region Public Methods
        private bool IsValid(string strInputValue)
        {
            // this method validates the ENTIRE string
            //	not each character
            // Rev 1: Added bool user param. This bypasses preliminary checks
            //	that allow -, this is used by the OnLeave event
            //	to prevent
            bool bIsValid = true;
            
            if (strInputValue.ToString().Length ==0)
                return bIsValid;

            // parse into dataType, errors indicate invalid value
            // NOTE: parsing also validates data type min/max
            try
            {
                switch (m_inpType)
                {                    
                    case NumEditType.Single:
                        if (float.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(0)) > _dblMaxValue) bIsValid = false;
                        break;
                    case NumEditType.Double:
                        if (double.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(_intPrecision)) > _dblMaxValue) bIsValid = false;
                        break;
                    case NumEditType.Decimal:
                        if (decimal.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(2)) > decimal.Parse(_dblMaxValue.ToString("N0"), clsGeneralInfo.GetNumberFormatProvider(2))) bIsValid = false;
                        break;
                    case NumEditType.SmallInteger:
                        if (short.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(0)) > _dblMaxValue) bIsValid = false;                        
                        break;
                    case NumEditType.Integer:
                        if (int.Parse(strInputValue) > _dblMaxValue) bIsValid = false;                        
                        break;
                    case NumEditType.LargeInteger:
                        if (long.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(0)) > _dblMaxValue) bIsValid = false;                        
                        break;
                    default:
                        throw new ApplicationException();
                }
            }
            catch
            {
                bIsValid = false;
            }
            return bIsValid;
        }
        

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			// trap Ctrl-V paste and prevent invalid values
			//	return false to allow further processing
			if(keyData == (Keys)Shortcut.CtrlV || keyData == (Keys)Shortcut.ShiftIns)
			{
				IDataObject iData = Clipboard.GetDataObject();
 
				// assemble new string and check IsValid
				string newText;
				newText = base.Text.Substring(0, base.SelectionStart)
					+ (string)iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);

				// check if data to be pasted is convertable to inputType
				if(!IsValid(newText))
					return true;
			}
			return base.ProcessCmdKey(ref msg, keyData);
		}

		protected override void OnLeave(EventArgs e)
		{
			// handle - and leading zeros input since KeyPress handler must allow this
            if (base.Text.Length > 0)
			{
                if (!IsValid(base.Text))
                    base.Text = string.Empty;
                else if (Double.Parse(base.Text, clsGeneralInfo.GetNumberFormatProvider(2)) == 0)	// this used for -0, 000 and other strings
                    base.Text = "0";
                else
                    //setting strings format after leaving numeric box.
                    base.Text = FormatText(base.Text);
			}
			base.OnLeave(e);
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
            char c = e.KeyChar;
			if(!Char.IsControl(c))	
			{
                // prevent "-" sign
                if (c.Equals('-') || c.Equals(','))
                {
                    e.Handled = true;
                    return;
                }

				// prevent spaces
                if (c.ToString().Trim().Length == 0)
				{
					e.Handled = true;
					return;
				}

				string newText = base.Text.Substring(0, base.SelectionStart)
                    + c.ToString() + base.Text.Substring(base.SelectionStart + base.SelectionLength);

                if (IsValid(newText))
                {
                    if (_intPrecision == 0)
                    {
                        if (newText.IndexOf(".") != -1)
                            e.Handled = true;
                    }
                    else
                    {
                        if ((newText.IndexOf(".") != -1) && (newText.Length - newText.IndexOf(".") - _intPrecision) > 1)
                            e.Handled = true;
                    }
                }   
                else
                {
                    e.Handled = true;                
                }
                
			}
			base.OnKeyPress(e);
		}

        private string FormatText(string strInputValue)
        {
            try
            {
                switch (m_inpType)
                {
                    case NumEditType.Double:
                        //return value in "*.x" format,where x= _intPrecision.
                        //return FormatTextWithPrecision(double.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(_intPrecision)).ToString(),_intPrecision);
                        return double.Parse(strInputValue).ToString("N" + _intPrecision.ToString());
                    case NumEditType.Decimal:
                        //return value in "*.x" format,where x= _intPrecision.
                        return FormatTextWithPrecision(decimal.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(2)).ToString(), 0);
                    case NumEditType.Integer:
                        return int.Parse(strInputValue).ToString();
                    case NumEditType.Single:
                        return Single.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(0)).ToString();                        
                    case NumEditType.SmallInteger:
                        return short.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(0)).ToString();
                    case NumEditType.LargeInteger:
                        return long.Parse(strInputValue, clsGeneralInfo.GetNumberFormatProvider(0)).ToString();
                    default:
                        throw new ApplicationException();
                }
            }
            catch
            {
                return string.Empty;
            }
        }

		#endregion

        #region FormatText in "*.x" format,where x= no of Precision.
        /// <summary>
        /// FormatTextWithPrecision(52,2)
        /// result : 52.00
        /// FormatTextWithPrecision(52.1,2)
        /// result : 52.10
        /// </summary>
        /// <param name="strInputValue"></param>
        /// <param name="intPrecision"></param>
        /// <returns></returns>
        private string FormatTextWithPrecision(string strInputValue, int intPrecision)
        {
            try
            {
                if (strInputValue == string.Empty) return string.Empty;
                else if (intPrecision == 0)
                {
                    if (strInputValue.IndexOf(".") != -1) { return strInputValue.Substring(0, strInputValue.IndexOf(".")); }
                    else { return strInputValue; }
                }
                else
                {
                    if (strInputValue.IndexOf(".") != -1)
                    {
                        return strInputValue.PadRight(strInputValue.Length + intPrecision - (strInputValue.Substring(strInputValue.IndexOf(".") + 1)).Length, '0');
                    }
                    else
                    {
                        return strInputValue + ".".PadRight(intPrecision + 1, '0');
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
