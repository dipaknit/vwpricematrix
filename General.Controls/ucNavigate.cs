/// <Page Name>ucNavigate</Page Name> 
/// <Description>
/// This is user control used for navigation while multiple items returned by search/show
/// </Description>
/// <Author>Darshan</Author>
/// <Created Date>03/08/2006</Created Date>
/// <Revision History>
/// <Date></Date>
/// <Author></Author>
/// <Comments></Comments>		
/// </Revision History>
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace General.Controls
{
    public partial class ucNavigate : UserControl
    { 

        #region "Variable Declaration"
        /// <summary>
        /// _intRecordCount = Record count
        /// _intCurrentRecordIndex = Current record index
        /// _objDataTbl = Datatable to hold the records
        /// objDataRow = To hold current record
        /// </summary>
        int _intRecordCount;
        int _intCurrentRecordIndex;
        DataTable _objDataTbl;
        //DataRow objDataRow;
        #endregion

        #region "Event Declaration"
        /// <summary>
        /// These user defined events are used to handle navigation button events. 
        /// </summary>
        public event EventHandler btnShow_Click;
        public event EventHandler btnFirst_Click;
        public event EventHandler btnPrevious_Click;
        public event EventHandler btnNext_Click;
        public event EventHandler btnLast_Click;
        #endregion

        #region "Constructor"
        public ucNavigate()
        {
            InitializeComponent();
            btnShow.Image = General.Resource.Properties.Resources.Show;
            btnFirst.Image = General.Resource.Properties.Resources.First;
            btnPrevious.Image = General.Resource.Properties.Resources.Prev;
            btnNext.Image = General.Resource.Properties.Resources.Next;
            btnLast.Image = General.Resource.Properties.Resources.Last;

        }
        #endregion

        #region "Public Properties"
        /// <summary>
        /// This property is used to Get or Set RecordCount.
        /// </summary>
        /// <param name=""></param>
        /// <returns>_intRecordCount</returns>
        /// <Return Type>integer</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public int RecordCount
        {
            get
            {
                return _intRecordCount;      
            }
            set
            {
                _intRecordCount = value;
            }
        }

        /// <summary>
        /// This property is used to Get or Set CurrentRecoprdIndex
        /// </summary>
        /// <param name=""></param>
        /// <returns>_intCurrentRecordIndex</returns>
        /// <Return Type>integer</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>22/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public int CurrentRecordIndex
        {
            get
            {
                return _intCurrentRecordIndex;
            }
            set
            {
                _intCurrentRecordIndex = value;
                if (_objDataTbl != null)
                    SetControlState();
                else
                {
                    btnShow.Enabled = true;
                    ResetControls();
                }
            }
        }

        /// <summary>
        /// This property is used to Get or Set DataSource to this control.
        /// </summary>
        /// <param name=""></param>
        /// <returns>objDataTable</returns>
        /// <Return Type>DataTable</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public DataTable DataSource
        {
            get
            {
                return _objDataTbl;
            }
            set
            {
                _objDataTbl = value;
            }
        }

        /// <summary>
        /// This property is used to Get current record (datarow). 
        /// </summary>
        /// <param name=""></param>
        /// <returns>objDataRow</returns>
        /// <Return Type>DataRow</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public DataRow CurrentRecord
        {
            get
            {
                return _objDataTbl.Rows[_intCurrentRecordIndex];
            }
        }

        /// <summary>
        /// Returns Show Button is Visible or not?
        /// </summary>
        /// <param name=""></param>
        /// <returns>btnShow.Visible</returns>
        /// <Return Type>bool</Return Type>
        /// <Author>Jay Parikh</Author>
        /// <Created Date>08/01/2007</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public bool bShow
        {
            get
            {
                return btnShow.Visible;
            }
        }

        /// <summary>
        /// Returns Next Button is Visible or not?
        /// </summary>
        /// <param name=""></param>
        /// <returns>btnNext.Visible</returns>
        /// <Return Type>bool</Return Type>
        /// <Author>Ravi Desai</Author>
        /// <Created Date>04/01/2007</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public bool bNext
        {
            get
            {
                return btnNext.Visible;  
            }
        }

        /// <summary>
        /// Returns Previous Button is Visible or not?
        /// </summary>
        /// <param name=""></param>
        /// <returns>btnPrevious.Visible</returns>
        /// <Return Type>bool</Return Type>
        /// <Author>Ravi Desai</Author>
        /// <Created Date>04/01/2007</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public bool bPrevious
        {
            get
            {
                return btnPrevious.Visible;
            }
        }

        /// <summary>
        /// Returns First Button is Visible or not?
        /// </summary>
        /// <param name=""></param>
        /// <returns>btnFirst.Visible</returns>
        /// <Return Type>bool</Return Type>
        /// <Author>Ravi Desai</Author>
        /// <Created Date>04/01/2007</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public bool bFirst
        {
            get
            {
                return btnFirst.Visible;
            }
        }

        /// <summary>
        /// Returns Last Button is Visible or not?
        /// </summary>
        /// <param name=""></param>
        /// <returns>btnLast.Visible</returns>
        /// <Return Type>bool</Return Type>
        /// <Author>Ravi Desai</Author>
        /// <Created Date>04/01/2007</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public bool bLast
        {
            get
            {
                return btnLast.Visible;
            }
        }
        #endregion

        #region "Set control state"
        /// <summary>
        /// This function sets controls' state from current recordindex and datasource.
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <Return Type>Void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>07/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void SetControlState()
        {
            this.lblStatus.Visible = true;
            
            this.btnNext.Visible = true;
            this.btnPrevious.Visible = true;
            this.btnFirst.Visible = true;
            this.btnLast.Visible = true;

            this.btnShow.Enabled = false;
            _intRecordCount = _objDataTbl.Rows.Count ;

            if (_intRecordCount < 2)
            {
                this.btnFirst.Visible = false;
                this.btnLast.Visible = false;
                this.btnNext.Visible = false;
                this.btnPrevious.Visible = false;
                lblStatus.Visible = false;
                       
            }
            else
            {
                if (_intCurrentRecordIndex == 0)
                {
                    btnFirst.Enabled = false;
                    btnPrevious.Enabled = false;
                    btnNext.Enabled = true;
                    btnLast.Enabled = true;
                    this.lblStatus.Visible = false;
                }
                else if (_intCurrentRecordIndex == _intRecordCount - 1)
                {
                    btnFirst.Enabled = true;
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = false;
                    btnLast.Enabled = false;
                }
                else
                {
                    btnFirst.Enabled = true;
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                    btnLast.Enabled = true;
                }
                lblStatus.Visible = true;
                lblStatus.Text = (_intCurrentRecordIndex + 1).ToString() + " of " + _objDataTbl.Rows.Count.ToString();
            }
            
        }
        #endregion

        #region "Function: Reset Controls"
        /// <summary>
        /// This function resets all the navigator control to its initial state
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <Return Type>Void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>07/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public void ResetControls()
        {
            btnShow.Visible = true;
            btnShow.Enabled = true;
            btnFirst.Visible = false;
            btnPrevious.Visible = false;
            btnNext.Visible = false;
            btnLast.Visible = false;
            lblStatus.Visible = false;
        }
        #endregion

        #region "Remove Current Record"
        /// <summary>
        /// This function remove current record and button according to that
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <Return Type>Boolean</Return Type>
        /// <Author>Ravi</Author>
        /// <Created Date>23/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public Boolean RemoveCurrentRow()
        {
            _objDataTbl.Rows.Remove(this.CurrentRecord);
            if (_objDataTbl.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                _intCurrentRecordIndex = _intCurrentRecordIndex % (_objDataTbl.Rows.Count - 1);
                SetControlState();
                return true;
            }
        }
        #endregion

        #region "Navigation Button Click Event Handler"
        /// <summary>
        /// on click event handler for Show button
        /// It also raise btnShow_OnClick() event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void _btnShow_Click(object sender, EventArgs e)
        {
            this.CurrentRecordIndex = 0;
            ResetControls();
            btnShow_Click(sender,e);

            btnPrevious.Enabled = false;
            btnFirst.Enabled = false;

            /// If result not found (Data) than return.
            if (_objDataTbl == null)
                 return;

            _intRecordCount = _objDataTbl.Rows.Count;

            if (_objDataTbl.Rows.Count == 1)
            {
                btnShow.Enabled = false;
            }
            else if ((_objDataTbl.Rows.Count > 1))
            {
                btnShow.Enabled = false;
                btnFirst.Visible = true;
                btnPrevious.Visible = true;
                btnNext.Visible = true;
                btnNext.Enabled = true;
                btnLast.Visible = true;
                btnLast.Enabled = true;

                btnFirst.Enabled = false;
                btnPrevious.Enabled = false;

                //Show search status
                lblStatus.Visible = true;
                lblStatus.Text = "1 of " + _objDataTbl.Rows.Count.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
            }
           
           
        }

        /// <summary>
        /// on click event handler for First button
        /// It also raise btnFirst_OnClick() event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void _btnFirst_Click(object sender, EventArgs e)
        {
            _intCurrentRecordIndex = 0;
            btnFirst.Enabled = false;
            btnPrevious.Enabled = false;
            btnNext.Enabled = true;
            btnLast.Enabled = true;

            btnFirst_Click(sender, e);
            lblStatus.Text = "1 of " + _objDataTbl.Rows.Count.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
        }

        /// <summary>
        /// on click event handler for Previous button
        /// It also raise btnPrevious_OnClick() event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void _btnPrevious_Click(object sender, EventArgs e)
        {
            if (_intCurrentRecordIndex < _objDataTbl.Rows.Count)
                _intCurrentRecordIndex--;
            btnNext.Enabled = true;
            btnLast.Enabled = true;

            if (_intCurrentRecordIndex == 0)
            {
                btnPrevious.Enabled = false;
                btnFirst.Enabled = false;
            }
            btnPrevious_Click(sender, e);
            lblStatus.Text = (_intCurrentRecordIndex + 1).ToString() + " of " + _objDataTbl.Rows.Count.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
        }

        /// <summary>
        /// on click event handler for Next button
        /// It also raise btnNext_OnClick() event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void _btnNext_Click(object sender, EventArgs e)
        {
            if (_intCurrentRecordIndex < _objDataTbl.Rows.Count)
                _intCurrentRecordIndex++;
            btnPrevious.Enabled = true;
            btnFirst.Enabled = true;

            if (_intCurrentRecordIndex == (_objDataTbl.Rows.Count - 1))
            {
                btnNext.Enabled = false;
                btnLast.Enabled = false;
            }

            btnNext_Click(sender, e);
            lblStatus.Text = (_intCurrentRecordIndex + 1).ToString() + " of " + _objDataTbl.Rows.Count.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
        }

        /// <summary>
        /// on click event handler for Last button
        /// It also raise btnLast_OnClick() event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <Author>Darshan</Author>
        /// <Created Date>03/08/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void _btnLast_Click(object sender, EventArgs e)
        {
            _intCurrentRecordIndex = _objDataTbl.Rows.Count -1;
            btnLast.Enabled = false;
            btnNext.Enabled = false;
            btnPrevious.Enabled = true;
            btnFirst.Enabled = true;
            btnLast_Click(sender, e);
            lblStatus.Text = (_intCurrentRecordIndex + 1).ToString() + " of " + _objDataTbl.Rows.Count.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
        }
        #endregion

    }
}
