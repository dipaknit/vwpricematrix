using System;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;


/// <Page Name>NumericUpDown</Page Name> 
/// <Description>Property Of NumericUpDown</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
    public partial class NumericUpDown : System.Windows.Forms.NumericUpDown, iRoot
    {
        #region Declaration
        bool blnApplyMySettings;
        #endregion


        #region Public Prioperties
        /// <summary>
        /// Get or set Numeric control Value
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return Value.ToString(clsGeneralInfo.GetNumberFormatProvider(2));
            }
            set
            {
                if (value != null)
                    if (value.Length == 0)
                        this.Value = 0;
                    else
                        this.Value = (value == null) ? 0 : decimal.Parse(value, clsGeneralInfo.GetNumberFormatProvider(2));
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion


        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {   
                //Set back color for current control
                this.BackColor = clsGeneralInfo.numUpdnBackColorCode;
                //Set font for current control
                this.Font = clsGeneralInfo.numUpdnFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode; 
            }
        }
        #endregion
    }
}
