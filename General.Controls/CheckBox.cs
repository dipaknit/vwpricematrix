using System;
using System.Drawing;
using System.Configuration;

/// <Page Name>CheckBox</Page Name> 
/// <Description>I-Link's User Defined Button Controls</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
	/// <summary>
	/// Summary description for CheckBox.
	/// </summary>
    public class CheckBox : System.Windows.Forms.CheckBox, iRoot
	{
        #region Variable Declaration               
        bool blnApplyMySettings;
        #endregion

		#region Public Prioperties
		/// <summary>
		/// ILink General property to access default property of the control
		/// </summary>
		public string CtrlProperty
		{
			get
			{
				//return ((Checked)?"1":"0");
                return Checked.ToString();
			}
			set
			{
                Checked = ((value.Equals("Y", StringComparison.CurrentCultureIgnoreCase)) || value.Equals("1") || value.Equals(true.ToString()));
			}
		}
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
		#endregion

        #region OnCreateControl
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <Return Type></Return Type>
        /// <Author>Hardik</Author>
        /// <Created Date></Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {      
                //Set back color for current control
                this.BackColor = Color.Transparent;
                //Set font for current control
                this.Font = clsGeneralInfo.chkFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode;
            }
        }
        #endregion
	}
}