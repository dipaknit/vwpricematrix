using System;
using System.Drawing;
using System.Configuration;

/// <Page Name>PanelHeader</Page Name> 
/// <Description>Properties OfHeader Panel</Description>
/// <Author>Dipak</Author>
/// <Created Date>27/7/2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    public class PanelHeader : System.Windows.Forms.Panel,iRoot
    {
        #region Declaration
        string strCtrlProp;
        bool blnApplyMySettings;
        #endregion

        #region Public Prioperties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return strCtrlProp;
            }
            set
            {
                strCtrlProp  = (value == null) ? string.Empty : value.Trim();;
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion

        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            /// <summary>
            /// set Back Ground Image.
            /// </summary>
            /// <Author>Dipak</Author>
            /// <Created Date>27/06/2006</Created Date>

            if (!this.DesignMode && blnApplyMySettings)
            {
                this.BackgroundImage = Properties.Resources.heading_strip;
                this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Tile;
            }
        }
        #endregion
    }
}

