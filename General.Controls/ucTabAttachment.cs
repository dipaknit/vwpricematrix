using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;


/// <Page Name>ucTabAttachment.cs</Page Name> 
/// <Description>
/// for File Attachment with record.
/// </Description>
/// <Author>Dipak</Author>
/// <Created Date>25/10/2006</Created Date>
/// <Revision History>
/// <Date></Date>
/// <Author></Author>
/// <Comments></Comments>		
/// </Revision History>
namespace Imms.Controls
{
    public partial class ucTabAttachment : UserControl
    {

        #region Variable Declaration
        DataTable _dtFileList;
        ListViewItem _lvwGetFilePath = new ListViewItem();
        public event EventHandler DownloadFile;
        #endregion

        #region Constructor
        public ucTabAttachment()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        /// <summary>
        /// for Set File List
        /// </summary>
        /// <param name=""></param>
        /// <returns>_dtFileList</returns>
        /// <Return Type>DataTable</Return Type>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public DataTable GetDataList
        {
            get
            {
                return _dtFileList;
            }
        }

        /// <summary>
        /// for Set File Path For Down Load Used on File Double Click
        /// </summary>
        /// <param name=""></param>
        /// <returns>_lvwGetFilePath</returns>
        /// <Return Type>ListViewItem</Return Type>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date>
        /// <Author></Author>
        /// <Comments></Comments>
        /// </Revision History>
        public ListViewItem GetFilePath
        {
            get
            {
                return _lvwGetFilePath;
            }
        }
        #endregion

        #region Button Click Event

        /// <summary>
        /// For Add in Files in List
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmFileDialog objfrmFileDialog = new frmFileDialog();
            ListViewItem lvwItem = new ListViewItem();

            objfrmFileDialog.ShowDialog();

            if (objfrmFileDialog.getFilePath.Length > 0)
            {
                lvwItem.Text = objfrmFileDialog.getFilePath;
                lvwItem.SubItems.Add(objfrmFileDialog.setModeOfDel);
                lvwFileAttach.Items.Add(lvwItem);
            }

            //Adding Items In Data table
            RefreshDataTable();
        }

        /// <summary>
        /// For Remove Files From List
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvwItem in lvwFileAttach.SelectedItems)
                lvwItem.Remove();

            RefreshDataTable();
        }
        #endregion

        #region List view Double Click Event
        /// <summary>
        /// For view and download file
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void lvwFileAttach_DoubleClick(object sender, EventArgs e)
        {
            foreach (ListViewItem _lvwTemp in lvwFileAttach.SelectedItems)
            {
                _lvwGetFilePath = _lvwTemp;

                //Call Event for Download File and view it.
                DownloadFile(sender, e);
            }
        }
        #endregion

        #region Form Load Event
        /// <summary>
        /// Form Load Event for adding Coloumn in data table
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
         private void ucTabAttachment_Load(object sender, EventArgs e)
        {
            _dtFileList = new DataTable();
            _dtFileList.Locale = CultureInfo.InvariantCulture;
            _dtFileList.Columns.Add("FilePath");
            //_dtFileList.Columns.Add("Type");
            _dtFileList.Columns.Add("Mode");
        }
        #endregion

        #region Other Function
        /// <summary>
        /// for Adding data in data table 
        /// </summary>
        /// <param name=""></param>
        /// <Author>Dipak</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author>
        /// <Comments></Comments>		
        /// </Revision History>
        private void RefreshDataTable()
        {
            _dtFileList.Rows.Clear();
            foreach (ListViewItem lvwItem in lvwFileAttach.Items)
            {
                _dtFileList.Rows.Add(_dtFileList.NewRow());
                _dtFileList.Rows[_dtFileList.Rows.Count - 1]["FilePath"] = lvwItem.Text;
                //_dtFileList.Rows[_dtFileList.Rows.Count - 1]["Type"] = lvwItem.SubItems[0].Text;
                _dtFileList.Rows[_dtFileList.Rows.Count - 1]["Mode"] = lvwItem.SubItems[0].Text;
            }
        }

        /// <summary>
        /// for Clear all Row in data table 
        /// </summary>
        /// <param name=""></param>
        /// <Author>Dipak</Author>
        /// <Created Date>02/12/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author>
        /// <Comments></Comments>		
        /// </Revision History>
        public void Clear()
        {
            lvwFileAttach.Items.Clear();
        }

        /// <summary>
        /// for Set Values to List View control from Datatable
        /// </summary>
        /// <param name=""></param>
        /// <Author>Dipak</Author>
        /// <Created Date>02/12/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author>
        /// <Comments></Comments>		
        /// </Revision History>
        public void SetValues(DataTable dtSetVal)
        {
            if (dtSetVal.Rows.Count > 0)
            {
                ListViewItem lvwItem;

                foreach (DataRow drItem in dtSetVal.Rows)
                {
                    lvwItem = new ListViewItem();

                    lvwItem.Text = drItem["XADDOCPATH"].ToString();
                    lvwItem.SubItems.Add(GetMode(drItem["XADSENTVIA"].ToString()));
                    lvwFileAttach.Items.Add(lvwItem);
                }
            }

            RefreshDataTable();
        }

        /// <summary>
        /// for get Values to List View control from Datatable
        /// </summary>
        /// <param name=""></param>
        /// <Author>Dipak</Author>
        /// <Created Date>04/12/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author>
        /// <Comments></Comments>		
        /// </Revision History>
        public ListView.ListViewItemCollection GetValues()
        {
            return lvwFileAttach.Items;
        }

        #endregion

        #region Get Full Text for Mode

        private string GetMode(string strShortMode)
        {
            string strMode;
            strMode = string.Empty;

            switch (strShortMode)
            {
                case "P":
                    strMode = "Post";
                    break;
                case "M":
                    strMode = "Mail";
                    break;
                case "B":
                    strMode = "Both";
                    break;

            }

            return strMode;
        }

        #endregion

    }
}
