using System;
using System.Drawing;
using System.Configuration;

/// <Page Name>Header Label</Page Name> 
/// <Description>Properties Of Header Label</Description>
/// <Author>Dipak</Author>
/// <Created Date>14/8/2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
    /// <summary>
    /// Summary description for Label.
    /// </summary>
    public class MessageLabel : System.Windows.Forms.Label, iRoot
    {
        #region Declaration        
        bool blnApplyMySettings;
        #endregion

        #region Public Prioperties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return base.Text.Trim();
            }
            set
            {
                base.Text = (value == null) ? string.Empty : value.Trim();
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion
        
        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            /// <summary>
            /// Access clsGeneralInfo's Propertes.
            /// </summary>
            /// <Author>Dipak</Author>
            /// <Created Date>21/06/2006</Created Date>

            if (!this.DesignMode && blnApplyMySettings)
            {
                //Set back color for current control
                this.BackColor = Color.Transparent;
                //Set font for current control
                this.Font = clsGeneralInfo.lblMsgFont;
                //Set fore color for current control
                this.ForeColor =  clsGeneralInfo.btnFontColorCode;
            }
        }
        #endregion
    }
}

