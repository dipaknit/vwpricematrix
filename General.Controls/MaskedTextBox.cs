using System;
using System.Drawing;
using System.Configuration;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;


/// <Page Name>MaskedTextBox</Page Name> 
/// <Description>Properties Of MaskedTextBox</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
    public delegate void ValidatingHandler(object sender, ref Boolean bFlag);

	/// <summary>
    /// Summary description for MaskedTextBox.
	/// </summary>
    public class MaskedTextBox : System.Windows.Forms.MaskedTextBox, iRoot
	{
        #region Declaration
        bool blnApplyMySettings, _bVerify = true;
        ucInfoMessage _ctlInfoMessage; 
        string _strInfoMessage = string.Empty;
                
        private static string strDateFormat = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
        //If culture found then use that pattern
        static DateTimeFormatInfo currentDateFormat = new DateTimeFormatInfo();
        string _strMinDate = string.Empty;
        string _strMaxDate = string.Empty;
        bool _blnConvertEnterToTab = true;
        public new event ValidatingHandler Validating; 
        #endregion

		#region Public Properties
		/// <summary>
		/// ILink General property to access default property of the control
		/// </summary>
		public string CtrlProperty
		{
			get
			{
                return base.Text;
			}
            set
            {
                if (base.Mask.IndexOf(":") < 0 )
                    base.Text = (value == null) ? string.Empty : clsGeneralInfo.FormatDate(value);
                else
                    base.Text = value;
            }
		}
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public override string Text
        {
            get
            {
                return CtrlProperty;
            }
            set
            {
                base.Text = (value == null) ? string.Empty : clsGeneralInfo.FormatDate(value);
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }

        /// <summary>
        /// ILink General property to decide in which infoMessage bar the message should display for the textbox
        /// </summary>
        public ucInfoMessage ctlInfoMessage
        {
            get
            {
                return _ctlInfoMessage;
            }
            set
            {
                _ctlInfoMessage = value;
            }
        }
        /// <summary>
        /// ILink General property to decide which messsage should display for the Textbox in InfoMessage control
        /// </summary>
        public string InfoMessage
        {
            get
            {
                return _strInfoMessage;
            }
            set
            {
                _strInfoMessage = value;
            }
        }

        /// <summary>
        /// Minimum date that compare with orignal input ,input date must not less than minimum date
        /// </summary>
        public string MinDate
        {
            get
            {
                if (_strMinDate.Length > 0)
                {
                    _strMinDate = clsGeneralInfo.FormatDate(_strMinDate);
                }
                return _strMinDate;
            }
            set
            {
                _strMinDate = value;
            }
        }

        /// <summary>
        /// Maximum date that compare with orignal input ,input date must not greater than minimum maximum date
        /// </summary>
        public string MaxDate
        {
            get
            {
                if (_strMaxDate.Length > 0)
                {
                    _strMaxDate = clsGeneralInfo.FormatDate(_strMaxDate);
                }
                return _strMaxDate;
            }
            set
            {
                _strMaxDate = value;
            }
        }

        /// <summary>
        /// ILink General property to specify whether convert Enter Key to Tab Key or not
        /// </summary>
        public bool ConvertEnterToTab
        {
            get
            {
                return _blnConvertEnterToTab;
            }
            set
            {
                _blnConvertEnterToTab = value;
            }
        }

        /// <summary>
        /// ILink General property to specify whether convert Enter Key to Tab Key or not
        /// </summary>
        public bool Verify
        {
            get
            {
                return _bVerify;
            }
            set
            {
                _bVerify = value;
            }
        }
        #endregion

        #region Set Ctrl Properties

        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {                  
                    
                //Set back color for current control
                this.BackColor = clsGeneralInfo.mskdtxtBackColorCode;

                //Set font for current control
                this.Font = clsGeneralInfo.mskdtxtFont;

                //Set fore color for current control
                this.ForeColor =  clsGeneralInfo.btnFontColorCode;;

                //Set Culture Info
                this.Culture = Application.CurrentCulture;
                  
                this.Verify = true;
            }
        }        
        #endregion

        #region Control Events

        /// <summary>
        /// Set InfoMessage bar Message emplt on control lostfocus
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        { 
            if ((!e.Control) && this != null && ConvertEnterToTab == true && e.KeyCode == Keys.Enter && e.KeyCode != Keys.F5)
            {
                Boolean bFlag = new Boolean();
                bFlag = true;
                if (this.Validating != null)
                    Validating(this, ref bFlag);
                if (bFlag == true)
                {
                    SendKeys.Send("{TAB}");
                }
                else
                {
                    return;
                }
            }
            else if(this != null && e.KeyCode == Keys.Space)
            {
                this.Text = string.Empty;
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            base.OnKeyDown(e);
            
        }

        /// <summary>
        /// Set InfoMessage bar Message on control gotfocus
        /// </summary>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            if (ctlInfoMessage != null)
                ctlInfoMessage.InfoMessage = InfoMessage;
        }

        /// <summary>
        /// Set InfoMessage bar Message emplt on control lostfocus
        /// </summary>
        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            if (ctlInfoMessage != null)
                ctlInfoMessage.InfoMessage = string.Empty;
        }

        #endregion

        #region Date Validation Function

        #region Format Date        
        /// <summary>
        /// Formate date to be displayed according to current culture
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns>return date</returns>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Dec-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public string FormatDate()
        {
            return clsGeneralInfo.FormatDate(this.Text);    
        }
        #endregion 
        
        #region Validate Date        
        /// <summary>
        /// validate date according to culture, parse date, and validate year,month and date
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns></returns>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Dec-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public bool ValidateDate()
        {
            return clsGeneralInfo.ValidateDate(this.Text);   
        }
        #endregion

        #region Compare Date
        /// <summary>
        /// Compare original date with maximum date and minimum date
        /// </summary>
        /// <param name="originaldate"></param>
        /// <returns></returns>
        //public bool CompareDate(string originaldate)
        //{
        //    currentDateFormat.ShortDatePattern = strDateFormat;

        //    bool bResult = true;

        //    if (MinDate.Length > 0)
        //    {
        //        if (DateTime.Parse(originaldate, currentDateFormat) < DateTime.Parse(MinDate, currentDateFormat))
        //        {
        //            bResult = false;
        //        }
        //    }

        //    if (MaxDate.Length > 0)
        //    {
        //        if (DateTime.Parse(originaldate, currentDateFormat) > DateTime.Parse(MaxDate, currentDateFormat))
        //        {
        //            bResult = false;
        //        }
        //    }
        //    return bResult;
        //}

        /// <summary>
        /// Compare original date with maximum date and minimum date
        /// </summary>
        /// <param name="originaldate"></param>
        /// <returns></returns>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Dec-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public bool CompareDate()
        {
             return clsGeneralInfo.CompareDate(this.Text, this.MinDate, this.MaxDate);     
        }
        #endregion

        #region Format Date To SQL        
        /// <summary>
        /// Formate date to be entered in SQL D/B.
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns></returns>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Dec-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public string FormatDateToSQL()
        {
            return clsGeneralInfo.FormatDateToSQL(this.Text);    
        }
        #endregion

        #region Convert Text To Date Time        
       /// <summary>
       /// Convert Text of Date to DateTime Type
       /// </summary>
       /// <param name="inputdate"></param>
       /// <returns></returns>
       /// <Author>Darshan</Author>
       /// <Created Date>19-Dec-2006</Created Date>
       /// <Revision History>
       /// <Date></Date><Author></Author><Comments></Comments>		
       /// </Revision History>
       public DateTime ConvertTextToDateTime()
       {
           return clsGeneralInfo.ConvertTextToDateTime(this.Text);  
           
       }

        #endregion

        #region Validate Null Date        
        /// <summary>
        /// Is Null Date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Dec-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>       
        public bool IsNullDate()
        {
            return clsGeneralInfo.IsNullDate(this.Text); 
        }
        #endregion
        #endregion
    }
}