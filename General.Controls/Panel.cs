using System;
using System.Drawing;
using System.Configuration;

/// <Page Name>Panel</Page Name> 
/// <Description>Properties of Panel</Description>
/// <Author>Dipak</Author>
/// <Created Date>27/7/2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    public class Panel : System.Windows.Forms.Panel,iRoot
    {
        #region Declaration       
        bool blnApplyMySettings;
        string strCtrlProp;
        #endregion

        #region Public Prioperties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return strCtrlProp;
            }
            set
            {
                strCtrlProp = (value == null) ? string.Empty : value.Trim();
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion

        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            /// <summary>
            /// set back-color.
            /// </summary>
            /// <Author>Dipak</Author>
            /// <Created Date>27/06/2006</Created Date>

            if (!this.DesignMode && blnApplyMySettings)
            {
              
                //Set back color for current control
                this.BackColor = clsGeneralInfo.pnlBackColorCode;
            }
        }
        #endregion
    }
}

