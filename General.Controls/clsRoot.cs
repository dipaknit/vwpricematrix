using System;
using System.Drawing;


/// <Page Name>clsRoot</Page Name> 
/// <Description>InterFace iroot Declaration</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    /// <summary>
    /// Summary description for clsRoot.
    /// </summary>
    public interface iRoot
    {
        #region Required properties
        /// <summary>
        /// variable that contains our value and related to the control's specific property
        /// </summary>
        string CtrlProperty { get; set;}
        bool ApplyMySettings { get;set;}
        #endregion
    }
}
