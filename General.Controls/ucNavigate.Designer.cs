namespace General.Controls
{
    partial class ucNavigate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucNavigate));
            this.lblSeperator = new General.Controls.Label();
            this.btnLast = new General.Controls.Button();
            this.btnNext = new General.Controls.Button();
            this.btnPrevious = new General.Controls.Button();
            this.btnFirst = new General.Controls.Button();
            this.btnShow = new General.Controls.Button();
            this.lblStatus = new General.Controls.Label();
            this.SuspendLayout();
            // 
            // lblSeperator
            // 
            this.lblSeperator.ApplyMySettings = false;
            this.lblSeperator.BackColor = System.Drawing.Color.Transparent;
            this.lblSeperator.CtrlProperty = "";
            this.lblSeperator.Image = ((System.Drawing.Image)(resources.GetObject("lblSeperator.Image")));
            this.lblSeperator.Location = new System.Drawing.Point(3, 7);
            this.lblSeperator.Name = "lblSeperator";
            this.lblSeperator.Size = new System.Drawing.Size(20, 54);
            this.lblSeperator.TabIndex = 5;
            // 
            // btnLast
            // 
            this.btnLast.ApplyMySettings = false;
            this.btnLast.CtrlProperty = "&Last";
            this.btnLast.FlatAppearance.BorderSize = 0;
            this.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLast.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.btnLast.ForeColor = System.Drawing.Color.White;
            this.btnLast.Location = new System.Drawing.Point(241, 7);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(55, 60);
            this.btnLast.TabIndex = 4;
            this.btnLast.Text = "&Last";
            this.btnLast.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLast.UseVisualStyleBackColor = false;
            this.btnLast.Visible = false;
            this.btnLast.Click += new System.EventHandler(this._btnLast_Click);
            // 
            // btnNext
            // 
            this.btnNext.ApplyMySettings = false;
            this.btnNext.CtrlProperty = "&Next";
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.btnNext.ForeColor = System.Drawing.Color.White;
            this.btnNext.Location = new System.Drawing.Point(186, 7);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(55, 60);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "&Next";
            this.btnNext.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this._btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.ApplyMySettings = false;
            this.btnPrevious.CtrlProperty = "&Prev";
            this.btnPrevious.FlatAppearance.BorderSize = 0;
            this.btnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevious.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.btnPrevious.ForeColor = System.Drawing.Color.White;
            this.btnPrevious.Location = new System.Drawing.Point(131, 7);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(55, 60);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "&Prev";
            this.btnPrevious.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPrevious.UseVisualStyleBackColor = false;
            this.btnPrevious.Visible = false;
            this.btnPrevious.Click += new System.EventHandler(this._btnPrevious_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.ApplyMySettings = false;
            this.btnFirst.CtrlProperty = "&First";
            this.btnFirst.FlatAppearance.BorderSize = 0;
            this.btnFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFirst.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.btnFirst.ForeColor = System.Drawing.Color.White;
            this.btnFirst.Location = new System.Drawing.Point(76, 7);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(55, 60);
            this.btnFirst.TabIndex = 1;
            this.btnFirst.Text = "&First";
            this.btnFirst.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFirst.UseVisualStyleBackColor = false;
            this.btnFirst.Visible = false;
            this.btnFirst.Click += new System.EventHandler(this._btnFirst_Click);
            // 
            // btnShow
            // 
            this.btnShow.ApplyMySettings = false;
            this.btnShow.CtrlProperty = "S&how";
            this.btnShow.FlatAppearance.BorderSize = 0;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.btnShow.ForeColor = System.Drawing.Color.White;
            this.btnShow.Location = new System.Drawing.Point(21, 7);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(55, 60);
            this.btnShow.TabIndex = 0;
            this.btnShow.Text = "S&how";
            this.btnShow.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this._btnShow_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.ApplyMySettings = false;
            this.lblStatus.CtrlProperty = "Status";
            this.lblStatus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(302, 49);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(112, 17);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Status";
            this.lblStatus.Visible = false;
            // 
            // ucNavigate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblSeperator);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.btnShow);
            this.Name = "ucNavigate";
            this.Size = new System.Drawing.Size(427, 70);
            this.ResumeLayout(false);

        }

        #endregion
 
        public  Button btnShow;
        public Button btnFirst;
        public Button btnPrevious;
        public Button btnNext;
        public Button btnLast;
        private Label lblSeperator;
        private Label lblStatus;
    }
}
