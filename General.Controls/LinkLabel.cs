using System;
using System.Drawing;
using System.Configuration;

/// <Page Name>LinkLabel</Page Name> 
/// <Description>Properties Of Link Label</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
    /// <summary>
    /// Summary description for LinkLabel.
    /// </summary>
    public class LinkLabel : System.Windows.Forms.LinkLabel, iRoot
    {
        #region Declaration       
        bool blnApplyMySettings;
        #endregion


        #region Public Prioperties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return base.Text.Trim();
            }
            set
            {
                base.Text = (value == null) ? string.Empty : value.Trim();
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion


        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {    
              
                //Set back color for current control
                this.BackColor = clsGeneralInfo.lnklblBackColorCode;
                //Set font for current control
                this.Font = clsGeneralInfo.lnklblFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode;
            }
        }
        #endregion
    }
}

