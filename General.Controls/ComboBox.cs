using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Windows.Forms;

/// <Page Name>ComboBox</Page Name> 
/// <Description>Properties of ComboBox</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    //public delegate void ValidateTextHandler(object sender, ref bool bFlag);

	/// <summary>
	/// Summary description for ComboBox.
	/// </summary>
    public class ComboBox : System.Windows.Forms.ComboBox, iRoot
	{
        #region Declaration
        bool blnApplyMySettings, _blnConvertEnterToTab = true;

        ucInfoMessage _ctlInfoMessage;
        string _strInfoMessage = string.Empty;


        #endregion

		#region Public Prioperties
		/// <summary>
		/// ILink General property to access default property of the control
		/// </summary>
		public string CtrlProperty
		{
			get
			{
				if(this.SelectedValue != null)
					return this.SelectedValue.ToString();
                else if (this.SelectedItem != null)
                    return this.SelectedItem.ToString();
				else
					return null;
			}
			set
			{

                if (value != null)
                {
                    if (value.Length == 0)
                        this.SelectedIndex = -1;
                    else if (this.Items.Count > 0)
                    {
                        if (this.DataSource != null)
                        {
                            this.SelectedValue = value;
                        }
                        else
                        {
                            this.SelectedItem = (value == null) ? -1 : Int32.Parse(value, clsGeneralInfo.GetNumberFormatProvider(0));
                        }
                    }
                }
			}
        }

        /// <summary>
        /// ILink General property to specify whether convert Enter Key to Tab Key or not
        /// </summary>
        public bool ConvertEnterToTab
        {
            get
            {
                return _blnConvertEnterToTab;
            }
            set
            {
                _blnConvertEnterToTab = value;
            }
        }

        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }

        /// <summary>
        /// ILink General property to decide in which infoMessage bar the message should display for the textbox
        /// </summary>
        public ucInfoMessage ctlInfoMessage
        {
            get
            {
                return _ctlInfoMessage;
            }
            set
            {
                _ctlInfoMessage = value;
            }
        }

        /// <summary>
        /// ILink General property to decide which messsage should display for the Textbox in InfoMessage control
        /// </summary>
        public string InfoMessage
        {
            get
            {
                return _strInfoMessage;
            }
            set
            {
                _strInfoMessage = value;
            }
        }

        #endregion

        #region Control Event

        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {
                //Set back color for current control
                this.BackColor = clsGeneralInfo.cmbBackColorCode;
                //Set font for current control
                this.Font = clsGeneralInfo.cmbFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode;
            }
        }

        /// <summary>
        /// Set InfoMessage bar Message on control gotfocus
        /// </summary>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            if (ctlInfoMessage != null)
                ctlInfoMessage.InfoMessage = InfoMessage;
        }

        /// <summary>
        /// Set InfoMessage bar Message Empty on control lostfocus
        /// </summary>
        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            if (ctlInfoMessage != null)
                ctlInfoMessage.InfoMessage = string.Empty;
        }

        /// <summary>
        /// Set InfoMessage bar Message emplt on control lostfocus
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if ((!e.Control) && this != null && ConvertEnterToTab == true && e.KeyCode == Keys.Enter && e.KeyCode != Keys.F5)
            {
                SendKeys.Send("{TAB}");
            }
        }

        #endregion

	}
}