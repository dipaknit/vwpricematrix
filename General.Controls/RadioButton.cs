using System;
using System.Drawing;
using System.Configuration;
using System.Windows.Forms;

/// <Page Name>RadioButton</Page Name> 
/// <Description>Properties Of Radio Button</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
	/// <summary>
	/// Summary description for RadioButton.
	/// </summary>
	public class RadioButton : System.Windows.Forms.RadioButton, iRoot
	{
        #region Declaration
        static int intHeight;     
        bool blnApplyMySettings;
        #endregion

		#region Public Prioperties
		/// <summary>
		/// ILink General property to access default property of the control
		/// </summary>
		public string CtrlProperty
		{
			get
			{
				return ((Checked)?"1":"0");
			}
			set
			{
                Checked = (value == null) ? false : (value == "1");
			}
		}
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion

        /// <summary>
        /// Set InfoMessage bar Message emplt on control lostfocus
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (this != null && e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }


        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {
                //Set back color for current control
                this.BackColor = Color.Transparent;
                //Set font for current control
                this.Font = clsGeneralInfo.rdoFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode;

                // Set the Radio Button Height using TextBox
                if (intHeight==0)
                {
                    General.Controls.TextBox txtSetHeight = new General.Controls.TextBox();
                    txtSetHeight.Font = this.Font;
                    intHeight=txtSetHeight.Height;
                }

                this.Height = intHeight;
            }
        }
        #endregion
	}
}