using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace General.Controls
{
    public partial class ucInfoMessage : UserControl
    {

        public ucInfoMessage()
        {
            InitializeComponent();

            this.BackColor = clsGeneralInfo.rdoBackColorCode;
            lblMessage.BackColor = clsGeneralInfo.rdoBackColorCode;
        }

        /// <summary>
        /// for Set Message in Label Control
        /// </summary>
        /// <param name=""></param>
        /// <returns>lblMessage.Text</returns>
        /// <Return Type>String</Return Type>
        /// <Author>Ravi Desai</Author>
        /// <Created Date>26/10/2006</Created Date>
        /// <Revision History><Date></Date><Author></Author><Comments></Comments>
        /// </Revision History>
        public string InfoMessage
        {
            get
            {
                return lblMessage.Text; 
            }
            set
            {
                lblMessage.Text =value; 
            }
        }
    }
}
