using System;
using System.Drawing;
using System.Configuration;


/// <Page Name>Button</Page Name> 
/// <Description>I-Link's User Defined Button Controls</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    /// <summary>
    /// Summary description for Button. 
    /// </summary>
    public class TreeView : System.Windows.Forms.TreeView, iRoot
    {
        #region Variable Declaration
        bool blnApplyMySettings;
        bool _blnReadOnly;
        #endregion

        #region Public Properties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return base.Text.Trim();
            }
            set
            {
                base.Text = (value == null) ? string.Empty : value.Trim();
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }

        public bool ReadOnly
        {
            get
            {
                return _blnReadOnly;
            }
            set
            {
                _blnReadOnly = value;
            }
        }
        #endregion

        #region OnCreateControl
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <Return Type></Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date></Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {
                //Set node fonts for current control
                this.Font = clsGeneralInfo.tvFont;

            }
        }
        #endregion

        #region OnBeforeCheck
        /// <summary>
        /// Check for read only
        /// </summary>
        /// <param name="e"></param>
        /// <Return Type></Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date></Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected override void  OnBeforeCheck(System.Windows.Forms.TreeViewCancelEventArgs e)
        {
            if(e.Action != System.Windows.Forms.TreeViewAction.Unknown)
                e.Cancel = _blnReadOnly;
        }
        #endregion
    }
}

