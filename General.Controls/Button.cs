using System;
using System.Drawing;
using System.Configuration;


/// <Page Name>Button</Page Name> 
/// <Description>I-Link's User Defined Button Controls</Description>
/// <Author>Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    /// <summary>
    /// Summary description for Button. 
    /// </summary>
    public class Button : System.Windows.Forms.Button, iRoot
    {
        #region Variable Declaration        
        bool blnApplyMySettings;
        #endregion

        #region Public Properties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return base.Text.Trim();
            }
            set
            {

                base.Text = (value == null) ? string.Empty : value;
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings  = value ;
            }
        }
        #endregion

        #region OnCreateControl
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <Return Type></Return Type>
        /// <Author>Hardik</Author>
        /// <Created Date></Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {                 

                //Set back color for current control
                    this.BackColor = clsGeneralInfo.btnBackColorCode;

                //Set font for current control
                this.Font = clsGeneralInfo.btnFont;

                //Set fore color for current control
                this.ForeColor = this.ForeColor = clsGeneralInfo.btnFontColorCode;

                //Set Button Style
                this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;

                //Set Border color for current control
                this.FlatAppearance.BorderColor = clsGeneralInfo.btnBorderColorCode;

                General.Controls.Label lblSetHeight = new General.Controls.Label();
                this.Height = lblSetHeight.Height + 1;
                this.TextAlign = ContentAlignment.MiddleCenter;
            }
        }
        #endregion
    }
}

