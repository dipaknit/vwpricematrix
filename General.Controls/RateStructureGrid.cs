/// <Page Name>RateStructureGrid</Page Name> 
/// <Description>Rate Structure Grid Control</Description>
/// <Author>Darshan</Author>
/// <Created Date>18-Oct-2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Globalization;


namespace Imms.Controls
{
    public partial class RateStructureGrid : Imms.Controls.DataGridView
    {
        #region Variable Declaration
        string _strRateStruCode;
        double _dblItemAmount;
        string _strActiveItemCode;

        double _dblPostableAmount;
        double _dblNonPostableAmount;
        double _dblTotalRateAmount;
        double _dblTotalItemAmount;
        bool _blnInsertRecords;

        Control[] CtrlArrGrid;
        public delegate void RSDetailsEventDelegate(object sender, string strSalesItemCode);
        public event RSDetailsEventDelegate RateStructureDetailsChanged;
        public event EventHandler RateCodeSelection;
        DataTable dtblRateStruDetails = new DataTable();
               
        #endregion

        #region DataTables for ComboValues
        //Data Tables for Rate Structure Control Grid
        private DataTable dtRsPerVal;
        private DataTable dtRsIncExc;
        #endregion

        #region Grid Column Enum
        enum eRsGridColumns
        {
            Index,
            RateCode,
            RateDesc,
            Inc_Exc,
            Per_Val,
            ApplicableOn,
            //ApplicableOnBtn,
            TaxValue,
            Post_NonPost,
            RateAmount,
            SalItemCode,
            TotalAmount
        }
        #endregion

        #region Grid Controls Declration
        TextBox txtRateCode = new TextBox();
        ComboBox cmbIncExc = new ComboBox();
        ComboBox cmbPerVal = new ComboBox();
        NumericTextBox txtTaxValue = new NumericTextBox();
        #endregion

        #region DataGridView Column Declaration
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inc_Exc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Per_Val;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApplicableOn;
        //private System.Windows.Forms.DataGridViewButtonColumn ApplicableOnBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxValue;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Post_NonPost;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalAmount;
        #endregion

        #region Constructor
        /// <summary>
        /// default constructor
        /// </summary>
        public RateStructureGrid()
        {
            InitializeComponent();
           
            #region Define control arrays for grid
            CtrlArrGrid = new Control[]{null, txtRateCode, null, cmbIncExc, cmbPerVal, null, txtTaxValue, null, null, null};
            #endregion

            #region Rate Structure Per_Val
            dtRsPerVal = new DataTable();
            dtRsPerVal.Locale = CultureInfo.InvariantCulture;
            
            dtRsPerVal.Columns.Add("Value");
            dtRsPerVal.Columns.Add("Text");

            dtRsPerVal.Rows.Add(dtRsPerVal.NewRow());
            dtRsPerVal.Rows[0]["Value"] = "P";
            dtRsPerVal.Rows[0]["Text"] = "Percentage";

            dtRsPerVal.Rows.Add(dtRsPerVal.NewRow());
            dtRsPerVal.Rows[1]["Value"] = "V";
            dtRsPerVal.Rows[1]["Text"] = "Value";
            #endregion

            #region Rate Structure Inc_Exc

            dtRsIncExc = new DataTable();
            dtRsIncExc.Locale = CultureInfo.InvariantCulture;

            dtRsIncExc.Columns.Add("Value");
            dtRsIncExc.Columns.Add("Text");

            dtRsIncExc.Rows.Add(dtRsIncExc.NewRow());
            dtRsIncExc.Rows[0]["Value"] = "I";
            dtRsIncExc.Rows[0]["Text"] = "Inclusive";

            dtRsIncExc.Rows.Add(dtRsIncExc.NewRow());
            dtRsIncExc.Rows[1]["Value"] = "E";
            dtRsIncExc.Rows[1]["Text"] = "Exclusive";
            #endregion

            #region Set properties of control array of Grid
            txtRateCode.Name = "txtRateCode";
            txtRateCode.ApplyMySettings = true;

            txtTaxValue.Name = "txtTaxValue";
            txtTaxValue.InputType = NumericTextBox.NumEditType.Double;
            txtTaxValue.Precision = 2;
            txtTaxValue.ApplyMySettings = true;

            cmbIncExc.DataSource = dtRsIncExc;
            cmbIncExc.ValueMember = "Value";
            cmbIncExc.DisplayMember = "Text";
            cmbIncExc.DropDownStyle = ComboBoxStyle.DropDownList;
            

            cmbPerVal.DataSource = dtRsPerVal;
            cmbPerVal.ValueMember = "Value";
            cmbPerVal.DisplayMember = "Text";
            cmbPerVal.DropDownStyle = ComboBoxStyle.DropDownList;

            txtTaxValue.Leave += new EventHandler(DataGridControl_Leave);
            #endregion

            this.RowHeadersVisible  = false;
            
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Get or Set Rate Structure Code 
        /// </summary>
        public string RateStructureCode
        {
            get{
                return _strRateStruCode;
            }
            set{
                _strRateStruCode = value;
            }
        }

        public double ItemAmount
        {
            get
            {
                return _dblItemAmount;
            }
            set
            {
                _dblItemAmount = value;
            }
        }

        public string ActiveItemCode
        {
            get
            {
                return _strActiveItemCode;
            }
            set
            {
                _strActiveItemCode = value;
            }
        }

        public double PostableAmount
        {
            get
            {
                return _dblPostableAmount;
            }
            set
            {
                _dblPostableAmount = value;
            }
        }

        public double NonPostableAmount
        {
            get
            {
                return _dblNonPostableAmount;
            }
            set
            {
                _dblNonPostableAmount = value;
            }
        }

        public double TotalRateAmount
        {
            get
            {
                return _dblTotalRateAmount;
            }
            set
            {
                _dblTotalRateAmount = value;
            }
        }

        public double TotalItemAmount
        {
            get
            {
                return _dblTotalItemAmount;
            }
            set
            {
                _dblTotalItemAmount = value;
            }
        }

        public DataTable RateStruDetails
        {
            get
            {
                return dtblRateStruDetails;
            }
            set
            {
                dtblRateStruDetails = value;
            }
        }

        public bool InsertRecords
        {
            get
            {
                return _blnInsertRecords;
            }
            set
            {
                _blnInsertRecords = value;
            }
        }
        #endregion

        #region Event (OnCreateControl)
        /// <summary>
        /// This event will be fired when user datagridview control will be created in design and runtime
        /// mode. It will check whether it has to apply settings or not.
        /// </summary>
        protected override void OnCreateControl()
        {
            //Call base event
            base.OnCreateControl();
            this.ApplyMySettings = true;
            this.MyEditGrid = true;
            this.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.AutoGenerateColumns = false;
            
            //Add Columns to DataGrid
            if (!DesignMode)
            {
                AddColumnsToGrid();
            }
        } 
        #endregion

        #region Add Columns to Grid
        /// <summary>
        /// Add Columns To RateStructure Grid
        /// </summary>
        /// <Return Type>void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>18-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void AddColumnsToGrid()
        {
            #region Define DataGrid Columns
          //  System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
          //  System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RateCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RateDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inc_Exc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Per_Val = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApplicableOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            //this.ApplicableOnBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TaxValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Post_NonPost = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RateAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            #endregion

            #region Set DataGrid Column Property
            //SET DATAGRID COLUMN PROPERTY ---------------------------------------------------
            // 
            // Index
            // 
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Visible = true;
            this.Index.DataPropertyName = "Index";
            // 
            // RateCode
            // 
            this.RateCode.HeaderText = "Rate Code";
            this.RateCode.Name = "RateCode";
            this.RateCode.ReadOnly = true;
            this.RateCode.DataPropertyName = "RateCode";
            // 
            // RateDesc
            // 
            this.RateDesc.HeaderText = "Rate Desc.";
            this.RateDesc.Name = "RateDesc";
            this.RateDesc.ReadOnly = true;
            this.RateDesc.DataPropertyName = "RateDesc";
            // 
            // Inc_Exc
            // 
            this.Inc_Exc.HeaderText = "I/E";
            this.Inc_Exc.Name = "Inc_Exc";
            this.Inc_Exc.DataPropertyName = "Inc_Exc";
            // 
            // Per_Val
            // 
            this.Per_Val.HeaderText = "P/V";
            this.Per_Val.Name = "Per_Val";
            this.Per_Val.DataPropertyName = "Per_Val";
            // 
            // ApplicableOn
            // 
            this.ApplicableOn.HeaderText = "Applicable On";
            this.ApplicableOn.Name = "ApplicableOn";
            this.ApplicableOn.DataPropertyName = "ApplicableOn";
            // 
            // ApplicableOnBtn
            // 
            //this.ApplicableOnBtn.UseColumnTextForButtonValue = true;
            //this.ApplicableOnBtn.CellTemplate.Style.ForeColor = System.Drawing.Color.White;
            //this.ApplicableOnBtn.CellTemplate.Style.BackColor = System.Drawing.Color.Gray;
            //this.ApplicableOnBtn.FlatStyle = FlatStyle.Flat;
            //this.ApplicableOnBtn.HeaderText = string.Empty;
            //this.ApplicableOnBtn.Name = "ApplicableOnBtn";
            //this.ApplicableOnBtn.Text = "...";
            //this.ApplicableOn.DataPropertyName = "ApplicableOn";
            // 
            // TaxValue
            //// 
            this.TaxValue.CellTemplate.Style.Format = "N2";
            this.TaxValue.CellTemplate.Style.NullValue = "0";
            this.TaxValue.HeaderText = "Tax Value";
            this.TaxValue.Name = "TaxValue";
            this.TaxValue.DataPropertyName = "TaxValue";
            // 
            // Post_NonPost
            // 
            this.Post_NonPost.HeaderText = "Post/Non post";
            this.Post_NonPost.Name = "Post_NonPost";
            this.Post_NonPost.DataPropertyName = "Post_NonPost";
            this.Post_NonPost.CellTemplate.Style.NullValue = false;
            this.Post_NonPost.FalseValue = false;
            this.Post_NonPost.TrueValue = true;
           // this.Post_NonPost.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            //this.Post_NonPost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // RateAmount
            // 
            //dataGridViewCellStyle2.Format = "N2";
            //dataGridViewCellStyle2.NullValue = "0";
            this.RateAmount.CellTemplate.Style.Format = "N2";
            this.RateAmount.CellTemplate.Style.NullValue = "0";
            this.RateAmount.HeaderText = "Rate Amount";
            this.RateAmount.ReadOnly = true;
            this.RateAmount.Name = "RateAmount";
            this.RateAmount.DataPropertyName = "RateAmount";
            // 
            // SalItemCode
            // 
            this.SalItemCode.HeaderText = "Sales Item Code";
            this.SalItemCode.Name = "SalItemCode";
            this.SalItemCode.ReadOnly = true;
            this.SalItemCode.DataPropertyName = "SalItemCode";
            // 
            // TotalAmount(To store Item Amount after applying tax amount)
            // 
            this.TotalAmount.CellTemplate.Style.Format = "N2";
            this.TotalAmount.CellTemplate.Style.NullValue = "0";
            this.TotalAmount.HeaderText = "Total Amount";
            this.TotalAmount.ReadOnly = true;
            this.TotalAmount.Name = "TotalAmount";
            this.TotalAmount.DataPropertyName = "TotalAmount";

            // END --------------------------------------------------------------------------------
            #endregion

            #region Add Columns to DataGrid
            //Add Columns to DataGrid
            this.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.RateCode,
            this.RateDesc,
            this.Inc_Exc,
            this.Per_Val,
            this.ApplicableOn,
///            this.ApplicableOnBtn,
            this.TaxValue,
            this.Post_NonPost,
            this.RateAmount,
            this.SalItemCode,
            this.TotalAmount});
            #endregion

            #region Set all columns Non-sortable
            foreach (DataGridViewColumn dgvwCol in this.Columns)
                dgvwCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            #endregion
        }
        #endregion

        #region Bind Rate Structure Grid
        /// <summary>
        /// Bind data to DataGrid as per given Rate Structure Code
        /// </summary>
        /// <Return Type>void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>18-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public void BindDataGrid()
        {
            if (_strActiveItemCode.Trim().Length != 0)
            {
                dtblRateStruDetails.DefaultView.RowFilter =  "SalItemCode = '" + _strActiveItemCode + "'" ;
                //this.DataSource = dtblRateStruDetails.DefaultView;
                CalcRateStru();
                this.DataSource = dtblRateStruDetails;
            }

            //CalcRateStru();
        }
        #endregion

        #region Add
        /// <summary>
        /// This function is used to add Rate Structure Details
        /// </summary>
        /// <param name="dtblRtStruDetails"></param>
        /// <Return Type>void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>18-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public void Add(DataTable dtblTemp)
        {
            if (dtblTemp == null) return;
            #region Add required Columns to Table
            dtblTemp.Columns.Add("Index");
            dtblTemp.Columns.Add("Post_NonPost");
            dtblTemp.Columns.Add("RateAmount", System.Type.GetType("System.Double"));
            dtblTemp.Columns.Add("SalItemCode");
            dtblTemp.Columns.Add("TotalAmount", System.Type.GetType("System.Double"));
            #endregion

            #region Set Index Column Values and other details
            //Set indexes of Rows
            int intIndex = 1;
            foreach (DataRow dr in dtblTemp.Rows)
            {
                dr["Index"] = intIndex.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
                intIndex++;
            }

            //Set SalItemCode
            foreach (DataRow dr in dtblTemp.Rows)
            {
                dr["SalItemCode"] = _strActiveItemCode;
                dr["Post_NonPost"] = false;
                dr["RateAmount"] = 0.00;
                dr["TotalAmount"] = 0.00;
            }
            #endregion

            
            #region Rename DataTable Columns
            dtblTemp.Columns[0].ColumnName = "RateCode";
            dtblTemp.Columns[1].ColumnName = "RateDesc";
            dtblTemp.Columns[2].ColumnName = "Inc_Exc";
            dtblTemp.Columns[3].ColumnName = "Per_Val";
            dtblTemp.Columns[4].ColumnName = "TaxValue";
            dtblTemp.Columns[5].ColumnName = "ApplicableOn";
            /*
            dtblTemp.Columns[6].ColumnName = "Index";
            dtblTemp.Columns[7].ColumnName = "Post_NonPost";
            dtblTemp.Columns[8].ColumnName = "RateAmount";
            dtblTemp.Columns[9].ColumnName = "SalItemCode";
            dtblTemp.Columns[10].ColumnName = "TotalAmount";
             */
            #endregion
  

            //string strSaltemCode = string.Empty;
            //if(dtblTemp.Rows[0]["SalItemCode"] != null)
            //     strSaltemCode = dtblTemp.Rows[0]["SalItemCode"].ToString();

           
            dtblRateStruDetails.Merge(dtblTemp, true);
        }
        #endregion

        #region Remove
        /// <summary>
        /// This function is used to remove Rate Structure Details of given sales item code
        /// </summary>
        /// <param name="strSalesItemCode">String</param>
        /// <Return Type>void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>18-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public void Remove(string strSalesItemCode)
        {
            foreach (DataRow drRow in dtblRateStruDetails.Rows)
            {
                if (drRow["SalItemCode"].ToString().Equals(strSalesItemCode, StringComparison.CurrentCultureIgnoreCase))
                    drRow.Delete();
            }
            dtblRateStruDetails.AcceptChanges();
        }
        #endregion

        #region DataGridView Cell Edit Events
        /// <summary>
        /// Set controls for editing mode
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e"> DataGridViewEditingControlShowingEventArgs</param>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Oct-2006</Created Date> 
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected override void  OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        {
            //  base.OnEditingControlShowing(e);
            Imms.Controls.ComboBox cmbTemp; 
            //If no cell selected then return .....
            if (this.CurrentCell == null) return;
            switch (this.CurrentCell.ColumnIndex)
            {
                case (int)eRsGridColumns.Post_NonPost:
                    return;
                case (int)eRsGridColumns.ApplicableOn:
                    OpenApplicableOnDialog();
                    this.EndEdit();
                    return;
                case (int)eRsGridColumns.RateCode:
                    CtrlArrGrid[this.CurrentCell.ColumnIndex].Parent = e.Control.Parent;
                    ((Imms.Controls.TextBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).Text = e.Control.Text.Replace(",", "");
                    break;
                case (int)eRsGridColumns.TaxValue:
                    CtrlArrGrid[this.CurrentCell.ColumnIndex].Parent = e.Control.Parent;
                    ((Imms.Controls.NumericTextBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).Text = e.Control.Text.Replace(",", "");
                    break;
                case (int)eRsGridColumns.Per_Val:
                case (int)eRsGridColumns.Inc_Exc:
                    CtrlArrGrid[this.CurrentCell.ColumnIndex].Parent = e.Control.Parent;
                    cmbTemp = (Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex];
                    cmbTemp.SelectedIndex = cmbTemp.FindStringExact(GetComboText(e.Control.Text, (DataTable)cmbTemp.DataSource));
                    break;
                default:
                    CtrlArrGrid[this.CurrentCell.ColumnIndex].Text = e.Control.Text;
                    break;
            }
            CtrlArrGrid[this.CurrentCell.ColumnIndex].Visible = true;
            CtrlArrGrid[this.CurrentCell.ColumnIndex].Width = this.CurrentCell.Size.Width;
            CtrlArrGrid[this.CurrentCell.ColumnIndex].Focus();    
            e.Control.SendToBack();
        }

        /// <summary>
        /// Set values of controls to datagrid cell values
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e"> DataGridViewCellEventArgs</param>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Oct-2006</Created Date> 
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected override void  OnCellEndEdit(DataGridViewCellEventArgs e)
        {
 	      // base.OnCellEndEdit(e);
            if (this.CurrentCell == null) return;

            switch (this.CurrentCell.ColumnIndex)
            {
                case (int)eRsGridColumns.Post_NonPost:
                case (int)eRsGridColumns.ApplicableOn:
                    CalcRateStru();
                    return;
                case (int)eRsGridColumns.RateCode:
                case (int)eRsGridColumns.TaxValue:
                    if (!VerifyInput(CtrlArrGrid[this.CurrentCell.ColumnIndex])) return;
                    this.CurrentCell.Value = CtrlArrGrid[this.CurrentCell.ColumnIndex].Text;
                    CtrlArrGrid[this.CurrentCell.ColumnIndex].Text = string.Empty;
                    break;
                case (int)eRsGridColumns.Per_Val:
                    this.CurrentCell.Value = GetComboValue(((Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).Text, (DataTable)((Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).DataSource);
                    ((Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).SelectedIndex = -1;
                    break;
                case (int)eRsGridColumns.Inc_Exc:
                    this.CurrentCell.Value = GetComboValue(((Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).Text, (DataTable)((Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).DataSource);
                    this.CurrentRow.Cells[(int)eRsGridColumns.Per_Val].Value = "V";
                    ((Imms.Controls.ComboBox)CtrlArrGrid[this.CurrentCell.ColumnIndex]).SelectedIndex = -1;
                    break;
                default:
                    this.CurrentCell.Value = CtrlArrGrid[this.CurrentCell.ColumnIndex].Text;
                    CtrlArrGrid[this.CurrentCell.ColumnIndex].Text = string.Empty;
                    break;
            }

            CtrlArrGrid[this.CurrentCell.ColumnIndex].Visible = false;
            if(this.CurrentCell.ColumnIndex != (int)eRsGridColumns.RateCode)
                    CalcRateStru();
        }
        /// <summary>
        /// This overriden method is used to handle ComboBox edit events
        /// </summary>
        /// <param name="e">EventArgs</param>
        /// <Author>Darshan</Author>
        /// <Created Date>26-Oct-2006</Created Date> 
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected override void OnSelectionChanged(EventArgs e)
        {
             //base.OnSelectionChanged(e);
            if (this.CurrentCell == null) return;

            if (this.CurrentCell.ColumnIndex == (int)eRsGridColumns.Post_NonPost)
            {
                this.BeginEdit(false);
            }
        }
        #endregion

        #region DataGrid Key Down Event
        protected override void OnKeyDown(KeyEventArgs e)
        {

             if (e.KeyCode == Keys.Enter)
             {
                 if (CurrentCell == null)
                     return;
                 e.Handled = true;
                 this.BeginEdit(false);
                 if(CurrentCell.ColumnIndex == (int)eRsGridColumns.Inc_Exc
                     || CurrentCell.ColumnIndex == (int)eRsGridColumns.Per_Val
                     || CurrentCell.ColumnIndex == (int)eRsGridColumns.TaxValue
                     || CurrentCell.ColumnIndex == (int)eRsGridColumns.RateCode )
                    CtrlArrGrid[CurrentCell.ColumnIndex].Focus();
             }
             else if (e.KeyCode == Keys.Insert)
             {
                 dtblRateStruDetails.Rows.Add(dtblRateStruDetails.NewRow());
                 //set values of newly added row
                 dtblRateStruDetails.Rows[dtblRateStruDetails.Rows.Count - 1][(int)eRsGridColumns.SalItemCode] = _strActiveItemCode;
                 dtblRateStruDetails.Rows[dtblRateStruDetails.Rows.Count - 1]["Index"] = this.Rows.Count;
                 dtblRateStruDetails.Rows[dtblRateStruDetails.Rows.Count - 1]["RateAmount"] = 0.00;
                 dtblRateStruDetails.Rows[dtblRateStruDetails.Rows.Count - 1]["Post_NonPost"] = false;

                 foreach (DataGridViewCell dgvCell in this.Rows[this.Rows.Count - 1].Cells)
                 {
                     if (dgvCell.ColumnIndex != (int)eRsGridColumns.RateCode)
                         dgvCell.ReadOnly = true;
                     else
                         dgvCell.ReadOnly = false;
                 }
             }
             else
                 base.OnKeyDown(e);
        }
        #endregion

        #region Rate Structure Calculation functions
        /// <summary>
        /// Calculate Rate Stru. Amount for all the rows
        /// </summary>
        /// <returns></returns>
        /// <Author>Darshan</Author>
        /// <Created Date>27-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public void CalcRateStru()
        {
            double dblBasicValue = _dblItemAmount;
            double dblBaseAmount = 0;
            double dblRateAmount = 0;
            double[] dblarrRateAmt = new double[dtblRateStruDetails.Rows.Count + 1];
            double[] dblarrTotalAmt = new double[dtblRateStruDetails.Rows.Count + 1];
            _dblTotalRateAmount = 0.0;
            _dblPostableAmount = 0.0;
            _dblNonPostableAmount = 0.0;
            _dblTotalItemAmount = 0.0;


            #region Calculate Inclusive Taxes first
            for (int i = 0; i < dtblRateStruDetails.DefaultView.Count; i++)
            {
                DataRowView drRecord = dtblRateStruDetails.DefaultView[i];
                if (drRecord["Inc_Exc"].ToString() == "I")
                {
                    dblRateAmount = 0;
                    dblRateAmount = CalcRateAmout(drRecord["Inc_Exc"].ToString(), dblBasicValue, drRecord["Per_Val"].ToString(), Convert.ToDouble(drRecord["TaxValue"],clsGeneralInfo.GetNumberFormatProvider(2)));
                    //this.Rows[i].Cells[(int)eRsGridColumns.RateAmount].Value = dblRateAmount;
                    drRecord["RateAmount"] = dblRateAmount;
                    dblarrRateAmt[i + 1] = dblRateAmount;

                    dblBasicValue = dblBasicValue - dblRateAmount;
                    dblarrTotalAmt[i + 1] = dblBasicValue;
                }
            }
            #endregion

            dblarrRateAmt[0] = dblBasicValue;

            #region Calculate Exculsive Taxes
            for (int i = 0; i < dtblRateStruDetails.DefaultView.Count; i++)
            {
                DataRowView drRecord = dtblRateStruDetails.DefaultView[i];
                if (drRecord["Inc_Exc"].ToString() == "E")
                {
                    dblRateAmount = 0;
                    // if Tax Type = Percentage (Per_Val = "P") then calculate BaseAmount else (if 'V' then) BaseAmount = BasicValue 
                    if (drRecord["Per_Val"].ToString() == "P")
                        dblBaseAmount = CalcBaseAmount(dblarrRateAmt, drRecord["ApplicableOn"].ToString());
                    else
                        dblBaseAmount = dblBasicValue;

                    dblRateAmount = CalcRateAmout(drRecord["Inc_Exc"].ToString(), dblBaseAmount, drRecord["Per_Val"].ToString(), Convert.ToDouble(drRecord["TaxValue"], clsGeneralInfo.GetNumberFormatProvider(2)));
                    dblarrRateAmt[i + 1] = dblRateAmount;

                    //this.Rows[i].Cells[(int)eRsGridColumns.RateAmount].Value = dblRateAmount;
                    drRecord["RateAmount"] = dblRateAmount;
                }
            }

            #endregion

            #region Calculate Total Rate Amount
            for (int i = 0; i < dtblRateStruDetails.DefaultView.Count; i++)
            {
                _dblTotalRateAmount += Convert.ToDouble(dtblRateStruDetails.DefaultView[i][(int)eRsGridColumns.RateAmount], clsGeneralInfo.GetNumberFormatProvider(2));
            }
            #endregion

            #region Calculate Total Amount
            CalcTotalAmount(dblarrRateAmt);
            #endregion

            #region Calculate Postable/Non Postable Amount
            for (int i = 0; i < dtblRateStruDetails.DefaultView.Count; i++)
            {
                if (Convert.ToBoolean(dtblRateStruDetails.DefaultView[i][(int)eRsGridColumns.Post_NonPost]))
                    _dblPostableAmount += Convert.ToDouble(dtblRateStruDetails.DefaultView[i][(int)eRsGridColumns.RateAmount],clsGeneralInfo.GetNumberFormatProvider(2));
                else
                    _dblNonPostableAmount += Convert.ToDouble(dtblRateStruDetails.DefaultView[i][(int)eRsGridColumns.RateAmount],clsGeneralInfo.GetNumberFormatProvider(2));

            }
            #endregion

            this.Refresh();
            RateStructureDetailsChanged(this, _strActiveItemCode);
        }

        /// <summary>
        /// This functions calculates Rate Amount for given tax type(Inc/Exc)(Per/Val), Base Amount and Tax Value
        /// </summary>
        /// <param name="strInc_Exc">string</param>
        /// <param name="dblBaseAmount">double</param>
        /// <param name="strPer_Val">string</param>
        /// <param name="dblTaxValue">double</param>
        /// <returns>Rate Amount</returns>
        /// <Author>Darshan</Author>
        /// <Created Date>27-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private static double CalcRateAmout(string strInc_Exc, double dblBaseAmount, string strPer_Val, double dblTaxValue)
        {
            double dblRateAmount = 0;

            //If TaxType = "Inc." and Per_Val = "P" then to calculate rate add taxvalue to 100
            //else calculate percentage as usual
            if (strInc_Exc.Equals("I", StringComparison.OrdinalIgnoreCase))
            {
                if (strPer_Val.Equals("P", StringComparison.OrdinalIgnoreCase))
                    dblRateAmount = (dblBaseAmount * dblTaxValue) / (100 + dblTaxValue);
                else
                    dblRateAmount = dblTaxValue;
            }
            else
            {
                if (strPer_Val.Equals("P", StringComparison.OrdinalIgnoreCase))
                    dblRateAmount = ((dblBaseAmount * dblTaxValue) / 100);
                else
                    dblRateAmount = dblTaxValue;
            }
            return Convert.ToDouble(dblRateAmount.ToString(clsGeneralInfo.GetNumberFormatProvider(2)), clsGeneralInfo.GetNumberFormatProvider(2));
        }

        /// <summary>
        /// This functions calculates Base Amount from  given Basic Value and Applicable On
        /// </summary>
        /// <param name="dblBasicVal">double</param>
        /// <param name="strApplicableOn">string</param>
        /// <returns>Base Amount </returns>
        /// <Author>Darshan</Author>
        /// <Created Date>27-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private static double CalcBaseAmount(double[] dblarrRateAmt, string strApplicableOn)
        {
            double dblBaseAmt = 0;
            //if string has invalid characters then ignore 
            foreach (char charTemp in strApplicableOn.Trim())
            {
                if (charTemp == '0' || charTemp == '1' || charTemp == '2' || charTemp == '3' || charTemp == '4'
                    || charTemp == '5' || charTemp == '6' || charTemp == '7' || charTemp == '8' || charTemp == '9')
                    continue;
                else
                    return dblarrRateAmt[0];
            }

            //calculate "base amount" from "applicable on" for all tax 
            foreach (char charTemp in strApplicableOn.Trim())
                dblBaseAmt += dblarrRateAmt[Convert.ToInt32(charTemp.ToString(clsGeneralInfo.GetNumberFormatProvider(0)), clsGeneralInfo.GetNumberFormatProvider(0))];

            return Convert.ToDouble(dblBaseAmt.ToString(clsGeneralInfo.GetNumberFormatProvider(2)), clsGeneralInfo.GetNumberFormatProvider(2));
        }

        /// <summary>
        /// This functions calculates Total Amount based on rate amount
        /// </summary>
        /// <param name="dblarrRateAmt">array containing Rate Amount</param>
        /// <returns>Base Amount </returns>
        /// <Author>Darshan</Author>
        /// <Created Date>30-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void CalcTotalAmount(double[] dblarrRateAmt)
        {
            double[] dblarrTotalAmt = new double[dblarrRateAmt.Length];
            dblarrTotalAmt[0] = dblarrRateAmt[0];
            //Set Total Amount for each row
            for (int i = 0; i < dtblRateStruDetails.DefaultView.Count; i++)
            {
                DataRowView drRecord = dtblRateStruDetails.DefaultView[i];
                dblarrTotalAmt[i + 1] = dblarrTotalAmt[i] + dblarrRateAmt[i + 1];
                //this.Rows[i].Cells[(int)eRsGridColumns.TotalAmount].Value = Convert.ToDouble(dblarrTotalAmt[i + 1].ToString("N2"));
                drRecord["TotalAmount"] = Convert.ToDouble(dblarrTotalAmt[i + 1].ToString(clsGeneralInfo.GetNumberFormatProvider(2)), clsGeneralInfo.GetNumberFormatProvider(2));
            }
            _dblTotalItemAmount = Convert.ToDouble(dtblRateStruDetails.DefaultView[dtblRateStruDetails.DefaultView.Count - 1]["TotalAmount"], clsGeneralInfo.GetNumberFormatProvider(2));

        }
        #endregion

        #region Other Functions

        #region Open Applicable On Dialog
        /// <summary>
        /// This functions opens model dialog for selecting Applicable On 
        /// and after closing the form itwill set applicable on value in grid.
        /// </summary>
        /// <Return Type>void</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>26-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private void OpenApplicableOnDialog()
        {
            string strApplicableOn = string.Empty;
            frmApplicableOn objApplicableOn = new frmApplicableOn();
            objApplicableOn.DataSource = dtblRateStruDetails;
            objApplicableOn.CurrentRowIndex = this.CurrentRow.Index;
            objApplicableOn.ShowDialog();
            strApplicableOn = objApplicableOn.ApplicableOn;
            this.CurrentRow.Cells[(int)eRsGridColumns.ApplicableOn].Value = strApplicableOn;
        }
        #endregion

        #region Get Combo Value - Text functions
        /// <summary>
        /// This function returns value for given text
        /// </summary>
        /// <param name="strText">string</param>
        /// <param name="dtblTemp">DataTable</param>
        /// <Return Type>string</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>26-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private static string GetComboValue(string strText, DataTable dtblTemp)
        {
            foreach (DataRow dr in dtblTemp.Rows)
            {
                if (dr["Text"].ToString() == strText)
                    return dr["Value"].ToString();
            }
            return "";
        }

        /// <summary>
        /// This function returns text for given value
        /// </summary>
        /// <param name="strValue">string</param>
        /// <param name="dtblTemp">DataTable</param>
        /// <Return Type>string</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>26-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private static string GetComboText(string strValue, DataTable dtblTemp)
        {
            foreach (DataRow dr in dtblTemp.Rows)
            {
                if (dr["Value"].ToString() == strValue)
                    return dr["Text"].ToString();
            }
            return "";
        }
        #endregion

        #region Verify grid input
        /// <summary>
        /// This function validates DataGrid's edit controls before setting controls' value to data grid
        /// </summary>
        /// <param name="objControl">Control</param>
        /// <Return Type>bool</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>10-Oct-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        private static bool VerifyInput(Control objControl)
        {
            if (objControl.GetType().ToString() == "Imms.Controls.TextBox")
            {
                return ((Imms.Controls.TextBox)objControl).Verify;
            }
            else if (objControl.GetType().ToString() == "Imms.Controls.NumericTextBox")
            {
                return ((Imms.Controls.NumericTextBox)objControl).Verify;
            }
            else if (objControl.GetType().ToString() == "Imms.Controls.ComboBox")
            {
                return true;
            }
            return true; ;
        }
        #endregion

        #endregion

        #region DataGridView Control Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridControl_Leave(object sender, EventArgs e)
        {
            Imms.Controls.TextBox txtTemp  = sender as NumericTextBox ;
            txtTemp.Verify = true;
            //Validate Tax Amount
            if (string.Equals(this.CurrentRow.Cells[(int)eRsGridColumns.Per_Val].Value, "P") && (Convert.ToDouble(txtTemp.Text.Trim(), clsGeneralInfo.GetNumberFormatProvider(2)) > 100))
            {
                Resource.clsShowMsg.ShowErrorMsg("Percentage can not be more than 100.");
                txtTemp.Verify = false;
                return;
            }
      }
        #endregion

        #region CheckSalesItemExists
        public bool CheckSalItemExists(string strSalesItemCode)
        {
            foreach (DataRow drRow in dtblRateStruDetails.Rows)
            {
                if (drRow["SalItemCode"].ToString().Equals(strSalesItemCode, StringComparison.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }

      #endregion 
   }
}



