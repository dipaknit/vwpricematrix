using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using General;
using System.Globalization;
using System.Threading;

/// <Page Name>clsGeneralInfo</Page Name> 
/// <Description>I-Link's User Defined Controls's Proporties .</Description>
/// <Author>Dipak</Author>
/// <Created Date>21/7/2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
 

namespace General.Controls  
{ 
    /// <summary>
    /// Summary description for clsGeneralInfo.
    /// </summary>
    public static class clsGeneralInfo
    {
        #region General Declaration
       
       // public const string clsGeneralInfo = "General.Controls";

        /// <summary>
        /// Declaration Of SQL MinDate & MaxDate
        /// </summary>
        public static DateTime dtMinSQLDate = DateTime.Parse("1/1/1753", GetDateFormatProvider("dd/MM/yyyy"));
        public static DateTime dtMaxSQLDate = DateTime.Parse("31/12/9999", GetDateFormatProvider("dd/MM/yyyy"));

        
        /// <summary>
        /// Declaration Of Property Of Button Conrtols.
        /// </summary>
        private static string[] strColorCode;

        private static Font _btnFont = new Font(Properties.Resources.fntFamily, float.Parse(Properties.Resources.fntSize, clsGeneralInfo.GetNumberFormatProvider(2)), FontStyle.Regular);
        
        //Define color code
        private static Color _btnFontColorCode;
        private static Color _btnBackColorCode;
        private static Color _btnBorderColorCode;
                
        /// <summary>
        /// Declaration Of Property Of CheckBox Conrtols.
        /// </summary>
        private static Color _chkBackColorCode;
        private static Font _chkFont = _btnFont;
        private static Color _chkFontColorCode;
        

        /// <summary>
        /// Declaration Of Property Of Radio Button Conrtols.
        /// </summary>
        private static Font _rdoFont = _btnFont;
        private static Color _rdoFontColorCode;
        private static Color _rdoBackColorCode; 

        /// <summary>
        /// Declaration Of Property Of ComboBox Conrtols.
        /// </summary>
        private static Font _cmbFont = _btnFont;
        private static Color _cmbFontColorCode;
        private static Color _cmbBackColorCode;

        /// <summary>
        /// Declaration Of Property Of TextBox Conrtols.
        /// </summary>
        private static Font _txtFont = _btnFont;
        private static Color _txtFontColorCode;
        private static Color _txtBackColorCode;
            
        /// <summary>
        /// Declaration Of Property Of MaskTextBox Conrtols.
        /// </summary>
        private static Font _mskdtxtFont = _btnFont;
        private static Color _mskdtxtFontColorCode;
        private static Color _mskdtxtBackColorCode;
        static DateTimeFormatInfo currentDateFormat = new DateTimeFormatInfo();
        private static char chrSeperator = Convert.ToChar(System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator);

        /// <summary>
        /// Declaration Of Property Of DateTimePicker Conrtols.
        /// </summary>
        private static Font _dtpFont = _btnFont;
        private static Color _dtpFontColorCode;
        private static Color _dtpBackColorCode;

        /// <summary>
        /// Declaration Of Property Of Numeric Up Down Conrtols.
        /// </summary>
        private static Font _numUpdnFont = _btnFont;
        private static Color _numUpdnFontColorCode;
        private static Color _numUpdnBackColorCode;

        /// <summary>
        /// Declaration for Property Of Numeric TextBox Conrtols.
        /// </summary>
        private static Font _numTxbxFont = _btnFont;
        private static Color _numTxbxFontColorCode;
        private static Color _numTxbxBackColorCode;

        /// <summary>
        /// Declaration for Property Of Label Conrtols.
        /// </summary>
        private static Font _lblFont = _btnFont;
        private static Color _lblFontColorCode;
        private static Color _lblBackColorCode;


        /// <summary>
        /// Declaration for Property Of Header Label Conrtols.
        /// </summary>
        private static Font _lblHdFont = new Font(Properties.Resources.fntFamily,float.Parse(Properties.Resources.lblhdFontSize, clsGeneralInfo.GetNumberFormatProvider(2)),FontStyle.Bold) ;
        private static Color _lblHdFontColorCode;
        private static Color _lblHdBackColorCode = Color.Transparent;


        /// <summary>
        /// Declaration for Property Of LinkLabel Conrtols.
        /// </summary>
        private static Font _lnklblFont = _btnFont;
        private static Color _lnklblFontColorCode;
        private static Color _lnklblBackColorCode;


        /// <summary>
        /// Declaration for Property Of MessageLabel Conrtols.
        /// </summary>        
        private static Font _lblMsgFont = _btnFont;
        private static Color _lblMsgFontColorCode;
        
        
        /// <summary>
        /// Declaration for Property Of ListBox Conrtols.
        /// </summary>
        private static Font _lstFont = _btnFont;
        private static Color _lstFontColorCode;
        private static Color _lstBackColorCode;

        /// <summary>
        /// Declaration for Property Of ListView Conrtols.
        /// </summary>
        private static Font _lvwFont = _btnFont;
        private static Color _lvwFontColorCode;
        private static Color _lvwBackColorCode;

        /// <summary>
        /// Declaration for Property Of ToolTip Conrtols.
        /// </summary>
        private static Font _tltipFont = _btnFont;
        private static Color _tltipFontColorCode;
        private static Color _tltipBackColorCode;

        /// <summary>
        /// Declaration for Property Of TabControl.
        /// </summary>
        private static Font _tbctlFont = _btnFont;
        private static Color _tbctlFontColorCode ;
        private static Color _tbctlBackColorCode;

        /// <summary>
        /// Declaration for Property Of DataGridView.
        /// </summary>
        private static Font _dgvCellFont = new Font(Properties.Resources.fntFamily,float.Parse(Properties.Resources.dgCellFontSize, clsGeneralInfo.GetNumberFormatProvider(2)),FontStyle.Regular);//8.25
        private static Font _dgvColHeaderFont = new Font(Properties.Resources.fntFamily, float.Parse(Properties.Resources.dgColHeaderfntSize, clsGeneralInfo.GetNumberFormatProvider(2)), FontStyle.Bold);//8.25
        private static Color _dtgrdvwFontColorCode;
        private static Color _dtgrdvwBackColorCode;
        private static Color _dtgrdvwAltColorCode;
        private static Color _dtgrdvwSelCellColorCode;
        private static Color _dtgrdvwSelCellForeColorCode;
        private static Color _dtgrdvwRowHeaderBackColorCode;
        private static Color _dtgrdvwRowHeaderForeColorCode;

        /// <summary> 
        /// Declaration for Property Of Form.
        /// </summary>
        private static Font _frmFont = _btnFont;
        private static Color _frmFontColorCode;
        private static Color _frmBackColorCode;

        /// <summary>
        /// Declaration for Property Of PictureBox.
        /// </summary>
        private static Font _pbFont = _btnFont;
        private static Color _pbFontColorCode;

        /// <summary>
        /// Declaration for Property Of Normal Panel.
        /// </summary>
        private static Color _pnlBackColorCode;

        ///// <summary>
        ///// Declaration for Property Of Header Panel.
        ///// </summary>
        //private static Image _pnlHeaderImage = global::General.Resource.Properties.Resources.HeadingStrip;
       
        /// <summary>
        /// Declaration for property of Tree View Control
        /// </summary>
        private static Font _tvFont = _btnFont;
        private static Color _tvFontColorCode;
        private static Color _tvBackColorCode ;

        /// <summary>
        /// 
        /// </summary>
        private static Color _frmChildBackColorCode;
#endregion
       
        #region Constructor 
        static clsGeneralInfo()
        {
            //Define color code
            strColorCode = Properties.Resources.fntColorCode.Split(',');
            _btnFontColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.btnBackColorCode.Split(',');
            _btnBackColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.btnBorderColorCode.Split(',');
            _btnBorderColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.frmBackColorCode.Split(',');            
            _chkBackColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.rdoBackColorCode.Split(',');
            _rdoBackColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.cmbBackColorCode.Split(',');
            _lstBackColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.dgAltRowColorCode.Split(',');
            _dtgrdvwAltColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode = Properties.Resources.dgSelCellBackColorCode.Split(',');
            _dtgrdvwSelCellColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            
            strColorCode = Properties.Resources.dgRowHeaderBackColor.Split(','); 
            _dtgrdvwRowHeaderBackColorCode= Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            strColorCode= Properties.Resources.frmChildBackColorCode.Split(',');
            _frmChildBackColorCode = Color.FromArgb(Int32.Parse(strColorCode[0], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[1], clsGeneralInfo.GetNumberFormatProvider(0)), Int32.Parse(strColorCode[2], clsGeneralInfo.GetNumberFormatProvider(0)));

            _chkFontColorCode = _btnFontColorCode;
            _rdoFontColorCode = _btnFontColorCode;
            _cmbFontColorCode = _btnFontColorCode;
            _txtFontColorCode = _btnFontColorCode;
            _txtBackColorCode = _lstBackColorCode;
            _mskdtxtFontColorCode = _btnFontColorCode;
            _mskdtxtBackColorCode = _lstBackColorCode;
            _dtpFontColorCode = _btnFontColorCode;
            _dtpBackColorCode = _chkBackColorCode;
            _numUpdnFontColorCode = _btnFontColorCode;
            _numUpdnBackColorCode = _chkBackColorCode;
            _numTxbxFontColorCode = _btnFontColorCode;
            _numTxbxBackColorCode = _chkBackColorCode;
            _lblFontColorCode = _btnFontColorCode;
            _lblBackColorCode = _rdoBackColorCode;
            _lblHdFontColorCode = _btnFontColorCode;
            _lnklblFontColorCode = _btnFontColorCode;
            _lnklblBackColorCode = _chkBackColorCode;
            _lblMsgFontColorCode = _btnFontColorCode;
            _lstFontColorCode = _btnFontColorCode;
            _lvwFontColorCode = _btnFontColorCode;
            _lvwBackColorCode = _lstBackColorCode;
            _cmbBackColorCode = _lstBackColorCode;
            _tltipFontColorCode = _btnFontColorCode;
            _tltipBackColorCode = _chkBackColorCode;
            _tbctlFontColorCode = _btnFontColorCode;
            _tbctlBackColorCode = _chkBackColorCode;
            _dtgrdvwFontColorCode = _btnFontColorCode;
            _dtgrdvwBackColorCode = _lstBackColorCode;
            _dtgrdvwSelCellForeColorCode = _btnFontColorCode;
            _dtgrdvwRowHeaderForeColorCode = _btnFontColorCode;
            _frmFontColorCode = _btnFontColorCode;
            _frmBackColorCode = _chkBackColorCode;
            _pbFontColorCode = _btnFontColorCode;
            _pnlBackColorCode = _chkBackColorCode;
            _tvFontColorCode = _btnFontColorCode;
            _tvBackColorCode = _chkBackColorCode;
           
        }

#endregion

        #region Public Prioperties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        /// <Author>Dipak</Author>
        /// <Created Date>21/07/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>

        #region Button Property

        /// <summary>
        /// Property of  Button Font 
        /// </summary>
        public static Font  btnFont
        {
            get
            {
                return _btnFont;
            }
        }


        /// <summary>
        /// Property of  Button Font Color Code
        /// </summary>
        public static Color btnFontColorCode
        {
            get
            {                
                return _btnFontColorCode;
            }            
        }

        /// <summary>
        /// Property of  Button Back Size
        /// </summary>
        public static Color btnBackColorCode
        {
            get
            {                
                return _btnBackColorCode;
            }            
        }


        /// <summary>
        /// Property of  Button Back Size
        /// </summary>
        public static Color btnBorderColorCode
        {
            get
            {                
                return _btnBorderColorCode;
            }            
        }


        #endregion

        #region CheckBox Property

        /// <summary>
        /// Property of  CheckBox Font Family
        /// </summary>
        public static Font chkFont
        {
            get
            {               
                return _chkFont;
            }            
        }

        /// <summary>
        /// Property of  CheckBox Font Color Code
        /// </summary>
        public static Color chkFontColorCode
        {
            get
            {               
                return _chkFontColorCode;
            }            
        }

        /// <summary>
        /// Property of  CheckBox Back Color Code
        /// </summary>
        public static Color chkBackColorCode
        {
            get
            {
                return _chkBackColorCode;
            }            
        }
        #endregion

        #region Radio Button Property

        /// <summary>
        /// Property of  Radio Button Font Family
        /// </summary>
        public static Font rdoFont
        {
            get
            {               
                return _rdoFont;
            }            
        }

        /// <summary>
        /// Property of  Radio Button Font Color Code
        /// </summary>
        public static Color rdoFontColorCode
        {
            get
            {
                return _rdoFontColorCode;
            }          
        }

        /// <summary>
        /// Property of  Radio Button BackColorCode
        /// </summary>
        public static Color rdoBackColorCode
        {
            get
            {                
                return _rdoBackColorCode;
            }            
        }

        #endregion

        #region ComboBox Property

        /// <summary>
        /// Property of  ComboBox Font Family
        /// </summary>
        public static Font cmbFont
        {
            get
            {                
                return _cmbFont;
            }            
        }

        /// <summary>
        /// Property of  ComboBox Font Color Code
        /// </summary>
        public static Color cmbFontColorCode
        {
            get
            {
                return _cmbFontColorCode;
            }            
        }

        /// <summary>
        /// Property of  ComboBox Back Color Code
        /// </summary>
        public static Color cmbBackColorCode
        {
            get
            {
                return _cmbBackColorCode;
            }            
        }

        #endregion

        #region Textbox Property

        /// <summary>
        /// Property of  Textbox Font 
        /// </summary>
        public static Font txtFont
        {
            get
            {
                return _txtFont;
            }            
        }

        /// <summary>
        /// Property of  Textbox Font Color code
        /// </summary>
        public static Color txtFontColorCode
        {
            get
            {
                return _txtFontColorCode;
            }            
        }

        /// <summary>
        /// Property of  Textbox Back Color Code
        /// </summary>
        public static Color txtBackColorCode
        {
            get
            {
                return _txtBackColorCode;
            }            
        }

        #endregion

        #region Mask Textbox

        /// <summary>
        /// Property of  Mask Textbox Font 
        /// </summary>
        public static Font mskdtxtFont
        {
            get
            {       
                return _mskdtxtFont;
            }            
        }

        /// <summary>
        /// Property of  Mask Textbox Font Color code
        /// </summary>
        public static Color mskdtxtFontColorCode
        {
            get
            {
                return _mskdtxtFontColorCode;
            }            
        }

        /// <summary>
        /// Property of  Mask Textbox Back Color code
        /// </summary>
        public static Color mskdtxtBackColorCode
        {
            get
            {
                return _mskdtxtBackColorCode;
            }            
        }
        #endregion

        #region Date Time Picker

        /// <summary>
        /// Property of  Date Time Picker Font 
        /// </summary>
        public static Font dtpFont
        {
            get
            {
                return _dtpFont;
            }            
        }

        /// <summary>
        /// Property of  Date Time Picker Font Color Code
        /// </summary>
        public static Color dtpFontColorCode
        {
            get
            {
                return _dtpFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Date Time Picker Back color code
        /// </summary>
        public static Color dtpBackColorCode
        {
            get
            {
                return _dtpBackColorCode;
            }
        }

        #endregion

        #region Numeric Updown Property

        /// <summary>
        /// Property of  Numeric Updown Font 
        /// </summary>
        public static Font numUpdnFont
        {
            get
            {
                return _numUpdnFont;
            }
        }

        /// <summary>
        /// Property of  Numeric Updown Font Color Code.
        /// </summary>
        public static Color numUpdnFontColorCode
        {
            get
            {
                return _numUpdnFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Numeric Updown Back Color Code
        /// </summary>
        public static Color numUpdnBackColorCode
        {
            get
            {
                return _numUpdnBackColorCode;
            }
        }
        #endregion

        #region Numeric TextBox Property

        /// <summary>
        /// Property of  Numeric TextBox Font 
        /// </summary>
        public static Font numTxbxFont
        {
            get
            {
                return _numTxbxFont;
            }
        }

        /// <summary>
        /// Property of  Numeric TextBox Font Color Code
        /// </summary>
        public static Color numTxbxFontColorCode
        {
            get
            {
                return _numTxbxFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Numeric TextBox Back color code
        /// </summary>
        public static Color numTxbxBackColorCode
        {
            get
            {
                return _numTxbxBackColorCode;
            }
        }
        #endregion

        #region Label Property

        /// <summary>
        /// Property of  Label Font Family
        /// </summary>
        public static Font lblFont
        {
            get
            {
                return _lblFont;
            }
        }

        /// <summary>
        /// Property of  Label Font Color Code
        /// </summary>
        public static Color lblFontColorCode
        {
            get
            {
                return _lblFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Label Back color code
        /// </summary>
        public static Color lblBackColorCode
        {
            get
            {
                return _lblBackColorCode;
            }
        }
        
        #endregion

        #region Header Label Property

        /// <summary>
        /// Property of  Header Label Font Family
        /// </summary>
        public static Font lblhdFont
        {
            get
            {
                return _lblHdFont;
            }
        }

        /// <summary>
        /// Property of Header Label Font Color Code
        /// </summary>
        public static Color lblhdFontColorCode
        {
            get
            {
                return _lblHdFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Label Back color code
        /// </summary>
        public static Color lblhdBackColorCode
        {
            get
            {
                return _lblHdBackColorCode;
            }
        }

        #endregion

        #region Message Label Property

        /// <summary>
        /// Property of  Message Label Font Family
        /// </summary>
        public static Font lblMsgFont
        {
            get
            {
                return _lblMsgFont;
            }
        }

        /// <summary>
        /// Property of Header Label Font Color Code
        /// </summary>
        public static Color lblMsgFontColorCode
        {
            get
            {
                return _lblMsgFontColorCode;
            }
        }

        #endregion

        #region Link Button Property

        /// <summary>
        /// Property of  Link Label Font 
        /// </summary>
        public static Font lnklblFont
        {
            get
            {
                return _lnklblFont;
            }
        }

        /// <summary>
        /// Property of  Link Label Font Color code
        /// </summary>
        public static Color lnklblFontColorCode
        {
            get
            {
                return _lnklblFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Link Label Back color Code
        /// </summary>
        public static Color lnklblBackColorCode
        {
            get
            {
                return _lnklblBackColorCode;
            }
        }

        #endregion

        #region Child Form Proeprty
        public static Color frmChildBackColorCode
        {
            get
            {
                return _frmChildBackColorCode;
            }
        }
        #endregion

        #region ListBox Property

        /// <summary>
        /// Property of  ListBox Font 
        /// </summary>
        public static Font lstFont
        {
            get
            {
                return _lstFont;
            }
        }

        /// <summary>
        /// Property of  ListBox Font Color Code
        /// </summary>
        public static Color lstFontColorCode
        {
            get
            {
                return _lstFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Label Back color code
        /// </summary>
        public static Color lstBackColorCode
        {
            get
            {
                return _lstBackColorCode;
            }
        }

        #endregion


        #region ListView Property

        /// <summary>
        /// Property of  ListBox Font 
        /// </summary>
        public static Font lvwFont
        {
            get
            {
                return _lvwFont;
            }
        }

        /// <summary>
        /// Property of  ListBox Font Color Code
        /// </summary>
        public static Color lvwFontColorCode
        {
            get
            {
                return _lvwFontColorCode;
            }
        }

        /// <summary>
        /// Property of  Label Back color code
        /// </summary>
        public static Color lvwBackColorCode
        {
            get
            {
                return _lvwBackColorCode;
            }
        }

        #endregion

        #region PictureBox Property

        /// <summary>
        /// Property of  PictureBox Font 
        /// </summary>
        public static Font pbFont
        {
            get
            {
                return _pbFont;
            }
        }

        /// <summary>
        /// Property of  PictureBox Font Color Code
        /// </summary>
        public static Color pbFontColorCode
        {
            get
            {
                return _pbFontColorCode;
            }
        }

        #endregion

        #region ToolTip Property

        /// <summary>
        /// Property of  ToolTip Font 
        /// </summary>
        public static Font tltipFont
        {
            get
            {
                return _tltipFont;
            }
        }

        /// <summary>
        /// Property of  ToolTip Font Color Code
        /// </summary>
        public static Color tltipFontColorCode
        {
            get
            {
                return _tltipFontColorCode;
            }
        }

        /// <summary>
        /// Property of  ToolTip Back Color code
        /// </summary>
        public static Color tltipBackColorCode
        {
            get
            {
                return _tltipBackColorCode;
            }
        }
        #endregion

        #region TabControl Property

        /// <summary>
        /// Property of  TabControl Font 
        /// </summary>
        public static Font tbctlFont
        {
            get
            {
                return _tbctlFont;
            }
        }

        /// <summary>
        /// Property of  TabControl Font Color Code
        /// </summary>
        public static Color tbctlFontColorCode
        {
            get
            {
                return _tbctlFontColorCode;
            }
        }

        /// <summary>
        /// Property of  TabControl Back Color Code
        /// </summary>
        public static Color tbctlBackColorCode
        {
            get
            {
                return _tbctlBackColorCode;
            }
        }
        #endregion

        #region DataGridView Property

        /// <summary>
        /// Property of  DataGridView Font 
        /// </summary>
        public static Font dgvCellFont
        {
            get
            {
                return _dgvCellFont;
            }
        }

        /// <summary>
        /// Property of  DataGridView Column Header Font 
        /// </summary>
        public static Font dgvColHeaderFont
        {
            get
            {
                return _dgvColHeaderFont;
            }
        }
            
        /// <summary>
        /// Property of  DataGridView Font Color Code
        /// </summary>
        public static Color dtgrdvwFontColorCode
        {
            get
            {
                return _dtgrdvwFontColorCode;
            }
        }

        /// <summary>
        /// Property of  DataGridView Back Color Code
        /// </summary>
        public static Color dtgrdvwBackColorCode
        {
            get
            {
                return _dtgrdvwBackColorCode;
            }
        }

        /// <summary> 
        /// Property of  DataGridView Alternet Row Color Code
        /// </summary>
        public static Color dtgrdvwAltColorCode
        {
            get
            {
                return _dtgrdvwAltColorCode;
            }
        }

        /// <summary>
        /// Property of  DataGridView Select Cell Back Color Code.
        /// </summary>
        public static Color dtgrdvwSelCellBackColorCode
        {
            get
            {
                return _dtgrdvwSelCellColorCode;
            }
        }

        /// <summary>
        /// Property of  DataGridView Select Cell Fore Color Code
        /// </summary>
        public static Color dtgrdvwSelCellForeColorCode
        {
            get
            {
                return _dtgrdvwSelCellForeColorCode;
            }
        }

        /// <summary>
        /// Property of  DataGridView Row Header Back Color Code
        /// </summary>
        public static Color dtgrdvwRowHeaderBackColorCode
        {
            get
            {
                return _dtgrdvwRowHeaderBackColorCode;
            }
        }

        /// <summary>
        /// Property of  DataGridView Row Header Fore Color Code
        /// </summary>
        public static Color dtgrdvwRowHeaderForeColorCode
        {
            get
            {
                return _dtgrdvwRowHeaderForeColorCode;
            }
        }

        #endregion

        #region Forms Property
        /// <summary>
        /// Property of Form Font 
        /// </summary>
        public static Font frmFont
        {
            get
            {
                return _frmFont;
            }
        }

        /// <summary>
        /// Property of Form Font Color Code
        /// </summary>
        public static Color frmFontColorCode
        {
            get
            {
                return _frmFontColorCode;
            }
        }

        /// <summary>
        /// Property of Form Back Color code
        /// </summary>
        public static Color frmBackColorCode
        {
            get
            {
                return _frmBackColorCode;
            }
        }

        #endregion

        #region TreeView Property
        /// <summary>
        /// Property of Form Font 
        /// </summary>
        public static Font tvFont
        {
            get
            {
                return _tvFont;
            }
        }

        /// <summary>
        /// Property of Form Font Color Code
        /// </summary>
        public static Color tvFontColorCode
        {
            get
            {
                return _tvFontColorCode;
            }
        }

        /// <summary>
        /// Property of Form Back Color code
        /// </summary>
        public static Color tvBackColorCode
        {
            get
            {
                return _tvBackColorCode;
            }
        }

        #endregion
        
        #region Normal Panels Property

        /// <summary>
        /// Property of Panel Back Color
        /// </summary>
        public static Color pnlBackColorCode
        {
            get
            {
                return _pnlBackColorCode;
            }
        }
        #endregion
               
        #region Panel Header Image Property

        /// <summary>
        /// Property of Panel Header Image
        /// </summary>

        //public static Image pnlHeaderImage
        //{
        //    get
        //    {
        //        return _pnlHeaderImage;
        //    }
        //}
        #endregion


    
#endregion

        #region General Methods 

        /// <summary>
        /// This function returns format Provider for Given Number
        /// </summary>
        /// <param name="intPrecision">int</param>
        /// <Return Type>System.Globalization.NumberFormatInfo</Return Type>
        /// <Author>Darshan</Author>
        /// <Created Date>25/10/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public static System.Globalization.NumberFormatInfo GetNumberFormatProvider(int intPrecision)
        {
            System.Globalization.NumberFormatInfo objNumFormatInfo = new System.Globalization.NumberFormatInfo();
            objNumFormatInfo.NumberDecimalDigits = intPrecision;
            return objNumFormatInfo;
        }

        /// <summary>
        /// This function returns format Provider for Given Date
        /// </summary>
        /// <param name="strFormat">string</param>
        /// <Return Type>System.Globalization.DateTimeFormatInfo</Return Type>
        /// <Author>Dipak</Author>
        /// <Created Date>24/11/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public static System.Globalization.DateTimeFormatInfo GetDateFormatProvider(string strFormat)
        {
            System.Globalization.DateTimeFormatInfo objDateFormatInfo = new System.Globalization.DateTimeFormatInfo();
            objDateFormatInfo.ShortDatePattern = strFormat;
            return objDateFormatInfo;
        }

        /// <summary>
        /// This function returns format Provider for Given Date
        /// </summary>
        /// <Return Type>System.Globalization.DateTimeFormatInfo</Return Type>
        /// <Author>Deepmala</Author>
        /// <Created Date>8/12/2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public static System.Globalization.DateTimeFormatInfo GetDateFormatProvider()
        {
            string strFormat = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
            System.Globalization.DateTimeFormatInfo objDateFormatInfo = new System.Globalization.DateTimeFormatInfo();
                    objDateFormatInfo.ShortDatePattern = strFormat;
            return objDateFormatInfo;
        }
               
        #region Format Date To SQL
        /// <summary>
        /// Formate date to be entered in SQL D/B.
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns></returns>
        public static string FormatDateToSQL(string inputdate)
        {
            char chrSqlSeperator = '-';

            string strsqlDateFormat = string.Empty;
            string strYear = string.Empty;
            string strDay = string.Empty;
            string strMonth = string.Empty;

            currentDateFormat.ShortDatePattern = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;

            try
            {
                //Parse date according to existing culture          
                DateTime dtInputDate = DateTime.Parse(inputdate, currentDateFormat);
                strYear = dtInputDate.Year.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
                strDay = dtInputDate.Day.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
                strMonth = dtInputDate.Month.ToString(clsGeneralInfo.GetNumberFormatProvider(0));
            }
            catch
            {
                strsqlDateFormat = string.Empty;
            }

            strsqlDateFormat = strYear + chrSqlSeperator + strMonth + chrSqlSeperator + strDay;
            return strsqlDateFormat;
        }
        #endregion

        #region Validate Date
        /// <summary>
        /// validate date according to culture, parse date, and validate year,month and date
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns></returns>
        public static bool ValidateDate(string strInputDate)
        {
            currentDateFormat.ShortDatePattern = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;
            try
            {
                //Parse date according to existing culture          
                DateTime dtInputDate = DateTime.Parse(strInputDate, currentDateFormat);

                if (dtInputDate < dtMinSQLDate || dtInputDate > dtMaxSQLDate)
                {
                    ///General.Resource.clsShowMsg.ShowErrorMsg(General.Resource.Messages.EGEN0001.Replace("<xxx>",dtMinSQLDate.ToString()).Replace("<yyy>",dtMaxSQLDate.ToString()));
                    return false;
                }
                else 
                    return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Convert Text To Date Time
        /// <summary>
        /// Convert Text of Date to DateTime Type
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns></returns>
        public static DateTime ConvertTextToDateTime(string strInputDate)
        {
            currentDateFormat.ShortDatePattern = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;

            //Parse date according to existing culture          
            return DateTime.Parse(strInputDate, currentDateFormat);

        }
        #endregion

        #region Format Date
        ///// <summary>
        ///// Formate date to be displayed according to current culture
        ///// </summary>
        ///// <param name="inputdate"></param>
        ///// <returns>return date</returns>
        //public static string FormatDate(string strInputDate)
        //{
        //    currentDateFormat.ShortDatePattern = strDateFormat;
        //    string strFormatedDate = string.Empty;                       

        //    DateTime dtInputDate;
        //    //Parse date according to existing culture          
        //    try
        //    {
        //        dtInputDate = DateTime.Parse(strInputDate, currentDateFormat);
        //    }
        //    catch
        //    {
        //        return string.Empty;
        //    }
            
        //    strFormatedDate = strDateFormat.ToUpper();
        //    strFormatedDate = strFormatedDate.Replace("M", (dtInputDate.Month.ToString().Length < 2) ? "0" + dtInputDate.Month.ToString() : dtInputDate.Month.ToString());
        //    strFormatedDate = strFormatedDate.Replace("YYYY", dtInputDate.Year.ToString());
        //    strFormatedDate = strFormatedDate.Replace("D", (dtInputDate.Day.ToString().Length < 2) ? "0" + dtInputDate.Day.ToString() : dtInputDate.Day.ToString());

        //    return strFormatedDate;
        //}

          /// <summary>
        /// Formate date to be displayed according to current culture
        /// </summary>
        /// <param name="inputdate"></param>
        /// <returns>return date</returns>
        public static string FormatDate(string strInputDate)
        {
            currentDateFormat.ShortDatePattern = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;
            string strFormatedDate = string.Empty;                       

            DateTime dtInputDate;
            //Parse date according to existing culture          
            try
            {
                dtInputDate = DateTime.Parse(strInputDate, currentDateFormat);
            }
            catch
            {
                return string.Empty;
            }
            //"dd/MM/yyyy"
            strFormatedDate = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;
            strFormatedDate = strFormatedDate.Replace("MM", (dtInputDate.Month.ToString().Length < 2) ? "0" + dtInputDate.Month.ToString() : dtInputDate.Month.ToString());
            strFormatedDate = strFormatedDate.Replace("yyyy", dtInputDate.Year.ToString());
            strFormatedDate = strFormatedDate.Replace("dd", (dtInputDate.Day.ToString().Length < 2) ? "0" + dtInputDate.Day.ToString() : dtInputDate.Day.ToString());

            return strFormatedDate;
        }
        
        #endregion
              

        #region Validate Null Date
        /// <summary>
        /// Very Date Is null 
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>        
        public static bool IsNullDate(string strInputDate)
        {
            // currentDateFormat.ShortDatePattern = strDateFormat;
            char chrReplaceSeperator = ' ';
            try
            {
                strInputDate = strInputDate.Replace(chrSeperator, chrReplaceSeperator);
                if (strInputDate.Trim().Length == 0) return true;
                else return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Compare Date
         /// <summary>
        /// Compare original date with maximum date and minimum date
        /// </summary>
        /// <param name="originaldate"></param>
        /// <returns></returns>
        /// <Author>Darshan</Author>
        /// <Created Date>19-Dec-2006</Created Date>
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        public static bool CompareDate(string strInputDate,string strMinDate, string strMaxDate)
        {
            currentDateFormat.ShortDatePattern = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;

            bool bResult = true;
            if (strMinDate.Length > 0)
            {
                if (DateTime.Parse(strInputDate, currentDateFormat) < DateTime.Parse(strMinDate, currentDateFormat))
                {
                    bResult = false;
                }
            }

            if (strMaxDate.Length > 0)
            {
                if (DateTime.Parse(strInputDate, currentDateFormat) > DateTime.Parse(strMaxDate, currentDateFormat))
                {
                    bResult = false;
                }
            }
            return bResult;
        }       
        #endregion

        #region Add Days
        /// <summary>
        /// Very Date Is null 
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>        
        public static string AddDays(string strInputDate, int intNumberOfDays)
        {
            // currentDateFormat.ShortDatePattern = strDateFormat;
            try
            {
                DateTime dtInputDate = DateTime.Parse(strInputDate, currentDateFormat);
                dtInputDate = dtInputDate.AddDays(intNumberOfDays);
                return dtInputDate.ToString();  
            }
            catch
            {
                return string.Empty;
            }
        }
        #endregion
        #endregion
    }
}