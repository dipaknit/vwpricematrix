namespace Imms.Controls
{
    partial class ucTabAttachment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new Imms.Controls.Panel();
            this.lvwFileAttach = new Imms.Controls.ListView();
            this.chFileName = new System.Windows.Forms.ColumnHeader();
            this.chMode = new System.Windows.Forms.ColumnHeader();
            this.btnAdd = new Imms.Controls.Button();
            this.btnRemove = new Imms.Controls.Button();
            this.lblAttachMessage = new Imms.Controls.HeaderLabel();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.ApplyMySettings = false;
            this.pnlMain.Controls.Add(this.lvwFileAttach);
            this.pnlMain.CtrlProperty = "";
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(729, 127);
            this.pnlMain.TabIndex = 234;
            // 
            // lvwFileAttach
            // 
            this.lvwFileAttach.ApplyMySettings = true;
            this.lvwFileAttach.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chFileName,
            this.chMode});
            this.lvwFileAttach.CtrlProperty = "";
            this.lvwFileAttach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwFileAttach.FullRowSelect = true;
            this.lvwFileAttach.GridLines = true;
            this.lvwFileAttach.Location = new System.Drawing.Point(0, 0);
            this.lvwFileAttach.MultiSelect = false;
            this.lvwFileAttach.Name = "lvwFileAttach";
            this.lvwFileAttach.Size = new System.Drawing.Size(729, 127);
            this.lvwFileAttach.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvwFileAttach.TabIndex = 232;
            this.lvwFileAttach.UseCompatibleStateImageBehavior = false;
            this.lvwFileAttach.View = System.Windows.Forms.View.Details;
            this.lvwFileAttach.DoubleClick += new System.EventHandler(this.lvwFileAttach_DoubleClick);
            // 
            // chFileName
            // 
            this.chFileName.Text = "File Name";
            this.chFileName.Width = 550;
            // 
            // chMode
            // 
            this.chMode.Text = "Mode";
            this.chMode.Width = 175;
            // 
            // btnAdd
            // 
            this.btnAdd.ApplyMySettings = true;
            this.btnAdd.CtrlProperty = "Add";
            this.btnAdd.Location = new System.Drawing.Point(747, 22);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(71, 25);
            this.btnAdd.TabIndex = 230;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.ApplyMySettings = true;
            this.btnRemove.CtrlProperty = "Remove";
            this.btnRemove.Location = new System.Drawing.Point(747, 71);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(71, 25);
            this.btnRemove.TabIndex = 231;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // lblAttachMessage
            // 
            this.lblAttachMessage.ApplyMySettings = true;
            this.lblAttachMessage.AutoSize = true;
            this.lblAttachMessage.CtrlProperty = "Note: To view a file, Double click on it.";
            this.lblAttachMessage.Location = new System.Drawing.Point(5, 130);
            this.lblAttachMessage.Name = "lblAttachMessage";
            this.lblAttachMessage.Size = new System.Drawing.Size(190, 13);
            this.lblAttachMessage.TabIndex = 233;
            this.lblAttachMessage.Text = "Note: To view a file, Double click on it.";
            // 
            // ucTabAttachment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.lblAttachMessage);
            this.Name = "ucTabAttachment";
            this.Size = new System.Drawing.Size(840, 148);
            this.Load += new System.EventHandler(this.ucTabAttachment_Load);
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HeaderLabel lblAttachMessage;
        private ListView lvwFileAttach;
        private System.Windows.Forms.ColumnHeader chFileName;
        private System.Windows.Forms.ColumnHeader chMode;
        private Button btnRemove;
        private Button btnAdd;
        private Panel pnlMain;

    }
}
