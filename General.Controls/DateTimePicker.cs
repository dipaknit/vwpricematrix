using System;
using System.Drawing;
using System.Configuration;


/// <Page Name>Date Time Picker</Page Name> 
/// <Description>Properties of Date Time Picker</Description>
/// <Author> Hardik</Author>
/// <Created Date></Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>

namespace General.Controls
{
    public class DateTimePicker : System.Windows.Forms.DateTimePicker, iRoot
    {
        #region Declaration
        bool blnApplyMySettings;
        
        #endregion

        #region Public Prioperties
        /// <summary>
        /// To set or get Time info
        /// </summary>
        public string CtrlProperty
        {

            get
            {
                return Value.TimeOfDay.ToString();
            }
            set
            {
                System.Globalization.DateTimeFormatInfo objDTFI = new System.Globalization.DateTimeFormatInfo();

                if(!string.IsNullOrEmpty(value))
                    Value = DateTime.Parse(value, objDTFI);
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion


        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (!this.DesignMode && blnApplyMySettings)
            {               
                //Set back color for current control
                    this.BackColor = clsGeneralInfo.dtpBackColorCode;
                //Set font for current control
                this.Font = clsGeneralInfo.dtpFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode; 
            }
        }
        #endregion
       
    }
}
