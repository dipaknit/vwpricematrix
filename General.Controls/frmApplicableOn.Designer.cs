namespace Imms.Controls
{
    partial class frmApplicableOn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpnlApplicableOn = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCancel = new Imms.Controls.Button();
            this.btnOK = new Imms.Controls.Button();
            this.lblApplicableOn = new Imms.Controls.HeaderLabel();
            this.SuspendLayout();
            // 
            // flpnlApplicableOn
            // 
            this.flpnlApplicableOn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpnlApplicableOn.Location = new System.Drawing.Point(20, 40);
            this.flpnlApplicableOn.Name = "flpnlApplicableOn";
            this.flpnlApplicableOn.Size = new System.Drawing.Size(420, 59);
            this.flpnlApplicableOn.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.ApplyMySettings = true;
            this.btnCancel.CtrlProperty = "&Cancel";
            this.btnCancel.Location = new System.Drawing.Point(365, 105);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.ApplyMySettings = true;
            this.btnOK.CtrlProperty = "&OK";
            this.btnOK.Location = new System.Drawing.Point(284, 105);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblApplicableOn
            // 
            this.lblApplicableOn.ApplyMySettings = true;
            this.lblApplicableOn.CtrlProperty = "Applicable On:";
            this.lblApplicableOn.Location = new System.Drawing.Point(17, 14);
            this.lblApplicableOn.Name = "lblApplicableOn";
            this.lblApplicableOn.Size = new System.Drawing.Size(125, 17);
            this.lblApplicableOn.TabIndex = 1;
            this.lblApplicableOn.Text = "Applicable On:";
            // 
            // frmApplicableOn
            // 
            this.ClientSize = new System.Drawing.Size(458, 149);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblApplicableOn);
            this.Controls.Add(this.flpnlApplicableOn);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmApplicableOn";
            this.Padding = new System.Windows.Forms.Padding(4);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Applicable On";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmApplicableOn_KeyDown);
            this.Load += new System.EventHandler(this.frmApplicableOn_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpnlApplicableOn;
        private Imms.Controls.HeaderLabel lblApplicableOn;
        private Imms.Controls.Button btnOK;
        private Button btnCancel;
    }
}
