using System;
using System.Drawing;
using System.Configuration;


/// <Page Name>ListBox</Page Name> 
/// <Description>Properties Of ListBox</Description>
/// <Author>Dipak</Author>
/// <Created Date>10/8/2006</Created Date>
/// <Revision History>
/// <Date></Date><Author></Author><Comments></Comments>		
/// </Revision History>
namespace General.Controls
{
    /// <summary>
    /// Summary description for ListBox.
    /// </summary>
    public class ListBox : System.Windows.Forms.ListBox, iRoot
    {

        #region Declaration
        bool blnApplyMySettings;
        #endregion

        #region Public Prioperties
        /// <summary>
        /// ILink General property to access default property of the control
        /// </summary>
        public string CtrlProperty
        {
            get
            {
                return base.Text.Trim();
            }
            set
            {
                base.Text = (value == null) ? string.Empty : value.Trim();
            }
        }
        /// <summary>
        /// ILink General property to decide whether we have to assign our properties
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }
        #endregion


        #region Set Ctrl Properties
        /// <summary>
        /// Set all required properties on control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            /// <summary>
            /// Access clsGeneralInfo's Propertes.
            /// </summary>
            /// <Author>Dipak</Author>
            /// <Created Date>21/06/2006</Created Date>

            if (!this.DesignMode && blnApplyMySettings)
            {                  
                //Set back color for current control
                this.BackColor = clsGeneralInfo.lstBackColorCode;
                //Set font for current control
                this.Font = clsGeneralInfo.lstFont;
                //Set fore color for current control
                this.ForeColor = clsGeneralInfo.btnFontColorCode; 
            }
        }
        #endregion
    }
}
