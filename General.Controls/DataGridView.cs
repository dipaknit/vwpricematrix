using System;
using System.Drawing;
using System.Windows.Forms;

/// <Page Name>DataGridView</Page Name> 
/// <Description>Declaration Property Of DataGridView</Description>
/// <Author>Dipak</Author>
/// <Created Date>21/07/2006</Created Date>
/// <Revision History>
/// <Date>16-Oct-2006</Date>
/// <Author>Darshan</Author>
/// <Comments>Override ColumnWidthChanged event</Comments>		
/// </Revision History>
namespace General.Controls
{
    public class DataGridView : System.Windows.Forms.DataGridView
    {
        #region Declaration
        /// <summary>
        /// Flag to identify whether grid settings are applied or not
        /// </summary>
        private bool blnAutoGenerateColumn;

        /// <summary>
        /// Flag to identify whether grid Editable Or not
        /// </summary>
        private bool blnEditGrid;

        /// <summary>
        /// Flag to identify whether grid settings are applied or not
        /// </summary>
        private bool blnApplyMySettings;

        /// <summary>
        /// Cell Style for all columns header
        /// </summary>
        private DataGridViewCellStyle dgvcsAllColumnStyle;

        /// <summary>
        /// Control to set Info Messages
        /// </summary>
        private ucInfoMessage _ctlInfoMessage;

        /// <summary>
        /// Array of Info Messages
        /// </summary>
        private string[] _strInfoMsgArr;
        
        /// <summary>
        /// Up-Down Key Allow Flag
        /// </summary>
        bool  _blnUpDownKey = false;        
        #endregion

        #region Public Prioperties

        /// <summary>
        /// ILink General property to decide in which infoMessage bar the message should display for the textbox
        /// </summary>
        public string[] strInfoMsgArr
        {
            get
            {
                return _strInfoMsgArr;
            }
            set
            {
                _strInfoMsgArr = value;
            }
        }

        /// <summary>
        /// ILink General property to decide in which infoMessage bar the message should display for the textbox
        /// </summary>
        public ucInfoMessage ctlInfoMessage
        {
            get
            {
                return _ctlInfoMessage;
            }
            set
            {
                _ctlInfoMessage = value;
            }
        }

        /// <summary>
        /// Identify whether user wants want to apply our settings in the grid or not
        /// </summary>
        public bool ApplyMySettings
        {
            get
            {
                return blnApplyMySettings;
            }
            set
            {
                blnApplyMySettings = value;
            }
        }

        /// <summary>
        /// Identify whether user wants want to apply our settings in the grid or not
        /// </summary>
        public bool MyAutoGenerateColumn
        {
            get
            {
                return blnAutoGenerateColumn;
            }
            set
            {
                blnAutoGenerateColumn = value;
                this.AutoGenerateColumns = MyAutoGenerateColumn;
            }
        }

        /// <summary>
        /// Identify whether user wants want to allow Edit in Grid
        /// </summary>
        public bool MyEditGrid
        {
            get
            {
                return blnEditGrid;
            }
            set
            {
                blnEditGrid = value;

                if (blnEditGrid == true)
                {
                    this.RowTemplate.ReadOnly = false;
                    this.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.NotSet;
                    this.ShowEditingIcon = true;

                    foreach (DataGridViewColumn dgvcItem in this.Columns)
                    {
                        dgvcItem.SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
                else
                {
                    this.RowTemplate.ReadOnly = true;
                    this.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                    this.ShowEditingIcon = false;

                    foreach (DataGridViewColumn dgvcItem in this.Columns)
                    {
                        dgvcItem.SortMode = DataGridViewColumnSortMode.Automatic;
                    }
                }
            }
        }

        ///// <summary>
        ///// ILink General property to decide in which infoMessage bar the message should display for the textbox
        ///// </summary>
        public bool AllowUpDownKey
        {
            get
            {
                return _blnUpDownKey;
            }
            set
            {
                _blnUpDownKey = value;
            }
        }
        
#endregion

        #region Constructor
        public DataGridView()
        {
         //   ApplyMySetting();
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToResizeColumns = true;
            this.CellEnter += new DataGridViewCellEventHandler(this.Datagrid_CellEnter);
        }
        
        #endregion

        #region Event (OnCreateControl)
        /// <summary>
        /// This event will be fired when user datagridview control will be created in design and runtime
        /// mode. It will check whether it has to apply settings or not.
        /// </summary>
        protected override void OnCreateControl()
        {
            //Call base event
            base.OnCreateControl();  

            //Run this only in execution mode
            if (!this.DesignMode && blnApplyMySettings)
            {
                ApplyMySetting();
            }
        }
        #endregion

        #region Event (OnLeave)
        ///// <summary>
        ///// To fire the Endedit event if cell is in edit mode
        ///// </summary>
        ///// <param name="sender">object</param>
        ///// <param name="e"> EventArgs</param>
        ///// <Author>Deepmala</Author>
        ///// <Created Date>20/02/2007</Created Date> 
        ///// <Revision History>
        ///// <Date></Date><Author></Author><Comments></Comments>		
        ///// </Revision History>
        //protected override void OnLeave(EventArgs e)
        //{
        //    if (this.IsCurrentCellInEditMode)
        //        this.EndEdit();

        //    base.OnLeave(e);
        //}
        #endregion

        #region Apply all standard setting for our grid view control in runtime
        /// <summary>
        /// This function will be called by the 'OnCreateControl' function. It will set all required properties
        /// for the DataGridView control.
        /// </summary>
        public void ApplyMySetting()
        {
            #region General Settings
            this.BackgroundColor = clsGeneralInfo.dtgrdvwBackColorCode;
            this.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.MultiSelect = false;
            this.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.RowTemplate.Height = 22;
            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.RowHeadersWidth = 100;
            //this.CausesValidation  = false;

            #endregion

            #region Cell Style

            #region Default Cell Style

            this.DefaultCellStyle = GetCellStyle();
          //  this.AlternatingRowsDefaultCellStyle = this.DefaultCellStyle;
           
            this.AlternatingRowsDefaultCellStyle.BackColor = clsGeneralInfo.dtgrdvwAltColorCode;
            #endregion

            #region Row Header Def Cell Style
            this.RowHeadersDefaultCellStyle = this.DefaultCellStyle;           
            this.RowHeadersDefaultCellStyle.BackColor = clsGeneralInfo.dtgrdvwRowHeaderBackColorCode;
            #endregion

            #region Column Header Default Cell Style
            this.ColumnHeadersDefaultCellStyle = GetColHeaderStyle();
            this.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            #endregion
            #endregion

            //Apply our column settings
            ApplyColumnSettings();

            if (blnEditGrid == true)
            {
                foreach (DataGridViewColumn dgvcItem in this.Columns)
                {
                    dgvcItem.SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
            else
            {
                foreach (DataGridViewColumn dgvcItem in this.Columns)
                {
                    dgvcItem.SortMode = DataGridViewColumnSortMode.Automatic;
                }
            }

        }
        #endregion

        #region Event (OnCellEnter)
        /// <summary>
        /// Set Info Message on cell enter
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e"> DataGridViewCellEventArgs</param>
        /// <Author>Deepmala</Author>
        /// <Created Date>15/1/2007</Created Date> 
        /// <Revision History>
        /// <Date></Date><Author></Author><Comments></Comments>		
        /// </Revision History>
        protected void Datagrid_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
         //    Dim dgv As DataGridView = DirectCast(sender, DataGridView)

        // Check for disabled. If so the force move to next control

            /*
            if(this.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly)
                SendKeys.Send("{TAB}");
         */
            //this.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected = true;

            int intColumnCount = this.ColumnCount;
            int intLastVisibleColumnIndex = 0;

            for (int intColIndex = 0; intColIndex < intColumnCount; intColIndex++)
            {
                if (this.CurrentRow.Cells[intColIndex].Visible)
                {
                    intLastVisibleColumnIndex = intColIndex;
                }
            }

            if (this.CurrentCell.ColumnIndex == intLastVisibleColumnIndex)
                this.FirstDisplayedScrollingColumnIndex = this.CurrentCell.ColumnIndex;

            if (_ctlInfoMessage == null)
                return;
            if (_strInfoMsgArr[e.ColumnIndex] != null)
                _ctlInfoMessage.InfoMessage = _strInfoMsgArr[e.ColumnIndex];
            else
                _ctlInfoMessage.InfoMessage = string.Empty;
         
        }
        #endregion

        #region Apply settings to the Column in the grid
        /// <summary>
        /// This function will be called to apply alignment settings to the GridView Columns.
        /// It will iterate througn columns and will make them proper alignment according to header.
        /// </summary>
        public void ApplyColumnSettings()
        {
            #region Assign our style to all columns
            foreach (DataGridViewColumn dgvcCurrColumn in this.Columns)
            {
                //Set Header style for each column
                dgvcCurrColumn.HeaderCell.Style = GetColHeaderStyle();

                //if proper alignment is not there then make it middleleft
                if (dgvcCurrColumn.DefaultCellStyle.Alignment == DataGridViewContentAlignment.NotSet)
                    dgvcCurrColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                //Set alignment for each column
                dgvcCurrColumn.HeaderCell.Style.Alignment = dgvcCurrColumn.DefaultCellStyle.Alignment;
            }
            #endregion
        }
        #endregion

        #region Get default column header style for all columns
        /// <summary>
        /// This function will set properties for Columns of DataGridViewControls
        /// </summary>
        /// <returns>
        /// DataGridView Cell style which is settled with all properties
        /// </returns>
        DataGridViewCellStyle GetColHeaderStyle()
        {
            #region Columns Header Cell Style

            dgvcsAllColumnStyle = new DataGridViewCellStyle();
         
            dgvcsAllColumnStyle.Font = clsGeneralInfo.dgvColHeaderFont;
            dgvcsAllColumnStyle.BackColor = clsGeneralInfo.dtgrdvwRowHeaderBackColorCode; 
            dgvcsAllColumnStyle.SelectionBackColor = BackColor;
            dgvcsAllColumnStyle.ForeColor = clsGeneralInfo.dtgrdvwRowHeaderForeColorCode;
            dgvcsAllColumnStyle.SelectionForeColor = clsGeneralInfo.dtgrdvwSelCellForeColorCode; 

            dgvcsAllColumnStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            #endregion

            return dgvcsAllColumnStyle;
        }
        #endregion

        #region Get default Cell style
        /// <summary>
        /// This function will set properties for Cell of DataGridViewControls
        /// </summary>
        /// <returns>
        /// DataGridView Cell style which is settled with all properties
        /// </returns>
        DataGridViewCellStyle GetCellStyle()
        {
            #region  Cell Style

            DataGridViewCellStyle dgvcsCellStyle = new DataGridViewCellStyle();
                       
            dgvcsCellStyle.BackColor = clsGeneralInfo.dtgrdvwBackColorCode;
            
            dgvcsCellStyle.Font = clsGeneralInfo.dgvCellFont;
            dgvcsCellStyle.ForeColor = clsGeneralInfo.btnFontColorCode;
            dgvcsCellStyle.SelectionBackColor = clsGeneralInfo.dtgrdvwSelCellBackColorCode;

            #endregion

            return dgvcsCellStyle;
        }
        #endregion
       
        #region Handle Column width change event
        /// <summary>
        /// End edit mode whenever column width changed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnColumnWidthChanged(DataGridViewColumnEventArgs e)
        {
            base.OnColumnWidthChanged(e);
         //   this.EndEdit();
        }
        #endregion

        #region Handle Enter key
        [System.Security.Permissions.UIPermission(
        System.Security.Permissions.SecurityAction.LinkDemand,
        Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        protected override bool ProcessDialogKey(Keys keyData)
        {
            // Extract the key code from the key value. 
            Keys key = (keyData & Keys.KeyCode);

            switch (key)
            {
                //Handle esacpe Key when cell is in edit mode
                case Keys.Escape:
                    if (this.IsCurrentCellInEditMode)
                        return true;
                    break;
                // Handle the ENTER key as if it were a RIGHT ARROW key. 
                case Keys.Enter:
                    if (this.IsCurrentCellInEditMode)
                    {
                        try
                        {
                            this.EndEdit();
                        }
                        catch
                        {
                        }
                    }
                    return true;
            }
            
            return base.ProcessDialogKey(keyData);
        }
        #endregion

        #region Set focus on next cell
        /// <summary>
        /// set focus on next cell
        /// </summary>
        public void SetFocusOnNextCell()
        {
            int intXindex, intYindex;
            int intRowCount = this.RowCount;
            int intColumnCount = this.ColumnCount;
            int intLastVisibleRowIndex=0, intLastVisibleColumnIndex=0;
            bool blnFocusSet = false;

            intXindex = this.CurrentCell.RowIndex;
            intYindex = this.CurrentCell.ColumnIndex;

            intLastVisibleRowIndex = intXindex;
            intLastVisibleColumnIndex = intYindex;

            if (this.SelectedCells.Count > 0)
                if (intYindex != this.SelectedCells[0].ColumnIndex || intXindex != this.SelectedCells[0].RowIndex)
                    return;

            int intColStartIndex = intYindex + 1;

            for (int intRowIndex = intXindex; intRowIndex < intRowCount; intRowIndex++)
            {
                for (int intColIndex = intColStartIndex; intColIndex < intColumnCount; intColIndex++)
                {
                    if(this.Rows[intRowIndex].Cells[intColIndex].Visible)
                    {
                        intLastVisibleRowIndex =intRowIndex;
                        intLastVisibleColumnIndex =intColIndex;
                        if (!this.Rows[intRowIndex].Cells[intColIndex].ReadOnly)
                        {
                            this.Rows[intRowIndex].Cells[intColIndex].Selected = true;
                            blnFocusSet = true;
                            return;
                        }
                    }
                }
                intColStartIndex = 0;
            }

            if (!blnFocusSet)
            {
                this.Rows[intLastVisibleRowIndex].Cells[intLastVisibleColumnIndex].Selected = true;
                //SendKeys.Send("{Tab}");
            }
        }
        #endregion

        #region Handle up/down key for grid combobox
        /// <summary>
        /// Handle up/down and enter key for combo box
        /// </summary>
        [System.Security.Permissions.SecurityPermission(
            System.Security.Permissions.SecurityAction.LinkDemand, Flags =
            System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)]
        protected override bool ProcessDataGridViewKey(KeyEventArgs e)
        {

            switch (e.KeyCode)
            {
                // Handle the ENTER key as if it were a RIGHT ARROW key.             
                case Keys.Enter:
                    if (_blnUpDownKey)
                        return false;
                    else
                        return true;                    
                case Keys.Down:
                    if (_blnUpDownKey)
                        return false;
                    break;
                case Keys.Up:
                    if (_blnUpDownKey)
                        return false;
                    break;

            }

            return base.ProcessDataGridViewKey(e);
        }
        #endregion

    }
}
