﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace VWPriceMatrix
{
    public partial class frmOtherPriceMatrixView : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmOtherPriceMatrixView()
        {
            InitializeComponent();
        }

        private void frmDEPriceMatrix_Load(object sender, EventArgs e)
        {
            Get1();
            Get2();
            Get3();
            Get4();
            Get5();
            Get6();
        }

        private void Get1()
        {
            string strQuery = " SELECT * FROM Other_Price_Matrix WHERE Id = '1' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgv1.AutoGenerateColumns = false;
            dgv1.DataSource = dtRegion;

            strQuery = " SELECT C.CustomerName + ' - ' + City AS CUST_DATA FROM Other_Price_Matrix O INNER JOIN Customers C ON O.CustomerId = C.Id WHERE O.Id = '1' AND O.isdeleted = 0 AND C.IsDeleted = 0";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);

            foreach (DataRow item in dtData.Rows)
            {
                hlbl1.Text = "(1) " + Support.ConvertToString(item["CUST_DATA"]);
                break;
            }
        }

        private void Get2()
        {
            string strQuery = " SELECT * FROM Other_Price_Matrix WHERE Id = '2' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgv2.AutoGenerateColumns = false;
            dgv2.DataSource = dtRegion;

            strQuery = " SELECT C.CustomerName + ' - ' + City AS CUST_DATA FROM Other_Price_Matrix O INNER JOIN Customers C ON O.CustomerId = C.Id WHERE O.Id = '2' AND O.isdeleted = 0 AND C.IsDeleted = 0";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);

            foreach (DataRow item in dtData.Rows)
            {
                hlbl2.Text = "(2) " + Support.ConvertToString(item["CUST_DATA"]);
                break;
            }
        }

        private void Get3()
        {
            string strQuery = " SELECT * FROM Other_Price_Matrix WHERE Id = '3' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgv3.AutoGenerateColumns = false;
            dgv3.DataSource = dtRegion;

            strQuery = " SELECT C.CustomerName + ' - ' + City AS CUST_DATA FROM Other_Price_Matrix O INNER JOIN Customers C ON O.CustomerId = C.Id WHERE O.Id = '3' AND O.isdeleted = 0 AND C.IsDeleted = 0";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);

            foreach (DataRow item in dtData.Rows)
            {
                hlbl3.Text = "(3) " + Support.ConvertToString(item["CUST_DATA"]);
                break;
            }
        }

        private void Get4()
        {
            string strQuery = " SELECT * FROM Other_Price_Matrix WHERE Id = '4' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgv4.AutoGenerateColumns = false;
            dgv4.DataSource = dtRegion;

            strQuery = " SELECT C.CustomerName + ' - ' + City AS CUST_DATA FROM Other_Price_Matrix O INNER JOIN Customers C ON O.CustomerId = C.Id WHERE O.Id = '4' AND O.isdeleted = 0 AND C.IsDeleted = 0";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);

            foreach (DataRow item in dtData.Rows)
            {
                hlbl4.Text = "(4) " + Support.ConvertToString(item["CUST_DATA"]);
                break;
            }
        }

        private void Get5()
        {
            string strQuery = " SELECT * FROM Other_Price_Matrix WHERE Id = '5' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgv5.AutoGenerateColumns = false;
            dgv5.DataSource = dtRegion;

            strQuery = " SELECT C.CustomerName + ' - ' + City AS CUST_DATA FROM Other_Price_Matrix O INNER JOIN Customers C ON O.CustomerId = C.Id WHERE O.Id = '5' AND O.isdeleted = 0 AND C.IsDeleted = 0";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);

            foreach (DataRow item in dtData.Rows)
            {
                hlbl5.Text = "(5) " + Support.ConvertToString(item["CUST_DATA"]);
                break;
            }
        }

        private void Get6()
        {
            string strQuery = " SELECT * FROM Other_Price_Matrix WHERE Id = '6' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgv6.AutoGenerateColumns = false;
            dgv6.DataSource = dtRegion;

            strQuery = " SELECT C.CustomerName + ' - ' + City AS CUST_DATA FROM Other_Price_Matrix O INNER JOIN Customers C ON O.CustomerId = C.Id WHERE O.Id = '6' AND O.isdeleted = 0 AND C.IsDeleted = 0";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);

            foreach (DataRow item in dtData.Rows)
            {
                hlbl6.Text = "(6) " + Support.ConvertToString(item["CUST_DATA"]);
                break;
            }
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.DisplayAlerts = false;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            //==(1)============================================================
            int intCol = 2;
            int intRow = 2;

            xlWorkSheet.Range["B1:O1"].Merge();
            xlWorkSheet.Cells[1, 2] = hlbl1.Text.Replace("(1)", "");
            xlWorkSheet.Range["B1:O1"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B1:O1"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B1:O1"].Cells.Font.Size = 14;


            foreach (DataGridViewColumn item in dgv1.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 3;

            foreach (DataGridViewRow Row in dgv1.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgv1.Columns.Count; i++)
                {
                    if (dgv1.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            Excel.Range range = xlWorkSheet.Range["B1:O3"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //===(2)===========================================================

            intCol = 2;
            intRow = 6;

            xlWorkSheet.Range["B5:O5"].Merge();
            xlWorkSheet.Cells[5, 2] = hlbl2.Text.Replace("(2)", "");
            xlWorkSheet.Range["B5:O5"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B5:O5"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B5:O5"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgv2.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 7;

            foreach (DataGridViewRow Row in dgv2.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgv2.Columns.Count; i++)
                {
                    if (dgv2.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B5:O7"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //====(3)==========================================================

            intCol = 2;
            intRow = 10;

            xlWorkSheet.Range["B9:O9"].Merge();
            xlWorkSheet.Cells[9, 2] = hlbl3.Text.Replace("(3)", "");
            xlWorkSheet.Range["B9:O9"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B9:O9"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B9:O9"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgv3.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 11;

            foreach (DataGridViewRow Row in dgv3.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgv3.Columns.Count; i++)
                {
                    if (dgv3.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B9:O11"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //====(4)==========================================================

            intCol = 2;
            intRow = 14;

            xlWorkSheet.Range["B13:O13"].Merge();
            xlWorkSheet.Cells[13, 2] = hlbl4.Text.Replace("(4)", "");
            xlWorkSheet.Range["B13:O13"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B13:O13"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B13:O13"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgv4.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 15;

            foreach (DataGridViewRow Row in dgv4.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgv4.Columns.Count; i++)
                {
                    if (dgv4.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B13:O15"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //====(5)==========================================================

            intCol = 2;
            intRow = 18;

            xlWorkSheet.Range["B17:O17"].Merge();
            xlWorkSheet.Cells[17, 2] = hlbl5.Text.Replace("(5)", "");
            xlWorkSheet.Range["B17:O17"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B17:O17"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B17:O17"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgv5.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 19;

            foreach (DataGridViewRow Row in dgv5.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgv5.Columns.Count; i++)
                {
                    if (dgv5.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B17:O19"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //====(6)==========================================================

            intCol = 2;
            intRow = 22;

            xlWorkSheet.Range["B21:O21"].Merge();
            xlWorkSheet.Cells[21, 2] = hlbl6.Text.Replace("(6)", "");
            xlWorkSheet.Range["B21:O21"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B21:O21"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B21:O21"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgv6.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 23;

            foreach (DataGridViewRow Row in dgv6.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgv6.Columns.Count; i++)
                {
                    if (dgv6.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B21:O23"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick; 
            range.Borders.Color = Color.Black;

            //==============================================================

            if (!Directory.Exists(System.Windows.Forms.Application.StartupPath + "\\Excel\\"))
            {
                Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + "\\Excel\\");
            }

            string strFilename = System.Windows.Forms.Application.StartupPath + "\\Excel\\" + "DE_PRICE_MATRIX_" + DateTime.Now.Ticks + ".xls";

            Excel.Range er = xlWorkSheet.get_Range("B:O", System.Type.Missing);
            er.EntireColumn.ColumnWidth = 15.71;

            Excel.Range r1 = xlWorkSheet.get_Range("B2:O2", System.Type.Missing);
            r1.WrapText = true;
            r1.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r2 = xlWorkSheet.get_Range("B6:O6", System.Type.Missing);
            r2.WrapText = true;
            r2.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r3 = xlWorkSheet.get_Range("B10:O10", System.Type.Missing);
            r3.WrapText = true;
            r3.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r4 = xlWorkSheet.get_Range("B14:O14", System.Type.Missing);
            r4.WrapText = true;
            r4.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r5 = xlWorkSheet.get_Range("B18:O18", System.Type.Missing);
            r5.WrapText = true;
            r5.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r6 = xlWorkSheet.get_Range("B22:O22", System.Type.Missing);
            r6.WrapText = true;
            r6.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            //xlWorkSheet.Rows.AutoFit();
            //xlWorkSheet.Columns.AutoFit();

            xlWorkBook.SaveAs(strFilename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            Process.Start(strFilename);
        }
    }
}
