﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VWPriceMatrix
{
    public static class Support
    {
        public static bool IsAuthenticated = false;
        public static string UserId;
        public static string Role = string.Empty;

        #region Methods

        public static string EncryptPassword(string txtPassword)
        {
            byte[] passBytes = System.Text.Encoding.Unicode.GetBytes(txtPassword);
            string encryptPassword = Convert.ToBase64String(passBytes);
            return encryptPassword;
        }

        public static string DecryptPassword(string encryptedPassword)
        {
            byte[] passByteData = Convert.FromBase64String(encryptedPassword);
            string originalPassword = System.Text.Encoding.Unicode.GetString(passByteData);
            return originalPassword;
        }

        public static int ConvertToInt(object objValue)
        {
            if (objValue == null)
                return 0;
            else if (Convert.ToString(objValue).Length == 0)
                return 0;
            else
                return Convert.ToInt32(objValue);
        }

        public static string ConvertToString(object objValue)
        {
            if (objValue == null)
                return string.Empty;
            else if (Convert.ToString(objValue).Length == 0)
                return string.Empty;
            else
                return Convert.ToString(objValue);
        }

        public static Decimal ConvertToDecimal(object objValue)
        {
            if (objValue == null)
                return 0;
            else if (Convert.ToString(objValue).Length == 0)
                return 0;
            else
                return Convert.ToDecimal(objValue);
        }

        public static double ConvertToDouble(object objValue)
        {
            if (objValue == null)
                return 0;
            else if (Convert.ToString(objValue).Length == 0)
                return 0;
            else
                return Convert.ToDouble(objValue);
        }

        #endregion
    }
}
