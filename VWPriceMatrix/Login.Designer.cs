﻿namespace VWPriceMatrix
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new General.Controls.Panel();
            this.btnClose = new General.Controls.Button();
            this.btnLogin = new General.Controls.Button();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.txtPassword = new General.Controls.TextBox();
            this.txtUserName = new General.Controls.TextBox();
            this.label2 = new General.Controls.Label();
            this.label1 = new General.Controls.Label();
            this.panel1.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnLogin);
            this.panel1.Controls.Add(this.panelHeader1);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.txtUserName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(291, 151);
            this.panel1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.ApplyMySettings = true;
            this.btnClose.CtrlProperty = "&Close";
            this.btnClose.Location = new System.Drawing.Point(189, 112);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.ApplyMySettings = true;
            this.btnLogin.CtrlProperty = "Login";
            this.btnLogin.Location = new System.Drawing.Point(108, 112);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel1);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Location = new System.Drawing.Point(1, 12);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(290, 23);
            this.panelHeader1.TabIndex = 5;
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "Login";
            this.headerLabel1.Location = new System.Drawing.Point(13, 4);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(33, 13);
            this.headerLabel1.TabIndex = 0;
            this.headerLabel1.Text = "Login";
            // 
            // txtPassword
            // 
            this.txtPassword.AllowAllCharacters = true;
            this.txtPassword.AllowCurrency = false;
            this.txtPassword.AllowSpace = true;
            this.txtPassword.ApplyMySettings = true;
            this.txtPassword.ConvertEnterToTab = false;
            this.txtPassword.ctlInfoMessage = null;
            this.txtPassword.CtrlProperty = "";
            this.txtPassword.InfoMessage = "";
            this.txtPassword.Location = new System.Drawing.Point(108, 83);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(156, 20);
            this.txtPassword.TabIndex = 4;
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.Verify = true;
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // txtUserName
            // 
            this.txtUserName.AllowAllCharacters = true;
            this.txtUserName.AllowCurrency = false;
            this.txtUserName.AllowSpace = true;
            this.txtUserName.ApplyMySettings = true;
            this.txtUserName.ConvertEnterToTab = false;
            this.txtUserName.ctlInfoMessage = null;
            this.txtUserName.CtrlProperty = "";
            this.txtUserName.InfoMessage = "";
            this.txtUserName.Location = new System.Drawing.Point(108, 57);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(156, 20);
            this.txtUserName.TabIndex = 3;
            this.txtUserName.Verify = true;
            this.txtUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // label2
            // 
            this.label2.ApplyMySettings = true;
            this.label2.CtrlProperty = "Password";
            this.label2.Location = new System.Drawing.Point(31, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.ApplyMySettings = true;
            this.label1.CtrlProperty = "User Name";
            this.label1.Location = new System.Drawing.Point(31, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "User Name";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 151);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private General.Controls.TextBox txtUserName;
        private General.Controls.Label label2;
        private General.Controls.Label label1;
        private General.Controls.TextBox txtPassword;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel1;
        private General.Controls.Button btnClose;
        private General.Controls.Button btnLogin;
    }
}