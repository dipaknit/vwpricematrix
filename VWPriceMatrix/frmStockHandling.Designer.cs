﻿namespace VWPriceMatrix
{
    partial class frmStockHandling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStockHandling));
            this.panel1 = new General.Controls.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel7 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvWC_Bibby = new General.Controls.DataGridView();
            this.BIBBY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_MT_CONTAINER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_QTY_24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_QTY_24_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvWC_Bibby_Curr = new General.Controls.DataGridView();
            this.BIBBY_CURR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_EUR_MT_25KG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BB_EUR_MT_BB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvWC_Mueller_Curr = new General.Controls.DataGridView();
            this.MUELLER_CURR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_EUR_MT_25KG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_EUR_MT_BB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvWC_Mueller = new General.Controls.DataGridView();
            this.MUELLER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MUELLER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_MT_CONTAINER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_QTY_24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MU_QTY_24_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvWC_MOLENBERGNATIE_CURR = new General.Controls.DataGridView();
            this.MOLENBERGNATIE_CURR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EUR_MT_25KG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EUR_MT_BB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvWC_MOLENBERGNATIE = new General.Controls.DataGridView();
            this.MOLENBERGNATIE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOLENBERGNATIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MT_CONTAINER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_24_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new General.Controls.Panel();
            this.btnWC_MOLENBERGNATIE_Calculate = new General.Controls.Button();
            this.btnWC_MOLENBERGNATIE_Save = new General.Controls.Button();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.panel2 = new General.Controls.Panel();
            this.btnWC_MOLENBERGNATIE_Curr_Calculate = new General.Controls.Button();
            this.btnWC_MOLENBERGNATIE_Curr_Save = new General.Controls.Button();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new General.Controls.Panel();
            this.btnWC_Mueller_Calculate = new General.Controls.Button();
            this.btnWC_Mueller_Save = new General.Controls.Button();
            this.headerLabel2 = new General.Controls.HeaderLabel();
            this.panel5 = new General.Controls.Panel();
            this.btnWC_Mueller_Curr_Calculate = new General.Controls.Button();
            this.btnWC_Mueller_Curr_Save = new General.Controls.Button();
            this.headerLabel4 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new General.Controls.Panel();
            this.btnWC_Bibby_Calculate = new General.Controls.Button();
            this.btnWC_Bibby_Save = new General.Controls.Button();
            this.headerLabel5 = new General.Controls.HeaderLabel();
            this.panel7 = new General.Controls.Panel();
            this.btnWC_Bibby_Curr_Calculate = new General.Controls.Button();
            this.btnWC_Bibby_Curr_Save = new General.Controls.Button();
            this.headerLabel6 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new General.Controls.Panel();
            this.btnWC_Aarhus_Calculate = new General.Controls.Button();
            this.btnWC_Aarhus_Save = new General.Controls.Button();
            this.headerLabel8 = new General.Controls.HeaderLabel();
            this.panel9 = new General.Controls.Panel();
            this.btnWC_Aarhus_Curr_Calculate = new General.Controls.Button();
            this.btnWC_Aarhus_Curr_Save = new General.Controls.Button();
            this.headerLabel9 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvWC_Aarhus = new General.Controls.DataGridView();
            this.dgvWC_Aarhus_Curr = new General.Controls.DataGridView();
            this.AARHUS_CURR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_EUR_MT_25KG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_EUR_MT_BB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AARHUS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AARHUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_MT_CONTAINER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_QTY_24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AA_QTY_24_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Bibby)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Bibby_Curr)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Mueller_Curr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Mueller)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_MOLENBERGNATIE_CURR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_MOLENBERGNATIE)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Aarhus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Aarhus_Curr)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel1.Size = new System.Drawing.Size(1008, 730);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1005, 730);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel7);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(999, 24);
            this.panelHeader1.TabIndex = 141;
            // 
            // headerLabel7
            // 
            this.headerLabel7.ApplyMySettings = true;
            this.headerLabel7.AutoSize = true;
            this.headerLabel7.CtrlProperty = "Stock Handling";
            this.headerLabel7.Location = new System.Drawing.Point(8, 6);
            this.headerLabel7.Name = "headerLabel7";
            this.headerLabel7.Size = new System.Drawing.Size(80, 13);
            this.headerLabel7.TabIndex = 0;
            this.headerLabel7.Text = "Stock Handling";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.dgvWC_Bibby, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.dgvWC_Bibby_Curr, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 408);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1005, 147);
            this.tableLayoutPanel7.TabIndex = 137;
            // 
            // dgvWC_Bibby
            // 
            this.dgvWC_Bibby.AllowUpDownKey = false;
            this.dgvWC_Bibby.AllowUserToAddRows = false;
            this.dgvWC_Bibby.AllowUserToDeleteRows = false;
            this.dgvWC_Bibby.AllowUserToResizeRows = false;
            this.dgvWC_Bibby.ApplyMySettings = false;
            this.dgvWC_Bibby.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_Bibby.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BIBBY_ID,
            this.BIBBY,
            this.BB_MT_CONTAINER,
            this.BB_QTY_24,
            this.BB_QTY_24_7});
            this.dgvWC_Bibby.ctlInfoMessage = null;
            this.dgvWC_Bibby.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_Bibby.Location = new System.Drawing.Point(505, 3);
            this.dgvWC_Bibby.MyAutoGenerateColumn = false;
            this.dgvWC_Bibby.MyEditGrid = true;
            this.dgvWC_Bibby.Name = "dgvWC_Bibby";
            this.dgvWC_Bibby.RowHeadersVisible = false;
            this.dgvWC_Bibby.Size = new System.Drawing.Size(497, 141);
            this.dgvWC_Bibby.strInfoMsgArr = null;
            this.dgvWC_Bibby.TabIndex = 3;
            // 
            // BIBBY_ID
            // 
            this.BIBBY_ID.DataPropertyName = "BIBBY_ID";
            this.BIBBY_ID.HeaderText = "ID";
            this.BIBBY_ID.Name = "BIBBY_ID";
            this.BIBBY_ID.ReadOnly = true;
            this.BIBBY_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BIBBY_ID.Visible = false;
            // 
            // BIBBY
            // 
            this.BIBBY.DataPropertyName = "BIBBY";
            this.BIBBY.HeaderText = "Bibby";
            this.BIBBY.Name = "BIBBY";
            this.BIBBY.ReadOnly = true;
            this.BIBBY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BIBBY.Width = 350;
            // 
            // BB_MT_CONTAINER
            // 
            this.BB_MT_CONTAINER.DataPropertyName = "MT_CONTAINER";
            this.BB_MT_CONTAINER.HeaderText = "Per container(MT/Container)";
            this.BB_MT_CONTAINER.Name = "BB_MT_CONTAINER";
            this.BB_MT_CONTAINER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BB_MT_CONTAINER.Width = 200;
            // 
            // BB_QTY_24
            // 
            this.BB_QTY_24.DataPropertyName = "QTY_24";
            this.BB_QTY_24.HeaderText = "24";
            this.BB_QTY_24.Name = "BB_QTY_24";
            this.BB_QTY_24.ReadOnly = true;
            this.BB_QTY_24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BB_QTY_24_7
            // 
            this.BB_QTY_24_7.DataPropertyName = "QTY_24_7";
            this.BB_QTY_24_7.HeaderText = "24.7";
            this.BB_QTY_24_7.Name = "BB_QTY_24_7";
            this.BB_QTY_24_7.ReadOnly = true;
            this.BB_QTY_24_7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvWC_Bibby_Curr
            // 
            this.dgvWC_Bibby_Curr.AllowUpDownKey = false;
            this.dgvWC_Bibby_Curr.AllowUserToAddRows = false;
            this.dgvWC_Bibby_Curr.AllowUserToDeleteRows = false;
            this.dgvWC_Bibby_Curr.AllowUserToResizeRows = false;
            this.dgvWC_Bibby_Curr.ApplyMySettings = false;
            this.dgvWC_Bibby_Curr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_Bibby_Curr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BIBBY_CURR_ID,
            this.BB_Type,
            this.BB_EUR,
            this.BB_EUR_MT_25KG,
            this.BB_EUR_MT_BB});
            this.dgvWC_Bibby_Curr.ctlInfoMessage = null;
            this.dgvWC_Bibby_Curr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_Bibby_Curr.Location = new System.Drawing.Point(3, 3);
            this.dgvWC_Bibby_Curr.MyAutoGenerateColumn = false;
            this.dgvWC_Bibby_Curr.MyEditGrid = true;
            this.dgvWC_Bibby_Curr.Name = "dgvWC_Bibby_Curr";
            this.dgvWC_Bibby_Curr.RowHeadersVisible = false;
            this.dgvWC_Bibby_Curr.Size = new System.Drawing.Size(496, 141);
            this.dgvWC_Bibby_Curr.strInfoMsgArr = null;
            this.dgvWC_Bibby_Curr.TabIndex = 2;
            // 
            // BIBBY_CURR_ID
            // 
            this.BIBBY_CURR_ID.DataPropertyName = "BIBBY_CURR_ID";
            this.BIBBY_CURR_ID.HeaderText = "ID";
            this.BIBBY_CURR_ID.Name = "BIBBY_CURR_ID";
            this.BIBBY_CURR_ID.ReadOnly = true;
            this.BIBBY_CURR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BIBBY_CURR_ID.Visible = false;
            // 
            // BB_Type
            // 
            this.BB_Type.DataPropertyName = "Type";
            this.BB_Type.HeaderText = "Type";
            this.BB_Type.Name = "BB_Type";
            this.BB_Type.ReadOnly = true;
            this.BB_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BB_Type.Width = 300;
            // 
            // BB_EUR
            // 
            this.BB_EUR.DataPropertyName = "EUR";
            this.BB_EUR.HeaderText = "EUR";
            this.BB_EUR.Name = "BB_EUR";
            this.BB_EUR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BB_EUR.Width = 150;
            // 
            // BB_EUR_MT_25KG
            // 
            this.BB_EUR_MT_25KG.DataPropertyName = "EUR_MT_25KG";
            this.BB_EUR_MT_25KG.HeaderText = "EUR/MT (25KG)";
            this.BB_EUR_MT_25KG.Name = "BB_EUR_MT_25KG";
            this.BB_EUR_MT_25KG.ReadOnly = true;
            this.BB_EUR_MT_25KG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BB_EUR_MT_25KG.Width = 150;
            // 
            // BB_EUR_MT_BB
            // 
            this.BB_EUR_MT_BB.DataPropertyName = "EUR_MT_BB";
            this.BB_EUR_MT_BB.HeaderText = "EUR/MT (BB)";
            this.BB_EUR_MT_BB.Name = "BB_EUR_MT_BB";
            this.BB_EUR_MT_BB.ReadOnly = true;
            this.BB_EUR_MT_BB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BB_EUR_MT_BB.Width = 150;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.dgvWC_Mueller_Curr, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.dgvWC_Mueller, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 233);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1005, 147);
            this.tableLayoutPanel3.TabIndex = 136;
            // 
            // dgvWC_Mueller_Curr
            // 
            this.dgvWC_Mueller_Curr.AllowUpDownKey = false;
            this.dgvWC_Mueller_Curr.AllowUserToAddRows = false;
            this.dgvWC_Mueller_Curr.AllowUserToDeleteRows = false;
            this.dgvWC_Mueller_Curr.AllowUserToResizeRows = false;
            this.dgvWC_Mueller_Curr.ApplyMySettings = false;
            this.dgvWC_Mueller_Curr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_Mueller_Curr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MUELLER_CURR_ID,
            this.MU_TYPE,
            this.MU_EUR,
            this.MU_EUR_MT_25KG,
            this.MU_EUR_MT_BB});
            this.dgvWC_Mueller_Curr.ctlInfoMessage = null;
            this.dgvWC_Mueller_Curr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_Mueller_Curr.Location = new System.Drawing.Point(3, 3);
            this.dgvWC_Mueller_Curr.MyAutoGenerateColumn = false;
            this.dgvWC_Mueller_Curr.MyEditGrid = true;
            this.dgvWC_Mueller_Curr.Name = "dgvWC_Mueller_Curr";
            this.dgvWC_Mueller_Curr.RowHeadersVisible = false;
            this.dgvWC_Mueller_Curr.Size = new System.Drawing.Size(496, 141);
            this.dgvWC_Mueller_Curr.strInfoMsgArr = null;
            this.dgvWC_Mueller_Curr.TabIndex = 1;
            // 
            // MUELLER_CURR_ID
            // 
            this.MUELLER_CURR_ID.DataPropertyName = "MUELLER_CURR_ID";
            this.MUELLER_CURR_ID.HeaderText = "ID";
            this.MUELLER_CURR_ID.Name = "MUELLER_CURR_ID";
            this.MUELLER_CURR_ID.ReadOnly = true;
            this.MUELLER_CURR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MUELLER_CURR_ID.Visible = false;
            // 
            // MU_TYPE
            // 
            this.MU_TYPE.DataPropertyName = "Type";
            this.MU_TYPE.HeaderText = "Type";
            this.MU_TYPE.Name = "MU_TYPE";
            this.MU_TYPE.ReadOnly = true;
            this.MU_TYPE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MU_TYPE.Width = 300;
            // 
            // MU_EUR
            // 
            this.MU_EUR.DataPropertyName = "EUR";
            this.MU_EUR.HeaderText = "EUR";
            this.MU_EUR.Name = "MU_EUR";
            this.MU_EUR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MU_EUR.Width = 150;
            // 
            // MU_EUR_MT_25KG
            // 
            this.MU_EUR_MT_25KG.DataPropertyName = "EUR_MT_25KG";
            this.MU_EUR_MT_25KG.HeaderText = "EUR/MT (25KG)";
            this.MU_EUR_MT_25KG.Name = "MU_EUR_MT_25KG";
            this.MU_EUR_MT_25KG.ReadOnly = true;
            this.MU_EUR_MT_25KG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MU_EUR_MT_25KG.Width = 150;
            // 
            // MU_EUR_MT_BB
            // 
            this.MU_EUR_MT_BB.DataPropertyName = "EUR_MT_BB";
            this.MU_EUR_MT_BB.HeaderText = "EUR/MT (BB)";
            this.MU_EUR_MT_BB.Name = "MU_EUR_MT_BB";
            this.MU_EUR_MT_BB.ReadOnly = true;
            this.MU_EUR_MT_BB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MU_EUR_MT_BB.Width = 150;
            // 
            // dgvWC_Mueller
            // 
            this.dgvWC_Mueller.AllowUpDownKey = false;
            this.dgvWC_Mueller.AllowUserToAddRows = false;
            this.dgvWC_Mueller.AllowUserToDeleteRows = false;
            this.dgvWC_Mueller.AllowUserToResizeRows = false;
            this.dgvWC_Mueller.ApplyMySettings = false;
            this.dgvWC_Mueller.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_Mueller.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MUELLER_ID,
            this.MUELLER,
            this.MU_MT_CONTAINER,
            this.MU_QTY_24,
            this.MU_QTY_24_7});
            this.dgvWC_Mueller.ctlInfoMessage = null;
            this.dgvWC_Mueller.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_Mueller.Location = new System.Drawing.Point(505, 3);
            this.dgvWC_Mueller.MyAutoGenerateColumn = false;
            this.dgvWC_Mueller.MyEditGrid = true;
            this.dgvWC_Mueller.Name = "dgvWC_Mueller";
            this.dgvWC_Mueller.RowHeadersVisible = false;
            this.dgvWC_Mueller.Size = new System.Drawing.Size(497, 141);
            this.dgvWC_Mueller.strInfoMsgArr = null;
            this.dgvWC_Mueller.TabIndex = 2;
            // 
            // MUELLER_ID
            // 
            this.MUELLER_ID.DataPropertyName = "MUELLER_ID";
            this.MUELLER_ID.HeaderText = "ID";
            this.MUELLER_ID.Name = "MUELLER_ID";
            this.MUELLER_ID.ReadOnly = true;
            this.MUELLER_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MUELLER_ID.Visible = false;
            // 
            // MUELLER
            // 
            this.MUELLER.DataPropertyName = "MUELLER";
            this.MUELLER.HeaderText = "MUELLER";
            this.MUELLER.Name = "MUELLER";
            this.MUELLER.ReadOnly = true;
            this.MUELLER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MUELLER.Width = 350;
            // 
            // MU_MT_CONTAINER
            // 
            this.MU_MT_CONTAINER.DataPropertyName = "MT_CONTAINER";
            this.MU_MT_CONTAINER.HeaderText = "Per container(MT/Container)";
            this.MU_MT_CONTAINER.Name = "MU_MT_CONTAINER";
            this.MU_MT_CONTAINER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MU_MT_CONTAINER.Width = 200;
            // 
            // MU_QTY_24
            // 
            this.MU_QTY_24.DataPropertyName = "QTY_24";
            this.MU_QTY_24.HeaderText = "24";
            this.MU_QTY_24.Name = "MU_QTY_24";
            this.MU_QTY_24.ReadOnly = true;
            this.MU_QTY_24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // MU_QTY_24_7
            // 
            this.MU_QTY_24_7.DataPropertyName = "QTY_24_7";
            this.MU_QTY_24_7.HeaderText = "24.7";
            this.MU_QTY_24_7.Name = "MU_QTY_24_7";
            this.MU_QTY_24_7.ReadOnly = true;
            this.MU_QTY_24_7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.dgvWC_MOLENBERGNATIE_CURR, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvWC_MOLENBERGNATIE, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 58);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1005, 147);
            this.tableLayoutPanel2.TabIndex = 132;
            // 
            // dgvWC_MOLENBERGNATIE_CURR
            // 
            this.dgvWC_MOLENBERGNATIE_CURR.AllowUpDownKey = false;
            this.dgvWC_MOLENBERGNATIE_CURR.AllowUserToAddRows = false;
            this.dgvWC_MOLENBERGNATIE_CURR.AllowUserToDeleteRows = false;
            this.dgvWC_MOLENBERGNATIE_CURR.AllowUserToResizeRows = false;
            this.dgvWC_MOLENBERGNATIE_CURR.ApplyMySettings = false;
            this.dgvWC_MOLENBERGNATIE_CURR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_MOLENBERGNATIE_CURR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MOLENBERGNATIE_CURR_ID,
            this.Type,
            this.EUR,
            this.EUR_MT_25KG,
            this.EUR_MT_BB});
            this.dgvWC_MOLENBERGNATIE_CURR.ctlInfoMessage = null;
            this.dgvWC_MOLENBERGNATIE_CURR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_MOLENBERGNATIE_CURR.Location = new System.Drawing.Point(3, 3);
            this.dgvWC_MOLENBERGNATIE_CURR.MyAutoGenerateColumn = false;
            this.dgvWC_MOLENBERGNATIE_CURR.MyEditGrid = true;
            this.dgvWC_MOLENBERGNATIE_CURR.Name = "dgvWC_MOLENBERGNATIE_CURR";
            this.dgvWC_MOLENBERGNATIE_CURR.RowHeadersVisible = false;
            this.dgvWC_MOLENBERGNATIE_CURR.Size = new System.Drawing.Size(496, 141);
            this.dgvWC_MOLENBERGNATIE_CURR.strInfoMsgArr = null;
            this.dgvWC_MOLENBERGNATIE_CURR.TabIndex = 1;
            // 
            // MOLENBERGNATIE_CURR_ID
            // 
            this.MOLENBERGNATIE_CURR_ID.DataPropertyName = "MOLENBERGNATIE_CURR_ID";
            this.MOLENBERGNATIE_CURR_ID.HeaderText = "ID";
            this.MOLENBERGNATIE_CURR_ID.Name = "MOLENBERGNATIE_CURR_ID";
            this.MOLENBERGNATIE_CURR_ID.ReadOnly = true;
            this.MOLENBERGNATIE_CURR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MOLENBERGNATIE_CURR_ID.Visible = false;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Type.Width = 300;
            // 
            // EUR
            // 
            this.EUR.DataPropertyName = "EUR";
            this.EUR.HeaderText = "EUR";
            this.EUR.Name = "EUR";
            this.EUR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EUR.Width = 150;
            // 
            // EUR_MT_25KG
            // 
            this.EUR_MT_25KG.DataPropertyName = "EUR_MT_25KG";
            this.EUR_MT_25KG.HeaderText = "EUR/MT (25KG)";
            this.EUR_MT_25KG.Name = "EUR_MT_25KG";
            this.EUR_MT_25KG.ReadOnly = true;
            this.EUR_MT_25KG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EUR_MT_25KG.Width = 150;
            // 
            // EUR_MT_BB
            // 
            this.EUR_MT_BB.DataPropertyName = "EUR_MT_BB";
            this.EUR_MT_BB.HeaderText = "EUR/MT (BB)";
            this.EUR_MT_BB.Name = "EUR_MT_BB";
            this.EUR_MT_BB.ReadOnly = true;
            this.EUR_MT_BB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EUR_MT_BB.Width = 150;
            // 
            // dgvWC_MOLENBERGNATIE
            // 
            this.dgvWC_MOLENBERGNATIE.AllowUpDownKey = false;
            this.dgvWC_MOLENBERGNATIE.AllowUserToAddRows = false;
            this.dgvWC_MOLENBERGNATIE.AllowUserToDeleteRows = false;
            this.dgvWC_MOLENBERGNATIE.AllowUserToResizeRows = false;
            this.dgvWC_MOLENBERGNATIE.ApplyMySettings = false;
            this.dgvWC_MOLENBERGNATIE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_MOLENBERGNATIE.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MOLENBERGNATIE_ID,
            this.MOLENBERGNATIE,
            this.MT_CONTAINER,
            this.QTY_24,
            this.QTY_24_7});
            this.dgvWC_MOLENBERGNATIE.ctlInfoMessage = null;
            this.dgvWC_MOLENBERGNATIE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_MOLENBERGNATIE.Location = new System.Drawing.Point(505, 3);
            this.dgvWC_MOLENBERGNATIE.MyAutoGenerateColumn = false;
            this.dgvWC_MOLENBERGNATIE.MyEditGrid = true;
            this.dgvWC_MOLENBERGNATIE.Name = "dgvWC_MOLENBERGNATIE";
            this.dgvWC_MOLENBERGNATIE.RowHeadersVisible = false;
            this.dgvWC_MOLENBERGNATIE.Size = new System.Drawing.Size(497, 141);
            this.dgvWC_MOLENBERGNATIE.strInfoMsgArr = null;
            this.dgvWC_MOLENBERGNATIE.TabIndex = 2;
            // 
            // MOLENBERGNATIE_ID
            // 
            this.MOLENBERGNATIE_ID.DataPropertyName = "MOLENBERGNATIE_ID";
            this.MOLENBERGNATIE_ID.HeaderText = "ID";
            this.MOLENBERGNATIE_ID.Name = "MOLENBERGNATIE_ID";
            this.MOLENBERGNATIE_ID.ReadOnly = true;
            this.MOLENBERGNATIE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MOLENBERGNATIE_ID.Visible = false;
            // 
            // MOLENBERGNATIE
            // 
            this.MOLENBERGNATIE.DataPropertyName = "MOLENBERGNATIE";
            this.MOLENBERGNATIE.HeaderText = "Molenbergnatie";
            this.MOLENBERGNATIE.Name = "MOLENBERGNATIE";
            this.MOLENBERGNATIE.ReadOnly = true;
            this.MOLENBERGNATIE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MOLENBERGNATIE.Width = 350;
            // 
            // MT_CONTAINER
            // 
            this.MT_CONTAINER.DataPropertyName = "MT_CONTAINER";
            this.MT_CONTAINER.HeaderText = "Per container(MT/Container)";
            this.MT_CONTAINER.Name = "MT_CONTAINER";
            this.MT_CONTAINER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MT_CONTAINER.Width = 200;
            // 
            // QTY_24
            // 
            this.QTY_24.DataPropertyName = "QTY_24";
            this.QTY_24.HeaderText = "24";
            this.QTY_24.Name = "QTY_24";
            this.QTY_24.ReadOnly = true;
            this.QTY_24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // QTY_24_7
            // 
            this.QTY_24_7.DataPropertyName = "QTY_24_7";
            this.QTY_24_7.HeaderText = "24.7";
            this.QTY_24_7.Name = "QTY_24_7";
            this.QTY_24_7.ReadOnly = true;
            this.QTY_24_7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1005, 28);
            this.tableLayoutPanel4.TabIndex = 133;
            // 
            // panel3
            // 
            this.panel3.ApplyMySettings = true;
            this.panel3.Controls.Add(this.btnWC_MOLENBERGNATIE_Calculate);
            this.panel3.Controls.Add(this.btnWC_MOLENBERGNATIE_Save);
            this.panel3.Controls.Add(this.headerLabel1);
            this.panel3.CtrlProperty = "";
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(502, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel3.Size = new System.Drawing.Size(503, 28);
            this.panel3.TabIndex = 132;
            // 
            // btnWC_MOLENBERGNATIE_Calculate
            // 
            this.btnWC_MOLENBERGNATIE_Calculate.ApplyMySettings = true;
            this.btnWC_MOLENBERGNATIE_Calculate.CtrlProperty = "Calculate";
            this.btnWC_MOLENBERGNATIE_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_MOLENBERGNATIE_Calculate.Location = new System.Drawing.Point(350, 0);
            this.btnWC_MOLENBERGNATIE_Calculate.Name = "btnWC_MOLENBERGNATIE_Calculate";
            this.btnWC_MOLENBERGNATIE_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_MOLENBERGNATIE_Calculate.TabIndex = 26;
            this.btnWC_MOLENBERGNATIE_Calculate.Text = "Calculate";
            this.btnWC_MOLENBERGNATIE_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_MOLENBERGNATIE_Calculate.Visible = false;
            this.btnWC_MOLENBERGNATIE_Calculate.Click += new System.EventHandler(this.btnWC_MOLENBERGNATIE_Calculate_Click);
            // 
            // btnWC_MOLENBERGNATIE_Save
            // 
            this.btnWC_MOLENBERGNATIE_Save.ApplyMySettings = true;
            this.btnWC_MOLENBERGNATIE_Save.CtrlProperty = "Save";
            this.btnWC_MOLENBERGNATIE_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_MOLENBERGNATIE_Save.Location = new System.Drawing.Point(425, 0);
            this.btnWC_MOLENBERGNATIE_Save.Name = "btnWC_MOLENBERGNATIE_Save";
            this.btnWC_MOLENBERGNATIE_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_MOLENBERGNATIE_Save.TabIndex = 23;
            this.btnWC_MOLENBERGNATIE_Save.Text = "Save";
            this.btnWC_MOLENBERGNATIE_Save.UseVisualStyleBackColor = true;
            this.btnWC_MOLENBERGNATIE_Save.Click += new System.EventHandler(this.btnWC_MOLENBERGNATIE_Save_Click);
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "Warehouse Costs (Molenbergnatie)";
            this.headerLabel1.Location = new System.Drawing.Point(8, 8);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(173, 13);
            this.headerLabel1.TabIndex = 1;
            this.headerLabel1.Text = "Warehouse Costs (Molenbergnatie)";
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.btnWC_MOLENBERGNATIE_Curr_Calculate);
            this.panel2.Controls.Add(this.btnWC_MOLENBERGNATIE_Curr_Save);
            this.panel2.Controls.Add(this.headerLabel3);
            this.panel2.CtrlProperty = "";
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel2.Size = new System.Drawing.Size(502, 28);
            this.panel2.TabIndex = 131;
            // 
            // btnWC_MOLENBERGNATIE_Curr_Calculate
            // 
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.ApplyMySettings = true;
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.CtrlProperty = "Calculate";
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Location = new System.Drawing.Point(349, 0);
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Name = "btnWC_MOLENBERGNATIE_Curr_Calculate";
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.TabIndex = 26;
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Text = "Calculate";
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Visible = false;
            this.btnWC_MOLENBERGNATIE_Curr_Calculate.Click += new System.EventHandler(this.btnWC_MOLENBERGNATIE_Curr_Calculate_Click);
            // 
            // btnWC_MOLENBERGNATIE_Curr_Save
            // 
            this.btnWC_MOLENBERGNATIE_Curr_Save.ApplyMySettings = true;
            this.btnWC_MOLENBERGNATIE_Curr_Save.CtrlProperty = "Save";
            this.btnWC_MOLENBERGNATIE_Curr_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_MOLENBERGNATIE_Curr_Save.Location = new System.Drawing.Point(424, 0);
            this.btnWC_MOLENBERGNATIE_Curr_Save.Name = "btnWC_MOLENBERGNATIE_Curr_Save";
            this.btnWC_MOLENBERGNATIE_Curr_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_MOLENBERGNATIE_Curr_Save.TabIndex = 23;
            this.btnWC_MOLENBERGNATIE_Curr_Save.Text = "Save";
            this.btnWC_MOLENBERGNATIE_Curr_Save.UseVisualStyleBackColor = true;
            this.btnWC_MOLENBERGNATIE_Curr_Save.Click += new System.EventHandler(this.btnWC_MOLENBERGNATIE_Curr_Save_Click);
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "Warehouse Costs (Molenbergnatie)";
            this.headerLabel3.Location = new System.Drawing.Point(8, 8);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(173, 13);
            this.headerLabel3.TabIndex = 1;
            this.headerLabel3.Text = "Warehouse Costs (Molenbergnatie)";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 205);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1005, 28);
            this.tableLayoutPanel5.TabIndex = 134;
            // 
            // panel4
            // 
            this.panel4.ApplyMySettings = true;
            this.panel4.Controls.Add(this.btnWC_Mueller_Calculate);
            this.panel4.Controls.Add(this.btnWC_Mueller_Save);
            this.panel4.Controls.Add(this.headerLabel2);
            this.panel4.CtrlProperty = "";
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(502, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel4.Size = new System.Drawing.Size(503, 28);
            this.panel4.TabIndex = 132;
            // 
            // btnWC_Mueller_Calculate
            // 
            this.btnWC_Mueller_Calculate.ApplyMySettings = true;
            this.btnWC_Mueller_Calculate.CtrlProperty = "Calculate";
            this.btnWC_Mueller_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Mueller_Calculate.Location = new System.Drawing.Point(350, 0);
            this.btnWC_Mueller_Calculate.Name = "btnWC_Mueller_Calculate";
            this.btnWC_Mueller_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Mueller_Calculate.TabIndex = 26;
            this.btnWC_Mueller_Calculate.Text = "Calculate";
            this.btnWC_Mueller_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_Mueller_Calculate.Visible = false;
            this.btnWC_Mueller_Calculate.Click += new System.EventHandler(this.btnWC_Mueller_Calculate_Click);
            // 
            // btnWC_Mueller_Save
            // 
            this.btnWC_Mueller_Save.ApplyMySettings = true;
            this.btnWC_Mueller_Save.CtrlProperty = "Save";
            this.btnWC_Mueller_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Mueller_Save.Location = new System.Drawing.Point(425, 0);
            this.btnWC_Mueller_Save.Name = "btnWC_Mueller_Save";
            this.btnWC_Mueller_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Mueller_Save.TabIndex = 23;
            this.btnWC_Mueller_Save.Text = "Save";
            this.btnWC_Mueller_Save.UseVisualStyleBackColor = true;
            this.btnWC_Mueller_Save.Click += new System.EventHandler(this.btnWC_Mueller_Save_Click);
            // 
            // headerLabel2
            // 
            this.headerLabel2.ApplyMySettings = true;
            this.headerLabel2.AutoSize = true;
            this.headerLabel2.CtrlProperty = "Warehouse Costs (Mueller)";
            this.headerLabel2.Location = new System.Drawing.Point(8, 8);
            this.headerLabel2.Name = "headerLabel2";
            this.headerLabel2.Size = new System.Drawing.Size(134, 13);
            this.headerLabel2.TabIndex = 1;
            this.headerLabel2.Text = "Warehouse Costs (Mueller)";
            // 
            // panel5
            // 
            this.panel5.ApplyMySettings = true;
            this.panel5.Controls.Add(this.btnWC_Mueller_Curr_Calculate);
            this.panel5.Controls.Add(this.btnWC_Mueller_Curr_Save);
            this.panel5.Controls.Add(this.headerLabel4);
            this.panel5.CtrlProperty = "";
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel5.Size = new System.Drawing.Size(502, 28);
            this.panel5.TabIndex = 131;
            // 
            // btnWC_Mueller_Curr_Calculate
            // 
            this.btnWC_Mueller_Curr_Calculate.ApplyMySettings = true;
            this.btnWC_Mueller_Curr_Calculate.CtrlProperty = "Calculate";
            this.btnWC_Mueller_Curr_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Mueller_Curr_Calculate.Location = new System.Drawing.Point(349, 0);
            this.btnWC_Mueller_Curr_Calculate.Name = "btnWC_Mueller_Curr_Calculate";
            this.btnWC_Mueller_Curr_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Mueller_Curr_Calculate.TabIndex = 26;
            this.btnWC_Mueller_Curr_Calculate.Text = "Calculate";
            this.btnWC_Mueller_Curr_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_Mueller_Curr_Calculate.Visible = false;
            this.btnWC_Mueller_Curr_Calculate.Click += new System.EventHandler(this.btnWC_Mueller_Curr_Calculate_Click);
            // 
            // btnWC_Mueller_Curr_Save
            // 
            this.btnWC_Mueller_Curr_Save.ApplyMySettings = true;
            this.btnWC_Mueller_Curr_Save.CtrlProperty = "Save";
            this.btnWC_Mueller_Curr_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Mueller_Curr_Save.Location = new System.Drawing.Point(424, 0);
            this.btnWC_Mueller_Curr_Save.Name = "btnWC_Mueller_Curr_Save";
            this.btnWC_Mueller_Curr_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Mueller_Curr_Save.TabIndex = 23;
            this.btnWC_Mueller_Curr_Save.Text = "Save";
            this.btnWC_Mueller_Curr_Save.UseVisualStyleBackColor = true;
            this.btnWC_Mueller_Curr_Save.Click += new System.EventHandler(this.btnWC_Mueller_Curr_Save_Click);
            // 
            // headerLabel4
            // 
            this.headerLabel4.ApplyMySettings = true;
            this.headerLabel4.AutoSize = true;
            this.headerLabel4.CtrlProperty = "Warehouse Costs (Mueller)";
            this.headerLabel4.Location = new System.Drawing.Point(8, 7);
            this.headerLabel4.Name = "headerLabel4";
            this.headerLabel4.Size = new System.Drawing.Size(134, 13);
            this.headerLabel4.TabIndex = 1;
            this.headerLabel4.Text = "Warehouse Costs (Mueller)";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.panel6, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 380);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1005, 28);
            this.tableLayoutPanel6.TabIndex = 135;
            // 
            // panel6
            // 
            this.panel6.ApplyMySettings = true;
            this.panel6.Controls.Add(this.btnWC_Bibby_Calculate);
            this.panel6.Controls.Add(this.btnWC_Bibby_Save);
            this.panel6.Controls.Add(this.headerLabel5);
            this.panel6.CtrlProperty = "";
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(502, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel6.Size = new System.Drawing.Size(503, 28);
            this.panel6.TabIndex = 132;
            // 
            // btnWC_Bibby_Calculate
            // 
            this.btnWC_Bibby_Calculate.ApplyMySettings = true;
            this.btnWC_Bibby_Calculate.CtrlProperty = "Calculate";
            this.btnWC_Bibby_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Bibby_Calculate.Location = new System.Drawing.Point(350, 0);
            this.btnWC_Bibby_Calculate.Name = "btnWC_Bibby_Calculate";
            this.btnWC_Bibby_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Bibby_Calculate.TabIndex = 26;
            this.btnWC_Bibby_Calculate.Text = "Calculate";
            this.btnWC_Bibby_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_Bibby_Calculate.Visible = false;
            this.btnWC_Bibby_Calculate.Click += new System.EventHandler(this.btnWC_Bibby_Calculate_Click);
            // 
            // btnWC_Bibby_Save
            // 
            this.btnWC_Bibby_Save.ApplyMySettings = true;
            this.btnWC_Bibby_Save.CtrlProperty = "Save";
            this.btnWC_Bibby_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Bibby_Save.Location = new System.Drawing.Point(425, 0);
            this.btnWC_Bibby_Save.Name = "btnWC_Bibby_Save";
            this.btnWC_Bibby_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Bibby_Save.TabIndex = 23;
            this.btnWC_Bibby_Save.Text = "Save";
            this.btnWC_Bibby_Save.UseVisualStyleBackColor = true;
            this.btnWC_Bibby_Save.Click += new System.EventHandler(this.btnWC_Bibby_Save_Click);
            // 
            // headerLabel5
            // 
            this.headerLabel5.ApplyMySettings = true;
            this.headerLabel5.AutoSize = true;
            this.headerLabel5.CtrlProperty = "Warehouse Costs (Bibby)";
            this.headerLabel5.Location = new System.Drawing.Point(8, 6);
            this.headerLabel5.Name = "headerLabel5";
            this.headerLabel5.Size = new System.Drawing.Size(126, 13);
            this.headerLabel5.TabIndex = 1;
            this.headerLabel5.Text = "Warehouse Costs (Bibby)";
            // 
            // panel7
            // 
            this.panel7.ApplyMySettings = true;
            this.panel7.Controls.Add(this.btnWC_Bibby_Curr_Calculate);
            this.panel7.Controls.Add(this.btnWC_Bibby_Curr_Save);
            this.panel7.Controls.Add(this.headerLabel6);
            this.panel7.CtrlProperty = "";
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel7.Size = new System.Drawing.Size(502, 28);
            this.panel7.TabIndex = 131;
            // 
            // btnWC_Bibby_Curr_Calculate
            // 
            this.btnWC_Bibby_Curr_Calculate.ApplyMySettings = true;
            this.btnWC_Bibby_Curr_Calculate.CtrlProperty = "Calculate";
            this.btnWC_Bibby_Curr_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Bibby_Curr_Calculate.Location = new System.Drawing.Point(349, 0);
            this.btnWC_Bibby_Curr_Calculate.Name = "btnWC_Bibby_Curr_Calculate";
            this.btnWC_Bibby_Curr_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Bibby_Curr_Calculate.TabIndex = 26;
            this.btnWC_Bibby_Curr_Calculate.Text = "Calculate";
            this.btnWC_Bibby_Curr_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_Bibby_Curr_Calculate.Visible = false;
            this.btnWC_Bibby_Curr_Calculate.Click += new System.EventHandler(this.btnWC_Bibby_Curr_Calculate_Click);
            // 
            // btnWC_Bibby_Curr_Save
            // 
            this.btnWC_Bibby_Curr_Save.ApplyMySettings = true;
            this.btnWC_Bibby_Curr_Save.CtrlProperty = "Save";
            this.btnWC_Bibby_Curr_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Bibby_Curr_Save.Location = new System.Drawing.Point(424, 0);
            this.btnWC_Bibby_Curr_Save.Name = "btnWC_Bibby_Curr_Save";
            this.btnWC_Bibby_Curr_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Bibby_Curr_Save.TabIndex = 23;
            this.btnWC_Bibby_Curr_Save.Text = "Save";
            this.btnWC_Bibby_Curr_Save.UseVisualStyleBackColor = true;
            this.btnWC_Bibby_Curr_Save.Click += new System.EventHandler(this.btnWC_Bibby_Curr_Save_Click);
            // 
            // headerLabel6
            // 
            this.headerLabel6.ApplyMySettings = true;
            this.headerLabel6.AutoSize = true;
            this.headerLabel6.CtrlProperty = "Warehouse Costs (Bibby)";
            this.headerLabel6.Location = new System.Drawing.Point(8, 6);
            this.headerLabel6.Name = "headerLabel6";
            this.headerLabel6.Size = new System.Drawing.Size(126, 13);
            this.headerLabel6.TabIndex = 1;
            this.headerLabel6.Text = "Warehouse Costs (Bibby)";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.panel8, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.panel9, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 555);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1005, 28);
            this.tableLayoutPanel8.TabIndex = 142;
            // 
            // panel8
            // 
            this.panel8.ApplyMySettings = true;
            this.panel8.Controls.Add(this.btnWC_Aarhus_Calculate);
            this.panel8.Controls.Add(this.btnWC_Aarhus_Save);
            this.panel8.Controls.Add(this.headerLabel8);
            this.panel8.CtrlProperty = "";
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(502, 0);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel8.Size = new System.Drawing.Size(503, 28);
            this.panel8.TabIndex = 132;
            // 
            // btnWC_Aarhus_Calculate
            // 
            this.btnWC_Aarhus_Calculate.ApplyMySettings = true;
            this.btnWC_Aarhus_Calculate.CtrlProperty = "Calculate";
            this.btnWC_Aarhus_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Aarhus_Calculate.Location = new System.Drawing.Point(350, 0);
            this.btnWC_Aarhus_Calculate.Name = "btnWC_Aarhus_Calculate";
            this.btnWC_Aarhus_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Aarhus_Calculate.TabIndex = 26;
            this.btnWC_Aarhus_Calculate.Text = "Calculate";
            this.btnWC_Aarhus_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_Aarhus_Calculate.Visible = false;
            this.btnWC_Aarhus_Calculate.Click += new System.EventHandler(this.btnWC_Aarhus_Calculate_Click);
            // 
            // btnWC_Aarhus_Save
            // 
            this.btnWC_Aarhus_Save.ApplyMySettings = true;
            this.btnWC_Aarhus_Save.CtrlProperty = "Save";
            this.btnWC_Aarhus_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Aarhus_Save.Location = new System.Drawing.Point(425, 0);
            this.btnWC_Aarhus_Save.Name = "btnWC_Aarhus_Save";
            this.btnWC_Aarhus_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Aarhus_Save.TabIndex = 23;
            this.btnWC_Aarhus_Save.Text = "Save";
            this.btnWC_Aarhus_Save.UseVisualStyleBackColor = true;
            this.btnWC_Aarhus_Save.Click += new System.EventHandler(this.btnWC_Aarhus_Save_Click);
            // 
            // headerLabel8
            // 
            this.headerLabel8.ApplyMySettings = true;
            this.headerLabel8.AutoSize = true;
            this.headerLabel8.CtrlProperty = "Warehouse Costs (Aarhus)";
            this.headerLabel8.Location = new System.Drawing.Point(8, 6);
            this.headerLabel8.Name = "headerLabel8";
            this.headerLabel8.Size = new System.Drawing.Size(133, 13);
            this.headerLabel8.TabIndex = 1;
            this.headerLabel8.Text = "Warehouse Costs (Aarhus)";
            // 
            // panel9
            // 
            this.panel9.ApplyMySettings = true;
            this.panel9.Controls.Add(this.btnWC_Aarhus_Curr_Calculate);
            this.panel9.Controls.Add(this.btnWC_Aarhus_Curr_Save);
            this.panel9.Controls.Add(this.headerLabel9);
            this.panel9.CtrlProperty = "";
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(0);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel9.Size = new System.Drawing.Size(502, 28);
            this.panel9.TabIndex = 131;
            // 
            // btnWC_Aarhus_Curr_Calculate
            // 
            this.btnWC_Aarhus_Curr_Calculate.ApplyMySettings = true;
            this.btnWC_Aarhus_Curr_Calculate.CtrlProperty = "Calculate";
            this.btnWC_Aarhus_Curr_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Aarhus_Curr_Calculate.Location = new System.Drawing.Point(349, 0);
            this.btnWC_Aarhus_Curr_Calculate.Name = "btnWC_Aarhus_Curr_Calculate";
            this.btnWC_Aarhus_Curr_Calculate.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Aarhus_Curr_Calculate.TabIndex = 26;
            this.btnWC_Aarhus_Curr_Calculate.Text = "Calculate";
            this.btnWC_Aarhus_Curr_Calculate.UseVisualStyleBackColor = true;
            this.btnWC_Aarhus_Curr_Calculate.Visible = false;
            this.btnWC_Aarhus_Curr_Calculate.Click += new System.EventHandler(this.btnWC_Aarhus_Curr_Calculate_Click);
            // 
            // btnWC_Aarhus_Curr_Save
            // 
            this.btnWC_Aarhus_Curr_Save.ApplyMySettings = true;
            this.btnWC_Aarhus_Curr_Save.CtrlProperty = "Save";
            this.btnWC_Aarhus_Curr_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWC_Aarhus_Curr_Save.Location = new System.Drawing.Point(424, 0);
            this.btnWC_Aarhus_Curr_Save.Name = "btnWC_Aarhus_Curr_Save";
            this.btnWC_Aarhus_Curr_Save.Size = new System.Drawing.Size(75, 28);
            this.btnWC_Aarhus_Curr_Save.TabIndex = 23;
            this.btnWC_Aarhus_Curr_Save.Text = "Save";
            this.btnWC_Aarhus_Curr_Save.UseVisualStyleBackColor = true;
            this.btnWC_Aarhus_Curr_Save.Click += new System.EventHandler(this.btnWC_Aarhus_Curr_Save_Click);
            // 
            // headerLabel9
            // 
            this.headerLabel9.ApplyMySettings = true;
            this.headerLabel9.AutoSize = true;
            this.headerLabel9.CtrlProperty = "Warehouse Costs (Aarhus)";
            this.headerLabel9.Location = new System.Drawing.Point(8, 6);
            this.headerLabel9.Name = "headerLabel9";
            this.headerLabel9.Size = new System.Drawing.Size(133, 13);
            this.headerLabel9.TabIndex = 1;
            this.headerLabel9.Text = "Warehouse Costs (Aarhus)";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.dgvWC_Aarhus, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.dgvWC_Aarhus_Curr, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 583);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1005, 147);
            this.tableLayoutPanel9.TabIndex = 143;
            // 
            // dgvWC_Aarhus
            // 
            this.dgvWC_Aarhus.AllowUpDownKey = false;
            this.dgvWC_Aarhus.AllowUserToAddRows = false;
            this.dgvWC_Aarhus.AllowUserToDeleteRows = false;
            this.dgvWC_Aarhus.AllowUserToResizeRows = false;
            this.dgvWC_Aarhus.ApplyMySettings = false;
            this.dgvWC_Aarhus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_Aarhus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AARHUS_ID,
            this.AARHUS,
            this.AA_MT_CONTAINER,
            this.AA_QTY_24,
            this.AA_QTY_24_7});
            this.dgvWC_Aarhus.ctlInfoMessage = null;
            this.dgvWC_Aarhus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_Aarhus.Location = new System.Drawing.Point(505, 3);
            this.dgvWC_Aarhus.MyAutoGenerateColumn = false;
            this.dgvWC_Aarhus.MyEditGrid = true;
            this.dgvWC_Aarhus.Name = "dgvWC_Aarhus";
            this.dgvWC_Aarhus.RowHeadersVisible = false;
            this.dgvWC_Aarhus.Size = new System.Drawing.Size(497, 141);
            this.dgvWC_Aarhus.strInfoMsgArr = null;
            this.dgvWC_Aarhus.TabIndex = 3;
            // 
            // dgvWC_Aarhus_Curr
            // 
            this.dgvWC_Aarhus_Curr.AllowUpDownKey = false;
            this.dgvWC_Aarhus_Curr.AllowUserToAddRows = false;
            this.dgvWC_Aarhus_Curr.AllowUserToDeleteRows = false;
            this.dgvWC_Aarhus_Curr.AllowUserToResizeRows = false;
            this.dgvWC_Aarhus_Curr.ApplyMySettings = false;
            this.dgvWC_Aarhus_Curr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWC_Aarhus_Curr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AARHUS_CURR_ID,
            this.AA_Type,
            this.AA_EUR,
            this.AA_EUR_MT_25KG,
            this.AA_EUR_MT_BB});
            this.dgvWC_Aarhus_Curr.ctlInfoMessage = null;
            this.dgvWC_Aarhus_Curr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWC_Aarhus_Curr.Location = new System.Drawing.Point(3, 3);
            this.dgvWC_Aarhus_Curr.MyAutoGenerateColumn = false;
            this.dgvWC_Aarhus_Curr.MyEditGrid = true;
            this.dgvWC_Aarhus_Curr.Name = "dgvWC_Aarhus_Curr";
            this.dgvWC_Aarhus_Curr.RowHeadersVisible = false;
            this.dgvWC_Aarhus_Curr.Size = new System.Drawing.Size(496, 141);
            this.dgvWC_Aarhus_Curr.strInfoMsgArr = null;
            this.dgvWC_Aarhus_Curr.TabIndex = 2;
            // 
            // AARHUS_CURR_ID
            // 
            this.AARHUS_CURR_ID.DataPropertyName = "AARHUS_CURR_ID";
            this.AARHUS_CURR_ID.HeaderText = "ID";
            this.AARHUS_CURR_ID.Name = "AARHUS_CURR_ID";
            this.AARHUS_CURR_ID.ReadOnly = true;
            this.AARHUS_CURR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AARHUS_CURR_ID.Visible = false;
            // 
            // AA_Type
            // 
            this.AA_Type.DataPropertyName = "Type";
            this.AA_Type.HeaderText = "Type";
            this.AA_Type.Name = "AA_Type";
            this.AA_Type.ReadOnly = true;
            this.AA_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AA_Type.Width = 300;
            // 
            // AA_EUR
            // 
            this.AA_EUR.DataPropertyName = "EUR";
            this.AA_EUR.HeaderText = "EUR";
            this.AA_EUR.Name = "AA_EUR";
            this.AA_EUR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AA_EUR.Width = 150;
            // 
            // AA_EUR_MT_25KG
            // 
            this.AA_EUR_MT_25KG.DataPropertyName = "EUR_MT_25KG";
            this.AA_EUR_MT_25KG.HeaderText = "EUR/MT (25KG)";
            this.AA_EUR_MT_25KG.Name = "AA_EUR_MT_25KG";
            this.AA_EUR_MT_25KG.ReadOnly = true;
            this.AA_EUR_MT_25KG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AA_EUR_MT_25KG.Width = 150;
            // 
            // AA_EUR_MT_BB
            // 
            this.AA_EUR_MT_BB.DataPropertyName = "EUR_MT_BB";
            this.AA_EUR_MT_BB.HeaderText = "EUR/MT (BB)";
            this.AA_EUR_MT_BB.Name = "AA_EUR_MT_BB";
            this.AA_EUR_MT_BB.ReadOnly = true;
            this.AA_EUR_MT_BB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AA_EUR_MT_BB.Width = 150;
            // 
            // AARHUS_ID
            // 
            this.AARHUS_ID.DataPropertyName = "AARHUS_ID";
            this.AARHUS_ID.HeaderText = "ID";
            this.AARHUS_ID.Name = "AARHUS_ID";
            this.AARHUS_ID.ReadOnly = true;
            this.AARHUS_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AARHUS_ID.Visible = false;
            // 
            // AARHUS
            // 
            this.AARHUS.DataPropertyName = "AARHUS";
            this.AARHUS.HeaderText = "Aarhus";
            this.AARHUS.Name = "AARHUS";
            this.AARHUS.ReadOnly = true;
            this.AARHUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AARHUS.Width = 350;
            // 
            // AA_MT_CONTAINER
            // 
            this.AA_MT_CONTAINER.DataPropertyName = "MT_CONTAINER";
            this.AA_MT_CONTAINER.HeaderText = "Per container(MT/Container)";
            this.AA_MT_CONTAINER.Name = "AA_MT_CONTAINER";
            this.AA_MT_CONTAINER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AA_MT_CONTAINER.Width = 200;
            // 
            // AA_QTY_24
            // 
            this.AA_QTY_24.DataPropertyName = "QTY_24";
            this.AA_QTY_24.HeaderText = "24";
            this.AA_QTY_24.Name = "AA_QTY_24";
            this.AA_QTY_24.ReadOnly = true;
            this.AA_QTY_24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AA_QTY_24_7
            // 
            this.AA_QTY_24_7.DataPropertyName = "QTY_24_7";
            this.AA_QTY_24_7.HeaderText = "24.7";
            this.AA_QTY_24_7.Name = "AA_QTY_24_7";
            this.AA_QTY_24_7.ReadOnly = true;
            this.AA_QTY_24_7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // frmStockHandling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStockHandling";
            this.Text = "Stock Handling";
            this.Load += new System.EventHandler(this.frmStockHandling_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Bibby)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Bibby_Curr)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Mueller_Curr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Mueller)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_MOLENBERGNATIE_CURR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_MOLENBERGNATIE)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Aarhus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWC_Aarhus_Curr)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.Panel panel2;
        private General.Controls.Button btnWC_MOLENBERGNATIE_Curr_Save;
        private General.Controls.HeaderLabel headerLabel3;
        private General.Controls.DataGridView dgvWC_MOLENBERGNATIE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private General.Controls.Button btnWC_MOLENBERGNATIE_Curr_Calculate;
        private General.Controls.Panel panel3;
        private General.Controls.Button btnWC_MOLENBERGNATIE_Calculate;
        private General.Controls.Button btnWC_MOLENBERGNATIE_Save;
        private General.Controls.HeaderLabel headerLabel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private General.Controls.Panel panel4;
        private General.Controls.Button btnWC_Mueller_Calculate;
        private General.Controls.Button btnWC_Mueller_Save;
        private General.Controls.HeaderLabel headerLabel2;
        private General.Controls.Panel panel5;
        private General.Controls.Button btnWC_Mueller_Curr_Calculate;
        private General.Controls.Button btnWC_Mueller_Curr_Save;
        private General.Controls.HeaderLabel headerLabel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private General.Controls.Panel panel6;
        private General.Controls.Button btnWC_Bibby_Calculate;
        private General.Controls.Button btnWC_Bibby_Save;
        private General.Controls.HeaderLabel headerLabel5;
        private General.Controls.Panel panel7;
        private General.Controls.Button btnWC_Bibby_Curr_Calculate;
        private General.Controls.Button btnWC_Bibby_Curr_Save;
        private General.Controls.HeaderLabel headerLabel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private General.Controls.DataGridView dgvWC_MOLENBERGNATIE_CURR;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private General.Controls.DataGridView dgvWC_Mueller_Curr;
        private General.Controls.DataGridView dgvWC_Mueller;
        private General.Controls.DataGridView dgvWC_Bibby;
        private General.Controls.DataGridView dgvWC_Bibby_Curr;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_MT_CONTAINER;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_QTY_24;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_QTY_24_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUELLER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUELLER;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_MT_CONTAINER;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_QTY_24;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_QTY_24_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOLENBERGNATIE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOLENBERGNATIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn MT_CONTAINER;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY_24;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY_24_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_CURR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_EUR_MT_25KG;
        private System.Windows.Forms.DataGridViewTextBoxColumn BB_EUR_MT_BB;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUELLER_CURR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_EUR_MT_25KG;
        private System.Windows.Forms.DataGridViewTextBoxColumn MU_EUR_MT_BB;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOLENBERGNATIE_CURR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn EUR_MT_25KG;
        private System.Windows.Forms.DataGridViewTextBoxColumn EUR_MT_BB;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private General.Controls.Panel panel8;
        private General.Controls.Button btnWC_Aarhus_Calculate;
        private General.Controls.Button btnWC_Aarhus_Save;
        private General.Controls.HeaderLabel headerLabel8;
        private General.Controls.Panel panel9;
        private General.Controls.Button btnWC_Aarhus_Curr_Calculate;
        private General.Controls.Button btnWC_Aarhus_Curr_Save;
        private General.Controls.HeaderLabel headerLabel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private General.Controls.DataGridView dgvWC_Aarhus;
        private General.Controls.DataGridView dgvWC_Aarhus_Curr;
        private System.Windows.Forms.DataGridViewTextBoxColumn AARHUS_CURR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_EUR_MT_25KG;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_EUR_MT_BB;
        private System.Windows.Forms.DataGridViewTextBoxColumn AARHUS_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AARHUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_MT_CONTAINER;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_QTY_24;
        private System.Windows.Forms.DataGridViewTextBoxColumn AA_QTY_24_7;
    }
}