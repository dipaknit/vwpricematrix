﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class frmInput : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmInput()
        {
            InitializeComponent();
        }

        #region Form Event
        
        private void frmInput_Load(object sender, EventArgs e)
        {
            GetAll();
        }

        #endregion

        #region Other Mothods       

        private void GetAll()
        {
            GetProduct();
            GetImportDuties();
            GetDesctination();
            GetForex();
            GetWarehouse();
            GetMargin();
            GetMargin_InterCompany();
        } 

        private void GetProduct()
        {
            //objclsSqlLayer.MakeDBConnection();
            string strQuery = " SELECT * FROM PRODUCTS WHERE isdeleted = 0 Order by ProductId ";
            DataTable dtProduct = objclsSqlLayer.GetDataTable(strQuery);
            dgvProduct.AutoGenerateColumns = false;
            //objclsSqlLayer.CloseDBConnection();
            dgvProduct.DataSource = dtProduct;

            dgvProduct.Columns["MEGAFAT88"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvProduct.Columns["MEGAFATEXTRA"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvProduct.Columns["MEGAENERGY"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvProduct.Columns["MEGABOOST"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvProduct.Columns["MEGAONE"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void GetImportDuties()
        {
            string strQuery = " SELECT * FROM Import_Duty WHERE isdeleted = 0 Order by ImportDutyId ";
            DataTable dtImportDuty = objclsSqlLayer.GetDataTable(strQuery);
            dgvImportDuties.AutoGenerateColumns = false;
            dgvImportDuties.DataSource = dtImportDuty;
        }

        private void GetDesctination()
        {
            //objclsSqlLayer.MakeDBConnection();
            string strQuery = " SELECT * FROM Destination WHERE isdeleted = 0 Order by DestinationId ";
            DataTable dtDestination = objclsSqlLayer.GetDataTable(strQuery);
            dgvDestination.AutoGenerateColumns = false;
            //objclsSqlLayer.CloseDBConnection();
            dgvDestination.DataSource = dtDestination;
            dgvDestination.Columns["Freight_40_FCL"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void GetForex()
        {
            string strQuery = " SELECT * FROM Forex WHERE isdeleted = 0 Order by ForexId ";
            DataTable dtForex = objclsSqlLayer.GetDataTable(strQuery);
            dgvForex.AutoGenerateColumns = false;
            dgvForex.DataSource = dtForex;
            dgvForex.Columns["Rate"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void GetWarehouse()
        {
            string strQuery = " SELECT * FROM Warehouse WHERE isdeleted = 0 Order by WarehouseId ";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);
            dgvWarehouse.AutoGenerateColumns = false;
            dgvWarehouse.DataSource = dtData;
            dgvWarehouse.Columns["ANTWERP"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvWarehouse.Columns["BRAKE"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvWarehouse.Columns["BIBBY"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvWarehouse.Columns["AARHUS"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvWarehouse.Columns["LIVERPOOL"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void GetMargin()
        {
            string strQuery = " SELECT * FROM Margin WHERE isdeleted = 0 Order by MarginId ";
            DataTable dtMargin = objclsSqlLayer.GetDataTable(strQuery);
            dgvMargin.AutoGenerateColumns = false;
            dgvMargin.DataSource = dtMargin;
            dgvMargin.Columns["M_MEGALAC"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvMargin.Columns["M_MEGAFAT88"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvMargin.Columns["M_MEGAFATEXTRA"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvMargin.Columns["M_MEGAENERGY"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvMargin.Columns["M_MEGABOOST"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvMargin.Columns["M_MEGAONE"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void GetMargin_InterCompany()
        {
            string strQuery = " SELECT * FROM Margin_InterCompany WHERE isdeleted = 0 Order by MarginId ";
            DataTable dtMargin = objclsSqlLayer.GetDataTable(strQuery);
            dgvInterCompanyMargin.AutoGenerateColumns = false;
            dgvInterCompanyMargin.DataSource = dtMargin;
            dgvInterCompanyMargin.Columns["MI_MEGALAC"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvInterCompanyMargin.Columns["MI_MEGAFAT88"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvInterCompanyMargin.Columns["MI_MEGAFATEXTRA"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvInterCompanyMargin.Columns["MI_MEGAENERGY"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvInterCompanyMargin.Columns["MI_MEGABOOST"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
            dgvInterCompanyMargin.Columns["MI_MEGAONE"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        #endregion

        #region Button Events

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            DataGridViewRow QtyRow = dgvProduct.Rows
            .Cast<DataGridViewRow>()
            .Where(r => r.Cells["Product"].Value.ToString().ToUpper().Equals("QUANTITY/FCL (MT)"))
            .FirstOrDefault();

            DataGridViewRow FobRow = dgvProduct.Rows
            .Cast<DataGridViewRow>()
            .Where(r => r.Cells["Product"].Value.ToString().ToUpper().Equals("FOB (USD/MT)"))
            .FirstOrDefault();

            string strMEGAFAT88_Quantity_FCL_MT;
            string strMEGAFAT88_FOB_USD;
            string strMEGAFATEXTRA_FOB_USD;
            string strMEGAENERGY_FOB_USD;
            string strMEGABOOST_FOB_USD;
            string strMEGAONE_FOB_USD;

            if (QtyRow != null)
            {
                strMEGAFAT88_Quantity_FCL_MT = QtyRow.Cells["MEGAFAT88"].Value.ToString();
                strMEGAFAT88_FOB_USD = FobRow.Cells["MEGAFAT88"].Value.ToString();
                strMEGAFATEXTRA_FOB_USD = FobRow.Cells["MEGAFATEXTRA"].Value.ToString();
                strMEGAENERGY_FOB_USD = FobRow.Cells["MEGAENERGY"].Value.ToString();
                strMEGABOOST_FOB_USD = FobRow.Cells["MEGABOOST"].Value.ToString();
                strMEGAONE_FOB_USD = FobRow.Cells["MEGAONE"].Value.ToString();

                foreach (DataGridViewRow item in dgvDestination.Rows)
                {
                    item.Cells["Freight_PMT"].Value = Math.Round((double)Convert.ToInt32(item.Cells["Freight_40_FCL"].Value) / Convert.ToInt32(strMEGAFAT88_Quantity_FCL_MT));
                    item.Cells["MEGAFAT88_CIF_USD"].Value = Convert.ToInt32(item.Cells["Freight_PMT"].Value) + Convert.ToInt32(strMEGAFAT88_FOB_USD);
                    item.Cells["MEGAFATEXTRA_CIF_USD"].Value = Convert.ToInt32(item.Cells["Freight_PMT"].Value) + Convert.ToInt32(strMEGAFATEXTRA_FOB_USD);
                    item.Cells["MEGAENERGY_CIF_USD"].Value = Convert.ToInt32(item.Cells["Freight_PMT"].Value) + Convert.ToInt32(strMEGAENERGY_FOB_USD);
                    item.Cells["MEGABOOST_CIF_USD"].Value = Convert.ToInt32(item.Cells["Freight_PMT"].Value) + Convert.ToInt32(strMEGABOOST_FOB_USD);
                    item.Cells["MEGAONE_CIF_USD"].Value = Convert.ToInt32(item.Cells["Freight_PMT"].Value) + Convert.ToInt32(strMEGAONE_FOB_USD);
                }
            }
            else
            {
                MessageBox.Show("Quantity/FCL (MT) not found in Product");
                return;
            }
        }

        private void btnProductSave_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvProduct.Rows)
            {
                string[] Param = { "@ProductId", "@Product", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["ProductId"].Value), Convert.ToString(row.Cells["Product"].Value), Convert.ToString(row.Cells["MEGAFAT88"].Value), Convert.ToString(row.Cells["MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["MEGAENERGY"].Value), Convert.ToString(row.Cells["MEGABOOST"].Value), Convert.ToString(row.Cells["MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Products_AddEdit", Param, Value);
            }

            MessageBox.Show("Product Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnDestinationSave_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvDestination.Rows)
            //{
            //    string[] Param = { "@DestinationId", "@Destination", "@Country", "@Freight_40_FCL", "@Freight_PMT", "@MEGAFAT88_CIF_USD", "@MEGAFATEXTRA_CIF_USD", "@MEGAENERGY_CIF_USD", "@MEGABOOST_CIF_USD", "@MEGAONE_CIF_USD", "@CreatedBy" };
            //    string[] Value = { Convert.ToString(row.Cells["DestinationId"].Value), Convert.ToString(row.Cells["Destination"].Value), Convert.ToString(row.Cells["Country"].Value), Convert.ToString(row.Cells["Freight_40_FCL"].Value), Convert.ToString(row.Cells["Freight_PMT"].Value), Convert.ToString(row.Cells["MEGAFAT88_CIF_USD"].Value), Convert.ToString(row.Cells["MEGAFATEXTRA_CIF_USD"].Value), Convert.ToString(row.Cells["MEGAENERGY_CIF_USD"].Value), Convert.ToString(row.Cells["MEGABOOST_CIF_USD"].Value), Convert.ToString(row.Cells["MEGAONE_CIF_USD"].Value), Support.UserId };
            //    objclsSqlLayer.ExecuteSP("csp_Destination_AddEdit", Param, Value);
            //}

            foreach (DataGridViewRow row in dgvDestination.Rows)
            {
                string[] Param = { "@DestinationId", "@Destination", "@Country", "@Freight_40_FCL", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["DestinationId"].Value), Convert.ToString(row.Cells["Destination"].Value), Convert.ToString(row.Cells["Country"].Value), Convert.ToString(row.Cells["Freight_40_FCL"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Destination_AddEdit", Param, Value);
            }

            MessageBox.Show("Destination Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnMarginSave_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvMargin.Rows)
            {
                string[] Param = { "@MarginId", "@Type", "@MEGALAC", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["M_MarginId"].Value), Convert.ToString(row.Cells["M_Type"].Value), Convert.ToString(row.Cells["M_MEGALAC"].Value), Convert.ToString(row.Cells["M_MEGAFAT88"].Value), Convert.ToString(row.Cells["M_MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["M_MEGAENERGY"].Value), Convert.ToString(row.Cells["M_MEGABOOST"].Value), Convert.ToString(row.Cells["M_MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Margin_AddEdit", Param, Value);
            }

            MessageBox.Show("Margin Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnImportDuties_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvImportDuties.Rows)
            {
                string[] Param = { "@ImportDutyId", "@Country", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["ImportDutyId"].Value), Convert.ToString(row.Cells["ID_Country"].Value), Convert.ToString(row.Cells["ID_MEGAFAT88"].Value), Convert.ToString(row.Cells["ID_MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["ID_MEGAENERGY"].Value), Convert.ToString(row.Cells["ID_MEGABOOST"].Value), Convert.ToString(row.Cells["ID_MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Import_Duty_AddEdit", Param, Value);
            }

            MessageBox.Show("Import Duties Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnForexSave_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvForex.Rows)
            {
                string[] Param = { "@ForexId", "@Type", "@Currency", "@Rate", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["ForexId"].Value), Convert.ToString(row.Cells["Type"].Value), Convert.ToString(row.Cells["Currency"].Value), Convert.ToString(row.Cells["Rate"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Forex_AddEdit", Param, Value);
            }

            MessageBox.Show("Forex Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnInterCompanyMargin_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvInterCompanyMargin.Rows)
            {
                string[] Param = { "@MarginId", "@Type", "@MEGALAC", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["MI_MarginId"].Value), Convert.ToString(row.Cells["MI_Type"].Value), Convert.ToString(row.Cells["MI_MEGALAC"].Value), Convert.ToString(row.Cells["MI_MEGAFAT88"].Value), Convert.ToString(row.Cells["MI_MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["MI_MEGAENERGY"].Value), Convert.ToString(row.Cells["MI_MEGABOOST"].Value), Convert.ToString(row.Cells["MI_MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Margin_InterCompany_AddEdit", Param, Value);
            }

            MessageBox.Show("Margin Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }        

        private void btnWarehouse_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvWarehouse.Rows)
            {
                string[] Param = { "@WarehouseId", "@ANTWERP", "@BRAKE", "@BIBBY", "@AARHUS", "@LIVERPOOL", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["WarehouseId"].Value), Convert.ToString(row.Cells["ANTWERP"].Value), Convert.ToString(row.Cells["BRAKE"].Value), Convert.ToString(row.Cells["BIBBY"].Value), Convert.ToString(row.Cells["AARHUS"].Value), Convert.ToString(row.Cells["LIVERPOOL"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Warehouse_AddEdit", Param, Value);
            }

            MessageBox.Show("Warehouse Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvProduct.Rows)
            {
                string[] Param = { "@ProductId", "@Product", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["ProductId"].Value), Convert.ToString(row.Cells["Product"].Value), Convert.ToString(row.Cells["MEGAFAT88"].Value), Convert.ToString(row.Cells["MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["MEGAENERGY"].Value), Convert.ToString(row.Cells["MEGABOOST"].Value), Convert.ToString(row.Cells["MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Products_AddEdit", Param, Value);
            }


            foreach (DataGridViewRow row in dgvImportDuties.Rows)
            {
                string[] Param = { "@ImportDutyId", "@Country", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["ImportDutyId"].Value), Convert.ToString(row.Cells["ID_Country"].Value), Convert.ToString(row.Cells["ID_MEGAFAT88"].Value), Convert.ToString(row.Cells["ID_MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["ID_MEGAENERGY"].Value), Convert.ToString(row.Cells["ID_MEGABOOST"].Value), Convert.ToString(row.Cells["ID_MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Import_Duty_AddEdit", Param, Value);
            }

            foreach (DataGridViewRow row in dgvDestination.Rows)
            {
                string[] Param = { "@DestinationId", "@Destination", "@Country", "@Freight_40_FCL", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["DestinationId"].Value), Convert.ToString(row.Cells["Destination"].Value), Convert.ToString(row.Cells["Country"].Value), Convert.ToString(row.Cells["Freight_40_FCL"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Destination_AddEdit", Param, Value);
            }


            foreach (DataGridViewRow row in dgvForex.Rows)
            {
                string[] Param = { "@ForexId", "@Type", "@Currency", "@Rate", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["ForexId"].Value), Convert.ToString(row.Cells["Type"].Value), Convert.ToString(row.Cells["Currency"].Value), Convert.ToString(row.Cells["Rate"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Forex_AddEdit", Param, Value);
            }


            foreach (DataGridViewRow row in dgvWarehouse.Rows)
            {
                string[] Param = { "@WarehouseId", "@ANTWERP", "@BRAKE", "@BIBBY", "@AARHUS", "@LIVERPOOL", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["WarehouseId"].Value), Convert.ToString(row.Cells["ANTWERP"].Value), Convert.ToString(row.Cells["BRAKE"].Value), Convert.ToString(row.Cells["BIBBY"].Value), Convert.ToString(row.Cells["AARHUS"].Value), Convert.ToString(row.Cells["LIVERPOOL"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Warehouse_AddEdit", Param, Value);
            }


            foreach (DataGridViewRow row in dgvMargin.Rows)
            {
                string[] Param = { "@MarginId", "@Type", "@MEGALAC", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["M_MarginId"].Value), Convert.ToString(row.Cells["M_Type"].Value), Convert.ToString(row.Cells["M_MEGALAC"].Value), Convert.ToString(row.Cells["M_MEGAFAT88"].Value), Convert.ToString(row.Cells["M_MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["M_MEGAENERGY"].Value), Convert.ToString(row.Cells["M_MEGABOOST"].Value), Convert.ToString(row.Cells["M_MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Margin_AddEdit", Param, Value);
            }


            foreach (DataGridViewRow row in dgvInterCompanyMargin.Rows)
            {
                string[] Param = { "@MarginId", "@Type", "@MEGALAC", "@MEGAFAT88", "@MEGAFATEXTRA", "@MEGAENERGY", "@MEGABOOST", "@MEGAONE", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["MI_MarginId"].Value), Convert.ToString(row.Cells["MI_Type"].Value), Convert.ToString(row.Cells["MI_MEGALAC"].Value), Convert.ToString(row.Cells["MI_MEGAFAT88"].Value), Convert.ToString(row.Cells["MI_MEGAFATEXTRA"].Value), Convert.ToString(row.Cells["MI_MEGAENERGY"].Value), Convert.ToString(row.Cells["MI_MEGABOOST"].Value), Convert.ToString(row.Cells["MI_MEGAONE"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Margin_InterCompany_AddEdit", Param, Value);
            }


            string[] Param1 = { "@UpdatedBy" };
            string[] Value1 = { Support.UserId };
            objclsSqlLayer.ExecuteSP("csp_Calculation", Param1, Value1);

            MessageBox.Show("Information Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        #endregion
    }
}
