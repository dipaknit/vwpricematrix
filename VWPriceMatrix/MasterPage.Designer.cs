﻿namespace VWPriceMatrix
{
    partial class MasterPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MasterPage));
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.mnuUserInput = new System.Windows.Forms.ToolStripMenuItem();
            this.inputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockHandlingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exWarehouseCostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mEGALACEURToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dEPriceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dKPriceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nEBEPriceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intercompanyPriceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherPriceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherPriceMatrixViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.Menu.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuUserInput,
            this.outputToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(1008, 24);
            this.Menu.TabIndex = 0;
            this.Menu.Text = "Menu";
            // 
            // mnuUserInput
            // 
            this.mnuUserInput.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inputToolStripMenuItem,
            this.stockHandlingToolStripMenuItem,
            this.exWarehouseCostToolStripMenuItem,
            this.mEGALACEURToolStripMenuItem,
            this.customerDataToolStripMenuItem});
            this.mnuUserInput.Name = "mnuUserInput";
            this.mnuUserInput.Size = new System.Drawing.Size(73, 20);
            this.mnuUserInput.Text = "Basic Data";
            // 
            // inputToolStripMenuItem
            // 
            this.inputToolStripMenuItem.Name = "inputToolStripMenuItem";
            this.inputToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.inputToolStripMenuItem.Text = "Input";
            this.inputToolStripMenuItem.Click += new System.EventHandler(this.userInputToolStripMenuItem_Click);
            // 
            // stockHandlingToolStripMenuItem
            // 
            this.stockHandlingToolStripMenuItem.Name = "stockHandlingToolStripMenuItem";
            this.stockHandlingToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.stockHandlingToolStripMenuItem.Text = "Stock Handling";
            this.stockHandlingToolStripMenuItem.Click += new System.EventHandler(this.stockHandlingToolStripMenuItem_Click);
            // 
            // exWarehouseCostToolStripMenuItem
            // 
            this.exWarehouseCostToolStripMenuItem.Name = "exWarehouseCostToolStripMenuItem";
            this.exWarehouseCostToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.exWarehouseCostToolStripMenuItem.Text = "Ex-Warehouse Cost";
            this.exWarehouseCostToolStripMenuItem.Click += new System.EventHandler(this.exWarehouseCostToolStripMenuItem_Click);
            // 
            // mEGALACEURToolStripMenuItem
            // 
            this.mEGALACEURToolStripMenuItem.Name = "mEGALACEURToolStripMenuItem";
            this.mEGALACEURToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.mEGALACEURToolStripMenuItem.Text = "Megalac Ex-Factory";
            this.mEGALACEURToolStripMenuItem.Click += new System.EventHandler(this.mEGALACEURToolStripMenuItem_Click);
            // 
            // customerDataToolStripMenuItem
            // 
            this.customerDataToolStripMenuItem.Name = "customerDataToolStripMenuItem";
            this.customerDataToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.customerDataToolStripMenuItem.Text = "Customer Data";
            this.customerDataToolStripMenuItem.Click += new System.EventHandler(this.customerDataToolStripMenuItem_Click);
            // 
            // outputToolStripMenuItem
            // 
            this.outputToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dEPriceMatrixToolStripMenuItem,
            this.dKPriceMatrixToolStripMenuItem,
            this.nEBEPriceMatrixToolStripMenuItem,
            this.intercompanyPriceMatrixToolStripMenuItem,
            this.otherPriceMatrixToolStripMenuItem,
            this.otherPriceMatrixViewToolStripMenuItem});
            this.outputToolStripMenuItem.Name = "outputToolStripMenuItem";
            this.outputToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.outputToolStripMenuItem.Text = "Output";
            // 
            // dEPriceMatrixToolStripMenuItem
            // 
            this.dEPriceMatrixToolStripMenuItem.Name = "dEPriceMatrixToolStripMenuItem";
            this.dEPriceMatrixToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.dEPriceMatrixToolStripMenuItem.Text = "DE Price Matrix";
            this.dEPriceMatrixToolStripMenuItem.Click += new System.EventHandler(this.dEPriceMatrixToolStripMenuItem_Click);
            // 
            // dKPriceMatrixToolStripMenuItem
            // 
            this.dKPriceMatrixToolStripMenuItem.Name = "dKPriceMatrixToolStripMenuItem";
            this.dKPriceMatrixToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.dKPriceMatrixToolStripMenuItem.Text = "DK Price Matrix";
            this.dKPriceMatrixToolStripMenuItem.Click += new System.EventHandler(this.dKPriceMatrixToolStripMenuItem_Click);
            // 
            // nEBEPriceMatrixToolStripMenuItem
            // 
            this.nEBEPriceMatrixToolStripMenuItem.Name = "nEBEPriceMatrixToolStripMenuItem";
            this.nEBEPriceMatrixToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.nEBEPriceMatrixToolStripMenuItem.Text = "NL BE Price Matrix";
            this.nEBEPriceMatrixToolStripMenuItem.Click += new System.EventHandler(this.nEBEPriceMatrixToolStripMenuItem_Click);
            // 
            // intercompanyPriceMatrixToolStripMenuItem
            // 
            this.intercompanyPriceMatrixToolStripMenuItem.Name = "intercompanyPriceMatrixToolStripMenuItem";
            this.intercompanyPriceMatrixToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.intercompanyPriceMatrixToolStripMenuItem.Text = "Intercompany Price Matrix";
            this.intercompanyPriceMatrixToolStripMenuItem.Click += new System.EventHandler(this.intercompanyPriceMatrixToolStripMenuItem_Click);
            // 
            // otherPriceMatrixToolStripMenuItem
            // 
            this.otherPriceMatrixToolStripMenuItem.Name = "otherPriceMatrixToolStripMenuItem";
            this.otherPriceMatrixToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.otherPriceMatrixToolStripMenuItem.Text = "Other Price Matrix Generate && View";
            this.otherPriceMatrixToolStripMenuItem.Click += new System.EventHandler(this.otherPriceMatrixToolStripMenuItem_Click);
            // 
            // otherPriceMatrixViewToolStripMenuItem
            // 
            this.otherPriceMatrixViewToolStripMenuItem.Name = "otherPriceMatrixViewToolStripMenuItem";
            this.otherPriceMatrixViewToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.otherPriceMatrixViewToolStripMenuItem.Text = "Other Price Matrix View";
            this.otherPriceMatrixViewToolStripMenuItem.Click += new System.EventHandler(this.otherPriceMatrixViewToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 708);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(85, 17);
            this.toolStripStatusLabel.Text = "Version: 1.0.0.2";
            // 
            // MasterPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::VWPriceMatrix.Properties.Resources.mdi_bckg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.Menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.Menu;
            this.Name = "MasterPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VW Price Matrix";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem mnuUserInput;
        private System.Windows.Forms.ToolStripMenuItem inputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockHandlingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exWarehouseCostToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mEGALACEURToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dEPriceMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dKPriceMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nEBEPriceMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intercompanyPriceMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherPriceMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherPriceMatrixViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerDataToolStripMenuItem;
    }
}



