﻿namespace VWPriceMatrix
{
    partial class frmCustomerAddEditDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomerAddEditDelete));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.panel2 = new General.Controls.Panel();
            this.btnCancel = new General.Controls.Button();
            this.btnSave = new General.Controls.Button();
            this.label2 = new General.Controls.Label();
            this.txtCity = new General.Controls.TextBox();
            this.label1 = new General.Controls.Label();
            this.txtPostcode = new General.Controls.TextBox();
            this.label3 = new General.Controls.Label();
            this.label7 = new General.Controls.Label();
            this.txtCustomerId = new General.Controls.TextBox();
            this.txtCustomerName = new General.Controls.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1010, 730);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel1);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1004, 24);
            this.panelHeader1.TabIndex = 126;
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "Customer";
            this.headerLabel1.Location = new System.Drawing.Point(14, 5);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(51, 13);
            this.headerLabel1.TabIndex = 0;
            this.headerLabel1.Text = "Customer";
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtCity);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtPostcode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtCustomerId);
            this.panel2.Controls.Add(this.txtCustomerName);
            this.panel2.CtrlProperty = "";
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1004, 694);
            this.panel2.TabIndex = 127;
            // 
            // btnCancel
            // 
            this.btnCancel.ApplyMySettings = true;
            this.btnCancel.CtrlProperty = "&Cancel";
            this.btnCancel.Location = new System.Drawing.Point(223, 131);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.ApplyMySettings = true;
            this.btnSave.CtrlProperty = "&Save";
            this.btnSave.Location = new System.Drawing.Point(142, 131);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.ApplyMySettings = true;
            this.label2.CtrlProperty = "City";
            this.label2.Location = new System.Drawing.Point(15, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "City";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCity
            // 
            this.txtCity.AllowAllCharacters = true;
            this.txtCity.AllowCurrency = false;
            this.txtCity.AllowSpace = true;
            this.txtCity.ApplyMySettings = true;
            this.txtCity.ConvertEnterToTab = true;
            this.txtCity.ctlInfoMessage = null;
            this.txtCity.CtrlProperty = "";
            this.txtCity.InfoMessage = "";
            this.txtCity.Location = new System.Drawing.Point(140, 96);
            this.txtCity.MaxLength = 50;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(158, 20);
            this.txtCity.TabIndex = 3;
            this.txtCity.Verify = true;
            // 
            // label1
            // 
            this.label1.ApplyMySettings = true;
            this.label1.CtrlProperty = "Postcode";
            this.label1.Location = new System.Drawing.Point(15, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Postcode";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPostcode
            // 
            this.txtPostcode.AllowAllCharacters = true;
            this.txtPostcode.AllowCurrency = false;
            this.txtPostcode.AllowSpace = true;
            this.txtPostcode.ApplyMySettings = true;
            this.txtPostcode.ConvertEnterToTab = true;
            this.txtPostcode.ctlInfoMessage = null;
            this.txtPostcode.CtrlProperty = "";
            this.txtPostcode.InfoMessage = "";
            this.txtPostcode.Location = new System.Drawing.Point(140, 70);
            this.txtPostcode.MaxLength = 50;
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(158, 20);
            this.txtPostcode.TabIndex = 2;
            this.txtPostcode.Verify = true;
            // 
            // label3
            // 
            this.label3.ApplyMySettings = true;
            this.label3.CtrlProperty = "Customer Id";
            this.label3.Location = new System.Drawing.Point(15, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Customer Id";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.ApplyMySettings = true;
            this.label7.CtrlProperty = "Customer Name";
            this.label7.Location = new System.Drawing.Point(15, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Customer Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.AllowAllCharacters = true;
            this.txtCustomerId.AllowCurrency = false;
            this.txtCustomerId.AllowSpace = true;
            this.txtCustomerId.ApplyMySettings = true;
            this.txtCustomerId.ConvertEnterToTab = true;
            this.txtCustomerId.ctlInfoMessage = null;
            this.txtCustomerId.CtrlProperty = "";
            this.txtCustomerId.InfoMessage = "";
            this.txtCustomerId.Location = new System.Drawing.Point(140, 18);
            this.txtCustomerId.MaxLength = 50;
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.Size = new System.Drawing.Size(158, 20);
            this.txtCustomerId.TabIndex = 0;
            this.txtCustomerId.Verify = true;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.AllowAllCharacters = true;
            this.txtCustomerName.AllowCurrency = false;
            this.txtCustomerName.AllowSpace = true;
            this.txtCustomerName.ApplyMySettings = true;
            this.txtCustomerName.ConvertEnterToTab = true;
            this.txtCustomerName.ctlInfoMessage = null;
            this.txtCustomerName.CtrlProperty = "";
            this.txtCustomerName.InfoMessage = "";
            this.txtCustomerName.Location = new System.Drawing.Point(140, 44);
            this.txtCustomerName.MaxLength = 50;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(347, 20);
            this.txtCustomerName.TabIndex = 1;
            this.txtCustomerName.Verify = true;
            // 
            // frmCustomerAddEditDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 730);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCustomerAddEditDelete";
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.frmCustomerAddEditDelete_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel1;
        private General.Controls.Panel panel2;
        private General.Controls.Label label2;
        private General.Controls.TextBox txtCity;
        private General.Controls.Label label1;
        private General.Controls.TextBox txtPostcode;
        private General.Controls.Label label7;
        private General.Controls.TextBox txtCustomerName;
        private General.Controls.Button btnCancel;
        private General.Controls.Button btnSave;
        private General.Controls.Label label3;
        private General.Controls.TextBox txtCustomerId;
    }
}