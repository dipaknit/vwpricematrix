﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Support.UserId = "1";

            Application.Run(new Login());
            if (Support.IsAuthenticated == true)
                Application.Run(new MasterPage());
        }
    }
}
