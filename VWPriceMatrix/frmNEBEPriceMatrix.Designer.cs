﻿namespace VWPriceMatrix
{
    partial class frmNEBEPriceMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNEBEPriceMatrix));
            this.panel1 = new General.Controls.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.panel7 = new General.Controls.Panel();
            this.btnExcelExport = new General.Controls.Button();
            this.headerLabel7 = new General.Controls.HeaderLabel();
            this.dgvData = new General.Controls.DataGridView();
            this.R1_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 730);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgvData, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel3);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1002, 24);
            this.panelHeader1.TabIndex = 140;
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "Netherlands Belgium Price Matrix";
            this.headerLabel3.Location = new System.Drawing.Point(8, 6);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(162, 13);
            this.headerLabel3.TabIndex = 0;
            this.headerLabel3.Text = "Netherlands Belgium Price Matrix";
            // 
            // panel7
            // 
            this.panel7.ApplyMySettings = true;
            this.panel7.Controls.Add(this.btnExcelExport);
            this.panel7.Controls.Add(this.headerLabel7);
            this.panel7.CtrlProperty = "";
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 33);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel7.Size = new System.Drawing.Size(1002, 24);
            this.panel7.TabIndex = 133;
            // 
            // btnExcelExport
            // 
            this.btnExcelExport.ApplyMySettings = true;
            this.btnExcelExport.CtrlProperty = "Excel Export";
            this.btnExcelExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExcelExport.Location = new System.Drawing.Point(924, 0);
            this.btnExcelExport.Name = "btnExcelExport";
            this.btnExcelExport.Size = new System.Drawing.Size(75, 24);
            this.btnExcelExport.TabIndex = 3;
            this.btnExcelExport.Text = "Excel Export";
            this.btnExcelExport.UseVisualStyleBackColor = true;
            this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
            // 
            // headerLabel7
            // 
            this.headerLabel7.ApplyMySettings = true;
            this.headerLabel7.AutoSize = true;
            this.headerLabel7.CtrlProperty = "NL BE Price Matrix";
            this.headerLabel7.Location = new System.Drawing.Point(8, 6);
            this.headerLabel7.Name = "headerLabel7";
            this.headerLabel7.Size = new System.Drawing.Size(96, 13);
            this.headerLabel7.TabIndex = 1;
            this.headerLabel7.Text = "NL BE Price Matrix";
            // 
            // dgvData
            // 
            this.dgvData.AllowUpDownKey = false;
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToResizeRows = false;
            this.dgvData.ApplyMySettings = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R1_ID,
            this.R1_MEGAFAT88_25,
            this.R1_MEGAFAT88_650,
            this.R1_MEGAFATEXTRA_25,
            this.R1_MEGAFATEXTRA_650,
            this.R1_MEGAENERGY_25,
            this.R1_MEGAENERGY_650,
            this.R1_MEGABOOST_25,
            this.R1_MEGABOOST_650,
            this.R1_MEGAONE_25,
            this.R1_MEGAONE_650,
            this.R1_MEGAMAX_25,
            this.R1_MEGAMAX_650,
            this.R1_MEGALAC_25,
            this.R1_MEGALAC_650});
            this.dgvData.ctlInfoMessage = null;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(3, 63);
            this.dgvData.MyAutoGenerateColumn = false;
            this.dgvData.MyEditGrid = false;
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowTemplate.ReadOnly = true;
            this.dgvData.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.ShowEditingIcon = false;
            this.dgvData.Size = new System.Drawing.Size(1002, 664);
            this.dgvData.strInfoMsgArr = null;
            this.dgvData.TabIndex = 134;
            // 
            // R1_ID
            // 
            this.R1_ID.DataPropertyName = "ID";
            this.R1_ID.HeaderText = "ID";
            this.R1_ID.Name = "R1_ID";
            this.R1_ID.ReadOnly = true;
            this.R1_ID.Visible = false;
            // 
            // R1_MEGAFAT88_25
            // 
            this.R1_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R1_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAFAT88_25.Name = "R1_MEGAFAT88_25";
            this.R1_MEGAFAT88_25.ReadOnly = true;
            this.R1_MEGAFAT88_25.Width = 150;
            // 
            // R1_MEGAFAT88_650
            // 
            this.R1_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R1_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R1_MEGAFAT88_650.Name = "R1_MEGAFAT88_650";
            this.R1_MEGAFAT88_650.ReadOnly = true;
            this.R1_MEGAFAT88_650.Width = 150;
            // 
            // R1_MEGAFATEXTRA_25
            // 
            this.R1_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R1_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAFATEXTRA_25.Name = "R1_MEGAFATEXTRA_25";
            this.R1_MEGAFATEXTRA_25.ReadOnly = true;
            this.R1_MEGAFATEXTRA_25.Width = 150;
            // 
            // R1_MEGAFATEXTRA_650
            // 
            this.R1_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R1_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R1_MEGAFATEXTRA_650.Name = "R1_MEGAFATEXTRA_650";
            this.R1_MEGAFATEXTRA_650.ReadOnly = true;
            this.R1_MEGAFATEXTRA_650.Width = 150;
            // 
            // R1_MEGAENERGY_25
            // 
            this.R1_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R1_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAENERGY_25.Name = "R1_MEGAENERGY_25";
            this.R1_MEGAENERGY_25.ReadOnly = true;
            this.R1_MEGAENERGY_25.Width = 150;
            // 
            // R1_MEGAENERGY_650
            // 
            this.R1_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R1_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R1_MEGAENERGY_650.Name = "R1_MEGAENERGY_650";
            this.R1_MEGAENERGY_650.ReadOnly = true;
            this.R1_MEGAENERGY_650.Width = 150;
            // 
            // R1_MEGABOOST_25
            // 
            this.R1_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R1_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R1_MEGABOOST_25.Name = "R1_MEGABOOST_25";
            this.R1_MEGABOOST_25.ReadOnly = true;
            this.R1_MEGABOOST_25.Width = 150;
            // 
            // R1_MEGABOOST_650
            // 
            this.R1_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R1_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R1_MEGABOOST_650.Name = "R1_MEGABOOST_650";
            this.R1_MEGABOOST_650.ReadOnly = true;
            this.R1_MEGABOOST_650.Width = 150;
            // 
            // R1_MEGAONE_25
            // 
            this.R1_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R1_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAONE_25.Name = "R1_MEGAONE_25";
            this.R1_MEGAONE_25.ReadOnly = true;
            this.R1_MEGAONE_25.Width = 150;
            // 
            // R1_MEGAONE_650
            // 
            this.R1_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R1_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R1_MEGAONE_650.Name = "R1_MEGAONE_650";
            this.R1_MEGAONE_650.ReadOnly = true;
            this.R1_MEGAONE_650.Width = 150;
            // 
            // R1_MEGAMAX_25
            // 
            this.R1_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R1_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAMAX_25.Name = "R1_MEGAMAX_25";
            this.R1_MEGAMAX_25.ReadOnly = true;
            this.R1_MEGAMAX_25.Width = 150;
            // 
            // R1_MEGAMAX_650
            // 
            this.R1_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R1_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R1_MEGAMAX_650.Name = "R1_MEGAMAX_650";
            this.R1_MEGAMAX_650.ReadOnly = true;
            this.R1_MEGAMAX_650.Width = 150;
            // 
            // R1_MEGALAC_25
            // 
            this.R1_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R1_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R1_MEGALAC_25.Name = "R1_MEGALAC_25";
            this.R1_MEGALAC_25.ReadOnly = true;
            this.R1_MEGALAC_25.Width = 150;
            // 
            // R1_MEGALAC_650
            // 
            this.R1_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R1_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R1_MEGALAC_650.Name = "R1_MEGALAC_650";
            this.R1_MEGALAC_650.ReadOnly = true;
            this.R1_MEGALAC_650.Width = 150;
            // 
            // frmNEBEPriceMatrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNEBEPriceMatrix";
            this.Text = "NL BE Price Matrix";
            this.Load += new System.EventHandler(this.frmDKPriceMatrix_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.Panel panel7;
        private General.Controls.HeaderLabel headerLabel7;
        private General.Controls.DataGridView dgvData;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel3;
        private General.Controls.Button btnExcelExport;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGALAC_650;
    }
}