﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class MasterPage : Form
    {
        public MasterPage()
        {
            InitializeComponent();
        }


        #region Menu Items        
        
        private void customerBasicDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmCustomer objfrmCustomer = new frmCustomer();
            objfrmCustomer.MdiParent = this;
            objfrmCustomer.WindowState = FormWindowState.Maximized;
            objfrmCustomer.Width = this.Width;
            objfrmCustomer.Height = this.Height - 50;
            objfrmCustomer.MaximizeBox = false;
            objfrmCustomer.MinimizeBox = false;
            objfrmCustomer.Show();
        }       

        private void userInputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmInput objfrmInput = new frmInput();
            objfrmInput.MdiParent = this;
            objfrmInput.WindowState = FormWindowState.Maximized;
            objfrmInput.Width = this.Width;
            objfrmInput.Height = this.Height - 50;
            objfrmInput.MaximizeBox = false;
            objfrmInput.MinimizeBox = false;
            objfrmInput.Show();
        }

        private void closallforms()
        {
            foreach (Form frm in this.MdiChildren)
            {
                if (frm != Parent)
                {
                    frm.Close();
                }
            }
        }

        private void stockHandlingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmStockHandling objfrmStockHandling = new frmStockHandling();
            objfrmStockHandling.MdiParent = this;
            objfrmStockHandling.WindowState = FormWindowState.Maximized;
            objfrmStockHandling.Width = this.Width;
            objfrmStockHandling.Height = this.Height - 50;
            objfrmStockHandling.MaximizeBox = false;
            objfrmStockHandling.MinimizeBox = false;
            objfrmStockHandling.Show();
        }        

        private void exWarehouseCostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmExWarehouseCost objfrmExWarehouseCost = new frmExWarehouseCost();
            objfrmExWarehouseCost.MdiParent = this;
            objfrmExWarehouseCost.WindowState = FormWindowState.Maximized;
            objfrmExWarehouseCost.Width = this.Width;
            objfrmExWarehouseCost.Height = this.Height - 50;
            objfrmExWarehouseCost.MaximizeBox = false;
            objfrmExWarehouseCost.MinimizeBox = false;
            objfrmExWarehouseCost.Show();
        }

        private void mEGALACEURToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmMegalacEUR objfrmMegalacEUR = new frmMegalacEUR();
            objfrmMegalacEUR.MdiParent = this;
            objfrmMegalacEUR.WindowState = FormWindowState.Maximized;
            objfrmMegalacEUR.Width = this.Width;
            objfrmMegalacEUR.Height = this.Height - 50;
            objfrmMegalacEUR.MaximizeBox = false;
            objfrmMegalacEUR.MinimizeBox = false;
            objfrmMegalacEUR.Show();
        }

        private void dEPriceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmDEPriceMatrix objfrmDEPriceMatrix = new frmDEPriceMatrix();
            objfrmDEPriceMatrix.MdiParent = this;
            objfrmDEPriceMatrix.WindowState = FormWindowState.Maximized;
            objfrmDEPriceMatrix.Width = this.Width;
            objfrmDEPriceMatrix.Height = this.Height - 50;
            objfrmDEPriceMatrix.MaximizeBox = false;
            objfrmDEPriceMatrix.MinimizeBox = false;
            objfrmDEPriceMatrix.Show();
        }

        #endregion

        private void dKPriceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmDKPriceMatrix objfrmPriceMatrix = new frmDKPriceMatrix();
            objfrmPriceMatrix.MdiParent = this;
            objfrmPriceMatrix.WindowState = FormWindowState.Maximized;
            objfrmPriceMatrix.Width = this.Width;
            objfrmPriceMatrix.Height = this.Height - 50;
            objfrmPriceMatrix.MaximizeBox = false;
            objfrmPriceMatrix.MinimizeBox = false;
            objfrmPriceMatrix.Show();
        }

        private void nEBEPriceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmNEBEPriceMatrix objfrmPriceMatrix = new frmNEBEPriceMatrix();
            objfrmPriceMatrix.MdiParent = this;
            objfrmPriceMatrix.WindowState = FormWindowState.Maximized;
            objfrmPriceMatrix.Width = this.Width;
            objfrmPriceMatrix.Height = this.Height - 50;
            objfrmPriceMatrix.MaximizeBox = false;
            objfrmPriceMatrix.MinimizeBox = false;
            objfrmPriceMatrix.Show();
        }

        private void intercompanyPriceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmInterCompanyPriceMatrix objfrmInterCompanyPriceMatrix = new frmInterCompanyPriceMatrix();
            objfrmInterCompanyPriceMatrix.MdiParent = this;
            objfrmInterCompanyPriceMatrix.WindowState = FormWindowState.Maximized;
            objfrmInterCompanyPriceMatrix.Width = this.Width;
            objfrmInterCompanyPriceMatrix.Height = this.Height - 50;
            objfrmInterCompanyPriceMatrix.MaximizeBox = false;
            objfrmInterCompanyPriceMatrix.MinimizeBox = false;
            objfrmInterCompanyPriceMatrix.Show();
        }

        private void otherPriceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmOtherPriceMatrix objfrmOtherPriceMatrix = new frmOtherPriceMatrix();
            objfrmOtherPriceMatrix.MdiParent = this;
            objfrmOtherPriceMatrix.WindowState = FormWindowState.Maximized;
            objfrmOtherPriceMatrix.Width = this.Width;
            objfrmOtherPriceMatrix.Height = this.Height - 50;
            objfrmOtherPriceMatrix.MaximizeBox = false;
            objfrmOtherPriceMatrix.MinimizeBox = false;
            objfrmOtherPriceMatrix.Show();
        }

        private void otherPriceMatrixViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmOtherPriceMatrixView objfrmOtherPriceMatrixView = new frmOtherPriceMatrixView();
            objfrmOtherPriceMatrixView.MdiParent = this;
            objfrmOtherPriceMatrixView.WindowState = FormWindowState.Maximized;
            objfrmOtherPriceMatrixView.Width = this.Width;
            objfrmOtherPriceMatrixView.Height = this.Height - 50;
            objfrmOtherPriceMatrixView.MaximizeBox = false;
            objfrmOtherPriceMatrixView.MinimizeBox = false;
            objfrmOtherPriceMatrixView.Show();
        }

        private void customerDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closallforms();

            frmCustomer objfrmCustomer = new frmCustomer();
            objfrmCustomer.MdiParent = this;
            objfrmCustomer.WindowState = FormWindowState.Maximized;
            objfrmCustomer.Width = this.Width;
            objfrmCustomer.Height = this.Height - 50;
            objfrmCustomer.MaximizeBox = false;
            objfrmCustomer.MinimizeBox = false;
            objfrmCustomer.Show();
        }
    }
}
