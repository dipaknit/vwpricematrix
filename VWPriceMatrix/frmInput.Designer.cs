﻿namespace VWPriceMatrix
{
    partial class frmInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInput));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new General.Controls.Panel();
            this.headerLabel6 = new General.Controls.HeaderLabel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.panel2 = new General.Controls.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvImportDuties = new General.Controls.DataGridView();
            this.ImportDutyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_MEGAFAT88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_MEGAFATEXTRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_MEGAENERGY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_MEGABOOST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_MEGAONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvProduct = new General.Controls.DataGridView();
            this.ProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAFAT88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAFATEXTRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAENERGY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGABOOST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvDestination = new General.Controls.DataGridView();
            this.DestinationId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Destination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Freight_40_FCL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Freight_PMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAFAT88_CIF_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAFATEXTRA_CIF_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAENERGY_CIF_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGABOOST_CIF_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MEGAONE_CIF_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new General.Controls.Panel();
            this.btnDestinationCalculate = new General.Controls.Button();
            this.btnDestinationSave = new General.Controls.Button();
            this.headerLabel2 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new General.Controls.Panel();
            this.btnImportDuties = new General.Controls.Button();
            this.headerLabel7 = new General.Controls.HeaderLabel();
            this.panel1 = new General.Controls.Panel();
            this.btnProductSave = new General.Controls.Button();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new General.Controls.Panel();
            this.btnInterCompanyMargin = new General.Controls.Button();
            this.headerLabel8 = new General.Controls.HeaderLabel();
            this.panel5 = new General.Controls.Panel();
            this.btnMarginSave = new General.Controls.Button();
            this.headerLabel5 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvInterCompanyMargin = new General.Controls.DataGridView();
            this.MI_MarginId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_MEGALAC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_MEGAFAT88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_MEGAFATEXTRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_MEGAENERGY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_MEGABOOST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MI_MEGAONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMargin = new General.Controls.DataGridView();
            this.M_MarginId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_MEGALAC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_MEGAFAT88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_MEGAFATEXTRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_MEGAENERGY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_MEGABOOST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_MEGAONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new General.Controls.Panel();
            this.btnWarehouse = new General.Controls.Button();
            this.headerLabel9 = new General.Controls.HeaderLabel();
            this.panel4 = new General.Controls.Panel();
            this.btnForexSave = new General.Controls.Button();
            this.headerLabel4 = new General.Controls.HeaderLabel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvWarehouse = new General.Controls.DataGridView();
            this.WarehouseId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANTWERP = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BRAKE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BIBBY = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AARHUS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.LIVERPOOL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvForex = new General.Controls.DataGridView();
            this.ForexId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new General.Controls.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImportDuties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDestination)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInterCompanyMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMargin)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvForex)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1259, 742);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.ApplyMySettings = true;
            this.panel6.Controls.Add(this.headerLabel6);
            this.panel6.CtrlProperty = "";
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 712);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1253, 27);
            this.panel6.TabIndex = 131;
            // 
            // headerLabel6
            // 
            this.headerLabel6.ApplyMySettings = true;
            this.headerLabel6.AutoSize = true;
            this.headerLabel6.CtrlProperty = "NOTE: EUR5/MT added to 25kg Megalac for BB differential";
            this.headerLabel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.headerLabel6.Location = new System.Drawing.Point(0, 0);
            this.headerLabel6.Name = "headerLabel6";
            this.headerLabel6.Size = new System.Drawing.Size(292, 13);
            this.headerLabel6.TabIndex = 1;
            this.headerLabel6.Text = "NOTE: EUR5/MT added to 25kg Megalac for BB differential";
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.btnSave);
            this.panelHeader1.Controls.Add(this.headerLabel1);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1253, 24);
            this.panelHeader1.TabIndex = 126;
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "User Input";
            this.headerLabel1.Location = new System.Drawing.Point(9, 5);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(56, 13);
            this.headerLabel1.TabIndex = 0;
            this.headerLabel1.Text = "User Input";
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.tableLayoutPanel5);
            this.panel2.CtrlProperty = "";
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1259, 124);
            this.panel2.TabIndex = 127;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.dgvImportDuties, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.dgvProduct, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1259, 124);
            this.tableLayoutPanel5.TabIndex = 138;
            // 
            // dgvImportDuties
            // 
            this.dgvImportDuties.AllowUpDownKey = false;
            this.dgvImportDuties.AllowUserToAddRows = false;
            this.dgvImportDuties.AllowUserToDeleteRows = false;
            this.dgvImportDuties.AllowUserToResizeRows = false;
            this.dgvImportDuties.ApplyMySettings = false;
            this.dgvImportDuties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvImportDuties.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ImportDutyId,
            this.ID_Country,
            this.ID_MEGAFAT88,
            this.ID_MEGAFATEXTRA,
            this.ID_MEGAENERGY,
            this.ID_MEGABOOST,
            this.ID_MEGAONE});
            this.dgvImportDuties.ctlInfoMessage = null;
            this.dgvImportDuties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvImportDuties.Location = new System.Drawing.Point(632, 3);
            this.dgvImportDuties.MyAutoGenerateColumn = false;
            this.dgvImportDuties.MyEditGrid = true;
            this.dgvImportDuties.Name = "dgvImportDuties";
            this.dgvImportDuties.RowHeadersVisible = false;
            this.dgvImportDuties.Size = new System.Drawing.Size(624, 118);
            this.dgvImportDuties.strInfoMsgArr = null;
            this.dgvImportDuties.TabIndex = 130;
            // 
            // ImportDutyId
            // 
            this.ImportDutyId.DataPropertyName = "ImportDutyId";
            this.ImportDutyId.HeaderText = "Import Duty Id";
            this.ImportDutyId.Name = "ImportDutyId";
            this.ImportDutyId.ReadOnly = true;
            this.ImportDutyId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ImportDutyId.Visible = false;
            // 
            // ID_Country
            // 
            this.ID_Country.DataPropertyName = "Country";
            this.ID_Country.HeaderText = "Country";
            this.ID_Country.Name = "ID_Country";
            this.ID_Country.ReadOnly = true;
            this.ID_Country.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID_Country.Width = 200;
            // 
            // ID_MEGAFAT88
            // 
            this.ID_MEGAFAT88.DataPropertyName = "MEGAFAT88";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ID_MEGAFAT88.DefaultCellStyle = dataGridViewCellStyle1;
            this.ID_MEGAFAT88.HeaderText = "MEGAFAT 88";
            this.ID_MEGAFAT88.Name = "ID_MEGAFAT88";
            this.ID_MEGAFAT88.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ID_MEGAFATEXTRA
            // 
            this.ID_MEGAFATEXTRA.DataPropertyName = "MEGAFATEXTRA";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ID_MEGAFATEXTRA.DefaultCellStyle = dataGridViewCellStyle2;
            this.ID_MEGAFATEXTRA.HeaderText = "MEGAFAT EXTRA";
            this.ID_MEGAFATEXTRA.Name = "ID_MEGAFATEXTRA";
            this.ID_MEGAFATEXTRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID_MEGAFATEXTRA.Width = 125;
            // 
            // ID_MEGAENERGY
            // 
            this.ID_MEGAENERGY.DataPropertyName = "MEGAENERGY";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ID_MEGAENERGY.DefaultCellStyle = dataGridViewCellStyle3;
            this.ID_MEGAENERGY.HeaderText = "MEGA ENERGY";
            this.ID_MEGAENERGY.Name = "ID_MEGAENERGY";
            this.ID_MEGAENERGY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID_MEGAENERGY.Width = 125;
            // 
            // ID_MEGABOOST
            // 
            this.ID_MEGABOOST.DataPropertyName = "MEGABOOST";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ID_MEGABOOST.DefaultCellStyle = dataGridViewCellStyle4;
            this.ID_MEGABOOST.HeaderText = "MEGA BOOST";
            this.ID_MEGABOOST.Name = "ID_MEGABOOST";
            this.ID_MEGABOOST.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID_MEGABOOST.Width = 125;
            // 
            // ID_MEGAONE
            // 
            this.ID_MEGAONE.DataPropertyName = "MEGAONE";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ID_MEGAONE.DefaultCellStyle = dataGridViewCellStyle5;
            this.ID_MEGAONE.HeaderText = "MEGA ONE";
            this.ID_MEGAONE.Name = "ID_MEGAONE";
            this.ID_MEGAONE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvProduct
            // 
            this.dgvProduct.AllowUpDownKey = false;
            this.dgvProduct.AllowUserToAddRows = false;
            this.dgvProduct.AllowUserToDeleteRows = false;
            this.dgvProduct.AllowUserToResizeRows = false;
            this.dgvProduct.ApplyMySettings = false;
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductId,
            this.Product,
            this.MEGAFAT88,
            this.MEGAFATEXTRA,
            this.MEGAENERGY,
            this.MEGABOOST,
            this.MEGAONE});
            this.dgvProduct.ctlInfoMessage = null;
            this.dgvProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduct.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvProduct.Location = new System.Drawing.Point(3, 3);
            this.dgvProduct.MyAutoGenerateColumn = false;
            this.dgvProduct.MyEditGrid = true;
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.RowHeadersVisible = false;
            this.dgvProduct.Size = new System.Drawing.Size(623, 118);
            this.dgvProduct.strInfoMsgArr = null;
            this.dgvProduct.TabIndex = 129;
            // 
            // ProductId
            // 
            this.ProductId.DataPropertyName = "ProductId";
            this.ProductId.HeaderText = "ProductId";
            this.ProductId.Name = "ProductId";
            this.ProductId.ReadOnly = true;
            this.ProductId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProductId.Visible = false;
            // 
            // Product
            // 
            this.Product.DataPropertyName = "Product";
            this.Product.HeaderText = "Product";
            this.Product.Name = "Product";
            this.Product.ReadOnly = true;
            this.Product.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Product.Width = 200;
            // 
            // MEGAFAT88
            // 
            this.MEGAFAT88.DataPropertyName = "MEGAFAT88";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MEGAFAT88.DefaultCellStyle = dataGridViewCellStyle6;
            this.MEGAFAT88.HeaderText = "MEGAFAT 88";
            this.MEGAFAT88.Name = "MEGAFAT88";
            this.MEGAFAT88.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // MEGAFATEXTRA
            // 
            this.MEGAFATEXTRA.DataPropertyName = "MEGAFATEXTRA";
            this.MEGAFATEXTRA.HeaderText = "MEGAFAT EXTRA";
            this.MEGAFATEXTRA.Name = "MEGAFATEXTRA";
            this.MEGAFATEXTRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGAFATEXTRA.Width = 125;
            // 
            // MEGAENERGY
            // 
            this.MEGAENERGY.DataPropertyName = "MEGAENERGY";
            this.MEGAENERGY.HeaderText = "MEGA ENERGY";
            this.MEGAENERGY.Name = "MEGAENERGY";
            this.MEGAENERGY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGAENERGY.Width = 125;
            // 
            // MEGABOOST
            // 
            this.MEGABOOST.DataPropertyName = "MEGABOOST";
            this.MEGABOOST.HeaderText = "MEGA BOOST";
            this.MEGABOOST.Name = "MEGABOOST";
            this.MEGABOOST.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGABOOST.Width = 125;
            // 
            // MEGAONE
            // 
            this.MEGAONE.DataPropertyName = "MEGAONE";
            this.MEGAONE.HeaderText = "MEGA ONE";
            this.MEGAONE.Name = "MEGAONE";
            this.MEGAONE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel2.Controls.Add(this.dgvDestination, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 217);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1253, 243);
            this.tableLayoutPanel2.TabIndex = 135;
            // 
            // dgvDestination
            // 
            this.dgvDestination.AllowUpDownKey = false;
            this.dgvDestination.AllowUserToAddRows = false;
            this.dgvDestination.AllowUserToDeleteRows = false;
            this.dgvDestination.AllowUserToResizeRows = false;
            this.dgvDestination.ApplyMySettings = false;
            this.dgvDestination.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDestination.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DestinationId,
            this.Destination,
            this.Country,
            this.Freight_40_FCL,
            this.Freight_PMT,
            this.MEGAFAT88_CIF_USD,
            this.MEGAFATEXTRA_CIF_USD,
            this.MEGAENERGY_CIF_USD,
            this.MEGABOOST_CIF_USD,
            this.MEGAONE_CIF_USD});
            this.dgvDestination.ctlInfoMessage = null;
            this.dgvDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDestination.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvDestination.Location = new System.Drawing.Point(3, 3);
            this.dgvDestination.MultiSelect = false;
            this.dgvDestination.MyAutoGenerateColumn = false;
            this.dgvDestination.MyEditGrid = true;
            this.dgvDestination.Name = "dgvDestination";
            this.dgvDestination.RowHeadersVisible = false;
            this.dgvDestination.Size = new System.Drawing.Size(1247, 237);
            this.dgvDestination.strInfoMsgArr = null;
            this.dgvDestination.TabIndex = 129;
            // 
            // DestinationId
            // 
            this.DestinationId.DataPropertyName = "DestinationId";
            this.DestinationId.HeaderText = "DestinationId";
            this.DestinationId.Name = "DestinationId";
            this.DestinationId.ReadOnly = true;
            this.DestinationId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DestinationId.Visible = false;
            // 
            // Destination
            // 
            this.Destination.DataPropertyName = "Destination";
            this.Destination.HeaderText = "Destination";
            this.Destination.Name = "Destination";
            this.Destination.ReadOnly = true;
            this.Destination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            this.Country.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Freight_40_FCL
            // 
            this.Freight_40_FCL.DataPropertyName = "Freight_40_FCL";
            this.Freight_40_FCL.HeaderText = "Freight (40\' FCL)";
            this.Freight_40_FCL.Name = "Freight_40_FCL";
            this.Freight_40_FCL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Freight_40_FCL.Width = 110;
            // 
            // Freight_PMT
            // 
            this.Freight_PMT.DataPropertyName = "Freight_PMT";
            this.Freight_PMT.HeaderText = "Freight (PMT)";
            this.Freight_PMT.Name = "Freight_PMT";
            this.Freight_PMT.ReadOnly = true;
            this.Freight_PMT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // MEGAFAT88_CIF_USD
            // 
            this.MEGAFAT88_CIF_USD.DataPropertyName = "MEGAFAT88_CIF_USD";
            this.MEGAFAT88_CIF_USD.HeaderText = "MEGAFAT 88 CIF (USD)";
            this.MEGAFAT88_CIF_USD.Name = "MEGAFAT88_CIF_USD";
            this.MEGAFAT88_CIF_USD.ReadOnly = true;
            this.MEGAFAT88_CIF_USD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGAFAT88_CIF_USD.Width = 150;
            // 
            // MEGAFATEXTRA_CIF_USD
            // 
            this.MEGAFATEXTRA_CIF_USD.DataPropertyName = "MEGAFATEXTRA_CIF_USD";
            this.MEGAFATEXTRA_CIF_USD.HeaderText = "MEGAFAT EXTRA CIF (USD)";
            this.MEGAFATEXTRA_CIF_USD.Name = "MEGAFATEXTRA_CIF_USD";
            this.MEGAFATEXTRA_CIF_USD.ReadOnly = true;
            this.MEGAFATEXTRA_CIF_USD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGAFATEXTRA_CIF_USD.Width = 180;
            // 
            // MEGAENERGY_CIF_USD
            // 
            this.MEGAENERGY_CIF_USD.DataPropertyName = "MEGAENERGY_CIF_USD";
            this.MEGAENERGY_CIF_USD.HeaderText = "MEGA ENERGY CIF (USD)";
            this.MEGAENERGY_CIF_USD.Name = "MEGAENERGY_CIF_USD";
            this.MEGAENERGY_CIF_USD.ReadOnly = true;
            this.MEGAENERGY_CIF_USD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGAENERGY_CIF_USD.Width = 170;
            // 
            // MEGABOOST_CIF_USD
            // 
            this.MEGABOOST_CIF_USD.DataPropertyName = "MEGABOOST_CIF_USD";
            this.MEGABOOST_CIF_USD.HeaderText = "MEGA BOOST CIF (USD)";
            this.MEGABOOST_CIF_USD.Name = "MEGABOOST_CIF_USD";
            this.MEGABOOST_CIF_USD.ReadOnly = true;
            this.MEGABOOST_CIF_USD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGABOOST_CIF_USD.Width = 160;
            // 
            // MEGAONE_CIF_USD
            // 
            this.MEGAONE_CIF_USD.DataPropertyName = "MEGAONE_CIF_USD";
            this.MEGAONE_CIF_USD.HeaderText = "MEGA ONE CIF (USD)";
            this.MEGAONE_CIF_USD.Name = "MEGAONE_CIF_USD";
            this.MEGAONE_CIF_USD.ReadOnly = true;
            this.MEGAONE_CIF_USD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MEGAONE_CIF_USD.Width = 140;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel3.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 187);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1253, 24);
            this.tableLayoutPanel3.TabIndex = 136;
            // 
            // panel3
            // 
            this.panel3.ApplyMySettings = true;
            this.panel3.Controls.Add(this.btnDestinationCalculate);
            this.panel3.Controls.Add(this.btnDestinationSave);
            this.panel3.Controls.Add(this.headerLabel2);
            this.panel3.CtrlProperty = "";
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel3.Size = new System.Drawing.Size(1253, 24);
            this.panel3.TabIndex = 131;
            // 
            // btnDestinationCalculate
            // 
            this.btnDestinationCalculate.ApplyMySettings = true;
            this.btnDestinationCalculate.CtrlProperty = "Calculate";
            this.btnDestinationCalculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDestinationCalculate.Location = new System.Drawing.Point(1100, 0);
            this.btnDestinationCalculate.Name = "btnDestinationCalculate";
            this.btnDestinationCalculate.Size = new System.Drawing.Size(75, 24);
            this.btnDestinationCalculate.TabIndex = 25;
            this.btnDestinationCalculate.Text = "Calculate";
            this.btnDestinationCalculate.UseVisualStyleBackColor = true;
            this.btnDestinationCalculate.Visible = false;
            this.btnDestinationCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnDestinationSave
            // 
            this.btnDestinationSave.ApplyMySettings = true;
            this.btnDestinationSave.CtrlProperty = "Save";
            this.btnDestinationSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDestinationSave.Location = new System.Drawing.Point(1175, 0);
            this.btnDestinationSave.Name = "btnDestinationSave";
            this.btnDestinationSave.Size = new System.Drawing.Size(75, 24);
            this.btnDestinationSave.TabIndex = 24;
            this.btnDestinationSave.Text = "Save";
            this.btnDestinationSave.UseVisualStyleBackColor = true;
            this.btnDestinationSave.Visible = false;
            this.btnDestinationSave.Click += new System.EventHandler(this.btnDestinationSave_Click);
            // 
            // headerLabel2
            // 
            this.headerLabel2.ApplyMySettings = true;
            this.headerLabel2.AutoSize = true;
            this.headerLabel2.CtrlProperty = "Destination";
            this.headerLabel2.Location = new System.Drawing.Point(8, 6);
            this.headerLabel2.Name = "headerLabel2";
            this.headerLabel2.Size = new System.Drawing.Size(60, 13);
            this.headerLabel2.TabIndex = 0;
            this.headerLabel2.Text = "Destination";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 33);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1253, 24);
            this.tableLayoutPanel4.TabIndex = 137;
            // 
            // panel7
            // 
            this.panel7.ApplyMySettings = true;
            this.panel7.Controls.Add(this.btnImportDuties);
            this.panel7.Controls.Add(this.headerLabel7);
            this.panel7.CtrlProperty = "";
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(626, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel7.Size = new System.Drawing.Size(627, 24);
            this.panel7.TabIndex = 131;
            // 
            // btnImportDuties
            // 
            this.btnImportDuties.ApplyMySettings = true;
            this.btnImportDuties.CtrlProperty = "Save";
            this.btnImportDuties.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnImportDuties.Location = new System.Drawing.Point(549, 0);
            this.btnImportDuties.Name = "btnImportDuties";
            this.btnImportDuties.Size = new System.Drawing.Size(75, 24);
            this.btnImportDuties.TabIndex = 23;
            this.btnImportDuties.Text = "Save";
            this.btnImportDuties.UseVisualStyleBackColor = true;
            this.btnImportDuties.Visible = false;
            this.btnImportDuties.Click += new System.EventHandler(this.btnImportDuties_Click);
            // 
            // headerLabel7
            // 
            this.headerLabel7.ApplyMySettings = true;
            this.headerLabel7.AutoSize = true;
            this.headerLabel7.CtrlProperty = "Import Duties";
            this.headerLabel7.Location = new System.Drawing.Point(8, 6);
            this.headerLabel7.Name = "headerLabel7";
            this.headerLabel7.Size = new System.Drawing.Size(69, 13);
            this.headerLabel7.TabIndex = 1;
            this.headerLabel7.Text = "Import Duties";
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.btnProductSave);
            this.panel1.Controls.Add(this.headerLabel3);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel1.Size = new System.Drawing.Size(626, 24);
            this.panel1.TabIndex = 130;
            // 
            // btnProductSave
            // 
            this.btnProductSave.ApplyMySettings = true;
            this.btnProductSave.CtrlProperty = "Save";
            this.btnProductSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProductSave.Location = new System.Drawing.Point(548, 0);
            this.btnProductSave.Name = "btnProductSave";
            this.btnProductSave.Size = new System.Drawing.Size(75, 24);
            this.btnProductSave.TabIndex = 23;
            this.btnProductSave.Text = "Save";
            this.btnProductSave.UseVisualStyleBackColor = true;
            this.btnProductSave.Visible = false;
            this.btnProductSave.Click += new System.EventHandler(this.btnProductSave_Click);
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "Product";
            this.headerLabel3.Location = new System.Drawing.Point(8, 6);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(44, 13);
            this.headerLabel3.TabIndex = 1;
            this.headerLabel3.Text = "Product";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 620);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1253, 24);
            this.tableLayoutPanel6.TabIndex = 138;
            // 
            // panel8
            // 
            this.panel8.ApplyMySettings = true;
            this.panel8.Controls.Add(this.btnInterCompanyMargin);
            this.panel8.Controls.Add(this.headerLabel8);
            this.panel8.CtrlProperty = "";
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(626, 0);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel8.Size = new System.Drawing.Size(627, 24);
            this.panel8.TabIndex = 135;
            // 
            // btnInterCompanyMargin
            // 
            this.btnInterCompanyMargin.ApplyMySettings = true;
            this.btnInterCompanyMargin.CtrlProperty = "Save";
            this.btnInterCompanyMargin.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnInterCompanyMargin.Location = new System.Drawing.Point(549, 0);
            this.btnInterCompanyMargin.Name = "btnInterCompanyMargin";
            this.btnInterCompanyMargin.Size = new System.Drawing.Size(75, 24);
            this.btnInterCompanyMargin.TabIndex = 24;
            this.btnInterCompanyMargin.Text = "Save";
            this.btnInterCompanyMargin.UseVisualStyleBackColor = true;
            this.btnInterCompanyMargin.Visible = false;
            this.btnInterCompanyMargin.Click += new System.EventHandler(this.btnInterCompanyMargin_Click);
            // 
            // headerLabel8
            // 
            this.headerLabel8.ApplyMySettings = true;
            this.headerLabel8.AutoSize = true;
            this.headerLabel8.CtrlProperty = "Inter Company Margin";
            this.headerLabel8.Location = new System.Drawing.Point(8, 6);
            this.headerLabel8.Name = "headerLabel8";
            this.headerLabel8.Size = new System.Drawing.Size(110, 13);
            this.headerLabel8.TabIndex = 0;
            this.headerLabel8.Text = "Inter Company Margin";
            // 
            // panel5
            // 
            this.panel5.ApplyMySettings = true;
            this.panel5.Controls.Add(this.btnMarginSave);
            this.panel5.Controls.Add(this.headerLabel5);
            this.panel5.CtrlProperty = "";
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel5.Size = new System.Drawing.Size(626, 24);
            this.panel5.TabIndex = 134;
            // 
            // btnMarginSave
            // 
            this.btnMarginSave.ApplyMySettings = true;
            this.btnMarginSave.CtrlProperty = "Save";
            this.btnMarginSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMarginSave.Location = new System.Drawing.Point(548, 0);
            this.btnMarginSave.Name = "btnMarginSave";
            this.btnMarginSave.Size = new System.Drawing.Size(75, 24);
            this.btnMarginSave.TabIndex = 24;
            this.btnMarginSave.Text = "Save";
            this.btnMarginSave.UseVisualStyleBackColor = true;
            this.btnMarginSave.Visible = false;
            this.btnMarginSave.Click += new System.EventHandler(this.btnMarginSave_Click);
            // 
            // headerLabel5
            // 
            this.headerLabel5.ApplyMySettings = true;
            this.headerLabel5.AutoSize = true;
            this.headerLabel5.CtrlProperty = "Margin";
            this.headerLabel5.Location = new System.Drawing.Point(8, 6);
            this.headerLabel5.Name = "headerLabel5";
            this.headerLabel5.Size = new System.Drawing.Size(39, 13);
            this.headerLabel5.TabIndex = 0;
            this.headerLabel5.Text = "Margin";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.dgvInterCompanyMargin, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.dgvMargin, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 650);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1253, 56);
            this.tableLayoutPanel7.TabIndex = 139;
            // 
            // dgvInterCompanyMargin
            // 
            this.dgvInterCompanyMargin.AllowUpDownKey = false;
            this.dgvInterCompanyMargin.AllowUserToAddRows = false;
            this.dgvInterCompanyMargin.AllowUserToDeleteRows = false;
            this.dgvInterCompanyMargin.AllowUserToResizeRows = false;
            this.dgvInterCompanyMargin.ApplyMySettings = false;
            this.dgvInterCompanyMargin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInterCompanyMargin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MI_MarginId,
            this.MI_Type,
            this.MI_MEGALAC,
            this.MI_MEGAFAT88,
            this.MI_MEGAFATEXTRA,
            this.MI_MEGAENERGY,
            this.MI_MEGABOOST,
            this.MI_MEGAONE});
            this.dgvInterCompanyMargin.ctlInfoMessage = null;
            this.dgvInterCompanyMargin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInterCompanyMargin.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvInterCompanyMargin.Location = new System.Drawing.Point(629, 3);
            this.dgvInterCompanyMargin.MyAutoGenerateColumn = false;
            this.dgvInterCompanyMargin.MyEditGrid = true;
            this.dgvInterCompanyMargin.Name = "dgvInterCompanyMargin";
            this.dgvInterCompanyMargin.RowHeadersVisible = false;
            this.dgvInterCompanyMargin.Size = new System.Drawing.Size(621, 50);
            this.dgvInterCompanyMargin.strInfoMsgArr = null;
            this.dgvInterCompanyMargin.TabIndex = 132;
            // 
            // MI_MarginId
            // 
            this.MI_MarginId.DataPropertyName = "MarginId";
            this.MI_MarginId.HeaderText = "MarginId";
            this.MI_MarginId.Name = "MI_MarginId";
            this.MI_MarginId.ReadOnly = true;
            this.MI_MarginId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MI_MarginId.Visible = false;
            // 
            // MI_Type
            // 
            this.MI_Type.DataPropertyName = "Type";
            this.MI_Type.HeaderText = "Type";
            this.MI_Type.Name = "MI_Type";
            this.MI_Type.ReadOnly = true;
            this.MI_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MI_Type.Width = 200;
            // 
            // MI_MEGALAC
            // 
            this.MI_MEGALAC.DataPropertyName = "MEGALAC";
            this.MI_MEGALAC.HeaderText = "MEGALAC (%)";
            this.MI_MEGALAC.Name = "MI_MEGALAC";
            this.MI_MEGALAC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // MI_MEGAFAT88
            // 
            this.MI_MEGAFAT88.DataPropertyName = "MEGAFAT88";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MI_MEGAFAT88.DefaultCellStyle = dataGridViewCellStyle7;
            this.MI_MEGAFAT88.HeaderText = "MEGAFAT 88 (PMT)";
            this.MI_MEGAFAT88.Name = "MI_MEGAFAT88";
            this.MI_MEGAFAT88.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // MI_MEGAFATEXTRA
            // 
            this.MI_MEGAFATEXTRA.DataPropertyName = "MEGAFATEXTRA";
            this.MI_MEGAFATEXTRA.HeaderText = "MEGAFAT EXTRA (PMT)";
            this.MI_MEGAFATEXTRA.Name = "MI_MEGAFATEXTRA";
            this.MI_MEGAFATEXTRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MI_MEGAFATEXTRA.Width = 125;
            // 
            // MI_MEGAENERGY
            // 
            this.MI_MEGAENERGY.DataPropertyName = "MEGAENERGY";
            this.MI_MEGAENERGY.HeaderText = "MEGA ENERGY (PMT)";
            this.MI_MEGAENERGY.Name = "MI_MEGAENERGY";
            this.MI_MEGAENERGY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MI_MEGAENERGY.Width = 125;
            // 
            // MI_MEGABOOST
            // 
            this.MI_MEGABOOST.DataPropertyName = "MEGABOOST";
            this.MI_MEGABOOST.HeaderText = "MEGA BOOST (PMT)";
            this.MI_MEGABOOST.Name = "MI_MEGABOOST";
            this.MI_MEGABOOST.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MI_MEGABOOST.Width = 125;
            // 
            // MI_MEGAONE
            // 
            this.MI_MEGAONE.DataPropertyName = "MEGAONE";
            this.MI_MEGAONE.HeaderText = "MEGA ONE (PMT)";
            this.MI_MEGAONE.Name = "MI_MEGAONE";
            this.MI_MEGAONE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvMargin
            // 
            this.dgvMargin.AllowUpDownKey = false;
            this.dgvMargin.AllowUserToAddRows = false;
            this.dgvMargin.AllowUserToDeleteRows = false;
            this.dgvMargin.AllowUserToResizeRows = false;
            this.dgvMargin.ApplyMySettings = false;
            this.dgvMargin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMargin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.M_MarginId,
            this.M_TYPE,
            this.M_MEGALAC,
            this.M_MEGAFAT88,
            this.M_MEGAFATEXTRA,
            this.M_MEGAENERGY,
            this.M_MEGABOOST,
            this.M_MEGAONE});
            this.dgvMargin.ctlInfoMessage = null;
            this.dgvMargin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMargin.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvMargin.Location = new System.Drawing.Point(3, 3);
            this.dgvMargin.MyAutoGenerateColumn = false;
            this.dgvMargin.MyEditGrid = true;
            this.dgvMargin.Name = "dgvMargin";
            this.dgvMargin.RowHeadersVisible = false;
            this.dgvMargin.Size = new System.Drawing.Size(620, 50);
            this.dgvMargin.strInfoMsgArr = null;
            this.dgvMargin.TabIndex = 131;
            // 
            // M_MarginId
            // 
            this.M_MarginId.DataPropertyName = "MarginId";
            this.M_MarginId.HeaderText = "MarginId";
            this.M_MarginId.Name = "M_MarginId";
            this.M_MarginId.ReadOnly = true;
            this.M_MarginId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.M_MarginId.Visible = false;
            // 
            // M_TYPE
            // 
            this.M_TYPE.DataPropertyName = "Type";
            this.M_TYPE.HeaderText = "Type";
            this.M_TYPE.Name = "M_TYPE";
            this.M_TYPE.ReadOnly = true;
            this.M_TYPE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.M_TYPE.Width = 200;
            // 
            // M_MEGALAC
            // 
            this.M_MEGALAC.DataPropertyName = "MEGALAC";
            this.M_MEGALAC.HeaderText = "MEGALAC";
            this.M_MEGALAC.Name = "M_MEGALAC";
            this.M_MEGALAC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // M_MEGAFAT88
            // 
            this.M_MEGAFAT88.DataPropertyName = "MEGAFAT88";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.M_MEGAFAT88.DefaultCellStyle = dataGridViewCellStyle8;
            this.M_MEGAFAT88.HeaderText = "MEGAFAT 88";
            this.M_MEGAFAT88.Name = "M_MEGAFAT88";
            this.M_MEGAFAT88.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // M_MEGAFATEXTRA
            // 
            this.M_MEGAFATEXTRA.DataPropertyName = "MEGAFATEXTRA";
            this.M_MEGAFATEXTRA.HeaderText = "MEGAFAT EXTRA";
            this.M_MEGAFATEXTRA.Name = "M_MEGAFATEXTRA";
            this.M_MEGAFATEXTRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.M_MEGAFATEXTRA.Width = 125;
            // 
            // M_MEGAENERGY
            // 
            this.M_MEGAENERGY.DataPropertyName = "MEGAENERGY";
            this.M_MEGAENERGY.HeaderText = "MEGA ENERGY";
            this.M_MEGAENERGY.Name = "M_MEGAENERGY";
            this.M_MEGAENERGY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.M_MEGAENERGY.Width = 125;
            // 
            // M_MEGABOOST
            // 
            this.M_MEGABOOST.DataPropertyName = "MEGABOOST";
            this.M_MEGABOOST.HeaderText = "MEGA BOOST";
            this.M_MEGABOOST.Name = "M_MEGABOOST";
            this.M_MEGABOOST.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.M_MEGABOOST.Width = 125;
            // 
            // M_MEGAONE
            // 
            this.M_MEGAONE.DataPropertyName = "MEGAONE";
            this.M_MEGAONE.HeaderText = "MEGA ONE";
            this.M_MEGAONE.Name = "M_MEGAONE";
            this.M_MEGAONE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.panel9, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 466);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1253, 24);
            this.tableLayoutPanel8.TabIndex = 140;
            // 
            // panel9
            // 
            this.panel9.ApplyMySettings = true;
            this.panel9.Controls.Add(this.btnWarehouse);
            this.panel9.Controls.Add(this.headerLabel9);
            this.panel9.CtrlProperty = "";
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(626, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(0);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel9.Size = new System.Drawing.Size(627, 24);
            this.panel9.TabIndex = 133;
            // 
            // btnWarehouse
            // 
            this.btnWarehouse.ApplyMySettings = true;
            this.btnWarehouse.CtrlProperty = "Save";
            this.btnWarehouse.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnWarehouse.Location = new System.Drawing.Point(549, 0);
            this.btnWarehouse.Name = "btnWarehouse";
            this.btnWarehouse.Size = new System.Drawing.Size(75, 24);
            this.btnWarehouse.TabIndex = 24;
            this.btnWarehouse.Text = "Save";
            this.btnWarehouse.UseVisualStyleBackColor = true;
            this.btnWarehouse.Visible = false;
            this.btnWarehouse.Click += new System.EventHandler(this.btnWarehouse_Click);
            // 
            // headerLabel9
            // 
            this.headerLabel9.ApplyMySettings = true;
            this.headerLabel9.AutoSize = true;
            this.headerLabel9.CtrlProperty = "Warehouse";
            this.headerLabel9.Location = new System.Drawing.Point(8, 6);
            this.headerLabel9.Name = "headerLabel9";
            this.headerLabel9.Size = new System.Drawing.Size(62, 13);
            this.headerLabel9.TabIndex = 0;
            this.headerLabel9.Text = "Warehouse";
            // 
            // panel4
            // 
            this.panel4.ApplyMySettings = true;
            this.panel4.Controls.Add(this.btnForexSave);
            this.panel4.Controls.Add(this.headerLabel4);
            this.panel4.CtrlProperty = "";
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel4.Size = new System.Drawing.Size(626, 24);
            this.panel4.TabIndex = 132;
            // 
            // btnForexSave
            // 
            this.btnForexSave.ApplyMySettings = true;
            this.btnForexSave.CtrlProperty = "Save";
            this.btnForexSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnForexSave.Location = new System.Drawing.Point(548, 0);
            this.btnForexSave.Name = "btnForexSave";
            this.btnForexSave.Size = new System.Drawing.Size(75, 24);
            this.btnForexSave.TabIndex = 24;
            this.btnForexSave.Text = "Save";
            this.btnForexSave.UseVisualStyleBackColor = true;
            this.btnForexSave.Visible = false;
            this.btnForexSave.Click += new System.EventHandler(this.btnForexSave_Click);
            // 
            // headerLabel4
            // 
            this.headerLabel4.ApplyMySettings = true;
            this.headerLabel4.AutoSize = true;
            this.headerLabel4.CtrlProperty = "Forex Input Area";
            this.headerLabel4.Location = new System.Drawing.Point(8, 6);
            this.headerLabel4.Name = "headerLabel4";
            this.headerLabel4.Size = new System.Drawing.Size(85, 13);
            this.headerLabel4.TabIndex = 0;
            this.headerLabel4.Text = "Forex Input Area";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.dgvWarehouse, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.dgvForex, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 496);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1253, 118);
            this.tableLayoutPanel9.TabIndex = 141;
            // 
            // dgvWarehouse
            // 
            this.dgvWarehouse.AllowUpDownKey = false;
            this.dgvWarehouse.AllowUserToAddRows = false;
            this.dgvWarehouse.AllowUserToDeleteRows = false;
            this.dgvWarehouse.AllowUserToResizeRows = false;
            this.dgvWarehouse.ApplyMySettings = false;
            this.dgvWarehouse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWarehouse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WarehouseId,
            this.ANTWERP,
            this.BRAKE,
            this.BIBBY,
            this.AARHUS,
            this.LIVERPOOL});
            this.dgvWarehouse.ctlInfoMessage = null;
            this.dgvWarehouse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWarehouse.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvWarehouse.Location = new System.Drawing.Point(629, 3);
            this.dgvWarehouse.MyAutoGenerateColumn = false;
            this.dgvWarehouse.MyEditGrid = true;
            this.dgvWarehouse.Name = "dgvWarehouse";
            this.dgvWarehouse.RowHeadersVisible = false;
            this.dgvWarehouse.Size = new System.Drawing.Size(621, 112);
            this.dgvWarehouse.strInfoMsgArr = null;
            this.dgvWarehouse.TabIndex = 134;
            // 
            // WarehouseId
            // 
            this.WarehouseId.DataPropertyName = "WarehouseId";
            this.WarehouseId.HeaderText = "WarehouseId";
            this.WarehouseId.Name = "WarehouseId";
            this.WarehouseId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WarehouseId.Visible = false;
            // 
            // ANTWERP
            // 
            this.ANTWERP.DataPropertyName = "ANTWERP";
            this.ANTWERP.HeaderText = "ANTWERP";
            this.ANTWERP.Name = "ANTWERP";
            this.ANTWERP.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ANTWERP.Width = 110;
            // 
            // BRAKE
            // 
            this.BRAKE.DataPropertyName = "BRAKE";
            this.BRAKE.HeaderText = "BRAKE";
            this.BRAKE.Name = "BRAKE";
            this.BRAKE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BRAKE.Width = 110;
            // 
            // BIBBY
            // 
            this.BIBBY.DataPropertyName = "BIBBY";
            this.BIBBY.HeaderText = "BIBBY";
            this.BIBBY.Name = "BIBBY";
            this.BIBBY.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BIBBY.Width = 110;
            // 
            // AARHUS
            // 
            this.AARHUS.DataPropertyName = "AARHUS";
            this.AARHUS.HeaderText = "AARHUS";
            this.AARHUS.Name = "AARHUS";
            this.AARHUS.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AARHUS.Width = 110;
            // 
            // LIVERPOOL
            // 
            this.LIVERPOOL.DataPropertyName = "LIVERPOOL";
            this.LIVERPOOL.HeaderText = "LIVERPOOL";
            this.LIVERPOOL.Name = "LIVERPOOL";
            this.LIVERPOOL.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.LIVERPOOL.Width = 110;
            // 
            // dgvForex
            // 
            this.dgvForex.AllowUpDownKey = false;
            this.dgvForex.AllowUserToAddRows = false;
            this.dgvForex.AllowUserToDeleteRows = false;
            this.dgvForex.AllowUserToResizeRows = false;
            this.dgvForex.ApplyMySettings = false;
            this.dgvForex.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvForex.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ForexId,
            this.Type,
            this.Currency,
            this.Rate});
            this.dgvForex.ctlInfoMessage = null;
            this.dgvForex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvForex.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvForex.Location = new System.Drawing.Point(3, 3);
            this.dgvForex.MyAutoGenerateColumn = false;
            this.dgvForex.MyEditGrid = true;
            this.dgvForex.Name = "dgvForex";
            this.dgvForex.RowHeadersVisible = false;
            this.dgvForex.Size = new System.Drawing.Size(620, 112);
            this.dgvForex.strInfoMsgArr = null;
            this.dgvForex.TabIndex = 133;
            // 
            // ForexId
            // 
            this.ForexId.DataPropertyName = "ForexId";
            this.ForexId.HeaderText = "ForexId";
            this.ForexId.Name = "ForexId";
            this.ForexId.ReadOnly = true;
            this.ForexId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ForexId.Visible = false;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Type.Width = 200;
            // 
            // Currency
            // 
            this.Currency.DataPropertyName = "Currency";
            this.Currency.HeaderText = "Currency";
            this.Currency.Name = "Currency";
            this.Currency.ReadOnly = true;
            this.Currency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Currency.Width = 200;
            // 
            // Rate
            // 
            this.Rate.DataPropertyName = "Rate";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Rate.DefaultCellStyle = dataGridViewCellStyle9;
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnSave
            // 
            this.btnSave.ApplyMySettings = true;
            this.btnSave.CtrlProperty = "Save";
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Location = new System.Drawing.Point(1178, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 24);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 742);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmInput";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Input";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmInput_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImportDuties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDestination)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInterCompanyMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMargin)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvForex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel1;
        private General.Controls.Panel panel2;
        private General.Controls.DataGridView dgvProduct;
        private General.Controls.DataGridView dgvDestination;
        private General.Controls.Panel panel1;
        private General.Controls.Panel panel3;
        private General.Controls.HeaderLabel headerLabel2;
        private General.Controls.HeaderLabel headerLabel3;
        private General.Controls.Button btnProductSave;
        private General.Controls.Panel panel4;
        private General.Controls.Button btnForexSave;
        private General.Controls.HeaderLabel headerLabel4;
        private General.Controls.DataGridView dgvForex;
        private General.Controls.Button btnDestinationCalculate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private General.Controls.Button btnDestinationSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn DestinationId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Destination;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Freight_40_FCL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Freight_PMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAFAT88_CIF_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAFATEXTRA_CIF_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAENERGY_CIF_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGABOOST_CIF_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAONE_CIF_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAFAT88;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAFATEXTRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAENERGY;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGABOOST;
        private System.Windows.Forms.DataGridViewTextBoxColumn MEGAONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ForexId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private General.Controls.Panel panel6;
        private General.Controls.HeaderLabel headerLabel6;
        private General.Controls.Panel panel7;
        private General.Controls.Button btnImportDuties;
        private General.Controls.HeaderLabel headerLabel7;
        private General.Controls.DataGridView dgvImportDuties;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportDutyId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_MEGAFAT88;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_MEGAFATEXTRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_MEGAENERGY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_MEGABOOST;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_MEGAONE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private General.Controls.Panel panel5;
        private General.Controls.Button btnMarginSave;
        private General.Controls.HeaderLabel headerLabel5;
        private General.Controls.Panel panel8;
        private General.Controls.Button btnInterCompanyMargin;
        private General.Controls.HeaderLabel headerLabel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private General.Controls.DataGridView dgvMargin;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MarginId;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MEGALAC;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MEGAFAT88;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MEGAFATEXTRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MEGAENERGY;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MEGABOOST;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_MEGAONE;
        private General.Controls.DataGridView dgvInterCompanyMargin;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MarginId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MEGALAC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MEGAFAT88;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MEGAFATEXTRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MEGAENERGY;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MEGABOOST;
        private System.Windows.Forms.DataGridViewTextBoxColumn MI_MEGAONE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private General.Controls.Panel panel9;
        private General.Controls.Button btnWarehouse;
        private General.Controls.HeaderLabel headerLabel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private General.Controls.DataGridView dgvWarehouse;
        private System.Windows.Forms.DataGridViewTextBoxColumn WarehouseId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ANTWERP;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BRAKE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BIBBY;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AARHUS;
        private System.Windows.Forms.DataGridViewCheckBoxColumn LIVERPOOL;
        private General.Controls.Button btnSave;
    }
}