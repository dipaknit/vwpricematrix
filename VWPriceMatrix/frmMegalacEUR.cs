﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class frmMegalacEUR : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmMegalacEUR()
        {
            InitializeComponent();
        }

        private void frmMegalacEUR_Load(object sender, EventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            string strQuery = " SELECT * FROM Megalac_ExFactory WHERE isdeleted = 0 Order by Id ";
            DataTable dtProduct = objclsSqlLayer.GetDataTable(strQuery);
            dgvMegalacEUR.AutoGenerateColumns = false;
            dgvMegalacEUR.DataSource = dtProduct;
        }

        private void dgvMegalacEUR_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow row = dgvMegalacEUR.Rows[e.RowIndex];

            if (
                Support.ConvertToString(dgvMegalacEUR.Rows[e.RowIndex].Cells["Id"].Value).ToUpper().Equals("3", StringComparison.CurrentCultureIgnoreCase) ||
                Support.ConvertToString(dgvMegalacEUR.Rows[e.RowIndex].Cells["Id"].Value).ToUpper().Equals("11", StringComparison.CurrentCultureIgnoreCase) ||
                Support.ConvertToString(dgvMegalacEUR.Rows[e.RowIndex].Cells["Id"].Value).ToUpper().Equals("15", StringComparison.CurrentCultureIgnoreCase) ||
                Support.ConvertToString(dgvMegalacEUR.Rows[e.RowIndex].Cells["Id"].Value).ToUpper().Equals("18", StringComparison.CurrentCultureIgnoreCase) ||
                Support.ConvertToString(dgvMegalacEUR.Rows[e.RowIndex].Cells["Id"].Value).ToUpper().Equals("22", StringComparison.CurrentCultureIgnoreCase)
                )
            {
                row.Cells["Amount"].ReadOnly = false;
                row.Cells["Amount"].Style.BackColor = System.Drawing.Color.FromArgb(255, 192, 192);
            }
            else
            {
                row.ReadOnly = true;
            }
        }

        private void btnMegalacExFactory_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvMegalacEUR.Rows)
            {
                if (
                    Support.ConvertToString(row.Cells["Id"].Value).ToUpper().Equals("3", StringComparison.CurrentCultureIgnoreCase) ||
                    Support.ConvertToString(row.Cells["Id"].Value).ToUpper().Equals("11", StringComparison.CurrentCultureIgnoreCase) ||
                    Support.ConvertToString(row.Cells["Id"].Value).ToUpper().Equals("15", StringComparison.CurrentCultureIgnoreCase) ||
                    Support.ConvertToString(row.Cells["Id"].Value).ToUpper().Equals("18", StringComparison.CurrentCultureIgnoreCase) ||
                    Support.ConvertToString(row.Cells["Id"].Value).ToUpper().Equals("22", StringComparison.CurrentCultureIgnoreCase)
                    )
                {
                    string[] Param = { "@Id", "@Amount", "@UpdatedBy" };
                    string[] Value = { Support.ConvertToString(row.Cells["Id"].Value), Support.ConvertToString(row.Cells["Amount"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_Megalac_ExFactory_Edit", Param, Value);
                }
            }

            MessageBox.Show("Information Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetData();
        }
    }
}
