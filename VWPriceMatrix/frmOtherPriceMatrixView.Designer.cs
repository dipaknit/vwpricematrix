﻿namespace VWPriceMatrix
{
    partial class frmOtherPriceMatrixView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOtherPriceMatrixView));
            this.panel1 = new General.Controls.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgv6 = new General.Controls.DataGridView();
            this.panel6 = new General.Controls.Panel();
            this.hlbl6 = new General.Controls.HeaderLabel();
            this.dgv5 = new General.Controls.DataGridView();
            this.panel5 = new General.Controls.Panel();
            this.hlbl5 = new General.Controls.HeaderLabel();
            this.dgv4 = new General.Controls.DataGridView();
            this.panel4 = new General.Controls.Panel();
            this.hlbl4 = new General.Controls.HeaderLabel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.btnExcelExport = new General.Controls.Button();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.dgv3 = new General.Controls.DataGridView();
            this.panel7 = new General.Controls.Panel();
            this.hlbl1 = new General.Controls.HeaderLabel();
            this.dgv1 = new General.Controls.DataGridView();
            this.panel2 = new General.Controls.Panel();
            this.hlbl2 = new General.Controls.HeaderLabel();
            this.dgv2 = new General.Controls.DataGridView();
            this.panel3 = new General.Controls.Panel();
            this.hlbl3 = new General.Controls.HeaderLabel();
            this.R1_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv6)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv4)).BeginInit();
            this.panel4.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv3)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv2)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 730);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dgv6, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.dgv5, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.dgv4, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgv3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgv1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dgv2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dgv6
            // 
            this.dgv6.AllowUpDownKey = false;
            this.dgv6.AllowUserToAddRows = false;
            this.dgv6.AllowUserToDeleteRows = false;
            this.dgv6.AllowUserToResizeRows = false;
            this.dgv6.ApplyMySettings = false;
            this.dgv6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.Column5,
            this.Column6,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39});
            this.dgv6.ctlInfoMessage = null;
            this.dgv6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv6.Location = new System.Drawing.Point(3, 643);
            this.dgv6.MyAutoGenerateColumn = false;
            this.dgv6.MyEditGrid = false;
            this.dgv6.Name = "dgv6";
            this.dgv6.RowHeadersVisible = false;
            this.dgv6.RowTemplate.ReadOnly = true;
            this.dgv6.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv6.ShowEditingIcon = false;
            this.dgv6.Size = new System.Drawing.Size(1002, 84);
            this.dgv6.strInfoMsgArr = null;
            this.dgv6.TabIndex = 147;
            // 
            // panel6
            // 
            this.panel6.ApplyMySettings = true;
            this.panel6.Controls.Add(this.hlbl6);
            this.panel6.CtrlProperty = "";
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 613);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel6.Size = new System.Drawing.Size(1002, 24);
            this.panel6.TabIndex = 146;
            // 
            // hlbl6
            // 
            this.hlbl6.ApplyMySettings = true;
            this.hlbl6.AutoSize = true;
            this.hlbl6.CtrlProperty = "Customer 6";
            this.hlbl6.Location = new System.Drawing.Point(8, 5);
            this.hlbl6.Name = "hlbl6";
            this.hlbl6.Size = new System.Drawing.Size(60, 13);
            this.hlbl6.TabIndex = 1;
            this.hlbl6.Text = "Customer 6";
            // 
            // dgv5
            // 
            this.dgv5.AllowUpDownKey = false;
            this.dgv5.AllowUserToAddRows = false;
            this.dgv5.AllowUserToDeleteRows = false;
            this.dgv5.AllowUserToResizeRows = false;
            this.dgv5.ApplyMySettings = false;
            this.dgv5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.Column1,
            this.Column2,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26});
            this.dgv5.ctlInfoMessage = null;
            this.dgv5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv5.Location = new System.Drawing.Point(3, 527);
            this.dgv5.MyAutoGenerateColumn = false;
            this.dgv5.MyEditGrid = false;
            this.dgv5.Name = "dgv5";
            this.dgv5.RowHeadersVisible = false;
            this.dgv5.RowTemplate.ReadOnly = true;
            this.dgv5.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv5.ShowEditingIcon = false;
            this.dgv5.Size = new System.Drawing.Size(1002, 80);
            this.dgv5.strInfoMsgArr = null;
            this.dgv5.TabIndex = 145;
            // 
            // panel5
            // 
            this.panel5.ApplyMySettings = true;
            this.panel5.Controls.Add(this.hlbl5);
            this.panel5.CtrlProperty = "";
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 497);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel5.Size = new System.Drawing.Size(1002, 24);
            this.panel5.TabIndex = 144;
            // 
            // hlbl5
            // 
            this.hlbl5.ApplyMySettings = true;
            this.hlbl5.AutoSize = true;
            this.hlbl5.CtrlProperty = "Customer 5";
            this.hlbl5.Location = new System.Drawing.Point(8, 5);
            this.hlbl5.Name = "hlbl5";
            this.hlbl5.Size = new System.Drawing.Size(60, 13);
            this.hlbl5.TabIndex = 1;
            this.hlbl5.Text = "Customer 5";
            // 
            // dgv4
            // 
            this.dgv4.AllowUpDownKey = false;
            this.dgv4.AllowUserToAddRows = false;
            this.dgv4.AllowUserToDeleteRows = false;
            this.dgv4.AllowUserToResizeRows = false;
            this.dgv4.ApplyMySettings = false;
            this.dgv4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.Column3,
            this.Column4,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dgv4.ctlInfoMessage = null;
            this.dgv4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv4.Location = new System.Drawing.Point(3, 411);
            this.dgv4.MyAutoGenerateColumn = false;
            this.dgv4.MyEditGrid = false;
            this.dgv4.Name = "dgv4";
            this.dgv4.RowHeadersVisible = false;
            this.dgv4.RowTemplate.ReadOnly = true;
            this.dgv4.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv4.ShowEditingIcon = false;
            this.dgv4.Size = new System.Drawing.Size(1002, 80);
            this.dgv4.strInfoMsgArr = null;
            this.dgv4.TabIndex = 143;
            // 
            // panel4
            // 
            this.panel4.ApplyMySettings = true;
            this.panel4.Controls.Add(this.hlbl4);
            this.panel4.CtrlProperty = "";
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 381);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel4.Size = new System.Drawing.Size(1002, 24);
            this.panel4.TabIndex = 142;
            // 
            // hlbl4
            // 
            this.hlbl4.ApplyMySettings = true;
            this.hlbl4.AutoSize = true;
            this.hlbl4.CtrlProperty = "Customer 4";
            this.hlbl4.Location = new System.Drawing.Point(8, 5);
            this.hlbl4.Name = "hlbl4";
            this.hlbl4.Size = new System.Drawing.Size(60, 13);
            this.hlbl4.TabIndex = 1;
            this.hlbl4.Text = "Customer 4";
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.btnExcelExport);
            this.panelHeader1.Controls.Add(this.headerLabel3);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1002, 24);
            this.panelHeader1.TabIndex = 140;
            // 
            // btnExcelExport
            // 
            this.btnExcelExport.ApplyMySettings = true;
            this.btnExcelExport.CtrlProperty = "Excel Export";
            this.btnExcelExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExcelExport.Location = new System.Drawing.Point(927, 0);
            this.btnExcelExport.Name = "btnExcelExport";
            this.btnExcelExport.Size = new System.Drawing.Size(75, 24);
            this.btnExcelExport.TabIndex = 4;
            this.btnExcelExport.Text = "Excel Export";
            this.btnExcelExport.UseVisualStyleBackColor = true;
            this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "Other Price Matrix";
            this.headerLabel3.Location = new System.Drawing.Point(8, 6);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(91, 13);
            this.headerLabel3.TabIndex = 0;
            this.headerLabel3.Text = "Other Price Matrix";
            // 
            // dgv3
            // 
            this.dgv3.AllowUpDownKey = false;
            this.dgv3.AllowUserToAddRows = false;
            this.dgv3.AllowUserToDeleteRows = false;
            this.dgv3.AllowUserToResizeRows = false;
            this.dgv3.ApplyMySettings = false;
            this.dgv3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R3_ID,
            this.R3_MEGAFAT88_25,
            this.R3_MEGAFAT88_650,
            this.R3_MEGAFATEXTRA_25,
            this.R3_MEGAFATEXTRA_650,
            this.R3_MEGAENERGY_25,
            this.R3_MEGAENERGY_650,
            this.R3_MEGABOOST_25,
            this.R3_MEGABOOST_650,
            this.R3_MEGAONE_25,
            this.R3_MEGAONE_650,
            this.R3_MEGAMAX_25,
            this.R3_MEGAMAX_650,
            this.R3_MEGALAC_25,
            this.R3_MEGALAC_650});
            this.dgv3.ctlInfoMessage = null;
            this.dgv3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv3.Location = new System.Drawing.Point(3, 295);
            this.dgv3.MyAutoGenerateColumn = false;
            this.dgv3.MyEditGrid = false;
            this.dgv3.Name = "dgv3";
            this.dgv3.RowHeadersVisible = false;
            this.dgv3.RowTemplate.ReadOnly = true;
            this.dgv3.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv3.ShowEditingIcon = false;
            this.dgv3.Size = new System.Drawing.Size(1002, 80);
            this.dgv3.strInfoMsgArr = null;
            this.dgv3.TabIndex = 138;
            // 
            // panel7
            // 
            this.panel7.ApplyMySettings = true;
            this.panel7.Controls.Add(this.hlbl1);
            this.panel7.CtrlProperty = "";
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 33);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel7.Size = new System.Drawing.Size(1002, 24);
            this.panel7.TabIndex = 133;
            // 
            // hlbl1
            // 
            this.hlbl1.ApplyMySettings = true;
            this.hlbl1.AutoSize = true;
            this.hlbl1.CtrlProperty = "Customer 1";
            this.hlbl1.Location = new System.Drawing.Point(8, 5);
            this.hlbl1.Name = "hlbl1";
            this.hlbl1.Size = new System.Drawing.Size(60, 13);
            this.hlbl1.TabIndex = 1;
            this.hlbl1.Text = "Customer 1";
            // 
            // dgv1
            // 
            this.dgv1.AllowUpDownKey = false;
            this.dgv1.AllowUserToAddRows = false;
            this.dgv1.AllowUserToDeleteRows = false;
            this.dgv1.AllowUserToResizeRows = false;
            this.dgv1.ApplyMySettings = false;
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R1_ID,
            this.R1_MEGAFAT88_25,
            this.R1_MEGAFAT88_650,
            this.R1_MEGAFATEXTRA_25,
            this.R1_MEGAFATEXTRA_650,
            this.R1_MEGAENERGY_25,
            this.R1_MEGAENERGY_650,
            this.R1_MEGABOOST_25,
            this.R1_MEGABOOST_650,
            this.R1_MEGAONE_25,
            this.R1_MEGAONE_650,
            this.R1_MEGAMAX_25,
            this.R1_MEGAMAX_650,
            this.R1_MEGALAC_25,
            this.R1_MEGALAC_650});
            this.dgv1.ctlInfoMessage = null;
            this.dgv1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv1.Location = new System.Drawing.Point(3, 63);
            this.dgv1.MyAutoGenerateColumn = false;
            this.dgv1.MyEditGrid = false;
            this.dgv1.Name = "dgv1";
            this.dgv1.RowHeadersVisible = false;
            this.dgv1.RowTemplate.ReadOnly = true;
            this.dgv1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv1.ShowEditingIcon = false;
            this.dgv1.Size = new System.Drawing.Size(1002, 80);
            this.dgv1.strInfoMsgArr = null;
            this.dgv1.TabIndex = 134;
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.hlbl2);
            this.panel2.CtrlProperty = "";
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 149);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel2.Size = new System.Drawing.Size(1002, 24);
            this.panel2.TabIndex = 135;
            // 
            // hlbl2
            // 
            this.hlbl2.ApplyMySettings = true;
            this.hlbl2.AutoSize = true;
            this.hlbl2.CtrlProperty = "Customer 2";
            this.hlbl2.Location = new System.Drawing.Point(8, 6);
            this.hlbl2.Name = "hlbl2";
            this.hlbl2.Size = new System.Drawing.Size(60, 13);
            this.hlbl2.TabIndex = 1;
            this.hlbl2.Text = "Customer 2";
            // 
            // dgv2
            // 
            this.dgv2.AllowUpDownKey = false;
            this.dgv2.AllowUserToAddRows = false;
            this.dgv2.AllowUserToDeleteRows = false;
            this.dgv2.AllowUserToResizeRows = false;
            this.dgv2.ApplyMySettings = false;
            this.dgv2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R2_ID,
            this.R2_MEGAFAT88_25,
            this.R2_MEGAFAT88_650,
            this.R2_MEGAFATEXTRA_25,
            this.R2_MEGAFATEXTRA_650,
            this.R2_MEGAENERGY_25,
            this.R2_MEGAENERGY_650,
            this.R2_MEGABOOST_25,
            this.R2_MEGABOOST_650,
            this.R2_MEGAONE_25,
            this.R2_MEGAONE_650,
            this.R2_MEGAMAX_25,
            this.R2_MEGAMAX_650,
            this.R2_MEGALAC_25,
            this.R2_MEGALAC_650});
            this.dgv2.ctlInfoMessage = null;
            this.dgv2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv2.Location = new System.Drawing.Point(3, 179);
            this.dgv2.MyAutoGenerateColumn = false;
            this.dgv2.MyEditGrid = false;
            this.dgv2.Name = "dgv2";
            this.dgv2.RowHeadersVisible = false;
            this.dgv2.RowTemplate.ReadOnly = true;
            this.dgv2.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv2.ShowEditingIcon = false;
            this.dgv2.Size = new System.Drawing.Size(1002, 80);
            this.dgv2.strInfoMsgArr = null;
            this.dgv2.TabIndex = 136;
            // 
            // panel3
            // 
            this.panel3.ApplyMySettings = true;
            this.panel3.Controls.Add(this.hlbl3);
            this.panel3.CtrlProperty = "";
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 265);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel3.Size = new System.Drawing.Size(1002, 24);
            this.panel3.TabIndex = 137;
            // 
            // hlbl3
            // 
            this.hlbl3.ApplyMySettings = true;
            this.hlbl3.AutoSize = true;
            this.hlbl3.CtrlProperty = "Customer 3";
            this.hlbl3.Location = new System.Drawing.Point(8, 5);
            this.hlbl3.Name = "hlbl3";
            this.hlbl3.Size = new System.Drawing.Size(60, 13);
            this.hlbl3.TabIndex = 1;
            this.hlbl3.Text = "Customer 3";
            // 
            // R1_ID
            // 
            this.R1_ID.DataPropertyName = "ID";
            this.R1_ID.HeaderText = "ID";
            this.R1_ID.Name = "R1_ID";
            this.R1_ID.ReadOnly = true;
            this.R1_ID.Visible = false;
            // 
            // R1_MEGAFAT88_25
            // 
            this.R1_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R1_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAFAT88_25.Name = "R1_MEGAFAT88_25";
            this.R1_MEGAFAT88_25.ReadOnly = true;
            this.R1_MEGAFAT88_25.Width = 150;
            // 
            // R1_MEGAFAT88_650
            // 
            this.R1_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R1_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R1_MEGAFAT88_650.Name = "R1_MEGAFAT88_650";
            this.R1_MEGAFAT88_650.ReadOnly = true;
            this.R1_MEGAFAT88_650.Width = 150;
            // 
            // R1_MEGAFATEXTRA_25
            // 
            this.R1_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R1_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAFATEXTRA_25.Name = "R1_MEGAFATEXTRA_25";
            this.R1_MEGAFATEXTRA_25.ReadOnly = true;
            this.R1_MEGAFATEXTRA_25.Width = 150;
            // 
            // R1_MEGAFATEXTRA_650
            // 
            this.R1_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R1_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R1_MEGAFATEXTRA_650.Name = "R1_MEGAFATEXTRA_650";
            this.R1_MEGAFATEXTRA_650.ReadOnly = true;
            this.R1_MEGAFATEXTRA_650.Width = 150;
            // 
            // R1_MEGAENERGY_25
            // 
            this.R1_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R1_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAENERGY_25.Name = "R1_MEGAENERGY_25";
            this.R1_MEGAENERGY_25.ReadOnly = true;
            this.R1_MEGAENERGY_25.Width = 150;
            // 
            // R1_MEGAENERGY_650
            // 
            this.R1_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R1_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R1_MEGAENERGY_650.Name = "R1_MEGAENERGY_650";
            this.R1_MEGAENERGY_650.ReadOnly = true;
            this.R1_MEGAENERGY_650.Width = 150;
            // 
            // R1_MEGABOOST_25
            // 
            this.R1_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R1_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R1_MEGABOOST_25.Name = "R1_MEGABOOST_25";
            this.R1_MEGABOOST_25.ReadOnly = true;
            this.R1_MEGABOOST_25.Width = 150;
            // 
            // R1_MEGABOOST_650
            // 
            this.R1_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R1_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R1_MEGABOOST_650.Name = "R1_MEGABOOST_650";
            this.R1_MEGABOOST_650.ReadOnly = true;
            this.R1_MEGABOOST_650.Width = 150;
            // 
            // R1_MEGAONE_25
            // 
            this.R1_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R1_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAONE_25.Name = "R1_MEGAONE_25";
            this.R1_MEGAONE_25.ReadOnly = true;
            this.R1_MEGAONE_25.Width = 150;
            // 
            // R1_MEGAONE_650
            // 
            this.R1_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R1_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R1_MEGAONE_650.Name = "R1_MEGAONE_650";
            this.R1_MEGAONE_650.ReadOnly = true;
            this.R1_MEGAONE_650.Width = 150;
            // 
            // R1_MEGAMAX_25
            // 
            this.R1_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R1_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAMAX_25.Name = "R1_MEGAMAX_25";
            this.R1_MEGAMAX_25.ReadOnly = true;
            this.R1_MEGAMAX_25.Width = 150;
            // 
            // R1_MEGAMAX_650
            // 
            this.R1_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R1_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R1_MEGAMAX_650.Name = "R1_MEGAMAX_650";
            this.R1_MEGAMAX_650.ReadOnly = true;
            this.R1_MEGAMAX_650.Width = 150;
            // 
            // R1_MEGALAC_25
            // 
            this.R1_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R1_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R1_MEGALAC_25.Name = "R1_MEGALAC_25";
            this.R1_MEGALAC_25.ReadOnly = true;
            this.R1_MEGALAC_25.Width = 150;
            // 
            // R1_MEGALAC_650
            // 
            this.R1_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R1_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R1_MEGALAC_650.Name = "R1_MEGALAC_650";
            this.R1_MEGALAC_650.ReadOnly = true;
            this.R1_MEGALAC_650.Width = 150;
            // 
            // R2_ID
            // 
            this.R2_ID.DataPropertyName = "ID";
            this.R2_ID.HeaderText = "ID";
            this.R2_ID.Name = "R2_ID";
            this.R2_ID.ReadOnly = true;
            this.R2_ID.Visible = false;
            // 
            // R2_MEGAFAT88_25
            // 
            this.R2_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R2_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAFAT88_25.Name = "R2_MEGAFAT88_25";
            this.R2_MEGAFAT88_25.ReadOnly = true;
            this.R2_MEGAFAT88_25.Width = 150;
            // 
            // R2_MEGAFAT88_650
            // 
            this.R2_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R2_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R2_MEGAFAT88_650.Name = "R2_MEGAFAT88_650";
            this.R2_MEGAFAT88_650.ReadOnly = true;
            this.R2_MEGAFAT88_650.Width = 150;
            // 
            // R2_MEGAFATEXTRA_25
            // 
            this.R2_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R2_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAFATEXTRA_25.Name = "R2_MEGAFATEXTRA_25";
            this.R2_MEGAFATEXTRA_25.ReadOnly = true;
            this.R2_MEGAFATEXTRA_25.Width = 150;
            // 
            // R2_MEGAFATEXTRA_650
            // 
            this.R2_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R2_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R2_MEGAFATEXTRA_650.Name = "R2_MEGAFATEXTRA_650";
            this.R2_MEGAFATEXTRA_650.ReadOnly = true;
            this.R2_MEGAFATEXTRA_650.Width = 150;
            // 
            // R2_MEGAENERGY_25
            // 
            this.R2_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R2_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAENERGY_25.Name = "R2_MEGAENERGY_25";
            this.R2_MEGAENERGY_25.ReadOnly = true;
            this.R2_MEGAENERGY_25.Width = 150;
            // 
            // R2_MEGAENERGY_650
            // 
            this.R2_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R2_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R2_MEGAENERGY_650.Name = "R2_MEGAENERGY_650";
            this.R2_MEGAENERGY_650.ReadOnly = true;
            this.R2_MEGAENERGY_650.Width = 150;
            // 
            // R2_MEGABOOST_25
            // 
            this.R2_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R2_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R2_MEGABOOST_25.Name = "R2_MEGABOOST_25";
            this.R2_MEGABOOST_25.ReadOnly = true;
            this.R2_MEGABOOST_25.Width = 150;
            // 
            // R2_MEGABOOST_650
            // 
            this.R2_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R2_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R2_MEGABOOST_650.Name = "R2_MEGABOOST_650";
            this.R2_MEGABOOST_650.ReadOnly = true;
            this.R2_MEGABOOST_650.Width = 150;
            // 
            // R2_MEGAONE_25
            // 
            this.R2_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R2_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAONE_25.Name = "R2_MEGAONE_25";
            this.R2_MEGAONE_25.ReadOnly = true;
            this.R2_MEGAONE_25.Width = 150;
            // 
            // R2_MEGAONE_650
            // 
            this.R2_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R2_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R2_MEGAONE_650.Name = "R2_MEGAONE_650";
            this.R2_MEGAONE_650.ReadOnly = true;
            this.R2_MEGAONE_650.Width = 150;
            // 
            // R2_MEGAMAX_25
            // 
            this.R2_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R2_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAMAX_25.Name = "R2_MEGAMAX_25";
            this.R2_MEGAMAX_25.ReadOnly = true;
            this.R2_MEGAMAX_25.Width = 150;
            // 
            // R2_MEGAMAX_650
            // 
            this.R2_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R2_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R2_MEGAMAX_650.Name = "R2_MEGAMAX_650";
            this.R2_MEGAMAX_650.ReadOnly = true;
            this.R2_MEGAMAX_650.Width = 150;
            // 
            // R2_MEGALAC_25
            // 
            this.R2_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R2_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R2_MEGALAC_25.Name = "R2_MEGALAC_25";
            this.R2_MEGALAC_25.ReadOnly = true;
            this.R2_MEGALAC_25.Width = 150;
            // 
            // R2_MEGALAC_650
            // 
            this.R2_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R2_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R2_MEGALAC_650.Name = "R2_MEGALAC_650";
            this.R2_MEGALAC_650.ReadOnly = true;
            this.R2_MEGALAC_650.Width = 150;
            // 
            // R3_ID
            // 
            this.R3_ID.DataPropertyName = "ID";
            this.R3_ID.HeaderText = "ID";
            this.R3_ID.Name = "R3_ID";
            this.R3_ID.ReadOnly = true;
            this.R3_ID.Visible = false;
            // 
            // R3_MEGAFAT88_25
            // 
            this.R3_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R3_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAFAT88_25.Name = "R3_MEGAFAT88_25";
            this.R3_MEGAFAT88_25.ReadOnly = true;
            this.R3_MEGAFAT88_25.Width = 150;
            // 
            // R3_MEGAFAT88_650
            // 
            this.R3_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R3_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R3_MEGAFAT88_650.Name = "R3_MEGAFAT88_650";
            this.R3_MEGAFAT88_650.ReadOnly = true;
            this.R3_MEGAFAT88_650.Width = 150;
            // 
            // R3_MEGAFATEXTRA_25
            // 
            this.R3_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R3_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAFATEXTRA_25.Name = "R3_MEGAFATEXTRA_25";
            this.R3_MEGAFATEXTRA_25.ReadOnly = true;
            this.R3_MEGAFATEXTRA_25.Width = 150;
            // 
            // R3_MEGAFATEXTRA_650
            // 
            this.R3_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R3_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R3_MEGAFATEXTRA_650.Name = "R3_MEGAFATEXTRA_650";
            this.R3_MEGAFATEXTRA_650.ReadOnly = true;
            this.R3_MEGAFATEXTRA_650.Width = 150;
            // 
            // R3_MEGAENERGY_25
            // 
            this.R3_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R3_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAENERGY_25.Name = "R3_MEGAENERGY_25";
            this.R3_MEGAENERGY_25.ReadOnly = true;
            this.R3_MEGAENERGY_25.Width = 150;
            // 
            // R3_MEGAENERGY_650
            // 
            this.R3_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R3_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R3_MEGAENERGY_650.Name = "R3_MEGAENERGY_650";
            this.R3_MEGAENERGY_650.ReadOnly = true;
            this.R3_MEGAENERGY_650.Width = 150;
            // 
            // R3_MEGABOOST_25
            // 
            this.R3_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R3_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R3_MEGABOOST_25.Name = "R3_MEGABOOST_25";
            this.R3_MEGABOOST_25.ReadOnly = true;
            this.R3_MEGABOOST_25.Width = 150;
            // 
            // R3_MEGABOOST_650
            // 
            this.R3_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R3_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R3_MEGABOOST_650.Name = "R3_MEGABOOST_650";
            this.R3_MEGABOOST_650.ReadOnly = true;
            this.R3_MEGABOOST_650.Width = 150;
            // 
            // R3_MEGAONE_25
            // 
            this.R3_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R3_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAONE_25.Name = "R3_MEGAONE_25";
            this.R3_MEGAONE_25.ReadOnly = true;
            this.R3_MEGAONE_25.Width = 150;
            // 
            // R3_MEGAONE_650
            // 
            this.R3_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R3_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R3_MEGAONE_650.Name = "R3_MEGAONE_650";
            this.R3_MEGAONE_650.ReadOnly = true;
            this.R3_MEGAONE_650.Width = 150;
            // 
            // R3_MEGAMAX_25
            // 
            this.R3_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R3_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAMAX_25.Name = "R3_MEGAMAX_25";
            this.R3_MEGAMAX_25.ReadOnly = true;
            this.R3_MEGAMAX_25.Width = 150;
            // 
            // R3_MEGAMAX_650
            // 
            this.R3_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R3_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R3_MEGAMAX_650.Name = "R3_MEGAMAX_650";
            this.R3_MEGAMAX_650.ReadOnly = true;
            this.R3_MEGAMAX_650.Width = 150;
            // 
            // R3_MEGALAC_25
            // 
            this.R3_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R3_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R3_MEGALAC_25.Name = "R3_MEGALAC_25";
            this.R3_MEGALAC_25.ReadOnly = true;
            this.R3_MEGALAC_25.Width = 150;
            // 
            // R3_MEGALAC_650
            // 
            this.R3_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R3_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R3_MEGALAC_650.Name = "R3_MEGALAC_650";
            this.R3_MEGALAC_650.ReadOnly = true;
            this.R3_MEGALAC_650.Width = 150;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn2.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn3.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn4.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn5.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 150;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn6.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn7.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn8.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 150;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn9.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn10.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 150;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn11.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "MEGAMAX_25";
            this.Column3.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "MEGAMAX_650";
            this.Column4.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "MEGALAC_25";
            this.dataGridViewTextBoxColumn12.HeaderText = "CaSalt (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "MEGALAC_650";
            this.dataGridViewTextBoxColumn13.HeaderText = "CaSalt  (BB 600Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 150;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn14.HeaderText = "ID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn15.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 150;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn16.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn17.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn18.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 150;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn19.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn20.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 150;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn21.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 150;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn22.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 150;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn23.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 150;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn24.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "MEGAMAX_25";
            this.Column1.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "MEGAMAX_650";
            this.Column2.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "MEGALAC_25";
            this.dataGridViewTextBoxColumn25.HeaderText = "CaSalt (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 150;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "MEGALAC_650";
            this.dataGridViewTextBoxColumn26.HeaderText = "CaSalt (BB 600Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 150;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn27.HeaderText = "ID";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Visible = false;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn28.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 150;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn29.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 150;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn30.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 150;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn31.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 150;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn32.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 150;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn33.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 150;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn34.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 150;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn35.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 150;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn36.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 150;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn37.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 150;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "MEGAMAX_25";
            this.Column5.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "MEGAMAX_650";
            this.Column6.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 150;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "MEGALAC_25";
            this.dataGridViewTextBoxColumn38.HeaderText = "CaSalt (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 150;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "MEGALAC_650";
            this.dataGridViewTextBoxColumn39.HeaderText = "CaSalt (BB 600Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 150;
            // 
            // frmOtherPriceMatrixView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmOtherPriceMatrixView";
            this.Text = "Other Price Matrix";
            this.Load += new System.EventHandler(this.frmDEPriceMatrix_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv6)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv3)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.Panel panel7;
        private General.Controls.HeaderLabel hlbl1;
        private General.Controls.DataGridView dgv1;
        private General.Controls.Panel panel2;
        private General.Controls.HeaderLabel hlbl2;
        private General.Controls.DataGridView dgv2;
        private General.Controls.Panel panel3;
        private General.Controls.HeaderLabel hlbl3;
        private General.Controls.DataGridView dgv3;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel3;
        private General.Controls.Panel panel4;
        private General.Controls.HeaderLabel hlbl4;
        private General.Controls.DataGridView dgv4;
        private General.Controls.Panel panel5;
        private General.Controls.HeaderLabel hlbl5;
        private General.Controls.DataGridView dgv5;
        private General.Controls.DataGridView dgv6;
        private General.Controls.Panel panel6;
        private General.Controls.HeaderLabel hlbl6;
        private General.Controls.Button btnExcelExport;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGALAC_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGALAC_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGALAC_650;
    }
}