﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class frmStockHandling : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        #region Form Event        

        public frmStockHandling()
        {
            InitializeComponent();
        }

        private void frmStockHandling_Load(object sender, EventArgs e)
        {
            GetAll();
        }

        #endregion

        #region Other Mothods        

        private void GetAll()
        {
            Get_Molenbergnatie_Curr();
            Get_Molenbergnatie();
            Get_MUELLER_Curr();
            Get_MUELLER();
            Get_BIBBY_Curr();
            Get_BIBBY();
            Get_Aarhus_Curr();
            Get_AARHUS();
        }

        private void Get_Molenbergnatie_Curr()
        {
            string strQuery = " SELECT * FROM WC_MOLENBERGNATIE_CURR WHERE isdeleted = 0 Order by MOLENBERGNATIE_CURR_ID ";
            DataTable dtdgvWC_MOLENBERGNATIE_CURR = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_MOLENBERGNATIE_CURR.AutoGenerateColumns = false;
            dgvWC_MOLENBERGNATIE_CURR.DataSource = dtdgvWC_MOLENBERGNATIE_CURR;

            dgvWC_MOLENBERGNATIE_CURR.Columns["EUR"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void Get_Molenbergnatie()
        {
            string strQuery = " SELECT * FROM WC_MOLENBERGNATIE WHERE isdeleted = 0 Order by MOLENBERGNATIE_ID ";
            DataTable dtWC_MOLENBERGNATIE = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_MOLENBERGNATIE.AutoGenerateColumns = false;
            dgvWC_MOLENBERGNATIE.DataSource = dtWC_MOLENBERGNATIE;
            dgvWC_MOLENBERGNATIE.Columns["MT_CONTAINER"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE_CURR.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE.Rows)
            {
                if (Support.ConvertToString(item.Cells["MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE.Rows)
                {
                    if (Support.ConvertToString(item.Cells["MOLENBERGNATIE"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_MOLENBERGNATIE.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["MOLENBERGNATIE"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_MOLENBERGNATIE.DataSource = dtTemp;
                }
            }
        }

        private void Get_MUELLER_Curr()
        {
            string strQuery = " SELECT * FROM WC_MUELLER_CURR WHERE isdeleted = 0 Order by MUELLER_CURR_ID ";
            DataTable dtdgvWC_Mueller_CURR = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_Mueller_Curr.AutoGenerateColumns = false;
            dgvWC_Mueller_Curr.DataSource = dtdgvWC_Mueller_CURR;
            dgvWC_Mueller_Curr.Columns["MU_EUR"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void Get_MUELLER()
        {
            string strQuery = " SELECT * FROM WC_MUELLER WHERE isdeleted = 0 Order by MUELLER_ID ";
            DataTable dtWC_MUELLER = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_Mueller.AutoGenerateColumns = false;
            dgvWC_Mueller.DataSource = dtWC_MUELLER;
            dgvWC_Mueller.Columns["MU_MT_CONTAINER"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_Mueller_Curr.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["MU_EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["MU_EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_Mueller.Rows)
            {
                if (Support.ConvertToString(item.Cells["MU_MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["MU_QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["MU_QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_Mueller.Rows)
                {
                    if (Support.ConvertToString(item.Cells["MUELLER"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["MU_QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["MU_QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }                   
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_Mueller.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["MUELLER"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_Mueller.DataSource = dtTemp;
                }
            }
        }

        private void Get_BIBBY_Curr()
        {
            string strQuery = " SELECT * FROM WC_BIBBY_CURR WHERE isdeleted = 0 Order by BIBBY_CURR_ID ";
            DataTable dtdgvWC_Bibby_CURR = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_Bibby_Curr.AutoGenerateColumns = false;
            dgvWC_Bibby_Curr.DataSource = dtdgvWC_Bibby_CURR;
            dgvWC_Bibby_Curr.Columns["BB_EUR"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void Get_BIBBY()
        {
            string strQuery = " SELECT * FROM WC_BIBBY WHERE isdeleted = 0 Order by BIBBY_ID ";
            DataTable dtWC_Bibby = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_Bibby.AutoGenerateColumns = false;
            dgvWC_Bibby.DataSource = dtWC_Bibby;
            dgvWC_Bibby.Columns["BB_MT_CONTAINER"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_Bibby_Curr.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["BB_EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["BB_EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_Bibby.Rows)
            {
                if (Support.ConvertToString(item.Cells["BB_MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["BB_QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["BB_QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_Bibby.Rows)
                {
                    if (Support.ConvertToString(item.Cells["BIBBY"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["BB_QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["BB_QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_Bibby.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["BIBBY"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_Bibby.DataSource = dtTemp;
                }
            }
        }

        private void Get_Aarhus_Curr()
        {
            string strQuery = " SELECT * FROM WC_AARHUS_CURR WHERE isdeleted = 0 Order by AARHUS_CURR_ID ";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_Aarhus_Curr.AutoGenerateColumns = false;
            dgvWC_Aarhus_Curr.DataSource = dtData;
            dgvWC_Aarhus_Curr.Columns["AA_EUR"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);
        }

        private void Get_AARHUS()
        {
            string strQuery = " SELECT * FROM WC_AARHUS WHERE isdeleted = 0 Order by AARHUS_ID ";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);
            dgvWC_Aarhus.AutoGenerateColumns = false;
            dgvWC_Aarhus.DataSource = dtData;
            dgvWC_Aarhus.Columns["AA_MT_CONTAINER"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192, 192);

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_Aarhus_Curr.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["AA_EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["AA_EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_Aarhus.Rows)
            {
                if (Support.ConvertToString(item.Cells["AA_MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["AA_QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["AA_QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_Aarhus.Rows)
                {
                    if (Support.ConvertToString(item.Cells["AARHUS"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["AA_QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["AA_QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_Aarhus.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["AARHUS"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_Aarhus.DataSource = dtTemp;
                }
            }
        }

        #endregion

        #region Button Events

        private void btnWC_MOLENBERGNATIE_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE.Rows)
            {
                if (Support.ConvertToString(item.Cells["MT_CONTAINER"].Value).Length > 0)
                {
                    item.Cells["QTY_24"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MT_CONTAINER"].Value) / 24, 2);                
                    item.Cells["QTY_24_7"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MT_CONTAINER"].Value) / 24.7, 2);
                }
            }

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE_CURR.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE.Rows)
            {
                if (Support.ConvertToString(item.Cells["MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE.Rows)
                {
                    if (Support.ConvertToString(item.Cells["MOLENBERGNATIE"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_MOLENBERGNATIE.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["MOLENBERGNATIE"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_MOLENBERGNATIE.DataSource = dtTemp;
                }
            }
        }

        private void btnWC_MOLENBERGNATIE_Save_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvWC_MOLENBERGNATIE.Rows)
            //{
            //    if (Support.ConvertToString(row.Cells["MT_CONTAINER"].Value).Length > 0)
            //    {
            //        string[] Param = { "@MOLENBERGNATIE_ID", "@MOLENBERGNATIE", "@MT_CONTAINER", "@QTY_24", "@QTY_24_7", "@CreatedBy" };
            //        string[] Value = { Convert.ToString(row.Cells["MOLENBERGNATIE_ID"].Value), Convert.ToString(row.Cells["MOLENBERGNATIE"].Value), Convert.ToString(row.Cells["MT_CONTAINER"].Value), Convert.ToString(row.Cells["QTY_24"].Value), Convert.ToString(row.Cells["QTY_24_7"].Value), Support.UserId };
            //        objclsSqlLayer.ExecuteSP("csp_WC_MOLENBERGNATIE_AddEdit", Param, Value);
            //    }
            //}

            foreach (DataGridViewRow row in dgvWC_MOLENBERGNATIE.Rows)
            {
                if (Support.ConvertToString(row.Cells["MT_CONTAINER"].Value).Length > 0)
                {
                    string[] Param = { "@MOLENBERGNATIE_ID", "@MOLENBERGNATIE", "@MT_CONTAINER", "@CreatedBy" };
                    string[] Value = { Convert.ToString(row.Cells["MOLENBERGNATIE_ID"].Value), Convert.ToString(row.Cells["MOLENBERGNATIE"].Value), Convert.ToString(row.Cells["MT_CONTAINER"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_WC_MOLENBERGNATIE_AddEdit", Param, Value);
                }
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnWC_MOLENBERGNATIE_Curr_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_MOLENBERGNATIE_CURR.Rows)
            {
                if (item.Cells["TYPE"].Value.ToString().Trim().ToUpper() == "RECEIVE HANDLE AND DISCHARGE (P/PALLET)")
                {
                    item.Cells["EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["EUR"].Value) / 1.2, 9);
                    item.Cells["EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["EUR"].Value) / 1.3, 9);
                }
                else if (item.Cells["TYPE"].Value.ToString().Trim().ToUpper() == "STORAGE P/P/W")
                {
                    item.Cells["EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["EUR"].Value) / 1.2 * 5, 9);
                    item.Cells["EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["EUR"].Value) / 1.3 * 5, 9);
                }
            }
        }

        private void btnWC_MOLENBERGNATIE_Curr_Save_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvWC_MOLENBERGNATIE_CURR.Rows)
            //{
            //    string[] Param = { "@MOLENBERGNATIE_CURR_ID", "@Type", "@EUR", "@EUR_MT_25KG", "@EUR_MT_BB", "@CreatedBy" };
            //    string[] Value = { Convert.ToString(row.Cells["MOLENBERGNATIE_CURR_ID"].Value), Convert.ToString(row.Cells["Type"].Value), Convert.ToString(row.Cells["EUR"].Value), Convert.ToString(row.Cells["EUR_MT_25KG"].Value), Convert.ToString(row.Cells["EUR_MT_BB"].Value), Support.UserId };
            //    objclsSqlLayer.ExecuteSP("csp_WC_MOLENBERGNATIE_CURR_AddEdit", Param, Value);
            //}

            foreach (DataGridViewRow row in dgvWC_MOLENBERGNATIE_CURR.Rows)
            {
                string[] Param = { "@MOLENBERGNATIE_CURR_ID", "@Type", "@EUR", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["MOLENBERGNATIE_CURR_ID"].Value), Convert.ToString(row.Cells["Type"].Value), Convert.ToString(row.Cells["EUR"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_WC_MOLENBERGNATIE_CURR_AddEdit", Param, Value);
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }        

        private void btnWC_Mueller_Curr_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_Mueller_Curr.Rows)
            {
                if (item.Cells["MU_TYPE"].Value.ToString().Trim().ToUpper() == "RECEIVE HANDLE AND DISCHARGE (P/PALLET)")
                {
                    item.Cells["MU_EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MU_EUR"].Value) / 1.2, 9);
                    item.Cells["MU_EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MU_EUR"].Value) / 1.3, 9);
                }
                else if (item.Cells["MU_TYPE"].Value.ToString().Trim().ToUpper() == "STORAGE P/P/W")
                {
                    item.Cells["MU_EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MU_EUR"].Value) / 1.2 * 5, 9);
                    item.Cells["MU_EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MU_EUR"].Value) / 1.3 * 5, 9);
                }
            }
        }

        private void btnWC_Mueller_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_Mueller.Rows)
            {
                if (Support.ConvertToString(item.Cells["MU_MT_CONTAINER"].Value).Length > 0)
                {
                    item.Cells["MU_QTY_24"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MU_MT_CONTAINER"].Value) / 24, 2);
                    item.Cells["MU_QTY_24_7"].Value = Math.Round(Support.ConvertToDouble(item.Cells["MU_MT_CONTAINER"].Value) / 24.7, 2);
                }
            }

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_Mueller_Curr.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["MU_EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["MU_EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_Mueller.Rows)
            {
                if (Support.ConvertToString(item.Cells["MU_MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["MU_QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["MU_QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_Mueller.Rows)
                {
                    if (Support.ConvertToString(item.Cells["MUELLER"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["MU_QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["MU_QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_Mueller.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["MUELLER"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_Mueller.DataSource = dtTemp;
                }
            }
        }

        private void btnWC_Mueller_Curr_Save_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvWC_Mueller_Curr.Rows)
            //{
            //    string[] Param = { "@MUELLER_CURR_ID", "@Type", "@EUR", "@EUR_MT_25KG", "@EUR_MT_BB", "@CreatedBy" };
            //    string[] Value = { Convert.ToString(row.Cells["MUELLER_CURR_ID"].Value), Convert.ToString(row.Cells["MU_Type"].Value), Convert.ToString(row.Cells["MU_EUR"].Value), Convert.ToString(row.Cells["MU_EUR_MT_25KG"].Value), Convert.ToString(row.Cells["MU_EUR_MT_BB"].Value), Support.UserId };
            //    objclsSqlLayer.ExecuteSP("csp_WC_MUELLER_CURR_AddEdit", Param, Value);
            //}

            foreach (DataGridViewRow row in dgvWC_Mueller_Curr.Rows)
            {
                string[] Param = { "@MUELLER_CURR_ID", "@Type", "@EUR", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["MUELLER_CURR_ID"].Value), Convert.ToString(row.Cells["MU_Type"].Value), Convert.ToString(row.Cells["MU_EUR"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_WC_MUELLER_CURR_AddEdit", Param, Value);
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnWC_Mueller_Save_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvWC_Mueller.Rows)
            //{
            //    if (Support.ConvertToString(row.Cells["MU_MT_CONTAINER"].Value).Length > 0)
            //    {
            //        string[] Param = { "@MUELLER_ID", "@MUELLER", "@MT_CONTAINER", "@QTY_24", "@QTY_24_7", "@CreatedBy" };
            //        string[] Value = { Convert.ToString(row.Cells["MUELLER_ID"].Value), Convert.ToString(row.Cells["MUELLER"].Value), Convert.ToString(row.Cells["MU_MT_CONTAINER"].Value), Convert.ToString(row.Cells["MU_QTY_24"].Value), Convert.ToString(row.Cells["MU_QTY_24_7"].Value), Support.UserId };
            //        objclsSqlLayer.ExecuteSP("csp_WC_MUELLER_AddEdit", Param, Value);
            //    }
            //}

            foreach (DataGridViewRow row in dgvWC_Mueller.Rows)
            {
                if (Support.ConvertToString(row.Cells["MU_MT_CONTAINER"].Value).Length > 0)
                {
                    string[] Param = { "@MUELLER_ID", "@MUELLER", "@MT_CONTAINER", "@CreatedBy" };
                    string[] Value = { Convert.ToString(row.Cells["MUELLER_ID"].Value), Convert.ToString(row.Cells["MUELLER"].Value), Convert.ToString(row.Cells["MU_MT_CONTAINER"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_WC_MUELLER_AddEdit", Param, Value);
                }
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnWC_Bibby_Curr_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_Bibby_Curr.Rows)
            {
                if (item.Cells["BB_TYPE"].Value.ToString().Trim().ToUpper() == "RECEIVE HANDLE AND DISCHARGE (P/PALLET)")
                {
                    item.Cells["BB_EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["BB_EUR"].Value) / 1.2, 9);
                    item.Cells["BB_EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["BB_EUR"].Value) / 1.3, 9);
                }
                else if (item.Cells["BB_TYPE"].Value.ToString().Trim().ToUpper() == "STORAGE P/P/W")
                {
                    item.Cells["BB_EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["BB_EUR"].Value) / 1.2 * 5, 9);
                    item.Cells["BB_EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["BB_EUR"].Value) / 1.3 * 5, 9);
                }
            }
        }

        private void btnWC_Bibby_Curr_Save_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvWC_Bibby_Curr.Rows)
            //{
            //    string[] Param = { "@BIBBY_CURR_ID", "@Type", "@EUR", "@EUR_MT_25KG", "@EUR_MT_BB", "@CreatedBy" };
            //    string[] Value = { Convert.ToString(row.Cells["BIBBY_CURR_ID"].Value), Convert.ToString(row.Cells["BB_Type"].Value), Convert.ToString(row.Cells["BB_EUR"].Value), Convert.ToString(row.Cells["BB_EUR_MT_25KG"].Value), Convert.ToString(row.Cells["BB_EUR_MT_BB"].Value), Support.UserId };
            //    objclsSqlLayer.ExecuteSP("csp_WC_BIBBY_CURR_AddEdit", Param, Value);
            //}

            foreach (DataGridViewRow row in dgvWC_Bibby_Curr.Rows)
            {
                string[] Param = { "@BIBBY_CURR_ID", "@Type", "@EUR", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["BIBBY_CURR_ID"].Value), Convert.ToString(row.Cells["BB_Type"].Value), Convert.ToString(row.Cells["BB_EUR"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_WC_BIBBY_CURR_AddEdit", Param, Value);
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnWC_Bibby_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_Bibby.Rows)
            {
                if (Support.ConvertToString(item.Cells["BB_MT_CONTAINER"].Value).Length > 0)
                {
                    item.Cells["BB_QTY_24"].Value = Math.Round(Support.ConvertToDouble(item.Cells["BB_MT_CONTAINER"].Value) / 24, 2);
                    item.Cells["BB_QTY_24_7"].Value = Math.Round(Support.ConvertToDouble(item.Cells["BB_MT_CONTAINER"].Value) / 24.7, 2);
                }
            }

            double dbl24 = 0;
            double dbl247 = 0;
            bool blnCheck = false;

            foreach (DataGridViewRow item in dgvWC_Bibby_Curr.Rows)
            {
                dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["BB_EUR_MT_25KG"].Value);
                dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["BB_EUR_MT_BB"].Value);

                blnCheck = true;
            }

            foreach (DataGridViewRow item in dgvWC_Bibby.Rows)
            {
                if (Support.ConvertToString(item.Cells["BB_MT_CONTAINER"].Value).Length > 0)
                {
                    dbl24 = dbl24 + Support.ConvertToDouble(item.Cells["BB_QTY_24"].Value);
                    dbl247 = dbl247 + Support.ConvertToDouble(item.Cells["BB_QTY_24_7"].Value);
                    blnCheck = true;
                }
            }

            if (blnCheck)
            {
                bool blnRowAddedCheck = false;

                foreach (DataGridViewRow item in dgvWC_Bibby.Rows)
                {
                    if (Support.ConvertToString(item.Cells["BIBBY"].Value).Trim().ToUpper() == "TOTAL WAREHOUSE AND HANDLING COST")
                    {
                        blnRowAddedCheck = true;
                        item.Cells["BB_QTY_24"].Value = Math.Round(dbl24, 2);
                        item.Cells["BB_QTY_24_7"].Value = Math.Round(dbl247, 2);
                        break;
                    }
                }

                if (blnRowAddedCheck == false)
                {
                    DataTable dtTemp = dgvWC_Bibby.DataSource as DataTable;
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["BIBBY"] = "TOTAL WAREHOUSE and HANDLING COST";
                    drTemp["QTY_24"] = Math.Round(dbl24, 2);
                    drTemp["QTY_24_7"] = Math.Round(dbl247, 2);
                    dtTemp.Rows.Add(drTemp);
                    dgvWC_Bibby.DataSource = dtTemp;
                }
            }
        }

        private void btnWC_Bibby_Save_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in dgvWC_Bibby.Rows)
            //{
            //    if (Support.ConvertToString(row.Cells["BB_MT_CONTAINER"].Value).Length > 0)
            //    {
            //        string[] Param = { "@BIBBY_ID", "@BIBBY", "@MT_CONTAINER", "@QTY_24", "@QTY_24_7", "@CreatedBy" };
            //        string[] Value = { Convert.ToString(row.Cells["BIBBY_ID"].Value), Convert.ToString(row.Cells["BIBBY"].Value), Convert.ToString(row.Cells["BB_MT_CONTAINER"].Value), Convert.ToString(row.Cells["BB_QTY_24"].Value), Convert.ToString(row.Cells["BB_QTY_24_7"].Value), Support.UserId };
            //        objclsSqlLayer.ExecuteSP("csp_WC_BIBBY_AddEdit", Param, Value);
            //    }
            //}

            foreach (DataGridViewRow row in dgvWC_Bibby.Rows)
            {
                if (Support.ConvertToString(row.Cells["BB_MT_CONTAINER"].Value).Length > 0)
                {
                    string[] Param = { "@BIBBY_ID", "@BIBBY", "@MT_CONTAINER", "@CreatedBy" };
                    string[] Value = { Convert.ToString(row.Cells["BIBBY_ID"].Value), Convert.ToString(row.Cells["BIBBY"].Value), Convert.ToString(row.Cells["BB_MT_CONTAINER"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_WC_BIBBY_AddEdit", Param, Value);
                }
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnWC_Aarhus_Curr_Calculate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvWC_Aarhus_Curr.Rows)
            {
                if (item.Cells["AA_TYPE"].Value.ToString().Trim().ToUpper() == "RECEIVE HANDLE AND DISCHARGE (P/PALLET)")
                {
                    item.Cells["AA_EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["AA_EUR"].Value) / 1.2, 9);
                    item.Cells["AA_EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["AA_EUR"].Value) / 1.3, 9);
                }
                else if (item.Cells["AA_TYPE"].Value.ToString().Trim().ToUpper() == "STORAGE P/P/W")
                {
                    item.Cells["AA_EUR_MT_25KG"].Value = Math.Round(Support.ConvertToDouble(item.Cells["AA_EUR"].Value) / 1.2 * 5, 9);
                    item.Cells["AA_EUR_MT_BB"].Value = Math.Round(Support.ConvertToDouble(item.Cells["AA_EUR"].Value) / 1.3 * 5, 9);
                }
            }
        }

        private void btnWC_Aarhus_Curr_Save_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvWC_Aarhus_Curr.Rows)
            {
                string[] Param = { "@AARHUS_CURR_ID", "@Type", "@EUR", "@CreatedBy" };
                string[] Value = { Convert.ToString(row.Cells["AARHUS_CURR_ID"].Value), Convert.ToString(row.Cells["AA_Type"].Value), Convert.ToString(row.Cells["AA_EUR"].Value), Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_WC_AARHUS_CURR_AddEdit", Param, Value);
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btnWC_Aarhus_Calculate_Click(object sender, EventArgs e)
        {

        }

        private void btnWC_Aarhus_Save_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvWC_Aarhus.Rows)
            {
                if (Support.ConvertToString(row.Cells["AA_MT_CONTAINER"].Value).Length > 0)
                {
                    string[] Param = { "@AARHUS_ID", "@AARHUS", "@MT_CONTAINER", "@CreatedBy" };
                    string[] Value = { Convert.ToString(row.Cells["AARHUS_ID"].Value), Convert.ToString(row.Cells["AARHUS"].Value), Convert.ToString(row.Cells["AA_MT_CONTAINER"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_WC_AARHUS_AddEdit", Param, Value);
                }
            }

            MessageBox.Show("Stock Handling Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        #endregion
    }
}
