﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace VWPriceMatrix
{
    public partial class frmNEBEPriceMatrix : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmNEBEPriceMatrix()
        {
            InitializeComponent();
        }


        private void Get_Data()
        {
            string strQuery = " SELECT * FROM NE_BE_Price_Matrix WHERE isdeleted = 0 Order by ID";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = dtData;
        }

        private void frmDKPriceMatrix_Load(object sender, EventArgs e)
        {
            Get_Data();
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.DisplayAlerts = false;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            int intCol = 2;
            xlWorkSheet.Range["B1:M1"].Merge();
            xlWorkSheet.Cells[1, 2] = "The Netherlands and Belgium - full load delivered prices";
            xlWorkSheet.Range["B1:M1"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B1:M1"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B1:M1"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgvData.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[2, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            int intRow = 3;
            

            foreach (DataGridViewRow Row in dgvData.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvData.Columns.Count; i++)
                {
                    if (dgvData.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            if (!Directory.Exists(System.Windows.Forms.Application.StartupPath + "\\Excel\\"))
            {
                Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + "\\Excel\\");
            }

            string strFilename = System.Windows.Forms.Application.StartupPath + "\\Excel\\" + "NE_BE_PRICE_MATRIX_" + DateTime.Now.Ticks + ".xls";

            Excel.Range range = xlWorkSheet.UsedRange;
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            Excel.Range er = xlWorkSheet.get_Range("B:O", System.Type.Missing);
            er.EntireColumn.ColumnWidth = 15.71;

            Excel.Range r1 = xlWorkSheet.get_Range("B2:O2", System.Type.Missing);
            r1.WrapText = true;
            r1.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            //xlWorkSheet.Rows.AutoFit();
            //xlWorkSheet.Columns.AutoFit();

            xlWorkBook.SaveAs(strFilename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            Process.Start(strFilename);
        }
    }
}
