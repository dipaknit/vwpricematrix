﻿namespace VWPriceMatrix
{
    partial class frmOtherPriceMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOtherPriceMatrix));
            this.panel1 = new General.Controls.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.panel2 = new General.Controls.Panel();
            this.chkDeleted = new General.Controls.CheckBox();
            this.label3 = new General.Controls.Label();
            this.label5 = new General.Controls.Label();
            this.txtEmail = new General.Controls.TextBox();
            this.btnGenerate = new General.Controls.Button();
            this.btnSearch = new General.Controls.Button();
            this.btnClear = new General.Controls.Button();
            this.label2 = new General.Controls.Label();
            this.txtCity = new General.Controls.TextBox();
            this.label1 = new General.Controls.Label();
            this.txtPostcode = new General.Controls.TextBox();
            this.label4 = new General.Controls.Label();
            this.label7 = new General.Controls.Label();
            this.txtCustomerId = new General.Controls.TextBox();
            this.txtCustomerName = new General.Controls.TextBox();
            this.dgvCustomer = new General.Controls.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CustomerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Region = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1045, 730);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgvCustomer, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1045, 730);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel1);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1039, 24);
            this.panelHeader1.TabIndex = 126;
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "Customer Search";
            this.headerLabel1.Location = new System.Drawing.Point(14, 5);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(88, 13);
            this.headerLabel1.TabIndex = 0;
            this.headerLabel1.Text = "Customer Search";
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.chkDeleted);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtEmail);
            this.panel2.Controls.Add(this.btnGenerate);
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Controls.Add(this.btnClear);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtCity);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtPostcode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtCustomerId);
            this.panel2.Controls.Add(this.txtCustomerName);
            this.panel2.CtrlProperty = "";
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1039, 74);
            this.panel2.TabIndex = 127;
            // 
            // chkDeleted
            // 
            this.chkDeleted.ApplyMySettings = true;
            this.chkDeleted.AutoSize = true;
            this.chkDeleted.CtrlProperty = "False";
            this.chkDeleted.Location = new System.Drawing.Point(585, 45);
            this.chkDeleted.Name = "chkDeleted";
            this.chkDeleted.Size = new System.Drawing.Size(15, 14);
            this.chkDeleted.TabIndex = 26;
            this.chkDeleted.UseVisualStyleBackColor = true;
            this.chkDeleted.Visible = false;
            // 
            // label3
            // 
            this.label3.ApplyMySettings = true;
            this.label3.CtrlProperty = "Deleted?";
            this.label3.Location = new System.Drawing.Point(486, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Deleted?";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Visible = false;
            // 
            // label5
            // 
            this.label5.ApplyMySettings = true;
            this.label5.CtrlProperty = "Email";
            this.label5.Location = new System.Drawing.Point(486, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Email";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEmail
            // 
            this.txtEmail.AllowAllCharacters = true;
            this.txtEmail.AllowCurrency = false;
            this.txtEmail.AllowSpace = true;
            this.txtEmail.ApplyMySettings = true;
            this.txtEmail.ConvertEnterToTab = true;
            this.txtEmail.ctlInfoMessage = null;
            this.txtEmail.CtrlProperty = "";
            this.txtEmail.InfoMessage = "";
            this.txtEmail.Location = new System.Drawing.Point(584, 17);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(129, 20);
            this.txtEmail.TabIndex = 22;
            this.txtEmail.Verify = true;
            // 
            // btnGenerate
            // 
            this.btnGenerate.ApplyMySettings = true;
            this.btnGenerate.CtrlProperty = "&Generate";
            this.btnGenerate.Location = new System.Drawing.Point(787, 41);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 7;
            this.btnGenerate.Text = "&Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.ApplyMySettings = true;
            this.btnSearch.CtrlProperty = "&Search";
            this.btnSearch.Location = new System.Drawing.Point(868, 41);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "&Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnClear
            // 
            this.btnClear.ApplyMySettings = true;
            this.btnClear.CtrlProperty = "&Clear";
            this.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClear.Location = new System.Drawing.Point(949, 41);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label2
            // 
            this.label2.ApplyMySettings = true;
            this.label2.CtrlProperty = "City";
            this.label2.Location = new System.Drawing.Point(249, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "City";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCity
            // 
            this.txtCity.AllowAllCharacters = true;
            this.txtCity.AllowCurrency = false;
            this.txtCity.AllowSpace = true;
            this.txtCity.ApplyMySettings = true;
            this.txtCity.ConvertEnterToTab = true;
            this.txtCity.ctlInfoMessage = null;
            this.txtCity.CtrlProperty = "";
            this.txtCity.InfoMessage = "";
            this.txtCity.Location = new System.Drawing.Point(347, 43);
            this.txtCity.MaxLength = 50;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(129, 20);
            this.txtCity.TabIndex = 3;
            this.txtCity.Verify = true;
            // 
            // label1
            // 
            this.label1.ApplyMySettings = true;
            this.label1.CtrlProperty = "Postcode";
            this.label1.Location = new System.Drawing.Point(249, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Postcode";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPostcode
            // 
            this.txtPostcode.AllowAllCharacters = true;
            this.txtPostcode.AllowCurrency = false;
            this.txtPostcode.AllowSpace = true;
            this.txtPostcode.ApplyMySettings = true;
            this.txtPostcode.ConvertEnterToTab = true;
            this.txtPostcode.ctlInfoMessage = null;
            this.txtPostcode.CtrlProperty = "";
            this.txtPostcode.InfoMessage = "";
            this.txtPostcode.Location = new System.Drawing.Point(347, 17);
            this.txtPostcode.MaxLength = 50;
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(129, 20);
            this.txtPostcode.TabIndex = 2;
            this.txtPostcode.Verify = true;
            // 
            // label4
            // 
            this.label4.ApplyMySettings = true;
            this.label4.CtrlProperty = "Customer Id";
            this.label4.Location = new System.Drawing.Point(13, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Customer Id";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.ApplyMySettings = true;
            this.label7.CtrlProperty = "Customer Name";
            this.label7.Location = new System.Drawing.Point(13, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Customer Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.AllowAllCharacters = true;
            this.txtCustomerId.AllowCurrency = false;
            this.txtCustomerId.AllowSpace = true;
            this.txtCustomerId.ApplyMySettings = true;
            this.txtCustomerId.ConvertEnterToTab = true;
            this.txtCustomerId.ctlInfoMessage = null;
            this.txtCustomerId.CtrlProperty = "";
            this.txtCustomerId.InfoMessage = "";
            this.txtCustomerId.Location = new System.Drawing.Point(111, 17);
            this.txtCustomerId.MaxLength = 50;
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.Size = new System.Drawing.Size(129, 20);
            this.txtCustomerId.TabIndex = 0;
            this.txtCustomerId.Verify = true;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.AllowAllCharacters = true;
            this.txtCustomerName.AllowCurrency = false;
            this.txtCustomerName.AllowSpace = true;
            this.txtCustomerName.ApplyMySettings = true;
            this.txtCustomerName.ConvertEnterToTab = true;
            this.txtCustomerName.ctlInfoMessage = null;
            this.txtCustomerName.CtrlProperty = "";
            this.txtCustomerName.InfoMessage = "";
            this.txtCustomerName.Location = new System.Drawing.Point(111, 43);
            this.txtCustomerName.MaxLength = 50;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(129, 20);
            this.txtCustomerName.TabIndex = 1;
            this.txtCustomerName.Verify = true;
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.AllowUpDownKey = false;
            this.dgvCustomer.AllowUserToAddRows = false;
            this.dgvCustomer.AllowUserToDeleteRows = false;
            this.dgvCustomer.AllowUserToResizeRows = false;
            this.dgvCustomer.ApplyMySettings = false;
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Select,
            this.CustomerId,
            this.CustomerName,
            this.PostCode,
            this.City,
            this.Country,
            this.Region,
            this.Email,
            this.IsDeleted});
            this.dgvCustomer.ctlInfoMessage = null;
            this.dgvCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCustomer.Location = new System.Drawing.Point(3, 113);
            this.dgvCustomer.MultiSelect = false;
            this.dgvCustomer.MyAutoGenerateColumn = false;
            this.dgvCustomer.MyEditGrid = true;
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvCustomer.Size = new System.Drawing.Size(1039, 614);
            this.dgvCustomer.strInfoMsgArr = null;
            this.dgvCustomer.TabIndex = 128;
            this.dgvCustomer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCustomer_CellClick);
            this.dgvCustomer.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvCustomer_DataBindingComplete);
            this.dgvCustomer.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvCustomer_DataError);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Id.Width = 50;
            // 
            // Select
            // 
            this.Select.HeaderText = "Select";
            this.Select.Name = "Select";
            this.Select.Width = 50;
            // 
            // CustomerId
            // 
            this.CustomerId.DataPropertyName = "CustomerId";
            this.CustomerId.HeaderText = "Customer Id";
            this.CustomerId.Name = "CustomerId";
            this.CustomerId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CustomerId.Width = 75;
            // 
            // CustomerName
            // 
            this.CustomerName.DataPropertyName = "CustomerName";
            this.CustomerName.HeaderText = "Customer Name";
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CustomerName.Width = 250;
            // 
            // PostCode
            // 
            this.PostCode.DataPropertyName = "PostCode";
            this.PostCode.HeaderText = "Postcode";
            this.PostCode.Name = "PostCode";
            this.PostCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PostCode.Width = 80;
            // 
            // City
            // 
            this.City.DataPropertyName = "City";
            this.City.HeaderText = "City";
            this.City.Name = "City";
            this.City.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Region
            // 
            this.Region.DataPropertyName = "Region";
            this.Region.HeaderText = "Region";
            this.Region.Name = "Region";
            this.Region.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Email.Width = 350;
            // 
            // IsDeleted
            // 
            this.IsDeleted.DataPropertyName = "IsDeleted";
            this.IsDeleted.HeaderText = "Deleted?";
            this.IsDeleted.Name = "IsDeleted";
            this.IsDeleted.ReadOnly = true;
            this.IsDeleted.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsDeleted.Visible = false;
            this.IsDeleted.Width = 60;
            // 
            // frmOtherPriceMatrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 730);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmOtherPriceMatrix";
            this.Text = "Other Price Matrix";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel1;
        private General.Controls.Panel panel2;
        private General.Controls.DataGridView dgvCustomer;
        private General.Controls.Label label7;
        private General.Controls.TextBox txtCustomerName;
        private General.Controls.Label label1;
        private General.Controls.TextBox txtPostcode;
        private General.Controls.Label label2;
        private General.Controls.TextBox txtCity;
        private General.Controls.Button btnSearch;
        private General.Controls.Button btnClear;
        private General.Controls.Label label4;
        private General.Controls.TextBox txtCustomerId;
        private General.Controls.Label label3;
        private General.Controls.Label label5;
        private General.Controls.TextBox txtEmail;
        private General.Controls.CheckBox chkDeleted;
        private General.Controls.Button btnGenerate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn City;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Region;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsDeleted;
    }
}