﻿namespace VWPriceMatrix
{
    partial class frmExWarehouseCost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExWarehouseCost));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel4 = new General.Controls.HeaderLabel();
            this.dgvEX_WC_BIBBY = new General.Controls.DataGridView();
            this.BIBBY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BIBBY_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new General.Controls.Panel();
            this.btn_ANTWERP_Calculate = new General.Controls.Button();
            this.btn_ANTWERP_Save = new General.Controls.Button();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.dgvEX_WC_ANTWERP = new General.Controls.DataGridView();
            this.ANTWERP_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANT_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new General.Controls.Panel();
            this.btn_BRAKE_Calculate = new General.Controls.Button();
            this.btn_BRAKE_Save = new General.Controls.Button();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.panel3 = new General.Controls.Panel();
            this.btn_BIBBY_Calculate = new General.Controls.Button();
            this.btn_BIBBY_Save = new General.Controls.Button();
            this.headerLabel2 = new General.Controls.HeaderLabel();
            this.dgvEX_WC_BRAKE = new General.Controls.DataGridView();
            this.BRAKE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRAKE_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new General.Controls.Panel();
            this.btn_AARHUS_Calculate = new General.Controls.Button();
            this.btn_AARHUS_Save = new General.Controls.Button();
            this.headerLabel5 = new General.Controls.HeaderLabel();
            this.dgvEX_WC_AARHUS = new General.Controls.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_BIBBY)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_ANTWERP)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_BRAKE)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_AARHUS)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dgvEX_WC_AARHUS, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvEX_WC_BIBBY, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgvEX_WC_ANTWERP, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.dgvEX_WC_BRAKE, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1066, 807);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel4);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1060, 24);
            this.panelHeader1.TabIndex = 141;
            // 
            // headerLabel4
            // 
            this.headerLabel4.ApplyMySettings = true;
            this.headerLabel4.AutoSize = true;
            this.headerLabel4.CtrlProperty = "Ex-Warehouse Cost";
            this.headerLabel4.Location = new System.Drawing.Point(8, 6);
            this.headerLabel4.Name = "headerLabel4";
            this.headerLabel4.Size = new System.Drawing.Size(101, 13);
            this.headerLabel4.TabIndex = 0;
            this.headerLabel4.Text = "Ex-Warehouse Cost";
            // 
            // dgvEX_WC_BIBBY
            // 
            this.dgvEX_WC_BIBBY.AllowUpDownKey = false;
            this.dgvEX_WC_BIBBY.AllowUserToAddRows = false;
            this.dgvEX_WC_BIBBY.AllowUserToDeleteRows = false;
            this.dgvEX_WC_BIBBY.AllowUserToResizeRows = false;
            this.dgvEX_WC_BIBBY.ApplyMySettings = false;
            this.dgvEX_WC_BIBBY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEX_WC_BIBBY.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BIBBY_ID,
            this.BIBBY_Type,
            this.BIBBY_MEGAFAT88_25,
            this.BIBBY_MEGAFAT88_650,
            this.BIBBY_MEGAFATEXTRA_25,
            this.BIBBY_MEGAFATEXTRA_650,
            this.BIBBY_MEGAENERGY_25,
            this.BIBBY_MEGAENERGY_650,
            this.BIBBY_MEGABOOST_25,
            this.BIBBY_MEGABOOST_650,
            this.BIBBY_MEGAONE_25,
            this.BIBBY_MEGAONE_650});
            this.dgvEX_WC_BIBBY.ctlInfoMessage = null;
            this.dgvEX_WC_BIBBY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEX_WC_BIBBY.Location = new System.Drawing.Point(3, 451);
            this.dgvEX_WC_BIBBY.MyAutoGenerateColumn = false;
            this.dgvEX_WC_BIBBY.MyEditGrid = false;
            this.dgvEX_WC_BIBBY.Name = "dgvEX_WC_BIBBY";
            this.dgvEX_WC_BIBBY.RowHeadersVisible = false;
            this.dgvEX_WC_BIBBY.RowTemplate.ReadOnly = true;
            this.dgvEX_WC_BIBBY.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEX_WC_BIBBY.ShowEditingIcon = false;
            this.dgvEX_WC_BIBBY.Size = new System.Drawing.Size(1060, 158);
            this.dgvEX_WC_BIBBY.strInfoMsgArr = null;
            this.dgvEX_WC_BIBBY.TabIndex = 137;
            this.dgvEX_WC_BIBBY.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvEX_WC_BIBBY_CellFormatting);
            // 
            // BIBBY_ID
            // 
            this.BIBBY_ID.DataPropertyName = "BIBBY_ID";
            this.BIBBY_ID.HeaderText = "ID";
            this.BIBBY_ID.Name = "BIBBY_ID";
            this.BIBBY_ID.ReadOnly = true;
            this.BIBBY_ID.Visible = false;
            // 
            // BIBBY_Type
            // 
            this.BIBBY_Type.DataPropertyName = "Type";
            this.BIBBY_Type.HeaderText = "Type";
            this.BIBBY_Type.Name = "BIBBY_Type";
            this.BIBBY_Type.ReadOnly = true;
            this.BIBBY_Type.Width = 200;
            // 
            // BIBBY_MEGAFAT88_25
            // 
            this.BIBBY_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.BIBBY_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized)";
            this.BIBBY_MEGAFAT88_25.Name = "BIBBY_MEGAFAT88_25";
            this.BIBBY_MEGAFAT88_25.ReadOnly = true;
            this.BIBBY_MEGAFAT88_25.Width = 150;
            // 
            // BIBBY_MEGAFAT88_650
            // 
            this.BIBBY_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.BIBBY_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg)";
            this.BIBBY_MEGAFAT88_650.Name = "BIBBY_MEGAFAT88_650";
            this.BIBBY_MEGAFAT88_650.ReadOnly = true;
            this.BIBBY_MEGAFAT88_650.Width = 150;
            // 
            // BIBBY_MEGAFATEXTRA_25
            // 
            this.BIBBY_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.BIBBY_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized)";
            this.BIBBY_MEGAFATEXTRA_25.Name = "BIBBY_MEGAFATEXTRA_25";
            this.BIBBY_MEGAFATEXTRA_25.ReadOnly = true;
            this.BIBBY_MEGAFATEXTRA_25.Width = 150;
            // 
            // BIBBY_MEGAFATEXTRA_650
            // 
            this.BIBBY_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.BIBBY_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg)";
            this.BIBBY_MEGAFATEXTRA_650.Name = "BIBBY_MEGAFATEXTRA_650";
            this.BIBBY_MEGAFATEXTRA_650.ReadOnly = true;
            this.BIBBY_MEGAFATEXTRA_650.Width = 150;
            // 
            // BIBBY_MEGAENERGY_25
            // 
            this.BIBBY_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.BIBBY_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized)";
            this.BIBBY_MEGAENERGY_25.Name = "BIBBY_MEGAENERGY_25";
            this.BIBBY_MEGAENERGY_25.ReadOnly = true;
            this.BIBBY_MEGAENERGY_25.Width = 150;
            // 
            // BIBBY_MEGAENERGY_650
            // 
            this.BIBBY_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.BIBBY_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg)";
            this.BIBBY_MEGAENERGY_650.Name = "BIBBY_MEGAENERGY_650";
            this.BIBBY_MEGAENERGY_650.ReadOnly = true;
            this.BIBBY_MEGAENERGY_650.Width = 150;
            // 
            // BIBBY_MEGABOOST_25
            // 
            this.BIBBY_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.BIBBY_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized)";
            this.BIBBY_MEGABOOST_25.Name = "BIBBY_MEGABOOST_25";
            this.BIBBY_MEGABOOST_25.ReadOnly = true;
            this.BIBBY_MEGABOOST_25.Width = 150;
            // 
            // BIBBY_MEGABOOST_650
            // 
            this.BIBBY_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.BIBBY_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg)";
            this.BIBBY_MEGABOOST_650.Name = "BIBBY_MEGABOOST_650";
            this.BIBBY_MEGABOOST_650.ReadOnly = true;
            this.BIBBY_MEGABOOST_650.Width = 150;
            // 
            // BIBBY_MEGAONE_25
            // 
            this.BIBBY_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.BIBBY_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized)";
            this.BIBBY_MEGAONE_25.Name = "BIBBY_MEGAONE_25";
            this.BIBBY_MEGAONE_25.ReadOnly = true;
            this.BIBBY_MEGAONE_25.Width = 150;
            // 
            // BIBBY_MEGAONE_650
            // 
            this.BIBBY_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.BIBBY_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg)";
            this.BIBBY_MEGAONE_650.Name = "BIBBY_MEGAONE_650";
            this.BIBBY_MEGAONE_650.ReadOnly = true;
            this.BIBBY_MEGAONE_650.Width = 150;
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.btn_ANTWERP_Calculate);
            this.panel2.Controls.Add(this.btn_ANTWERP_Save);
            this.panel2.Controls.Add(this.headerLabel3);
            this.panel2.CtrlProperty = "";
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 30);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel2.Size = new System.Drawing.Size(1066, 30);
            this.panel2.TabIndex = 132;
            // 
            // btn_ANTWERP_Calculate
            // 
            this.btn_ANTWERP_Calculate.ApplyMySettings = true;
            this.btn_ANTWERP_Calculate.CtrlProperty = "Calculate";
            this.btn_ANTWERP_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_ANTWERP_Calculate.Location = new System.Drawing.Point(913, 0);
            this.btn_ANTWERP_Calculate.Name = "btn_ANTWERP_Calculate";
            this.btn_ANTWERP_Calculate.Size = new System.Drawing.Size(75, 30);
            this.btn_ANTWERP_Calculate.TabIndex = 26;
            this.btn_ANTWERP_Calculate.Text = "Calculate";
            this.btn_ANTWERP_Calculate.UseVisualStyleBackColor = true;
            this.btn_ANTWERP_Calculate.Visible = false;
            this.btn_ANTWERP_Calculate.Click += new System.EventHandler(this.btn_ANTWERP_Calculate_Click);
            // 
            // btn_ANTWERP_Save
            // 
            this.btn_ANTWERP_Save.ApplyMySettings = true;
            this.btn_ANTWERP_Save.CtrlProperty = "Save";
            this.btn_ANTWERP_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_ANTWERP_Save.Location = new System.Drawing.Point(988, 0);
            this.btn_ANTWERP_Save.Name = "btn_ANTWERP_Save";
            this.btn_ANTWERP_Save.Size = new System.Drawing.Size(75, 30);
            this.btn_ANTWERP_Save.TabIndex = 23;
            this.btn_ANTWERP_Save.Text = "Save";
            this.btn_ANTWERP_Save.UseVisualStyleBackColor = true;
            this.btn_ANTWERP_Save.Visible = false;
            this.btn_ANTWERP_Save.Click += new System.EventHandler(this.btn_ANTWERP_Save_Click);
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "Based on REPLACEMENT costs - ANTWERP";
            this.headerLabel3.Location = new System.Drawing.Point(8, 8);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(227, 13);
            this.headerLabel3.TabIndex = 1;
            this.headerLabel3.Text = "Based on REPLACEMENT costs - ANTWERP";
            // 
            // dgvEX_WC_ANTWERP
            // 
            this.dgvEX_WC_ANTWERP.AllowUpDownKey = false;
            this.dgvEX_WC_ANTWERP.AllowUserToAddRows = false;
            this.dgvEX_WC_ANTWERP.AllowUserToDeleteRows = false;
            this.dgvEX_WC_ANTWERP.AllowUserToResizeRows = false;
            this.dgvEX_WC_ANTWERP.ApplyMySettings = false;
            this.dgvEX_WC_ANTWERP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEX_WC_ANTWERP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ANTWERP_ID,
            this.Type,
            this.ANT_MEGAFAT88_25,
            this.ANT_MEGAFAT88_650,
            this.ANT_MEGAFATEXTRA_25,
            this.ANT_MEGAFATEXTRA_650,
            this.ANT_MEGAENERGY_25,
            this.ANT_MEGAENERGY_650,
            this.ANT_MEGABOOST_25,
            this.ANT_MEGABOOST_650,
            this.ANT_MEGAONE_25,
            this.ANT_MEGAONE_650});
            this.dgvEX_WC_ANTWERP.ctlInfoMessage = null;
            this.dgvEX_WC_ANTWERP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEX_WC_ANTWERP.Location = new System.Drawing.Point(3, 63);
            this.dgvEX_WC_ANTWERP.MyAutoGenerateColumn = false;
            this.dgvEX_WC_ANTWERP.MyEditGrid = false;
            this.dgvEX_WC_ANTWERP.Name = "dgvEX_WC_ANTWERP";
            this.dgvEX_WC_ANTWERP.RowHeadersVisible = false;
            this.dgvEX_WC_ANTWERP.RowTemplate.ReadOnly = true;
            this.dgvEX_WC_ANTWERP.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEX_WC_ANTWERP.ShowEditingIcon = false;
            this.dgvEX_WC_ANTWERP.Size = new System.Drawing.Size(1060, 158);
            this.dgvEX_WC_ANTWERP.strInfoMsgArr = null;
            this.dgvEX_WC_ANTWERP.TabIndex = 133;
            this.dgvEX_WC_ANTWERP.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvEX_WC_ANTWERP_CellFormatting);
            // 
            // ANTWERP_ID
            // 
            this.ANTWERP_ID.DataPropertyName = "ANTWERP_ID";
            this.ANTWERP_ID.HeaderText = "ID";
            this.ANTWERP_ID.Name = "ANTWERP_ID";
            this.ANTWERP_ID.ReadOnly = true;
            this.ANTWERP_ID.Visible = false;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 200;
            // 
            // ANT_MEGAFAT88_25
            // 
            this.ANT_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.ANT_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized)";
            this.ANT_MEGAFAT88_25.Name = "ANT_MEGAFAT88_25";
            this.ANT_MEGAFAT88_25.ReadOnly = true;
            this.ANT_MEGAFAT88_25.Width = 150;
            // 
            // ANT_MEGAFAT88_650
            // 
            this.ANT_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.ANT_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg)";
            this.ANT_MEGAFAT88_650.Name = "ANT_MEGAFAT88_650";
            this.ANT_MEGAFAT88_650.ReadOnly = true;
            this.ANT_MEGAFAT88_650.Width = 150;
            // 
            // ANT_MEGAFATEXTRA_25
            // 
            this.ANT_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.ANT_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized)";
            this.ANT_MEGAFATEXTRA_25.Name = "ANT_MEGAFATEXTRA_25";
            this.ANT_MEGAFATEXTRA_25.ReadOnly = true;
            this.ANT_MEGAFATEXTRA_25.Width = 150;
            // 
            // ANT_MEGAFATEXTRA_650
            // 
            this.ANT_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.ANT_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg)";
            this.ANT_MEGAFATEXTRA_650.Name = "ANT_MEGAFATEXTRA_650";
            this.ANT_MEGAFATEXTRA_650.ReadOnly = true;
            this.ANT_MEGAFATEXTRA_650.Width = 150;
            // 
            // ANT_MEGAENERGY_25
            // 
            this.ANT_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.ANT_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized)";
            this.ANT_MEGAENERGY_25.Name = "ANT_MEGAENERGY_25";
            this.ANT_MEGAENERGY_25.ReadOnly = true;
            this.ANT_MEGAENERGY_25.Width = 150;
            // 
            // ANT_MEGAENERGY_650
            // 
            this.ANT_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.ANT_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg)";
            this.ANT_MEGAENERGY_650.Name = "ANT_MEGAENERGY_650";
            this.ANT_MEGAENERGY_650.ReadOnly = true;
            this.ANT_MEGAENERGY_650.Width = 150;
            // 
            // ANT_MEGABOOST_25
            // 
            this.ANT_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.ANT_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized)";
            this.ANT_MEGABOOST_25.Name = "ANT_MEGABOOST_25";
            this.ANT_MEGABOOST_25.ReadOnly = true;
            this.ANT_MEGABOOST_25.Width = 150;
            // 
            // ANT_MEGABOOST_650
            // 
            this.ANT_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.ANT_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg)";
            this.ANT_MEGABOOST_650.Name = "ANT_MEGABOOST_650";
            this.ANT_MEGABOOST_650.ReadOnly = true;
            this.ANT_MEGABOOST_650.Width = 150;
            // 
            // ANT_MEGAONE_25
            // 
            this.ANT_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.ANT_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized)";
            this.ANT_MEGAONE_25.Name = "ANT_MEGAONE_25";
            this.ANT_MEGAONE_25.ReadOnly = true;
            this.ANT_MEGAONE_25.Width = 150;
            // 
            // ANT_MEGAONE_650
            // 
            this.ANT_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.ANT_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg)";
            this.ANT_MEGAONE_650.Name = "ANT_MEGAONE_650";
            this.ANT_MEGAONE_650.ReadOnly = true;
            this.ANT_MEGAONE_650.Width = 150;
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.btn_BRAKE_Calculate);
            this.panel1.Controls.Add(this.btn_BRAKE_Save);
            this.panel1.Controls.Add(this.headerLabel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 224);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel1.Size = new System.Drawing.Size(1066, 30);
            this.panel1.TabIndex = 134;
            // 
            // btn_BRAKE_Calculate
            // 
            this.btn_BRAKE_Calculate.ApplyMySettings = true;
            this.btn_BRAKE_Calculate.CtrlProperty = "Calculate";
            this.btn_BRAKE_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_BRAKE_Calculate.Location = new System.Drawing.Point(913, 0);
            this.btn_BRAKE_Calculate.Name = "btn_BRAKE_Calculate";
            this.btn_BRAKE_Calculate.Size = new System.Drawing.Size(75, 30);
            this.btn_BRAKE_Calculate.TabIndex = 26;
            this.btn_BRAKE_Calculate.Text = "Calculate";
            this.btn_BRAKE_Calculate.UseVisualStyleBackColor = true;
            this.btn_BRAKE_Calculate.Visible = false;
            this.btn_BRAKE_Calculate.Click += new System.EventHandler(this.btn_BRAKE_Calculate_Click);
            // 
            // btn_BRAKE_Save
            // 
            this.btn_BRAKE_Save.ApplyMySettings = true;
            this.btn_BRAKE_Save.CtrlProperty = "Save";
            this.btn_BRAKE_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_BRAKE_Save.Location = new System.Drawing.Point(988, 0);
            this.btn_BRAKE_Save.Name = "btn_BRAKE_Save";
            this.btn_BRAKE_Save.Size = new System.Drawing.Size(75, 30);
            this.btn_BRAKE_Save.TabIndex = 23;
            this.btn_BRAKE_Save.Text = "Save";
            this.btn_BRAKE_Save.UseVisualStyleBackColor = true;
            this.btn_BRAKE_Save.Visible = false;
            this.btn_BRAKE_Save.Click += new System.EventHandler(this.btn_BRAKE_Save_Click);
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "Based on REPLACEMENT costs - BRAKE";
            this.headerLabel1.Location = new System.Drawing.Point(8, 8);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(208, 13);
            this.headerLabel1.TabIndex = 1;
            this.headerLabel1.Text = "Based on REPLACEMENT costs - BRAKE";
            // 
            // panel3
            // 
            this.panel3.ApplyMySettings = true;
            this.panel3.Controls.Add(this.btn_BIBBY_Calculate);
            this.panel3.Controls.Add(this.btn_BIBBY_Save);
            this.panel3.Controls.Add(this.headerLabel2);
            this.panel3.CtrlProperty = "";
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 418);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel3.Size = new System.Drawing.Size(1066, 30);
            this.panel3.TabIndex = 135;
            // 
            // btn_BIBBY_Calculate
            // 
            this.btn_BIBBY_Calculate.ApplyMySettings = true;
            this.btn_BIBBY_Calculate.CtrlProperty = "Calculate";
            this.btn_BIBBY_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_BIBBY_Calculate.Location = new System.Drawing.Point(913, 0);
            this.btn_BIBBY_Calculate.Name = "btn_BIBBY_Calculate";
            this.btn_BIBBY_Calculate.Size = new System.Drawing.Size(75, 30);
            this.btn_BIBBY_Calculate.TabIndex = 26;
            this.btn_BIBBY_Calculate.Text = "Calculate";
            this.btn_BIBBY_Calculate.UseVisualStyleBackColor = true;
            this.btn_BIBBY_Calculate.Visible = false;
            this.btn_BIBBY_Calculate.Click += new System.EventHandler(this.btn_BIBBY_Calculate_Click);
            // 
            // btn_BIBBY_Save
            // 
            this.btn_BIBBY_Save.ApplyMySettings = true;
            this.btn_BIBBY_Save.CtrlProperty = "Save";
            this.btn_BIBBY_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_BIBBY_Save.Location = new System.Drawing.Point(988, 0);
            this.btn_BIBBY_Save.Name = "btn_BIBBY_Save";
            this.btn_BIBBY_Save.Size = new System.Drawing.Size(75, 30);
            this.btn_BIBBY_Save.TabIndex = 23;
            this.btn_BIBBY_Save.Text = "Save";
            this.btn_BIBBY_Save.UseVisualStyleBackColor = true;
            this.btn_BIBBY_Save.Visible = false;
            this.btn_BIBBY_Save.Click += new System.EventHandler(this.btn_BIBBY_Save_Click);
            // 
            // headerLabel2
            // 
            this.headerLabel2.ApplyMySettings = true;
            this.headerLabel2.AutoSize = true;
            this.headerLabel2.CtrlProperty = "Based on REPLACEMENT COST - Bibby";
            this.headerLabel2.Location = new System.Drawing.Point(8, 8);
            this.headerLabel2.Name = "headerLabel2";
            this.headerLabel2.Size = new System.Drawing.Size(202, 13);
            this.headerLabel2.TabIndex = 1;
            this.headerLabel2.Text = "Based on REPLACEMENT COST - Bibby";
            // 
            // dgvEX_WC_BRAKE
            // 
            this.dgvEX_WC_BRAKE.AllowUpDownKey = false;
            this.dgvEX_WC_BRAKE.AllowUserToAddRows = false;
            this.dgvEX_WC_BRAKE.AllowUserToDeleteRows = false;
            this.dgvEX_WC_BRAKE.AllowUserToResizeRows = false;
            this.dgvEX_WC_BRAKE.ApplyMySettings = false;
            this.dgvEX_WC_BRAKE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEX_WC_BRAKE.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BRAKE_ID,
            this.BRAKE_Type,
            this.BRAKE_MEGAFAT88_25,
            this.BRAKE_MEGAFAT88_650,
            this.BRAKE_MEGAFATEXTRA_25,
            this.BRAKE_MEGAFATEXTRA_650,
            this.BRAKE_MEGAENERGY_25,
            this.BRAKE_MEGAENERGY_650,
            this.BRAKE_MEGABOOST_25,
            this.BRAKE_MEGABOOST_650,
            this.BRAKE_MEGAONE_25,
            this.BRAKE_MEGAONE_650});
            this.dgvEX_WC_BRAKE.ctlInfoMessage = null;
            this.dgvEX_WC_BRAKE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEX_WC_BRAKE.Location = new System.Drawing.Point(3, 257);
            this.dgvEX_WC_BRAKE.MyAutoGenerateColumn = false;
            this.dgvEX_WC_BRAKE.MyEditGrid = false;
            this.dgvEX_WC_BRAKE.Name = "dgvEX_WC_BRAKE";
            this.dgvEX_WC_BRAKE.RowHeadersVisible = false;
            this.dgvEX_WC_BRAKE.RowTemplate.ReadOnly = true;
            this.dgvEX_WC_BRAKE.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEX_WC_BRAKE.ShowEditingIcon = false;
            this.dgvEX_WC_BRAKE.Size = new System.Drawing.Size(1060, 158);
            this.dgvEX_WC_BRAKE.strInfoMsgArr = null;
            this.dgvEX_WC_BRAKE.TabIndex = 136;
            this.dgvEX_WC_BRAKE.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvEX_WC_BRAKE_CellFormatting);
            // 
            // BRAKE_ID
            // 
            this.BRAKE_ID.DataPropertyName = "BRAKE_ID";
            this.BRAKE_ID.HeaderText = "ID";
            this.BRAKE_ID.Name = "BRAKE_ID";
            this.BRAKE_ID.ReadOnly = true;
            this.BRAKE_ID.Visible = false;
            // 
            // BRAKE_Type
            // 
            this.BRAKE_Type.DataPropertyName = "Type";
            this.BRAKE_Type.HeaderText = "Type";
            this.BRAKE_Type.Name = "BRAKE_Type";
            this.BRAKE_Type.ReadOnly = true;
            this.BRAKE_Type.Width = 200;
            // 
            // BRAKE_MEGAFAT88_25
            // 
            this.BRAKE_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.BRAKE_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized)";
            this.BRAKE_MEGAFAT88_25.Name = "BRAKE_MEGAFAT88_25";
            this.BRAKE_MEGAFAT88_25.ReadOnly = true;
            this.BRAKE_MEGAFAT88_25.Width = 150;
            // 
            // BRAKE_MEGAFAT88_650
            // 
            this.BRAKE_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.BRAKE_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg)";
            this.BRAKE_MEGAFAT88_650.Name = "BRAKE_MEGAFAT88_650";
            this.BRAKE_MEGAFAT88_650.ReadOnly = true;
            this.BRAKE_MEGAFAT88_650.Width = 150;
            // 
            // BRAKE_MEGAFATEXTRA_25
            // 
            this.BRAKE_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.BRAKE_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized)";
            this.BRAKE_MEGAFATEXTRA_25.Name = "BRAKE_MEGAFATEXTRA_25";
            this.BRAKE_MEGAFATEXTRA_25.ReadOnly = true;
            this.BRAKE_MEGAFATEXTRA_25.Width = 150;
            // 
            // BRAKE_MEGAFATEXTRA_650
            // 
            this.BRAKE_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.BRAKE_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg)";
            this.BRAKE_MEGAFATEXTRA_650.Name = "BRAKE_MEGAFATEXTRA_650";
            this.BRAKE_MEGAFATEXTRA_650.ReadOnly = true;
            this.BRAKE_MEGAFATEXTRA_650.Width = 150;
            // 
            // BRAKE_MEGAENERGY_25
            // 
            this.BRAKE_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.BRAKE_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized)";
            this.BRAKE_MEGAENERGY_25.Name = "BRAKE_MEGAENERGY_25";
            this.BRAKE_MEGAENERGY_25.ReadOnly = true;
            this.BRAKE_MEGAENERGY_25.Width = 150;
            // 
            // BRAKE_MEGAENERGY_650
            // 
            this.BRAKE_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.BRAKE_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg)";
            this.BRAKE_MEGAENERGY_650.Name = "BRAKE_MEGAENERGY_650";
            this.BRAKE_MEGAENERGY_650.ReadOnly = true;
            this.BRAKE_MEGAENERGY_650.Width = 150;
            // 
            // BRAKE_MEGABOOST_25
            // 
            this.BRAKE_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.BRAKE_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized)";
            this.BRAKE_MEGABOOST_25.Name = "BRAKE_MEGABOOST_25";
            this.BRAKE_MEGABOOST_25.ReadOnly = true;
            this.BRAKE_MEGABOOST_25.Width = 150;
            // 
            // BRAKE_MEGABOOST_650
            // 
            this.BRAKE_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.BRAKE_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg)";
            this.BRAKE_MEGABOOST_650.Name = "BRAKE_MEGABOOST_650";
            this.BRAKE_MEGABOOST_650.ReadOnly = true;
            this.BRAKE_MEGABOOST_650.Width = 150;
            // 
            // BRAKE_MEGAONE_25
            // 
            this.BRAKE_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.BRAKE_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized)";
            this.BRAKE_MEGAONE_25.Name = "BRAKE_MEGAONE_25";
            this.BRAKE_MEGAONE_25.ReadOnly = true;
            this.BRAKE_MEGAONE_25.Width = 150;
            // 
            // BRAKE_MEGAONE_650
            // 
            this.BRAKE_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.BRAKE_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg)";
            this.BRAKE_MEGAONE_650.Name = "BRAKE_MEGAONE_650";
            this.BRAKE_MEGAONE_650.ReadOnly = true;
            this.BRAKE_MEGAONE_650.Width = 150;
            // 
            // panel4
            // 
            this.panel4.ApplyMySettings = true;
            this.panel4.Controls.Add(this.btn_AARHUS_Calculate);
            this.panel4.Controls.Add(this.btn_AARHUS_Save);
            this.panel4.Controls.Add(this.headerLabel5);
            this.panel4.CtrlProperty = "";
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 612);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel4.Size = new System.Drawing.Size(1066, 30);
            this.panel4.TabIndex = 142;
            // 
            // btn_AARHUS_Calculate
            // 
            this.btn_AARHUS_Calculate.ApplyMySettings = true;
            this.btn_AARHUS_Calculate.CtrlProperty = "Calculate";
            this.btn_AARHUS_Calculate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_AARHUS_Calculate.Location = new System.Drawing.Point(913, 0);
            this.btn_AARHUS_Calculate.Name = "btn_AARHUS_Calculate";
            this.btn_AARHUS_Calculate.Size = new System.Drawing.Size(75, 30);
            this.btn_AARHUS_Calculate.TabIndex = 26;
            this.btn_AARHUS_Calculate.Text = "Calculate";
            this.btn_AARHUS_Calculate.UseVisualStyleBackColor = true;
            this.btn_AARHUS_Calculate.Visible = false;
            // 
            // btn_AARHUS_Save
            // 
            this.btn_AARHUS_Save.ApplyMySettings = true;
            this.btn_AARHUS_Save.CtrlProperty = "Save";
            this.btn_AARHUS_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_AARHUS_Save.Location = new System.Drawing.Point(988, 0);
            this.btn_AARHUS_Save.Name = "btn_AARHUS_Save";
            this.btn_AARHUS_Save.Size = new System.Drawing.Size(75, 30);
            this.btn_AARHUS_Save.TabIndex = 23;
            this.btn_AARHUS_Save.Text = "Save";
            this.btn_AARHUS_Save.UseVisualStyleBackColor = true;
            this.btn_AARHUS_Save.Visible = false;
            // 
            // headerLabel5
            // 
            this.headerLabel5.ApplyMySettings = true;
            this.headerLabel5.AutoSize = true;
            this.headerLabel5.CtrlProperty = "Based on REPLACEMENT COST - Aarhus";
            this.headerLabel5.Location = new System.Drawing.Point(8, 8);
            this.headerLabel5.Name = "headerLabel5";
            this.headerLabel5.Size = new System.Drawing.Size(209, 13);
            this.headerLabel5.TabIndex = 1;
            this.headerLabel5.Text = "Based on REPLACEMENT COST - Aarhus";
            // 
            // dgvEX_WC_AARHUS
            // 
            this.dgvEX_WC_AARHUS.AllowUpDownKey = false;
            this.dgvEX_WC_AARHUS.AllowUserToAddRows = false;
            this.dgvEX_WC_AARHUS.AllowUserToDeleteRows = false;
            this.dgvEX_WC_AARHUS.AllowUserToResizeRows = false;
            this.dgvEX_WC_AARHUS.ApplyMySettings = false;
            this.dgvEX_WC_AARHUS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEX_WC_AARHUS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.dgvEX_WC_AARHUS.ctlInfoMessage = null;
            this.dgvEX_WC_AARHUS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEX_WC_AARHUS.Location = new System.Drawing.Point(3, 645);
            this.dgvEX_WC_AARHUS.MyAutoGenerateColumn = false;
            this.dgvEX_WC_AARHUS.MyEditGrid = false;
            this.dgvEX_WC_AARHUS.Name = "dgvEX_WC_AARHUS";
            this.dgvEX_WC_AARHUS.RowHeadersVisible = false;
            this.dgvEX_WC_AARHUS.RowTemplate.ReadOnly = true;
            this.dgvEX_WC_AARHUS.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEX_WC_AARHUS.ShowEditingIcon = false;
            this.dgvEX_WC_AARHUS.Size = new System.Drawing.Size(1060, 159);
            this.dgvEX_WC_AARHUS.strInfoMsgArr = null;
            this.dgvEX_WC_AARHUS.TabIndex = 143;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BIBBY_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Type";
            this.dataGridViewTextBoxColumn2.HeaderText = "Type";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn3.HeaderText = "MEGAFAT 88 (25Kg Palletized)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn4.HeaderText = "MEGAFAT 88 (BB 650Kg)";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn5.HeaderText = "MEGAFAT Extra (25Kg Palletized)";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 150;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn6.HeaderText = "MEGAFAT Extra (BB 650Kg)";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn7.HeaderText = "MEGA Energy (25Kg Palletized)";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn8.HeaderText = "MEGA Energy (BB 650Kg)";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 150;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn9.HeaderText = "MEGA Boost (25Kg Palletized)";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn10.HeaderText = "MEGA Boost (BB 650Kg)";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 150;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn11.HeaderText = "MEGA One (25Kg Palletized)";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 150;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn12.HeaderText = "MEGA One (BB 650Kg)";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // frmExWarehouseCost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 807);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmExWarehouseCost";
            this.Text = "Ex-Warehouse Cost";
            this.Load += new System.EventHandler(this.frmExWarehouseCost_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_BIBBY)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_ANTWERP)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_BRAKE)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEX_WC_AARHUS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.Panel panel2;
        private General.Controls.Button btn_ANTWERP_Calculate;
        private General.Controls.Button btn_ANTWERP_Save;
        private General.Controls.HeaderLabel headerLabel3;
        private General.Controls.DataGridView dgvEX_WC_ANTWERP;
        private General.Controls.Panel panel1;
        private General.Controls.Button btn_BRAKE_Calculate;
        private General.Controls.Button btn_BRAKE_Save;
        private General.Controls.HeaderLabel headerLabel1;
        private General.Controls.Panel panel3;
        private General.Controls.Button btn_BIBBY_Calculate;
        private General.Controls.Button btn_BIBBY_Save;
        private General.Controls.HeaderLabel headerLabel2;
        private General.Controls.DataGridView dgvEX_WC_BIBBY;
        private General.Controls.DataGridView dgvEX_WC_BRAKE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANTWERP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANT_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRAKE_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn BIBBY_MEGAONE_650;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel4;
        private General.Controls.Panel panel4;
        private General.Controls.Button btn_AARHUS_Calculate;
        private General.Controls.Button btn_AARHUS_Save;
        private General.Controls.HeaderLabel headerLabel5;
        private General.Controls.DataGridView dgvEX_WC_AARHUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
    }
}