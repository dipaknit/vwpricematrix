﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class frmCustomerAddEditDelete : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmCustomerAddEditDelete()
        {
            InitializeComponent();
        }

        #region Property

        public string Mode { get; set; }

        public string CustomerRowId { get; set; }

        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            frmCustomer objfrmCustomer = new frmCustomer();
            objfrmCustomer.MdiParent = MasterPage.ActiveForm;
            objfrmCustomer.WindowState = FormWindowState.Maximized;
            objfrmCustomer.Width = this.Width;
            objfrmCustomer.Height = this.Height;
            this.Close();
            objfrmCustomer.Show();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string strCustomerId = txtCustomerId.Text.Trim();
            string strCustomerName = txtCustomerName.Text.Trim();
            string strPostCode = txtPostcode.Text.Trim();
            string strCity = txtCity.Text.Trim();
            string strQuery = string.Empty;

            if (Mode == "Add")
            {
                //objclsSqlLayer.MakeDBConnection();
                string[] Param = { "@CustomerId", "@CustomerName", "@PostCode", "@City", "@CreatedBy" };
                string[] Value = { strCustomerId, strCustomerName , strPostCode , strCity, Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Customers_Add", Param, Value);
                //objclsSqlLayer.CloseDBConnection();                
            }
            else if (Mode == "Edit")
            {
                //objclsSqlLayer.MakeDBConnection();
                string[] Param = { "@CustomerRowId", "@CustomerId", "@CustomerName", "@PostCode", "@City", "@UpdatedBy" };
                string[] Value = { CustomerRowId, strCustomerId, strCustomerName, strPostCode, strCity, Support.UserId };
                objclsSqlLayer.ExecuteSP("csp_Customers_Edit", Param, Value);
                //objclsSqlLayer.CloseDBConnection();
            }            

            frmCustomer objfrmCustomer = new frmCustomer();
            objfrmCustomer.MdiParent = MasterPage.ActiveForm;
            objfrmCustomer.WindowState = FormWindowState.Maximized;
            objfrmCustomer.Width = this.Width;
            objfrmCustomer.Height = this.Height;
            this.Close();
            objfrmCustomer.Show();
        }

        private void frmCustomerAddEditDelete_Load(object sender, EventArgs e)
        {
            if (Mode == "Edit")
            {
                string strQuery = " SELECT * FROM CUSTOMERS WHERE CustomerRowId = '" + CustomerRowId + "' AND isdeleted = 0 ";
                //objclsSqlLayer.MakeDBConnection();
                DataTable dtContactDate = objclsSqlLayer.GetDataTable(strQuery);
                //objclsSqlLayer.CloseDBConnection();

                foreach (DataRow item in dtContactDate.Rows)
                {
                    txtCustomerId.Text = item["CustomerId"].ToString();
                    txtCustomerName.Text = item["CustomerName"].ToString();
                    txtPostcode.Text = item["PostCode"].ToString();
                    txtCity.Text = item["City"].ToString();
                    break;
                }
            }
        }
    }
}
