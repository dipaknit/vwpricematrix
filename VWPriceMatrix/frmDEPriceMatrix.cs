﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace VWPriceMatrix
{
    public partial class frmDEPriceMatrix : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmDEPriceMatrix()
        {
            InitializeComponent();
        }

        private void frmDEPriceMatrix_Load(object sender, EventArgs e)
        {
            Get_Region1();
            Get_Region2();
            Get_Region3();
        }

        private void Get_Region1()
        {
            string strQuery = " SELECT * FROM DE_Price_Matrix WHERE Region = 'Region 1' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgvRegion1.AutoGenerateColumns = false;
            dgvRegion1.DataSource = dtRegion;

            dgvDEPLRegion1.AutoGenerateColumns = false;
            dgvDEPLRegion1.DataSource = dtRegion;
        }

        private void Get_Region2()
        {
            string strQuery = " SELECT * FROM DE_Price_Matrix WHERE Region = 'Region 2' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgvRegion2.AutoGenerateColumns = false;
            dgvRegion2.DataSource = dtRegion;

            dgvDEPLRegion2.AutoGenerateColumns = false;
            dgvDEPLRegion2.DataSource = dtRegion;
        }

        private void Get_Region3()
        {
            string strQuery = " SELECT * FROM DE_Price_Matrix WHERE Region = 'Region 3' AND isdeleted = 0 Order by ID";
            DataTable dtRegion = objclsSqlLayer.GetDataTable(strQuery);
            dgvRegion3.AutoGenerateColumns = false;
            dgvRegion3.DataSource = dtRegion;

            dgvDEPLRegion3.AutoGenerateColumns = false;
            dgvDEPLRegion3.DataSource = dtRegion;
        }

        private void btnDEExcelExport_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.DisplayAlerts = false;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            //==============================================================
            int intCol = 2;
            int intRow = 2;

            xlWorkSheet.Range["B1:O1"].Merge();
            xlWorkSheet.Cells[1,2] = "Germany Region 1 - full load delivered prices";
            xlWorkSheet.Range["B1:O1"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B1:O1"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B1:O1"].Cells.Font.Size = 14;


            foreach (DataGridViewColumn item in dgvRegion1.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 3;

            foreach (DataGridViewRow Row in dgvRegion1.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvRegion1.Columns.Count; i++)
                {
                    if (dgvRegion1.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            Excel.Range range = xlWorkSheet.Range["B1:O3"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //==============================================================
            
            intCol = 2;
            intRow = 6;

            xlWorkSheet.Range["B5:O5"].Merge();
            xlWorkSheet.Cells[5, 2] = "Germany Region 2 - full load delivered prices";
            xlWorkSheet.Range["B5:O5"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B5:O5"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B5:O5"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgvRegion2.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 7;

            foreach (DataGridViewRow Row in dgvRegion2.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvRegion2.Columns.Count; i++)
                {
                    if (dgvRegion2.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B5:O7"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //==============================================================

            intCol = 2;
            intRow = 10;

            xlWorkSheet.Range["B9:O9"].Merge();
            xlWorkSheet.Cells[9, 2] = "Germany Region 3 - full load delivered prices";
            xlWorkSheet.Range["B9:O9"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B9:O9"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B9:O9"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgvRegion3.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 11;

            foreach (DataGridViewRow Row in dgvRegion3.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvRegion3.Columns.Count; i++)
                {
                    if (dgvRegion3.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B9:O11"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;
            
            //==============================================================

            if (!Directory.Exists(System.Windows.Forms.Application.StartupPath + "\\Excel\\"))
            {
                Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + "\\Excel\\");
            }

            string strFilename = System.Windows.Forms.Application.StartupPath + "\\Excel\\" + "DE_PRICE_MATRIX_" + DateTime.Now.Ticks + ".xls";


            Excel.Range er = xlWorkSheet.get_Range("B:O", System.Type.Missing);
            er.EntireColumn.ColumnWidth = 15.71;

            Excel.Range r1 = xlWorkSheet.get_Range("B2:O2", System.Type.Missing);
            r1.WrapText = true;
            r1.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r2 = xlWorkSheet.get_Range("B6:O6", System.Type.Missing);
            r2.WrapText = true;
            r2.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r3 = xlWorkSheet.get_Range("B10:O10", System.Type.Missing);
            r3.WrapText = true;
            r3.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            //xlWorkSheet.Rows.AutoFit();
            //xlWorkSheet.Columns.AutoFit();

            xlWorkBook.SaveAs(strFilename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            Process.Start(strFilename);
        }

        private void btnDEPLExcelExport_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.DisplayAlerts = false;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            //==============================================================
            int intCol = 2;
            int intRow = 2;

            xlWorkSheet.Range["B1:O1"].Merge();
            xlWorkSheet.Cells[1, 2] = "Germany Region 1 - full load delivered prices";
            xlWorkSheet.Range["B1:O1"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B1:O1"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B1:O1"].Cells.Font.Size = 14;


            foreach (DataGridViewColumn item in dgvDEPLRegion1.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 3;

            foreach (DataGridViewRow Row in dgvDEPLRegion1.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvDEPLRegion1.Columns.Count; i++)
                {
                    if (dgvDEPLRegion1.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            Excel.Range range = xlWorkSheet.Range["B1:O3"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //==============================================================

            intCol = 2;
            intRow = 6;

            xlWorkSheet.Range["B5:O5"].Merge();
            xlWorkSheet.Cells[5, 2] = "Germany Region 2 - full load delivered prices";
            xlWorkSheet.Range["B5:O5"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B5:O5"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B5:O5"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgvDEPLRegion2.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 7;

            foreach (DataGridViewRow Row in dgvDEPLRegion2.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvDEPLRegion2.Columns.Count; i++)
                {
                    if (dgvDEPLRegion2.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B5:O7"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //==============================================================

            intCol = 2;
            intRow = 10;

            xlWorkSheet.Range["B9:O9"].Merge();
            xlWorkSheet.Cells[9, 2] = "Germany Region 3 - full load delivered prices";
            xlWorkSheet.Range["B9:O9"].Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWorkSheet.Range["B9:O9"].Cells.Font.Bold = true;
            xlWorkSheet.Range["B9:O9"].Cells.Font.Size = 14;

            foreach (DataGridViewColumn item in dgvDEPLRegion3.Columns)
            {
                if (item.Visible)
                {
                    xlWorkSheet.Cells[intRow, intCol] = item.HeaderText;
                    intCol++;
                }
            }

            intRow = 11;

            foreach (DataGridViewRow Row in dgvDEPLRegion3.Rows)
            {
                intCol = 2;

                for (int i = 0; i < dgvDEPLRegion3.Columns.Count; i++)
                {
                    if (dgvDEPLRegion3.Columns[i].Visible)
                    {
                        xlWorkSheet.Cells[intRow, intCol] = Row.Cells[i].Value;
                        intCol++;
                    }
                }

                intRow++;
            }

            range = xlWorkSheet.Range["B9:O11"];
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;
            range.Borders.Color = Color.Black;

            //==============================================================

            if (!Directory.Exists(System.Windows.Forms.Application.StartupPath + "\\Excel\\"))
            {
                Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + "\\Excel\\");
            }

            string strFilename = System.Windows.Forms.Application.StartupPath + "\\Excel\\" + "DE_PRICE_MATRIX_" + DateTime.Now.Ticks + ".xls";

            Excel.Range er = xlWorkSheet.get_Range("B:O", System.Type.Missing);
            er.EntireColumn.ColumnWidth = 15.71;

            Excel.Range r1 = xlWorkSheet.get_Range("B2:O2", System.Type.Missing);
            r1.WrapText = true;
            r1.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r2 = xlWorkSheet.get_Range("B6:O6", System.Type.Missing);
            r2.WrapText = true;
            r2.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.Range r3 = xlWorkSheet.get_Range("B10:O10", System.Type.Missing);
            r3.WrapText = true;
            r3.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            //xlWorkSheet.Rows.AutoFit();
            //xlWorkSheet.Columns.AutoFit();

            xlWorkBook.SaveAs(strFilename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            Process.Start(strFilename);
        }
    }
}
