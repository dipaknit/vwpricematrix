﻿namespace VWPriceMatrix
{
    partial class frmDEPriceMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDEPriceMatrix));
            this.panel1 = new General.Controls.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvDEPLRegion3 = new General.Controls.DataGridView();
            this.panel6 = new General.Controls.Panel();
            this.headerLabel8 = new General.Controls.HeaderLabel();
            this.dgvDEPLRegion2 = new General.Controls.DataGridView();
            this.panel5 = new General.Controls.Panel();
            this.headerLabel6 = new General.Controls.HeaderLabel();
            this.dgvDEPLRegion1 = new General.Controls.DataGridView();
            this.panel4 = new General.Controls.Panel();
            this.btnDEPLExcelExport = new General.Controls.Button();
            this.headerLabel5 = new General.Controls.HeaderLabel();
            this.panelHeader2 = new General.Controls.PanelHeader();
            this.headerLabel4 = new General.Controls.HeaderLabel();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.dgvRegion3 = new General.Controls.DataGridView();
            this.panel7 = new General.Controls.Panel();
            this.btnDEExcelExport = new General.Controls.Button();
            this.headerLabel7 = new General.Controls.HeaderLabel();
            this.dgvRegion1 = new General.Controls.DataGridView();
            this.panel2 = new General.Controls.Panel();
            this.headerLabel1 = new General.Controls.HeaderLabel();
            this.dgvRegion2 = new General.Controls.DataGridView();
            this.panel3 = new General.Controls.Panel();
            this.headerLabel2 = new General.Controls.HeaderLabel();
            this.R1_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R1_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R2_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFAT88_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFAT88_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFATEXTRA_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAFATEXTRA_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAENERGY_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAENERGY_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGABOOST_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGABOOST_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAONE_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAONE_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAMAX_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGAMAX_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGALAC_25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R3_MEGALAC_650 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDEPLRegion3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDEPLRegion2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDEPLRegion1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panelHeader2.SuspendLayout();
            this.panelHeader1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegion3)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegion1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegion2)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 730);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dgvDEPLRegion3, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.dgvDEPLRegion2, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.dgvDEPLRegion1, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.panelHeader2, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvRegion3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgvRegion1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dgvRegion2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dgvDEPLRegion3
            // 
            this.dgvDEPLRegion3.AllowUpDownKey = false;
            this.dgvDEPLRegion3.AllowUserToAddRows = false;
            this.dgvDEPLRegion3.AllowUserToDeleteRows = false;
            this.dgvDEPLRegion3.AllowUserToResizeRows = false;
            this.dgvDEPLRegion3.ApplyMySettings = false;
            this.dgvDEPLRegion3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDEPLRegion3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.Column5,
            this.Column6,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39});
            this.dgvDEPLRegion3.ctlInfoMessage = null;
            this.dgvDEPLRegion3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDEPLRegion3.Location = new System.Drawing.Point(3, 648);
            this.dgvDEPLRegion3.MyAutoGenerateColumn = false;
            this.dgvDEPLRegion3.MyEditGrid = false;
            this.dgvDEPLRegion3.Name = "dgvDEPLRegion3";
            this.dgvDEPLRegion3.RowHeadersVisible = false;
            this.dgvDEPLRegion3.RowTemplate.ReadOnly = true;
            this.dgvDEPLRegion3.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDEPLRegion3.ShowEditingIcon = false;
            this.dgvDEPLRegion3.Size = new System.Drawing.Size(1002, 79);
            this.dgvDEPLRegion3.strInfoMsgArr = null;
            this.dgvDEPLRegion3.TabIndex = 147;
            // 
            // panel6
            // 
            this.panel6.ApplyMySettings = true;
            this.panel6.Controls.Add(this.headerLabel8);
            this.panel6.CtrlProperty = "";
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 615);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel6.Size = new System.Drawing.Size(1008, 30);
            this.panel6.TabIndex = 146;
            // 
            // headerLabel8
            // 
            this.headerLabel8.ApplyMySettings = true;
            this.headerLabel8.AutoSize = true;
            this.headerLabel8.CtrlProperty = "Germany Region 3";
            this.headerLabel8.Location = new System.Drawing.Point(8, 9);
            this.headerLabel8.Name = "headerLabel8";
            this.headerLabel8.Size = new System.Drawing.Size(95, 13);
            this.headerLabel8.TabIndex = 1;
            this.headerLabel8.Text = "Germany Region 3";
            // 
            // dgvDEPLRegion2
            // 
            this.dgvDEPLRegion2.AllowUpDownKey = false;
            this.dgvDEPLRegion2.AllowUserToAddRows = false;
            this.dgvDEPLRegion2.AllowUserToDeleteRows = false;
            this.dgvDEPLRegion2.AllowUserToResizeRows = false;
            this.dgvDEPLRegion2.ApplyMySettings = false;
            this.dgvDEPLRegion2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDEPLRegion2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.Column1,
            this.Column2,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26});
            this.dgvDEPLRegion2.ctlInfoMessage = null;
            this.dgvDEPLRegion2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDEPLRegion2.Location = new System.Drawing.Point(3, 537);
            this.dgvDEPLRegion2.MyAutoGenerateColumn = false;
            this.dgvDEPLRegion2.MyEditGrid = false;
            this.dgvDEPLRegion2.Name = "dgvDEPLRegion2";
            this.dgvDEPLRegion2.RowHeadersVisible = false;
            this.dgvDEPLRegion2.RowTemplate.ReadOnly = true;
            this.dgvDEPLRegion2.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDEPLRegion2.ShowEditingIcon = false;
            this.dgvDEPLRegion2.Size = new System.Drawing.Size(1002, 75);
            this.dgvDEPLRegion2.strInfoMsgArr = null;
            this.dgvDEPLRegion2.TabIndex = 145;
            // 
            // panel5
            // 
            this.panel5.ApplyMySettings = true;
            this.panel5.Controls.Add(this.headerLabel6);
            this.panel5.CtrlProperty = "";
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 504);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel5.Size = new System.Drawing.Size(1008, 30);
            this.panel5.TabIndex = 144;
            // 
            // headerLabel6
            // 
            this.headerLabel6.ApplyMySettings = true;
            this.headerLabel6.AutoSize = true;
            this.headerLabel6.CtrlProperty = "Germany Region 2";
            this.headerLabel6.Location = new System.Drawing.Point(8, 9);
            this.headerLabel6.Name = "headerLabel6";
            this.headerLabel6.Size = new System.Drawing.Size(95, 13);
            this.headerLabel6.TabIndex = 1;
            this.headerLabel6.Text = "Germany Region 2";
            // 
            // dgvDEPLRegion1
            // 
            this.dgvDEPLRegion1.AllowUpDownKey = false;
            this.dgvDEPLRegion1.AllowUserToAddRows = false;
            this.dgvDEPLRegion1.AllowUserToDeleteRows = false;
            this.dgvDEPLRegion1.AllowUserToResizeRows = false;
            this.dgvDEPLRegion1.ApplyMySettings = false;
            this.dgvDEPLRegion1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDEPLRegion1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.Column3,
            this.Column4,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dgvDEPLRegion1.ctlInfoMessage = null;
            this.dgvDEPLRegion1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDEPLRegion1.Location = new System.Drawing.Point(3, 426);
            this.dgvDEPLRegion1.MyAutoGenerateColumn = false;
            this.dgvDEPLRegion1.MyEditGrid = false;
            this.dgvDEPLRegion1.Name = "dgvDEPLRegion1";
            this.dgvDEPLRegion1.RowHeadersVisible = false;
            this.dgvDEPLRegion1.RowTemplate.ReadOnly = true;
            this.dgvDEPLRegion1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDEPLRegion1.ShowEditingIcon = false;
            this.dgvDEPLRegion1.Size = new System.Drawing.Size(1002, 75);
            this.dgvDEPLRegion1.strInfoMsgArr = null;
            this.dgvDEPLRegion1.TabIndex = 143;
            // 
            // panel4
            // 
            this.panel4.ApplyMySettings = true;
            this.panel4.Controls.Add(this.btnDEPLExcelExport);
            this.panel4.Controls.Add(this.headerLabel5);
            this.panel4.CtrlProperty = "";
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 396);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel4.Size = new System.Drawing.Size(1002, 24);
            this.panel4.TabIndex = 142;
            // 
            // btnDEPLExcelExport
            // 
            this.btnDEPLExcelExport.ApplyMySettings = true;
            this.btnDEPLExcelExport.CtrlProperty = "Excel Export";
            this.btnDEPLExcelExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDEPLExcelExport.Location = new System.Drawing.Point(924, 0);
            this.btnDEPLExcelExport.Name = "btnDEPLExcelExport";
            this.btnDEPLExcelExport.Size = new System.Drawing.Size(75, 24);
            this.btnDEPLExcelExport.TabIndex = 4;
            this.btnDEPLExcelExport.Text = "Excel Export";
            this.btnDEPLExcelExport.UseVisualStyleBackColor = true;
            this.btnDEPLExcelExport.Click += new System.EventHandler(this.btnDEPLExcelExport_Click);
            // 
            // headerLabel5
            // 
            this.headerLabel5.ApplyMySettings = true;
            this.headerLabel5.AutoSize = true;
            this.headerLabel5.CtrlProperty = "Germany Region 1";
            this.headerLabel5.Location = new System.Drawing.Point(8, 5);
            this.headerLabel5.Name = "headerLabel5";
            this.headerLabel5.Size = new System.Drawing.Size(95, 13);
            this.headerLabel5.TabIndex = 1;
            this.headerLabel5.Text = "Germany Region 1";
            // 
            // panelHeader2
            // 
            this.panelHeader2.ApplyMySettings = true;
            this.panelHeader2.Controls.Add(this.headerLabel4);
            this.panelHeader2.CtrlProperty = "";
            this.panelHeader2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader2.Location = new System.Drawing.Point(3, 366);
            this.panelHeader2.Name = "panelHeader2";
            this.panelHeader2.Size = new System.Drawing.Size(1002, 24);
            this.panelHeader2.TabIndex = 141;
            // 
            // headerLabel4
            // 
            this.headerLabel4.ApplyMySettings = true;
            this.headerLabel4.AutoSize = true;
            this.headerLabel4.CtrlProperty = "DE PL Price Matrix";
            this.headerLabel4.Location = new System.Drawing.Point(8, 6);
            this.headerLabel4.Name = "headerLabel4";
            this.headerLabel4.Size = new System.Drawing.Size(96, 13);
            this.headerLabel4.TabIndex = 0;
            this.headerLabel4.Text = "DE PL Price Matrix";
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel3);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1002, 24);
            this.panelHeader1.TabIndex = 140;
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "DE Price Matrix";
            this.headerLabel3.Location = new System.Drawing.Point(8, 6);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(80, 13);
            this.headerLabel3.TabIndex = 0;
            this.headerLabel3.Text = "DE Price Matrix";
            // 
            // dgvRegion3
            // 
            this.dgvRegion3.AllowUpDownKey = false;
            this.dgvRegion3.AllowUserToAddRows = false;
            this.dgvRegion3.AllowUserToDeleteRows = false;
            this.dgvRegion3.AllowUserToResizeRows = false;
            this.dgvRegion3.ApplyMySettings = false;
            this.dgvRegion3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRegion3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R3_ID,
            this.R3_MEGAFAT88_25,
            this.R3_MEGAFAT88_650,
            this.R3_MEGAFATEXTRA_25,
            this.R3_MEGAFATEXTRA_650,
            this.R3_MEGAENERGY_25,
            this.R3_MEGAENERGY_650,
            this.R3_MEGABOOST_25,
            this.R3_MEGABOOST_650,
            this.R3_MEGAONE_25,
            this.R3_MEGAONE_650,
            this.R3_MEGAMAX_25,
            this.R3_MEGAMAX_650,
            this.R3_MEGALAC_25,
            this.R3_MEGALAC_650});
            this.dgvRegion3.ctlInfoMessage = null;
            this.dgvRegion3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRegion3.Location = new System.Drawing.Point(3, 285);
            this.dgvRegion3.MyAutoGenerateColumn = false;
            this.dgvRegion3.MyEditGrid = false;
            this.dgvRegion3.Name = "dgvRegion3";
            this.dgvRegion3.RowHeadersVisible = false;
            this.dgvRegion3.RowTemplate.ReadOnly = true;
            this.dgvRegion3.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRegion3.ShowEditingIcon = false;
            this.dgvRegion3.Size = new System.Drawing.Size(1002, 75);
            this.dgvRegion3.strInfoMsgArr = null;
            this.dgvRegion3.TabIndex = 138;
            // 
            // panel7
            // 
            this.panel7.ApplyMySettings = true;
            this.panel7.Controls.Add(this.btnDEExcelExport);
            this.panel7.Controls.Add(this.headerLabel7);
            this.panel7.CtrlProperty = "";
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 33);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel7.Size = new System.Drawing.Size(1002, 24);
            this.panel7.TabIndex = 133;
            // 
            // btnDEExcelExport
            // 
            this.btnDEExcelExport.ApplyMySettings = true;
            this.btnDEExcelExport.CtrlProperty = "Excel Export";
            this.btnDEExcelExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDEExcelExport.Location = new System.Drawing.Point(924, 0);
            this.btnDEExcelExport.Name = "btnDEExcelExport";
            this.btnDEExcelExport.Size = new System.Drawing.Size(75, 24);
            this.btnDEExcelExport.TabIndex = 3;
            this.btnDEExcelExport.Text = "Excel Export";
            this.btnDEExcelExport.UseVisualStyleBackColor = true;
            this.btnDEExcelExport.Click += new System.EventHandler(this.btnDEExcelExport_Click);
            // 
            // headerLabel7
            // 
            this.headerLabel7.ApplyMySettings = true;
            this.headerLabel7.AutoSize = true;
            this.headerLabel7.CtrlProperty = "Germany Region 1";
            this.headerLabel7.Location = new System.Drawing.Point(8, 5);
            this.headerLabel7.Name = "headerLabel7";
            this.headerLabel7.Size = new System.Drawing.Size(95, 13);
            this.headerLabel7.TabIndex = 1;
            this.headerLabel7.Text = "Germany Region 1";
            // 
            // dgvRegion1
            // 
            this.dgvRegion1.AllowUpDownKey = false;
            this.dgvRegion1.AllowUserToAddRows = false;
            this.dgvRegion1.AllowUserToDeleteRows = false;
            this.dgvRegion1.AllowUserToResizeRows = false;
            this.dgvRegion1.ApplyMySettings = false;
            this.dgvRegion1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRegion1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R1_ID,
            this.R1_MEGAFAT88_25,
            this.R1_MEGAFAT88_650,
            this.R1_MEGAFATEXTRA_25,
            this.R1_MEGAFATEXTRA_650,
            this.R1_MEGAENERGY_25,
            this.R1_MEGAENERGY_650,
            this.R1_MEGABOOST_25,
            this.R1_MEGABOOST_650,
            this.R1_MEGAONE_25,
            this.R1_MEGAONE_650,
            this.R1_MEGAMAX_25,
            this.R1_MEGAMAX_650,
            this.R1_MEGALAC_25,
            this.R1_MEGALAC_650});
            this.dgvRegion1.ctlInfoMessage = null;
            this.dgvRegion1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRegion1.Location = new System.Drawing.Point(3, 63);
            this.dgvRegion1.MyAutoGenerateColumn = false;
            this.dgvRegion1.MyEditGrid = false;
            this.dgvRegion1.Name = "dgvRegion1";
            this.dgvRegion1.RowHeadersVisible = false;
            this.dgvRegion1.RowTemplate.ReadOnly = true;
            this.dgvRegion1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRegion1.ShowEditingIcon = false;
            this.dgvRegion1.Size = new System.Drawing.Size(1002, 75);
            this.dgvRegion1.strInfoMsgArr = null;
            this.dgvRegion1.TabIndex = 134;
            // 
            // panel2
            // 
            this.panel2.ApplyMySettings = true;
            this.panel2.Controls.Add(this.headerLabel1);
            this.panel2.CtrlProperty = "";
            this.panel2.Location = new System.Drawing.Point(0, 141);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel2.Size = new System.Drawing.Size(1008, 30);
            this.panel2.TabIndex = 135;
            // 
            // headerLabel1
            // 
            this.headerLabel1.ApplyMySettings = true;
            this.headerLabel1.AutoSize = true;
            this.headerLabel1.CtrlProperty = "Germany Region 2";
            this.headerLabel1.Location = new System.Drawing.Point(8, 9);
            this.headerLabel1.Name = "headerLabel1";
            this.headerLabel1.Size = new System.Drawing.Size(95, 13);
            this.headerLabel1.TabIndex = 1;
            this.headerLabel1.Text = "Germany Region 2";
            // 
            // dgvRegion2
            // 
            this.dgvRegion2.AllowUpDownKey = false;
            this.dgvRegion2.AllowUserToAddRows = false;
            this.dgvRegion2.AllowUserToDeleteRows = false;
            this.dgvRegion2.AllowUserToResizeRows = false;
            this.dgvRegion2.ApplyMySettings = false;
            this.dgvRegion2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRegion2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.R2_ID,
            this.R2_MEGAFAT88_25,
            this.R2_MEGAFAT88_650,
            this.R2_MEGAFATEXTRA_25,
            this.R2_MEGAFATEXTRA_650,
            this.R2_MEGAENERGY_25,
            this.R2_MEGAENERGY_650,
            this.R2_MEGABOOST_25,
            this.R2_MEGABOOST_650,
            this.R2_MEGAONE_25,
            this.R2_MEGAONE_650,
            this.R2_MEGAMAX_25,
            this.R2_MEGAMAX_650,
            this.R2_MEGALAC_25,
            this.R2_MEGALAC_650});
            this.dgvRegion2.ctlInfoMessage = null;
            this.dgvRegion2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRegion2.Location = new System.Drawing.Point(3, 174);
            this.dgvRegion2.MyAutoGenerateColumn = false;
            this.dgvRegion2.MyEditGrid = false;
            this.dgvRegion2.Name = "dgvRegion2";
            this.dgvRegion2.RowHeadersVisible = false;
            this.dgvRegion2.RowTemplate.ReadOnly = true;
            this.dgvRegion2.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRegion2.ShowEditingIcon = false;
            this.dgvRegion2.Size = new System.Drawing.Size(1002, 75);
            this.dgvRegion2.strInfoMsgArr = null;
            this.dgvRegion2.TabIndex = 136;
            // 
            // panel3
            // 
            this.panel3.ApplyMySettings = true;
            this.panel3.Controls.Add(this.headerLabel2);
            this.panel3.CtrlProperty = "";
            this.panel3.Location = new System.Drawing.Point(0, 252);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel3.Size = new System.Drawing.Size(1008, 30);
            this.panel3.TabIndex = 137;
            // 
            // headerLabel2
            // 
            this.headerLabel2.ApplyMySettings = true;
            this.headerLabel2.AutoSize = true;
            this.headerLabel2.CtrlProperty = "Germany Region 3";
            this.headerLabel2.Location = new System.Drawing.Point(8, 9);
            this.headerLabel2.Name = "headerLabel2";
            this.headerLabel2.Size = new System.Drawing.Size(95, 13);
            this.headerLabel2.TabIndex = 1;
            this.headerLabel2.Text = "Germany Region 3";
            // 
            // R1_ID
            // 
            this.R1_ID.DataPropertyName = "ID";
            this.R1_ID.HeaderText = "ID";
            this.R1_ID.Name = "R1_ID";
            this.R1_ID.ReadOnly = true;
            this.R1_ID.Visible = false;
            // 
            // R1_MEGAFAT88_25
            // 
            this.R1_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R1_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAFAT88_25.Name = "R1_MEGAFAT88_25";
            this.R1_MEGAFAT88_25.ReadOnly = true;
            this.R1_MEGAFAT88_25.Width = 150;
            // 
            // R1_MEGAFAT88_650
            // 
            this.R1_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R1_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R1_MEGAFAT88_650.Name = "R1_MEGAFAT88_650";
            this.R1_MEGAFAT88_650.ReadOnly = true;
            this.R1_MEGAFAT88_650.Width = 150;
            // 
            // R1_MEGAFATEXTRA_25
            // 
            this.R1_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R1_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAFATEXTRA_25.Name = "R1_MEGAFATEXTRA_25";
            this.R1_MEGAFATEXTRA_25.ReadOnly = true;
            this.R1_MEGAFATEXTRA_25.Width = 150;
            // 
            // R1_MEGAFATEXTRA_650
            // 
            this.R1_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R1_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R1_MEGAFATEXTRA_650.Name = "R1_MEGAFATEXTRA_650";
            this.R1_MEGAFATEXTRA_650.ReadOnly = true;
            this.R1_MEGAFATEXTRA_650.Width = 150;
            // 
            // R1_MEGAENERGY_25
            // 
            this.R1_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R1_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAENERGY_25.Name = "R1_MEGAENERGY_25";
            this.R1_MEGAENERGY_25.ReadOnly = true;
            this.R1_MEGAENERGY_25.Width = 150;
            // 
            // R1_MEGAENERGY_650
            // 
            this.R1_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R1_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R1_MEGAENERGY_650.Name = "R1_MEGAENERGY_650";
            this.R1_MEGAENERGY_650.ReadOnly = true;
            this.R1_MEGAENERGY_650.Width = 150;
            // 
            // R1_MEGABOOST_25
            // 
            this.R1_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R1_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R1_MEGABOOST_25.Name = "R1_MEGABOOST_25";
            this.R1_MEGABOOST_25.ReadOnly = true;
            this.R1_MEGABOOST_25.Width = 150;
            // 
            // R1_MEGABOOST_650
            // 
            this.R1_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R1_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R1_MEGABOOST_650.Name = "R1_MEGABOOST_650";
            this.R1_MEGABOOST_650.ReadOnly = true;
            this.R1_MEGABOOST_650.Width = 150;
            // 
            // R1_MEGAONE_25
            // 
            this.R1_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R1_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAONE_25.Name = "R1_MEGAONE_25";
            this.R1_MEGAONE_25.ReadOnly = true;
            this.R1_MEGAONE_25.Width = 150;
            // 
            // R1_MEGAONE_650
            // 
            this.R1_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R1_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R1_MEGAONE_650.Name = "R1_MEGAONE_650";
            this.R1_MEGAONE_650.ReadOnly = true;
            this.R1_MEGAONE_650.Width = 150;
            // 
            // R1_MEGAMAX_25
            // 
            this.R1_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R1_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R1_MEGAMAX_25.Name = "R1_MEGAMAX_25";
            this.R1_MEGAMAX_25.ReadOnly = true;
            this.R1_MEGAMAX_25.Width = 150;
            // 
            // R1_MEGAMAX_650
            // 
            this.R1_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R1_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R1_MEGAMAX_650.Name = "R1_MEGAMAX_650";
            this.R1_MEGAMAX_650.ReadOnly = true;
            this.R1_MEGAMAX_650.Width = 150;
            // 
            // R1_MEGALAC_25
            // 
            this.R1_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R1_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R1_MEGALAC_25.Name = "R1_MEGALAC_25";
            this.R1_MEGALAC_25.ReadOnly = true;
            this.R1_MEGALAC_25.Width = 150;
            // 
            // R1_MEGALAC_650
            // 
            this.R1_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R1_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R1_MEGALAC_650.Name = "R1_MEGALAC_650";
            this.R1_MEGALAC_650.ReadOnly = true;
            this.R1_MEGALAC_650.Width = 150;
            // 
            // R2_ID
            // 
            this.R2_ID.DataPropertyName = "ID";
            this.R2_ID.HeaderText = "ID";
            this.R2_ID.Name = "R2_ID";
            this.R2_ID.ReadOnly = true;
            this.R2_ID.Visible = false;
            // 
            // R2_MEGAFAT88_25
            // 
            this.R2_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R2_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAFAT88_25.Name = "R2_MEGAFAT88_25";
            this.R2_MEGAFAT88_25.ReadOnly = true;
            this.R2_MEGAFAT88_25.Width = 150;
            // 
            // R2_MEGAFAT88_650
            // 
            this.R2_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R2_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R2_MEGAFAT88_650.Name = "R2_MEGAFAT88_650";
            this.R2_MEGAFAT88_650.ReadOnly = true;
            this.R2_MEGAFAT88_650.Width = 150;
            // 
            // R2_MEGAFATEXTRA_25
            // 
            this.R2_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R2_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAFATEXTRA_25.Name = "R2_MEGAFATEXTRA_25";
            this.R2_MEGAFATEXTRA_25.ReadOnly = true;
            this.R2_MEGAFATEXTRA_25.Width = 150;
            // 
            // R2_MEGAFATEXTRA_650
            // 
            this.R2_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R2_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R2_MEGAFATEXTRA_650.Name = "R2_MEGAFATEXTRA_650";
            this.R2_MEGAFATEXTRA_650.ReadOnly = true;
            this.R2_MEGAFATEXTRA_650.Width = 150;
            // 
            // R2_MEGAENERGY_25
            // 
            this.R2_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R2_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAENERGY_25.Name = "R2_MEGAENERGY_25";
            this.R2_MEGAENERGY_25.ReadOnly = true;
            this.R2_MEGAENERGY_25.Width = 150;
            // 
            // R2_MEGAENERGY_650
            // 
            this.R2_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R2_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R2_MEGAENERGY_650.Name = "R2_MEGAENERGY_650";
            this.R2_MEGAENERGY_650.ReadOnly = true;
            this.R2_MEGAENERGY_650.Width = 150;
            // 
            // R2_MEGABOOST_25
            // 
            this.R2_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R2_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R2_MEGABOOST_25.Name = "R2_MEGABOOST_25";
            this.R2_MEGABOOST_25.ReadOnly = true;
            this.R2_MEGABOOST_25.Width = 150;
            // 
            // R2_MEGABOOST_650
            // 
            this.R2_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R2_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R2_MEGABOOST_650.Name = "R2_MEGABOOST_650";
            this.R2_MEGABOOST_650.ReadOnly = true;
            this.R2_MEGABOOST_650.Width = 150;
            // 
            // R2_MEGAONE_25
            // 
            this.R2_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R2_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAONE_25.Name = "R2_MEGAONE_25";
            this.R2_MEGAONE_25.ReadOnly = true;
            this.R2_MEGAONE_25.Width = 150;
            // 
            // R2_MEGAONE_650
            // 
            this.R2_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R2_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R2_MEGAONE_650.Name = "R2_MEGAONE_650";
            this.R2_MEGAONE_650.ReadOnly = true;
            this.R2_MEGAONE_650.Width = 150;
            // 
            // R2_MEGAMAX_25
            // 
            this.R2_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R2_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R2_MEGAMAX_25.Name = "R2_MEGAMAX_25";
            this.R2_MEGAMAX_25.ReadOnly = true;
            this.R2_MEGAMAX_25.Width = 150;
            // 
            // R2_MEGAMAX_650
            // 
            this.R2_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R2_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R2_MEGAMAX_650.Name = "R2_MEGAMAX_650";
            this.R2_MEGAMAX_650.ReadOnly = true;
            this.R2_MEGAMAX_650.Width = 150;
            // 
            // R2_MEGALAC_25
            // 
            this.R2_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R2_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R2_MEGALAC_25.Name = "R2_MEGALAC_25";
            this.R2_MEGALAC_25.ReadOnly = true;
            this.R2_MEGALAC_25.Width = 150;
            // 
            // R2_MEGALAC_650
            // 
            this.R2_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R2_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R2_MEGALAC_650.Name = "R2_MEGALAC_650";
            this.R2_MEGALAC_650.ReadOnly = true;
            this.R2_MEGALAC_650.Width = 150;
            // 
            // R3_ID
            // 
            this.R3_ID.DataPropertyName = "ID";
            this.R3_ID.HeaderText = "ID";
            this.R3_ID.Name = "R3_ID";
            this.R3_ID.ReadOnly = true;
            this.R3_ID.Visible = false;
            // 
            // R3_MEGAFAT88_25
            // 
            this.R3_MEGAFAT88_25.DataPropertyName = "MEGAFAT88_25";
            this.R3_MEGAFAT88_25.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAFAT88_25.Name = "R3_MEGAFAT88_25";
            this.R3_MEGAFAT88_25.ReadOnly = true;
            this.R3_MEGAFAT88_25.Width = 150;
            // 
            // R3_MEGAFAT88_650
            // 
            this.R3_MEGAFAT88_650.DataPropertyName = "MEGAFAT88_650";
            this.R3_MEGAFAT88_650.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.R3_MEGAFAT88_650.Name = "R3_MEGAFAT88_650";
            this.R3_MEGAFAT88_650.ReadOnly = true;
            this.R3_MEGAFAT88_650.Width = 150;
            // 
            // R3_MEGAFATEXTRA_25
            // 
            this.R3_MEGAFATEXTRA_25.DataPropertyName = "MEGAFATEXTRA_25";
            this.R3_MEGAFATEXTRA_25.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAFATEXTRA_25.Name = "R3_MEGAFATEXTRA_25";
            this.R3_MEGAFATEXTRA_25.ReadOnly = true;
            this.R3_MEGAFATEXTRA_25.Width = 150;
            // 
            // R3_MEGAFATEXTRA_650
            // 
            this.R3_MEGAFATEXTRA_650.DataPropertyName = "MEGAFATEXTRA_650";
            this.R3_MEGAFATEXTRA_650.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.R3_MEGAFATEXTRA_650.Name = "R3_MEGAFATEXTRA_650";
            this.R3_MEGAFATEXTRA_650.ReadOnly = true;
            this.R3_MEGAFATEXTRA_650.Width = 150;
            // 
            // R3_MEGAENERGY_25
            // 
            this.R3_MEGAENERGY_25.DataPropertyName = "MEGAENERGY_25";
            this.R3_MEGAENERGY_25.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAENERGY_25.Name = "R3_MEGAENERGY_25";
            this.R3_MEGAENERGY_25.ReadOnly = true;
            this.R3_MEGAENERGY_25.Width = 150;
            // 
            // R3_MEGAENERGY_650
            // 
            this.R3_MEGAENERGY_650.DataPropertyName = "MEGAENERGY_650";
            this.R3_MEGAENERGY_650.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.R3_MEGAENERGY_650.Name = "R3_MEGAENERGY_650";
            this.R3_MEGAENERGY_650.ReadOnly = true;
            this.R3_MEGAENERGY_650.Width = 150;
            // 
            // R3_MEGABOOST_25
            // 
            this.R3_MEGABOOST_25.DataPropertyName = "MEGABOOST_25";
            this.R3_MEGABOOST_25.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.R3_MEGABOOST_25.Name = "R3_MEGABOOST_25";
            this.R3_MEGABOOST_25.ReadOnly = true;
            this.R3_MEGABOOST_25.Width = 150;
            // 
            // R3_MEGABOOST_650
            // 
            this.R3_MEGABOOST_650.DataPropertyName = "MEGABOOST_650";
            this.R3_MEGABOOST_650.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.R3_MEGABOOST_650.Name = "R3_MEGABOOST_650";
            this.R3_MEGABOOST_650.ReadOnly = true;
            this.R3_MEGABOOST_650.Width = 150;
            // 
            // R3_MEGAONE_25
            // 
            this.R3_MEGAONE_25.DataPropertyName = "MEGAONE_25";
            this.R3_MEGAONE_25.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAONE_25.Name = "R3_MEGAONE_25";
            this.R3_MEGAONE_25.ReadOnly = true;
            this.R3_MEGAONE_25.Width = 150;
            // 
            // R3_MEGAONE_650
            // 
            this.R3_MEGAONE_650.DataPropertyName = "MEGAONE_650";
            this.R3_MEGAONE_650.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.R3_MEGAONE_650.Name = "R3_MEGAONE_650";
            this.R3_MEGAONE_650.ReadOnly = true;
            this.R3_MEGAONE_650.Width = 150;
            // 
            // R3_MEGAMAX_25
            // 
            this.R3_MEGAMAX_25.DataPropertyName = "MEGAMAX_25";
            this.R3_MEGAMAX_25.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.R3_MEGAMAX_25.Name = "R3_MEGAMAX_25";
            this.R3_MEGAMAX_25.ReadOnly = true;
            this.R3_MEGAMAX_25.Width = 150;
            // 
            // R3_MEGAMAX_650
            // 
            this.R3_MEGAMAX_650.DataPropertyName = "MEGAMAX_650";
            this.R3_MEGAMAX_650.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.R3_MEGAMAX_650.Name = "R3_MEGAMAX_650";
            this.R3_MEGAMAX_650.ReadOnly = true;
            this.R3_MEGAMAX_650.Width = 150;
            // 
            // R3_MEGALAC_25
            // 
            this.R3_MEGALAC_25.DataPropertyName = "MEGALAC_25";
            this.R3_MEGALAC_25.HeaderText = "MEGALAC (25Kg Palletized) (EUR/MT)";
            this.R3_MEGALAC_25.Name = "R3_MEGALAC_25";
            this.R3_MEGALAC_25.ReadOnly = true;
            this.R3_MEGALAC_25.Width = 150;
            // 
            // R3_MEGALAC_650
            // 
            this.R3_MEGALAC_650.DataPropertyName = "MEGALAC_650";
            this.R3_MEGALAC_650.HeaderText = "MEGALAC (BB 600Kg) (EUR/MT)";
            this.R3_MEGALAC_650.Name = "R3_MEGALAC_650";
            this.R3_MEGALAC_650.ReadOnly = true;
            this.R3_MEGALAC_650.Width = 150;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn2.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn3.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn4.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn5.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 150;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn6.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn7.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn8.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 150;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn9.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn10.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 150;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn11.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "MEGAMAX_25";
            this.Column3.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "MEGAMAX_650";
            this.Column4.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "MEGALAC_25";
            this.dataGridViewTextBoxColumn12.HeaderText = "CaSalt (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "MEGALAC_650";
            this.dataGridViewTextBoxColumn13.HeaderText = "CaSalt  (BB 600Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 150;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn14.HeaderText = "ID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn15.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 150;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn16.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn17.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn18.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 150;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn19.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn20.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 150;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn21.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 150;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn22.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 150;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn23.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 150;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn24.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "MEGAMAX_25";
            this.Column1.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "MEGAMAX_650";
            this.Column2.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "MEGALAC_25";
            this.dataGridViewTextBoxColumn25.HeaderText = "CaSalt (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 150;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "MEGALAC_650";
            this.dataGridViewTextBoxColumn26.HeaderText = "CaSalt (BB 600Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 150;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn27.HeaderText = "ID";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Visible = false;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "MEGAFAT88_25";
            this.dataGridViewTextBoxColumn28.HeaderText = "MEGAFAT 88 (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 150;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "MEGAFAT88_650";
            this.dataGridViewTextBoxColumn29.HeaderText = "MEGAFAT 88 (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 150;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "MEGAFATEXTRA_25";
            this.dataGridViewTextBoxColumn30.HeaderText = "MEGAFAT Extra (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 150;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "MEGAFATEXTRA_650";
            this.dataGridViewTextBoxColumn31.HeaderText = "MEGAFAT Extra (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 150;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "MEGAENERGY_25";
            this.dataGridViewTextBoxColumn32.HeaderText = "MEGA Energy (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 150;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "MEGAENERGY_650";
            this.dataGridViewTextBoxColumn33.HeaderText = "MEGA Energy (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 150;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "MEGABOOST_25";
            this.dataGridViewTextBoxColumn34.HeaderText = "MEGA Boost (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 150;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "MEGABOOST_650";
            this.dataGridViewTextBoxColumn35.HeaderText = "MEGA Boost (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 150;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "MEGAONE_25";
            this.dataGridViewTextBoxColumn36.HeaderText = "MEGA One (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 150;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "MEGAONE_650";
            this.dataGridViewTextBoxColumn37.HeaderText = "MEGA One (BB 650Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 150;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "MEGAMAX_25";
            this.Column5.HeaderText = "MEGAMAX (25Kg Palletized) (EUR/MT)";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "MEGAMAX_650";
            this.Column6.HeaderText = "MEGAMAX (BB 600Kg) (EUR/MT)";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 150;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "MEGALAC_25";
            this.dataGridViewTextBoxColumn38.HeaderText = "CaSalt (25Kg Palletized) (EUR/MT)";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 150;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "MEGALAC_650";
            this.dataGridViewTextBoxColumn39.HeaderText = "CaSalt (BB 600Kg) (EUR/MT)";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 150;
            // 
            // frmDEPriceMatrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDEPriceMatrix";
            this.Text = "DE Price Matrix";
            this.Load += new System.EventHandler(this.frmDEPriceMatrix_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDEPLRegion3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDEPLRegion2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDEPLRegion1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelHeader2.ResumeLayout(false);
            this.panelHeader2.PerformLayout();
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegion3)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegion1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegion2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.Panel panel7;
        private General.Controls.HeaderLabel headerLabel7;
        private General.Controls.DataGridView dgvRegion1;
        private General.Controls.Panel panel2;
        private General.Controls.HeaderLabel headerLabel1;
        private General.Controls.DataGridView dgvRegion2;
        private General.Controls.Panel panel3;
        private General.Controls.HeaderLabel headerLabel2;
        private General.Controls.DataGridView dgvRegion3;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel3;
        private General.Controls.PanelHeader panelHeader2;
        private General.Controls.HeaderLabel headerLabel4;
        private General.Controls.Panel panel4;
        private General.Controls.HeaderLabel headerLabel5;
        private General.Controls.DataGridView dgvDEPLRegion1;
        private General.Controls.Panel panel5;
        private General.Controls.HeaderLabel headerLabel6;
        private General.Controls.DataGridView dgvDEPLRegion2;
        private General.Controls.DataGridView dgvDEPLRegion3;
        private General.Controls.Panel panel6;
        private General.Controls.HeaderLabel headerLabel8;
        private General.Controls.Button btnDEExcelExport;
        private General.Controls.Button btnDEPLExcelExport;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R1_MEGALAC_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R2_MEGALAC_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFAT88_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFAT88_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFATEXTRA_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAFATEXTRA_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAENERGY_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAENERGY_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGABOOST_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGABOOST_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAONE_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAONE_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAMAX_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGAMAX_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGALAC_25;
        private System.Windows.Forms.DataGridViewTextBoxColumn R3_MEGALAC_650;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
    }
}