using System;
using System.Data;
using System.Globalization;
using System.Web;
using System.Data.OleDb;
using System.Configuration;
using System.Windows.Forms;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSqlLayer.
/// </summary>
public class clsSqlLayer
{
    #region Declaration
    /// <summary>
    /// SQL ConnSQL
    /// ection object
    /// </summary>
    SqlConnection objSQLCon;
    /// <summary>
    /// SQL Transaction object
    /// </summary>

    // SqlTransaction objSQLTran;
    #endregion

    #region Properties

    #region Get Connection
    public SqlConnection GetDBConnection
    {
        get
        {
            return ((this.objSQLCon == null) ? null : this.objSQLCon);
        }
        set
        {
            this.objSQLCon = value as SqlConnection;
        }
    }
    #endregion

    #endregion

    #region Constructors
    public clsSqlLayer()
    {
    }
    #endregion

    #region Database Methods

    #region Make Connection
    public void MakeDBConnection()
    {
        string strConnString = string.Empty;

        strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnSQL"].ToString();
        //strConnString = strConnString.Replace("[PATH]", Application.StartupPath);

        this.objSQLCon = new SqlConnection(strConnString);

        this.objSQLCon.Open();
    }
    #endregion

    #region Close Connection
    public void CloseDBConnection()
    {
        if (this.objSQLCon != null)
        {
            if (this.objSQLCon.State != ConnectionState.Closed)
                this.objSQLCon.Close();
            this.objSQLCon.Dispose();
            this.objSQLCon = null;
        }
    }
    #endregion

    public SqlCommand SetCmdProperties(string strStoredProcedureName)
    {
        SqlCommand cmdToExecute = new SqlCommand();
        cmdToExecute.CommandText = strStoredProcedureName;
        cmdToExecute.CommandType = CommandType.StoredProcedure;
        cmdToExecute.Connection = this.GetDBConnection;
        cmdToExecute.CommandTimeout = 60;
        return cmdToExecute;
    }

    public SqlCommand SetQryProperties(string strStoredProcedureName)
    {
        SqlCommand cmdToExecute = new SqlCommand();
        cmdToExecute.CommandText = strStoredProcedureName;
        cmdToExecute.CommandType = CommandType.Text;
        cmdToExecute.Connection = this.GetDBConnection;
        cmdToExecute.CommandTimeout = 60;
        return cmdToExecute;
    }

    #region Get DataTable
    public DataTable GetDataTable(string strSQL)
    {
        MakeDBConnection();
        SqlDataAdapter objSqlDA = new SqlDataAdapter(strSQL, this.objSQLCon);
        DataTable dtData = new DataTable();
        dtData.Locale = CultureInfo.InvariantCulture;

        try
        {
            objSqlDA.Fill(dtData);
            objSqlDA.Dispose();
            CloseDBConnection();
        }
        catch (Exception Ex)
        {
            if (objSqlDA != null)
            {
                objSqlDA.Dispose();
                objSqlDA = null;
            }

            if (objSQLCon.State != ConnectionState.Closed)
            {
                CloseDBConnection();
            }
            throw Ex;
        }
        return dtData;
    }
    #endregion

    #region Get DataSet and DataAdapter
    public void GetDataSetAndDataAdapter(string strSQL, ref DataSet objDataSet, ref SqlDataAdapter objDataAdapter)
    {
        objDataAdapter = new SqlDataAdapter(strSQL, this.objSQLCon);
        objDataSet = new DataSet();
        objDataSet.Locale = CultureInfo.InvariantCulture;

        try
        {
            objDataAdapter.Fill(objDataSet);
        }
        catch (Exception Ex)
        {
            if (objDataAdapter != null)
            {
                objDataAdapter.Dispose();
                objDataAdapter = null;
            }
            throw Ex;
        }
    }
    #endregion

    #region Get Scalar value
    public object GetScalarValue(string strSQL)
    {
        SqlCommand objSQMComm = new SqlCommand(strSQL, this.objSQLCon);

        try
        {
            object objScalarVal = objSQMComm.ExecuteScalar();
            objSQMComm.Dispose();
            return objScalarVal;
        }
        catch (Exception Ex)
        {
            if (objSQMComm != null)
            {
                objSQMComm.Dispose();
                objSQMComm = null;
            }
            throw Ex;
        }
        //return null;
    }
    #endregion

    public void ExecuteDML(string strSQL)
    {
        if (strSQL.Length != 0)
        {
            try
            {
                SqlCommand objSQMComm = new SqlCommand(strSQL, this.objSQLCon);
                objSQMComm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public void ExecuteSP(string strSP, string[] Param, string[] Value)
    {
        if (strSP.Length != 0)
        {
            try
            {
                MakeDBConnection();
                SqlCommand objSQMComm = new SqlCommand(strSP, this.objSQLCon);
                objSQMComm.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i < Param.Length; i++)
                {
                    objSQMComm.Parameters.Add(new SqlParameter(Param[i], Value[i]));
                }
                
                objSQMComm.ExecuteNonQuery();
                CloseDBConnection();
            }
            catch (Exception ex)
            {
                if (objSQLCon.State != ConnectionState.Closed)
                {
                    CloseDBConnection();
                }

                throw ex;
            }
        }
    }

    #endregion
}
