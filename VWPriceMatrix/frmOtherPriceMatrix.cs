﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class frmOtherPriceMatrix : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public frmOtherPriceMatrix()
        {
            InitializeComponent();
        }

        #region Button Events        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            /*
            frmCustomerAddEditDelete objfrmCustomerAddEditDelete = new frmCustomerAddEditDelete();
            objfrmCustomerAddEditDelete.MdiParent = MasterPage.ActiveForm;
            objfrmCustomerAddEditDelete.WindowState = FormWindowState.Maximized;
            objfrmCustomerAddEditDelete.Width = this.Width;
            objfrmCustomerAddEditDelete.Height = this.Height;
            objfrmCustomerAddEditDelete.Mode = "Add";
            this.Close();
            objfrmCustomerAddEditDelete.Show();
            */

            DataTable dtTemp = dgvCustomer.DataSource as DataTable;
            DataRow drNewRow = dtTemp.NewRow();
            dtTemp.Rows.Add(drNewRow);
            dgvCustomer.DataSource = dtTemp;
            dgvCustomer.Rows[dgvCustomer.Rows.Count - 1].Cells["CustomerId"].Selected = true;
            dgvCustomer.Focus();
        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string strCustomerId = txtCustomerId.Text.Trim();
            string strCustomerName = txtCustomerName.Text.Trim();
            string strPostCode = txtPostcode.Text.Trim();
            string strCity = txtCity.Text.Trim();
            string strEmail = txtEmail.Text.Trim();
            int intDeleted = chkDeleted.Checked ? 1 : 0;

            string strQuery = " SELECT * FROM CUSTOMERS WHERE ISNULL(CustomerId,'') LIKE '%" + strCustomerId + "%' AND ISNULL(CustomerName,'') LIKE '%" + strCustomerName + "%' AND ISNULL(PostCode,'') LIKE '%" + strPostCode + "%' AND  ISNULL(CITY,'') LIKE '%" + strCity + "%' AND  ISNULL(Email,'') LIKE '%" + strEmail + "%' AND isdeleted = " + intDeleted + " Order by Id ";
            DataTable dtContactDate = objclsSqlLayer.GetDataTable(strQuery);
            dgvCustomer.AutoGenerateColumns = false;
            dgvCustomer.DataSource = dtContactDate;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtCustomerId.Clear();
            txtCustomerName.Clear();
            txtPostcode.Clear();
            txtCity.Clear();
            txtEmail.Clear();
            chkDeleted.Checked = false;
            dgvCustomer.DataSource = null;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            int intCount = 0;
            string strCustomerId = string.Empty;

            foreach (DataGridViewRow item in dgvCustomer.Rows)
            {
                if (Support.ConvertToString(item.Cells["Select"].Value) == "True")
                {
                    strCustomerId += Support.ConvertToString(item.Cells["Id"].Value) + ",";
                    intCount++;
                }
            }

            if (intCount == 0)
            {
                MessageBox.Show("No item selected"); 
                return;
            }

            if (intCount > 6)
            {
                MessageBox.Show("You can't select more than 6 items");
                return;
            }

            string[] Param = {  };
            string[] Value = {  };
            objclsSqlLayer.ExecuteSP("csp_Calc_Other_Price_Matrix_Backup", Param, Value);

            foreach (DataGridViewRow item in dgvCustomer.Rows)
            {
                if (Support.ConvertToString(item.Cells["Select"].Value) == "True")
                {
                    string[] Param1 = { "@CustomerId", "@UpdatedBy" };
                    string[] Value1 = { Support.ConvertToString(item.Cells["Id"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_Calc_Other_Price_Matrix", Param1, Value1);
                }
            }

            
            frmOtherPriceMatrixView objfrmOtherPriceMatrixView = new frmOtherPriceMatrixView();
            objfrmOtherPriceMatrixView.MdiParent = this.MdiParent;
            objfrmOtherPriceMatrixView.WindowState = FormWindowState.Maximized;
            objfrmOtherPriceMatrixView.Width = this.MdiParent.Width;
            objfrmOtherPriceMatrixView.Height = this.MdiParent.Height - 50;
            objfrmOtherPriceMatrixView.MaximizeBox = false;
            objfrmOtherPriceMatrixView.MinimizeBox = false;
            this.Close();
            objfrmOtherPriceMatrixView.Show();
        }

        #endregion

        #region Datagridview Events        

        private void dgvCustomer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            return;

            if (e.ColumnIndex == dgvCustomer.Columns["btnSave"].Index && e.RowIndex >= 0)
            {
                DataGridViewRow deTemp = dgvCustomer.Rows[e.RowIndex];

                if (Support.ConvertToString(deTemp.Cells["Id"].Value).Length == 0)
                {
                    string[] Param = { "@CustomerId", "@CustomerName", "@PostCode", "@City", "@Country", "@Region", "@M_D_DRY_FATS", "@M_D_CA_SALT_PL", "@Price_List_Description", "@Email", "@DIS_RATE_Antwerp", "@DIS_RATE_Brake", "@DIS_RATE_Arhus", "@DIS_RATE_Liverpool", "@DIS_RATE_BristolAvonmouth", "@CreatedBy" };
                    string[] Value = { Support.ConvertToString(deTemp.Cells["CustomerId"].Value), Support.ConvertToString(deTemp.Cells["CustomerName"].Value), Support.ConvertToString(deTemp.Cells["PostCode"].Value), Support.ConvertToString(deTemp.Cells["City"].Value), Support.ConvertToString(deTemp.Cells["Country"].Value), Support.ConvertToString(deTemp.Cells["Region"].Value), Support.ConvertToInt(deTemp.Cells["M_D_DRY_FATS"].Value).ToString(), Support.ConvertToString(deTemp.Cells["M_D_CA_SALT_PL"].Value).ToString(), Support.ConvertToString(deTemp.Cells["Price_List_Description"].Value), Support.ConvertToString(deTemp.Cells["Email"].Value), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Antwerp"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Brake"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Arhus"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Liverpool"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_BristolAvonmouth"].Value).ToString(), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_Customers_Add", Param, Value);
                    MessageBox.Show("Customer added successfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnSearch_Click(null, null);
                }
                else
                {
                    string[] Param = { "@Id", "@CustomerId", "@CustomerName", "@PostCode", "@City", "@Country", "@Region", "@M_D_DRY_FATS", "@M_D_CA_SALT_PL", "@Price_List_Description", "@Email", "@DIS_RATE_Antwerp", "@DIS_RATE_Brake", "@DIS_RATE_Arhus", "@DIS_RATE_Liverpool", "@DIS_RATE_BristolAvonmouth", "@UpdatedBy" };
                    string[] Value = { Support.ConvertToString(deTemp.Cells["Id"].Value), Support.ConvertToString(deTemp.Cells["CustomerId"].Value), Support.ConvertToString(deTemp.Cells["CustomerName"].Value), Support.ConvertToString(deTemp.Cells["PostCode"].Value), Support.ConvertToString(deTemp.Cells["City"].Value), Support.ConvertToString(deTemp.Cells["Country"].Value), Support.ConvertToString(deTemp.Cells["Region"].Value), Support.ConvertToInt(deTemp.Cells["M_D_DRY_FATS"].Value).ToString(), Support.ConvertToString(deTemp.Cells["M_D_CA_SALT_PL"].Value).ToString(), Support.ConvertToString(deTemp.Cells["Price_List_Description"].Value), Support.ConvertToString(deTemp.Cells["Email"].Value), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Antwerp"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Brake"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Arhus"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_Liverpool"].Value).ToString(), Support.ConvertToInt(deTemp.Cells["DIS_RATE_BristolAvonmouth"].Value).ToString(), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_Customers_Edit", Param, Value);
                    MessageBox.Show("Customer updated successfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnSearch_Click(null, null);
                }
            }
            else if (e.ColumnIndex == dgvCustomer.Columns["btnDelete"].Index && e.RowIndex >= 0)
            {
                DataGridViewRow deTemp = dgvCustomer.Rows[e.RowIndex];
                string strId = deTemp.Cells["Id"].Value.ToString();

                if (MessageBox.Show("Are you sure to delete this customer ??", "Confirm Delete!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string[] Param = { "@Id", "@UpdatedBy" };
                    string[] Value = { strId, Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_Customers_Delete", Param, Value);
                    MessageBox.Show("Customer deleted successfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnSearch_Click(null, null);
                }
            }
        }

        private void dgvCustomer_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dgvCustomer_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn dgvCol in dgvCustomer.Columns)
            {
                dgvCol.SortMode = DataGridViewColumnSortMode.Automatic;
            }
        }

        #endregion
    }
}
