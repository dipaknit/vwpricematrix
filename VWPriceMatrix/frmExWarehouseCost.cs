﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class frmExWarehouseCost : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        #region Form Event

        public frmExWarehouseCost()
        {
            InitializeComponent();
        }

        private void frmExWarehouseCost_Load(object sender, EventArgs e)
        {
            GetAll();
        }

        #endregion

        #region Button Events

        private void btn_ANTWERP_Calculate_Click(object sender, EventArgs e)
        {
            Calculate_EX_WC_ANTWERP();
        }

        private void btn_ANTWERP_Save_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
            {
                if (Support.ConvertToString(row.Cells["Type"].Value).Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
                {
                    string[] Param = { "@ANTWERP_ID", "@Type", "@MEGAFAT88_25", "@MEGAFAT88_650", "@MEGAFATEXTRA_25", "@MEGAFATEXTRA_650", "@MEGAENERGY_25", "@MEGAENERGY_650", "@MEGABOOST_25", "@MEGABOOST_650", "@MEGAONE_25", "@MEGAONE_650", "@CreatedBy" };
                    string[] Value = { Support.ConvertToString(row.Cells["ANTWERP_ID"].Value), Support.ConvertToString(row.Cells["Type"].Value), Support.ConvertToString(row.Cells["ANT_MEGAFAT88_25"].Value), Support.ConvertToString(row.Cells["ANT_MEGAFAT88_650"].Value), Support.ConvertToString(row.Cells["ANT_MEGAFATEXTRA_25"].Value), Support.ConvertToString(row.Cells["ANT_MEGAFATEXTRA_650"].Value), Support.ConvertToString(row.Cells["ANT_MEGAENERGY_25"].Value), Support.ConvertToString(row.Cells["ANT_MEGAENERGY_650"].Value), Support.ConvertToString(row.Cells["ANT_MEGABOOST_25"].Value), Support.ConvertToString(row.Cells["ANT_MEGABOOST_650"].Value), Support.ConvertToString(row.Cells["ANT_MEGAONE_25"].Value), Support.ConvertToString(row.Cells["ANT_MEGAONE_650"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_EX_WC_ANTWERP_AddEdit", Param, Value);
                    break;
                }
            }

            MessageBox.Show("Information Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btn_BRAKE_Calculate_Click(object sender, EventArgs e)
        {
            Calculate_EX_WC_BRAKE();
        }

        private void btn_BRAKE_Save_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
            {
                if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
                {
                    string[] Param = { "@BRAKE_ID", "@Type", "@MEGAFAT88_25", "@MEGAFAT88_650", "@MEGAFATEXTRA_25", "@MEGAFATEXTRA_650", "@MEGAENERGY_25", "@MEGAENERGY_650", "@MEGABOOST_25", "@MEGABOOST_650", "@MEGAONE_25", "@MEGAONE_650", "@CreatedBy" };
                    string[] Value = { Support.ConvertToString(row.Cells["BRAKE_ID"].Value), Support.ConvertToString(row.Cells["BRAKE_Type"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAFAT88_25"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAFAT88_650"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAFATEXTRA_25"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAFATEXTRA_650"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAENERGY_25"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAENERGY_650"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGABOOST_25"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGABOOST_650"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAONE_25"].Value), Support.ConvertToString(row.Cells["BRAKE_MEGAONE_650"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_EX_WC_BRAKE_AddEdit", Param, Value);
                    break;
                }
            }

            MessageBox.Show("Information Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        private void btn_BIBBY_Calculate_Click(object sender, EventArgs e)
        {
            Calculate_EX_WC_BIBBY();
        }

        private void btn_BIBBY_Save_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
            {
                if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
                {
                    string[] Param = { "@BIBBY_ID", "@Type", "@MEGAFAT88_25", "@MEGAFAT88_650", "@MEGAFATEXTRA_25", "@MEGAFATEXTRA_650", "@MEGAENERGY_25", "@MEGAENERGY_650", "@MEGABOOST_25", "@MEGABOOST_650", "@MEGAONE_25", "@MEGAONE_650", "@CreatedBy" };
                    string[] Value = { Support.ConvertToString(row.Cells["BIBBY_ID"].Value), Support.ConvertToString(row.Cells["BIBBY_Type"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAFAT88_25"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAFAT88_650"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAFATEXTRA_25"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAFATEXTRA_650"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAENERGY_25"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAENERGY_650"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGABOOST_25"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGABOOST_650"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAONE_25"].Value), Support.ConvertToString(row.Cells["BIBBY_MEGAONE_650"].Value), Support.UserId };
                    objclsSqlLayer.ExecuteSP("csp_EX_WC_BIBBY_AddEdit", Param, Value);
                    break;
                }
            }

            MessageBox.Show("Information Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetAll();
        }

        #endregion

        #region Other Methods

        private void GetAll()
        {
            Get_EX_WC_ANTWERP();
            Get_EX_WC_BRAKE();
            Get_EX_WC_BIBBY();
            Get_EX_WC_AARHUS();
        }

        private void Get_EX_WC_ANTWERP()
        {
            string strQuery = " SELECT * FROM EX_WC_ANTWERP WHERE isdeleted = 0 Order by ANTWERP_ID ";
            DataTable dtEx_WC_ANTWERP = objclsSqlLayer.GetDataTable(strQuery);
            dgvEX_WC_ANTWERP.AutoGenerateColumns = false;
            dgvEX_WC_ANTWERP.DataSource = dtEx_WC_ANTWERP;
        }

        private void Get_EX_WC_BRAKE()
        {
            string strQuery = " SELECT * FROM EX_WC_BRAKE WHERE isdeleted = 0 Order by BRAKE_ID ";
            DataTable dtEx_WC_BRAKE = objclsSqlLayer.GetDataTable(strQuery);
            dgvEX_WC_BRAKE.AutoGenerateColumns = false;
            dgvEX_WC_BRAKE.DataSource = dtEx_WC_BRAKE;
        }

        private void Get_EX_WC_BIBBY()
        {
            string strQuery = " SELECT * FROM EX_WC_BIBBY WHERE isdeleted = 0 Order by BIBBY_ID ";
            DataTable dtEx_WC_BIBBY = objclsSqlLayer.GetDataTable(strQuery);
            dgvEX_WC_BIBBY.AutoGenerateColumns = false;
            dgvEX_WC_BIBBY.DataSource = dtEx_WC_BIBBY;
        }

        private void Get_EX_WC_AARHUS()
        {
            string strQuery = " SELECT * FROM EX_WC_AARHUS WHERE isdeleted = 0 Order by AARHUS_ID ";
            DataTable dtData = objclsSqlLayer.GetDataTable(strQuery);
            dgvEX_WC_AARHUS.AutoGenerateColumns = false;
            dgvEX_WC_AARHUS.DataSource = dtData;
        }

        private void Calculate_EX_WC_ANTWERP()
        {
            #region Get Prodct Data           

            string strQuery = " SELECT * FROM PRODUCTS WHERE isdeleted = 0 Order by ProductId ";
            DataTable dtProduct = objclsSqlLayer.GetDataTable(strQuery);

            #endregion

            #region Get Destination Data

            strQuery = " SELECT * FROM Destination WHERE isdeleted = 0 Order by DestinationId ";
            DataTable dtDestination = objclsSqlLayer.GetDataTable(strQuery);

            var varDestinationAntwerp = dtDestination.AsEnumerable().Where(row => Support.ConvertToString(row["Destination"]).Equals("ANTWERP", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("Freight_40_FCL"));
            double dblDestinationAntwerp = 0;

            if (varDestinationAntwerp != null)
            {
                dblDestinationAntwerp = Support.ConvertToDouble(varDestinationAntwerp.FirstOrDefault());
            }

            #endregion

            #region Get Forex Data           

            strQuery = " SELECT * FROM Forex WHERE isdeleted = 0 Order by ForexId ";
            DataTable dtForex = objclsSqlLayer.GetDataTable(strQuery);

            double dblEUR_USD = 0;

            foreach (DataRow drTemp in dtForex.Rows)
            {
                if (Support.ConvertToString(drTemp["Currency"]).Equals("EUR/USD", StringComparison.CurrentCultureIgnoreCase))
                {
                    dblEUR_USD = Support.ConvertToDouble(drTemp["Rate"]);
                    break;
                }
            }

            #endregion

            #region Get WC_MOLENBERGNATIE_CURR Data

            strQuery = " SELECT * FROM WC_MOLENBERGNATIE_CURR WHERE isdeleted = 0 Order by MOLENBERGNATIE_CURR_ID ";
            DataTable dtWC_MOLENBERGNATIE_CURR = objclsSqlLayer.GetDataTable(strQuery);

            double dblWC_MOLENBERGNATIE_CURR_EUR_MT_25KG = 0;
            double dblWC_MOLENBERGNATIE_CURR_EUR_MT_BB = 0;

            foreach (DataRow drTemp in dtWC_MOLENBERGNATIE_CURR.Rows)
            {
                dblWC_MOLENBERGNATIE_CURR_EUR_MT_25KG = dblWC_MOLENBERGNATIE_CURR_EUR_MT_25KG + Support.ConvertToDouble(drTemp["EUR_MT_25KG"]);
                dblWC_MOLENBERGNATIE_CURR_EUR_MT_BB = dblWC_MOLENBERGNATIE_CURR_EUR_MT_BB + Support.ConvertToDouble(drTemp["EUR_MT_BB"]);
            }

            #endregion

            #region Get WC_MOLENBERGNATIE_Data

            strQuery = " SELECT * FROM WC_MOLENBERGNATIE WHERE isdeleted = 0 Order by MOLENBERGNATIE_ID ";
            DataTable dtWC_MOLENBERGNATIE = objclsSqlLayer.GetDataTable(strQuery);

            double dblWC_MOLENBERGNATIE_QTY_24 = 0;
            double dblWC_MOLENBERGNATIE_QTY_24_7 = 0;

            foreach (DataRow drTemp in dtWC_MOLENBERGNATIE.Rows)
            {
                dblWC_MOLENBERGNATIE_QTY_24 = dblWC_MOLENBERGNATIE_QTY_24 + Support.ConvertToDouble(drTemp["QTY_24"]);
                dblWC_MOLENBERGNATIE_QTY_24_7 = dblWC_MOLENBERGNATIE_QTY_24_7 + Support.ConvertToDouble(drTemp["QTY_24_7"]);
            }

            #endregion

            foreach (DataGridViewColumn item in dgvEX_WC_ANTWERP.Columns)
            {
                if (item.Name == "ANTWERP_ID" || item.Name == "Type")
                {
                    continue;
                }

                #region Calculate CIF COST - USD (Wilmar) Row

                int FTL_MT_Rowindex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
                    {
                        FTL_MT_Rowindex = row.Index;
                        break;
                    }
                }

                double dblFTL_MT = Support.ConvertToDouble(dgvEX_WC_ANTWERP.Rows[FTL_MT_Rowindex].Cells[item.Index].Value);
                double dbl13 = 0;

                if (item.Name == "ANT_MEGAFAT88_25" || item.Name == "ANT_MEGAFAT88_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAFAT88")).FirstOrDefault());
                }
                else if (item.Name == "ANT_MEGAFATEXTRA_25" || item.Name == "ANT_MEGAFATEXTRA_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAFATEXTRA")).FirstOrDefault());
                }
                else if (item.Name == "ANT_MEGAENERGY_25" || item.Name == "ANT_MEGAENERGY_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAENERGY")).FirstOrDefault());
                }
                else if (item.Name == "ANT_MEGABOOST_25" || item.Name == "ANT_MEGABOOST_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGABOOST")).FirstOrDefault());
                }
                else if (item.Name == "ANT_MEGAONE_25" || item.Name == "ANT_MEGAONE_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAONE")).FirstOrDefault());
                }

                int CIF_COST_USD_Wilmar_Rowindex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("CIF COST - USD (WILMAR)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        CIF_COST_USD_Wilmar_Rowindex = row.Index;
                        break;
                    }
                }

                double dblCIF_COST_USD_Wilmar = Math.Round(dbl13 + (dblDestinationAntwerp / dblFTL_MT));

                dgvEX_WC_ANTWERP.Rows[CIF_COST_USD_Wilmar_Rowindex].Cells[item.Index].Value = dblCIF_COST_USD_Wilmar;

                #endregion

                #region Calculate CIF COST (EUR/MT) - Wilmar Row

                double dblCIF_COST_EUR_MT_Wilmar = Math.Round(dblCIF_COST_USD_Wilmar / dblEUR_USD);
                int dblCIF_COST_EUR_MT_Wilmar_RowIndex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("CIF COST (EUR/MT) - Wilmar", StringComparison.CurrentCultureIgnoreCase))
                    {
                        dblCIF_COST_EUR_MT_Wilmar_RowIndex = row.Index;
                        break;
                    }
                }

                dgvEX_WC_ANTWERP.Rows[dblCIF_COST_EUR_MT_Wilmar_RowIndex].Cells[item.Index].Value = dblCIF_COST_EUR_MT_Wilmar;

                #endregion

                #region Calculate Import duties Row

                double dblProductMegaMalasia = 0;

                if (item.Name == "ANT_MEGAFAT88_25" || item.Name == "ANT_MEGAFAT88_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAFAT88"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "ANT_MEGAFATEXTRA_25" || item.Name == "ANT_MEGAFATEXTRA_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAFATEXTRA"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "ANT_MEGAENERGY_25" || item.Name == "ANT_MEGAENERGY_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAENERGY"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "ANT_MEGABOOST_25" || item.Name == "ANT_MEGABOOST_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGABOOST"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "ANT_MEGAONE_25" || item.Name == "ANT_MEGAONE_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAONE"]);
                            break;
                        }
                    }
                }

                double dblImport_Dduties = Math.Round((dblProductMegaMalasia / 100) * dblCIF_COST_EUR_MT_Wilmar);
                int intImportDdutiesRowIndex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("Import duties", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intImportDdutiesRowIndex = row.Index;
                        dgvEX_WC_ANTWERP.Rows[intImportDdutiesRowIndex].Cells[item.Index].Value = dblImport_Dduties;
                        break;
                    }
                }

                #endregion

                #region Calculate Warehouse Costs Row

                int intWarehouseCostsRowIndex = 0;
                double dblWC_MOLENBERGNATIE_CURR = 0;

                if (item.Name == "ANT_MEGAFAT88_25" || item.Name == "ANT_MEGAFATEXTRA_25" || item.Name == "ANT_MEGAENERGY_25" || item.Name == "ANT_MEGABOOST_25" || item.Name == "ANT_MEGAONE_25")
                {
                    dblWC_MOLENBERGNATIE_CURR = Math.Round(dblWC_MOLENBERGNATIE_CURR_EUR_MT_25KG);
                }
                else if (item.Name == "ANT_MEGAFAT88_650" || item.Name == "ANT_MEGAFATEXTRA_650" || item.Name == "ANT_MEGAENERGY_650" || item.Name == "ANT_MEGABOOST_650" || item.Name == "ANT_MEGAONE_650")
                {
                    dblWC_MOLENBERGNATIE_CURR = Math.Round(dblWC_MOLENBERGNATIE_CURR_EUR_MT_BB);
                }

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("Warehouse Costs", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intWarehouseCostsRowIndex = row.Index;
                        dgvEX_WC_ANTWERP.Rows[intWarehouseCostsRowIndex].Cells[item.Index].Value = dblWC_MOLENBERGNATIE_CURR;
                        break;
                    }
                }

                #endregion

                #region Calculate Import charges and Handling Row

                int intImportChargesHandlingRowIndex = 0;
                double dblWC_MOLENBERGNATIE = 0;

                if (item.Name == "ANT_MEGAFAT88_25" || item.Name == "ANT_MEGAFATEXTRA_25" || item.Name == "ANT_MEGAENERGY_25" || item.Name == "ANT_MEGABOOST_25" || item.Name == "ANT_MEGAONE_25")
                {
                    dblWC_MOLENBERGNATIE = Math.Round(dblWC_MOLENBERGNATIE_QTY_24);
                }
                else if (item.Name == "ANT_MEGAFAT88_650" || item.Name == "ANT_MEGAFATEXTRA_650" || item.Name == "ANT_MEGAENERGY_650" || item.Name == "ANT_MEGABOOST_650" || item.Name == "ANT_MEGAONE_650")
                {
                    dblWC_MOLENBERGNATIE = Math.Round(dblWC_MOLENBERGNATIE_QTY_24_7);
                }

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("Import charges and Handling", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intImportChargesHandlingRowIndex = row.Index;
                        dgvEX_WC_ANTWERP.Rows[intImportChargesHandlingRowIndex].Cells[item.Index].Value = dblWC_MOLENBERGNATIE;
                        break;
                    }
                }

                #endregion

                #region Calculate Ex Warehouse (EUR/MT)

                int intExWarehouse_EUR_MT_RowIndex = 0;
                double dblExWarehouse_EUR_MT = 0;

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (
                        Support.ConvertToString(row.Cells["Type"].Value).Equals("CIF COST (EUR/MT) - Wilmar", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["Type"].Value).Equals("Import duties", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["Type"].Value).Equals("Warehouse Costs", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["Type"].Value).Equals("Import charges and Handling", StringComparison.CurrentCultureIgnoreCase)
                        )
                    {
                        dblExWarehouse_EUR_MT = dblExWarehouse_EUR_MT + Support.ConvertToDouble(row.Cells[item.Index].Value);
                    }

                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("Ex Warehouse (EUR/MT)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intExWarehouse_EUR_MT_RowIndex = row.Index;

                        if (dblCIF_COST_EUR_MT_Wilmar <= 0)
                        {
                            dblExWarehouse_EUR_MT = 0;
                        }

                        dgvEX_WC_ANTWERP.Rows[intExWarehouse_EUR_MT_RowIndex].Cells[item.Index].Value = dblExWarehouse_EUR_MT;
                        break;
                    }
                }

                #endregion

                #region Calculate T2 - Imported cost (Not-warehoused) Row

                int intT2_Imported_Cost_RowIndex = 0;
                double dblT2_Imported_Cost = 0;

                dblT2_Imported_Cost = dblExWarehouse_EUR_MT - dblWC_MOLENBERGNATIE_CURR;

                foreach (DataGridViewRow row in dgvEX_WC_ANTWERP.Rows)
                {
                    if (Support.ConvertToString(row.Cells["Type"].Value).Equals("T2 - Imported cost (Not-warehoused)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intT2_Imported_Cost_RowIndex = row.Index;
                        dgvEX_WC_ANTWERP.Rows[intT2_Imported_Cost_RowIndex].Cells[item.Index].Value = dblT2_Imported_Cost;
                        break;
                    }
                }

                #endregion
            }
        }

        private void Calculate_EX_WC_BRAKE()
        {
            #region Get Prodct Data           

            string strQuery = " SELECT * FROM PRODUCTS WHERE isdeleted = 0 Order by ProductId ";
            DataTable dtProduct = objclsSqlLayer.GetDataTable(strQuery);

            #endregion

            #region Get Destination Data

            strQuery = " SELECT * FROM Destination WHERE isdeleted = 0 Order by DestinationId ";
            DataTable dtDestination = objclsSqlLayer.GetDataTable(strQuery);

            var varDestinationBrake = dtDestination.AsEnumerable().Where(row => Support.ConvertToString(row["Destination"]).Equals("Bremerhaven", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("Freight_40_FCL"));
            double dblDestinationBrake = 0;

            if (varDestinationBrake != null)
            {
                dblDestinationBrake = Support.ConvertToDouble(varDestinationBrake.FirstOrDefault());
            }

            #endregion

            #region Get Forex Data           

            strQuery = " SELECT * FROM Forex WHERE isdeleted = 0 Order by ForexId ";
            DataTable dtForex = objclsSqlLayer.GetDataTable(strQuery);

            double dblEUR_USD = 0;

            foreach (DataRow drTemp in dtForex.Rows)
            {
                if (Support.ConvertToString(drTemp["Currency"]).Equals("EUR/USD", StringComparison.CurrentCultureIgnoreCase))
                {
                    dblEUR_USD = Support.ConvertToDouble(drTemp["Rate"]);
                    break;
                }
            }

            #endregion

            #region Get WC_MUELLER_CURR Data

            strQuery = " SELECT * FROM WC_MUELLER_CURR WHERE isdeleted = 0 Order by MUELLER_CURR_ID ";
            DataTable dtWC_MUELLER_CURR = objclsSqlLayer.GetDataTable(strQuery);

            double dblWC_MUELLER_CURR_EUR_MT_25KG = 0;
            double dblWC_MUELLER_CURR_EUR_MT_BB = 0;

            foreach (DataRow drTemp in dtWC_MUELLER_CURR.Rows)
            {
                dblWC_MUELLER_CURR_EUR_MT_25KG = dblWC_MUELLER_CURR_EUR_MT_25KG + Support.ConvertToDouble(drTemp["EUR_MT_25KG"]);
                dblWC_MUELLER_CURR_EUR_MT_BB = dblWC_MUELLER_CURR_EUR_MT_BB + Support.ConvertToDouble(drTemp["EUR_MT_BB"]);
            }

            #endregion

            #region Get WC_WC_MUELLER_Data

            strQuery = " SELECT * FROM WC_MUELLER WHERE isdeleted = 0 Order by MUELLER_ID ";
            DataTable dtWC_MUELLER = objclsSqlLayer.GetDataTable(strQuery);

            double dblWC_MUELLER_QTY_24 = 0;
            double dblWC_MUELLER_QTY_24_7 = 0;

            foreach (DataRow drTemp in dtWC_MUELLER.Rows)
            {
                dblWC_MUELLER_QTY_24 = dblWC_MUELLER_QTY_24 + Support.ConvertToDouble(drTemp["QTY_24"]);
                dblWC_MUELLER_QTY_24_7 = dblWC_MUELLER_QTY_24_7 + Support.ConvertToDouble(drTemp["QTY_24_7"]);
            }

            #endregion

            foreach (DataGridViewColumn item in dgvEX_WC_BRAKE.Columns)
            {
                if (item.Name == "BRAKE_ID" || item.Name == "BRAKE_Type")
                {
                    continue;
                }

                #region Calculate CIF COST - USD (Wilmar) Row

                int FTL_MT_Rowindex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
                    {
                        FTL_MT_Rowindex = row.Index;
                        break;
                    }
                }

                double dblFTL_MT = Support.ConvertToDouble(dgvEX_WC_BRAKE.Rows[FTL_MT_Rowindex].Cells[item.Index].Value);
                double dbl13 = 0;

                if (item.Name == "BRAKE_MEGAFAT88_25" || item.Name == "BRAKE_MEGAFAT88_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAFAT88")).FirstOrDefault());
                }
                else if (item.Name == "BRAKE_MEGAFATEXTRA_25" || item.Name == "BRAKE_MEGAFATEXTRA_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAFATEXTRA")).FirstOrDefault());
                }
                else if (item.Name == "BRAKE_MEGAENERGY_25" || item.Name == "BRAKE_MEGAENERGY_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAENERGY")).FirstOrDefault());
                }
                else if (item.Name == "BRAKE_MEGABOOST_25" || item.Name == "BRAKE_MEGABOOST_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGABOOST")).FirstOrDefault());
                }
                else if (item.Name == "BRAKE_MEGAONE_25" || item.Name == "BRAKE_MEGAONE_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAONE")).FirstOrDefault());
                }

                int CIF_COST_USD_Wilmar_Rowindex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("CIF COST - USD (WILMAR)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        CIF_COST_USD_Wilmar_Rowindex = row.Index;
                        break;
                    }
                }

                double dblCIF_COST_USD_Wilmar = Math.Round(dbl13 + (dblDestinationBrake / dblFTL_MT));

                dgvEX_WC_BRAKE.Rows[CIF_COST_USD_Wilmar_Rowindex].Cells[item.Index].Value = dblCIF_COST_USD_Wilmar;

                #endregion

                #region Calculate CIF COST (EUR/MT) - Wilmar Row

                double dblCIF_COST_EUR_MT_Wilmar = Math.Round(dblCIF_COST_USD_Wilmar / dblEUR_USD);
                int dblCIF_COST_EUR_MT_Wilmar_RowIndex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("CIF COST (EUR/MT) - Wilmar", StringComparison.CurrentCultureIgnoreCase))
                    {
                        dblCIF_COST_EUR_MT_Wilmar_RowIndex = row.Index;
                        break;
                    }
                }

                dgvEX_WC_BRAKE.Rows[dblCIF_COST_EUR_MT_Wilmar_RowIndex].Cells[item.Index].Value = dblCIF_COST_EUR_MT_Wilmar;

                #endregion

                #region Calculate Import duties Row

                double dblProductMegaMalasia = 0;

                if (item.Name == "BRAKE_MEGAFAT88_25" || item.Name == "BRAKE_MEGAFAT88_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAFAT88"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BRAKE_MEGAFATEXTRA_25" || item.Name == "BRAKE_MEGAFATEXTRA_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAFATEXTRA"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BRAKE_MEGAENERGY_25" || item.Name == "BRAKE_MEGAENERGY_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAENERGY"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BRAKE_MEGABOOST_25" || item.Name == "BRAKE_MEGABOOST_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGABOOST"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BRAKE_MEGAONE_25" || item.Name == "BRAKE_MEGAONE_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAONE"]);
                            break;
                        }
                    }
                }

                double dblImport_Dduties = Math.Round((dblProductMegaMalasia / 100) * dblCIF_COST_EUR_MT_Wilmar);
                int intImportDdutiesRowIndex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Import duties", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intImportDdutiesRowIndex = row.Index;
                        dgvEX_WC_BRAKE.Rows[intImportDdutiesRowIndex].Cells[item.Index].Value = dblImport_Dduties;
                        break;
                    }
                }

                #endregion

                #region Calculate Warehouse Costs Row

                int intWarehouseCostsRowIndex = 0;
                double dblWC_MUELLER_CURR = 0;

                if (item.Name == "BRAKE_MEGAFAT88_25" || item.Name == "BRAKE_MEGAFATEXTRA_25" || item.Name == "BRAKE_MEGAENERGY_25" || item.Name == "BRAKE_MEGABOOST_25" || item.Name == "BRAKE_MEGAONE_25")
                {
                    dblWC_MUELLER_CURR = Math.Round(dblWC_MUELLER_CURR_EUR_MT_25KG);
                }
                else if (item.Name == "BRAKE_MEGAFAT88_650" || item.Name == "BRAKE_MEGAFATEXTRA_650" || item.Name == "BRAKE_MEGAENERGY_650" || item.Name == "BRAKE_MEGABOOST_650" || item.Name == "BRAKE_MEGAONE_650")
                {
                    dblWC_MUELLER_CURR = Math.Round(dblWC_MUELLER_CURR_EUR_MT_BB);
                }

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Warehouse Costs", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intWarehouseCostsRowIndex = row.Index;
                        dgvEX_WC_BRAKE.Rows[intWarehouseCostsRowIndex].Cells[item.Index].Value = dblWC_MUELLER_CURR;
                        break;
                    }
                }

                #endregion

                #region Calculate Import charges and Handling Row

                int intImportChargesHandlingRowIndex = 0;
                double dblWC_MOLENBERGNATIE = 0;

                if (item.Name == "BRAKE_MEGAFAT88_25" || item.Name == "BRAKE_MEGAFATEXTRA_25" || item.Name == "BRAKE_MEGAENERGY_25" || item.Name == "BRAKE_MEGABOOST_25" || item.Name == "BRAKE_MEGAONE_25")
                {
                    dblWC_MOLENBERGNATIE = Math.Round(dblWC_MUELLER_QTY_24);
                }
                else if (item.Name == "BRAKE_MEGAFAT88_650" || item.Name == "BRAKE_MEGAFATEXTRA_650" || item.Name == "BRAKE_MEGAENERGY_650" || item.Name == "BRAKE_MEGABOOST_650" || item.Name == "BRAKE_MEGAONE_650")
                {
                    dblWC_MOLENBERGNATIE = Math.Round(dblWC_MUELLER_QTY_24_7);
                }

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Import charges and Handling", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intImportChargesHandlingRowIndex = row.Index;
                        dgvEX_WC_BRAKE.Rows[intImportChargesHandlingRowIndex].Cells[item.Index].Value = dblWC_MOLENBERGNATIE;
                        break;
                    }
                }

                #endregion

                #region Calculate Ex Warehouse (EUR/MT)

                int intExWarehouse_EUR_MT_RowIndex = 0;
                double dblExWarehouse_EUR_MT = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (
                        Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("CIF COST (EUR/MT) - Wilmar", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Import duties", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Warehouse Costs", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Import charges and Handling", StringComparison.CurrentCultureIgnoreCase)
                        )
                    {
                        dblExWarehouse_EUR_MT = dblExWarehouse_EUR_MT + Support.ConvertToDouble(row.Cells[item.Index].Value);
                    }

                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("Ex Warehouse (EUR/MT)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intExWarehouse_EUR_MT_RowIndex = row.Index;

                        if (dblCIF_COST_EUR_MT_Wilmar <= 0)
                        {
                            dblExWarehouse_EUR_MT = 0;
                        }

                        dgvEX_WC_BRAKE.Rows[intExWarehouse_EUR_MT_RowIndex].Cells[item.Index].Value = dblExWarehouse_EUR_MT;
                        break;
                    }
                }

                #endregion

                #region Calculate T2 - Imported cost (Not-warehoused) Row

                int intT2_Imported_Cost_RowIndex = 0;
                double dblT2_Imported_Cost = 0;

                dblT2_Imported_Cost = dblExWarehouse_EUR_MT - dblWC_MUELLER_CURR;

                foreach (DataGridViewRow row in dgvEX_WC_BRAKE.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BRAKE_Type"].Value).Equals("T2 - Imported cost (Not-warehoused)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intT2_Imported_Cost_RowIndex = row.Index;
                        dgvEX_WC_BRAKE.Rows[intT2_Imported_Cost_RowIndex].Cells[item.Index].Value = dblT2_Imported_Cost;
                        break;
                    }
                }

                #endregion
            }
        }

        private void Calculate_EX_WC_BIBBY()
        {
            #region Get Prodct Data           

            string strQuery = " SELECT * FROM PRODUCTS WHERE isdeleted = 0 Order by ProductId ";
            DataTable dtProduct = objclsSqlLayer.GetDataTable(strQuery);

            #endregion

            #region Get Destination Data

            strQuery = " SELECT * FROM Destination WHERE isdeleted = 0 Order by DestinationId ";
            DataTable dtDestination = objclsSqlLayer.GetDataTable(strQuery);

            var varDestinationBibby = dtDestination.AsEnumerable().Where(row => Support.ConvertToString(row["Destination"]).Equals("Southampton", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("Freight_40_FCL"));
            double dblDestinationBibby = 0;

            if (varDestinationBibby != null)
            {
                dblDestinationBibby = Support.ConvertToDouble(varDestinationBibby.FirstOrDefault());
            }

            #endregion

            #region Get Forex Data           

            strQuery = " SELECT * FROM Forex WHERE isdeleted = 0 Order by ForexId ";
            DataTable dtForex = objclsSqlLayer.GetDataTable(strQuery);

            double dblEUR_USD = 0;

            foreach (DataRow drTemp in dtForex.Rows)
            {
                if (Support.ConvertToString(drTemp["Currency"]).Equals("EUR/USD", StringComparison.CurrentCultureIgnoreCase))
                {
                    dblEUR_USD = Support.ConvertToDouble(drTemp["Rate"]);
                    break;
                }
            }

            #endregion

            #region Get WC_BIBBY_CURR Data

            strQuery = " SELECT * FROM WC_BIBBY_CURR WHERE isdeleted = 0 Order by BIBBY_CURR_ID ";
            DataTable dtWC_BIBBY_CURR = objclsSqlLayer.GetDataTable(strQuery);

            double dblWC_BIBBY_CURR_EUR_MT_25KG = 0;
            double dblWC_BIBBY_CURR_EUR_MT_BB = 0;

            foreach (DataRow drTemp in dtWC_BIBBY_CURR.Rows)
            {
                dblWC_BIBBY_CURR_EUR_MT_25KG = dblWC_BIBBY_CURR_EUR_MT_25KG + Support.ConvertToDouble(drTemp["EUR_MT_25KG"]);
                dblWC_BIBBY_CURR_EUR_MT_BB = dblWC_BIBBY_CURR_EUR_MT_BB + Support.ConvertToDouble(drTemp["EUR_MT_BB"]);
            }

            #endregion

            #region Get WC_WC_BIBBY_Data

            strQuery = " SELECT * FROM WC_BIBBY WHERE isdeleted = 0 Order by BIBBY_ID ";
            DataTable dtWC_BIBBY = objclsSqlLayer.GetDataTable(strQuery);

            double dblWC_BIBBY_QTY_24 = 0;
            double dblWC_BIBBY_QTY_24_7 = 0;

            foreach (DataRow drTemp in dtWC_BIBBY.Rows)
            {
                dblWC_BIBBY_QTY_24 = dblWC_BIBBY_QTY_24 + Support.ConvertToDouble(drTemp["QTY_24"]);
                dblWC_BIBBY_QTY_24_7 = dblWC_BIBBY_QTY_24_7 + Support.ConvertToDouble(drTemp["QTY_24_7"]);
            }

            #endregion

            foreach (DataGridViewColumn item in dgvEX_WC_BIBBY.Columns)
            {
                if (item.Name == "BIBBY_ID" || item.Name == "BIBBY_Type")
                {
                    continue;
                }

                #region Calculate CIF COST - USD (Wilmar) Row

                int FTL_MT_Rowindex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
                    {
                        FTL_MT_Rowindex = row.Index;
                        break;
                    }
                }

                double dblFTL_MT = Support.ConvertToDouble(dgvEX_WC_BIBBY.Rows[FTL_MT_Rowindex].Cells[item.Index].Value);
                double dbl13 = 0;

                if (item.Name == "BIBBY_MEGAFAT88_25" || item.Name == "BIBBY_MEGAFAT88_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAFAT88")).FirstOrDefault());
                }
                else if (item.Name == "BIBBY_MEGAFATEXTRA_25" || item.Name == "BIBBY_MEGAFATEXTRA_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAFATEXTRA")).FirstOrDefault());
                }
                else if (item.Name == "BIBBY_MEGAENERGY_25" || item.Name == "BIBBY_MEGAENERGY_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAENERGY")).FirstOrDefault());
                }
                else if (item.Name == "BIBBY_MEGABOOST_25" || item.Name == "BIBBY_MEGABOOST_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGABOOST")).FirstOrDefault());
                }
                else if (item.Name == "BIBBY_MEGAONE_25" || item.Name == "BIBBY_MEGAONE_650")
                {
                    dbl13 = Support.ConvertToDouble(dtProduct.AsEnumerable().Where(row => Support.ConvertToString(row["Product"]).Equals("FOB (USD/MT)", StringComparison.CurrentCultureIgnoreCase)).Select(row => row.Field<string>("MEGAONE")).FirstOrDefault());
                }

                int CIF_COST_USD_Wilmar_Rowindex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("CIF COST - USD (WILMAR)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        CIF_COST_USD_Wilmar_Rowindex = row.Index;
                        break;
                    }
                }

                double dblCIF_COST_USD_Wilmar = Math.Round(dbl13 + (dblDestinationBibby / dblFTL_MT));

                dgvEX_WC_BIBBY.Rows[CIF_COST_USD_Wilmar_Rowindex].Cells[item.Index].Value = dblCIF_COST_USD_Wilmar;

                #endregion

                #region Calculate CIF COST (EUR/MT) - Wilmar Row

                double dblCIF_COST_EUR_MT_Wilmar = Math.Round(dblCIF_COST_USD_Wilmar / dblEUR_USD);
                int dblCIF_COST_EUR_MT_Wilmar_RowIndex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("CIF COST (EUR/MT) - Wilmar", StringComparison.CurrentCultureIgnoreCase))
                    {
                        dblCIF_COST_EUR_MT_Wilmar_RowIndex = row.Index;
                        break;
                    }
                }

                dgvEX_WC_BIBBY.Rows[dblCIF_COST_EUR_MT_Wilmar_RowIndex].Cells[item.Index].Value = dblCIF_COST_EUR_MT_Wilmar;

                #endregion

                #region Calculate Import duties Row

                double dblProductMegaMalasia = 0;

                if (item.Name == "BIBBY_MEGAFAT88_25" || item.Name == "BIBBY_MEGAFAT88_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAFAT88"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BIBBY_MEGAFATEXTRA_25" || item.Name == "BIBBY_MEGAFATEXTRA_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAFATEXTRA"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BIBBY_MEGAENERGY_25" || item.Name == "BIBBY_MEGAENERGY_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAENERGY"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BIBBY_MEGABOOST_25" || item.Name == "BIBBY_MEGABOOST_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGABOOST"]);
                            break;
                        }
                    }
                }
                else if (item.Name == "BIBBY_MEGAONE_25" || item.Name == "BIBBY_MEGAONE_650")
                {
                    foreach (DataRow drTemp in dtProduct.Rows)
                    {
                        if (Support.ConvertToString(drTemp["Product"]).Equals("Malaysia", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dblProductMegaMalasia = Support.ConvertToDouble(drTemp["MEGAONE"]);
                            break;
                        }
                    }
                }

                double dblImport_Dduties = Math.Round((dblProductMegaMalasia / 100) * dblCIF_COST_EUR_MT_Wilmar);
                int intImportDdutiesRowIndex = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Import duties", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intImportDdutiesRowIndex = row.Index;
                        dgvEX_WC_BIBBY.Rows[intImportDdutiesRowIndex].Cells[item.Index].Value = dblImport_Dduties;
                        break;
                    }
                }

                #endregion

                #region Calculate Warehouse Costs Row

                int intWarehouseCostsRowIndex = 0;
                double dblWC_BIBBY_CURR = 0;

                if (item.Name == "BIBBY_MEGAFAT88_25" || item.Name == "BIBBY_MEGAFATEXTRA_25" || item.Name == "BIBBY_MEGAENERGY_25" || item.Name == "BIBBY_MEGABOOST_25" || item.Name == "BIBBY_MEGAONE_25")
                {
                    dblWC_BIBBY_CURR = Math.Round(dblWC_BIBBY_CURR_EUR_MT_25KG);
                }
                else if (item.Name == "BIBBY_MEGAFAT88_650" || item.Name == "BIBBY_MEGAFATEXTRA_650" || item.Name == "BIBBY_MEGAENERGY_650" || item.Name == "BIBBY_MEGABOOST_650" || item.Name == "BIBBY_MEGAONE_650")
                {
                    dblWC_BIBBY_CURR = Math.Round(dblWC_BIBBY_CURR_EUR_MT_BB);
                }

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Warehouse Costs", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intWarehouseCostsRowIndex = row.Index;
                        dgvEX_WC_BIBBY.Rows[intWarehouseCostsRowIndex].Cells[item.Index].Value = dblWC_BIBBY_CURR;
                        break;
                    }
                }

                #endregion

                #region Calculate Import charges and Handling Row

                int intImportChargesHandlingRowIndex = 0;
                double dblWC_MOLENBERGNATIE = 0;

                if (item.Name == "BIBBY_MEGAFAT88_25" || item.Name == "BIBBY_MEGAFATEXTRA_25" || item.Name == "BIBBY_MEGAENERGY_25" || item.Name == "BIBBY_MEGABOOST_25" || item.Name == "BIBBY_MEGAONE_25")
                {
                    dblWC_MOLENBERGNATIE = Math.Round(dblWC_BIBBY_QTY_24);
                }
                else if (item.Name == "BIBBY_MEGAFAT88_650" || item.Name == "BIBBY_MEGAFATEXTRA_650" || item.Name == "BIBBY_MEGAENERGY_650" || item.Name == "BIBBY_MEGABOOST_650" || item.Name == "BIBBY_MEGAONE_650")
                {
                    dblWC_MOLENBERGNATIE = Math.Round(dblWC_BIBBY_QTY_24_7);
                }

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Import charges and Handling", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intImportChargesHandlingRowIndex = row.Index;
                        dgvEX_WC_BIBBY.Rows[intImportChargesHandlingRowIndex].Cells[item.Index].Value = dblWC_MOLENBERGNATIE;
                        break;
                    }
                }

                #endregion

                #region Calculate Ex Warehouse (EUR/MT)

                int intExWarehouse_EUR_MT_RowIndex = 0;
                double dblExWarehouse_EUR_MT = 0;

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (
                        Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("CIF COST (EUR/MT) - Wilmar", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Import duties", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Warehouse Costs", StringComparison.CurrentCultureIgnoreCase) ||
                        Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Import charges and Handling", StringComparison.CurrentCultureIgnoreCase)
                        )
                    {
                        dblExWarehouse_EUR_MT = dblExWarehouse_EUR_MT + Support.ConvertToDouble(row.Cells[item.Index].Value);
                    }

                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("Ex Warehouse (EUR/MT)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intExWarehouse_EUR_MT_RowIndex = row.Index;

                        if (dblCIF_COST_EUR_MT_Wilmar <= 0)
                        {
                            dblExWarehouse_EUR_MT = 0;
                        }

                        dgvEX_WC_BIBBY.Rows[intExWarehouse_EUR_MT_RowIndex].Cells[item.Index].Value = dblExWarehouse_EUR_MT;
                        break;
                    }
                }

                #endregion

                #region Calculate T2 - Imported cost (Not-warehoused) Row

                int intT2_Imported_Cost_RowIndex = 0;
                double dblT2_Imported_Cost = 0;

                dblT2_Imported_Cost = dblExWarehouse_EUR_MT - dblWC_BIBBY_CURR;

                foreach (DataGridViewRow row in dgvEX_WC_BIBBY.Rows)
                {
                    if (Support.ConvertToString(row.Cells["BIBBY_Type"].Value).Equals("T2 - Imported cost (Not-warehoused)", StringComparison.CurrentCultureIgnoreCase))
                    {
                        intT2_Imported_Cost_RowIndex = row.Index;
                        dgvEX_WC_BIBBY.Rows[intT2_Imported_Cost_RowIndex].Cells[item.Index].Value = dblT2_Imported_Cost;
                        break;
                    }
                }

                #endregion
            }
        }

        #endregion

        #region DataGridview Events

        private void dgvEX_WC_ANTWERP_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            return;

            DataGridViewRow row = dgvEX_WC_ANTWERP.Rows[e.RowIndex];

            if (Support.ConvertToString(dgvEX_WC_ANTWERP.Rows[e.RowIndex].Cells["Type"].Value).ToUpper().Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
            {
                row.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(255, 192, 192);
            }
            else
            {
                row.ReadOnly = true;
            }
        }

        private void dgvEX_WC_BRAKE_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            return;

            DataGridViewRow row = dgvEX_WC_BRAKE.Rows[e.RowIndex];

            if (Support.ConvertToString(dgvEX_WC_BRAKE.Rows[e.RowIndex].Cells["BRAKE_Type"].Value).ToUpper().Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
            {
                row.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(255, 192, 192);
            }
            else
            {
                row.ReadOnly = true;
            }
        }

        private void dgvEX_WC_BIBBY_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            return;

            DataGridViewRow row = dgvEX_WC_BIBBY.Rows[e.RowIndex];

            if (Support.ConvertToString(dgvEX_WC_BIBBY.Rows[e.RowIndex].Cells["BIBBY_Type"].Value).ToUpper().Equals("FTL MT", StringComparison.CurrentCultureIgnoreCase))
            {
                row.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(255, 192, 192);
            }
            else
            {
                row.ReadOnly = true;
            }
        }

        #endregion
    }
}
