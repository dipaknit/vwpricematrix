﻿namespace VWPriceMatrix
{
    partial class frmMegalacEUR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMegalacEUR));
            this.panel1 = new General.Controls.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new General.Controls.Panel();
            this.btnMegalacExFactory = new General.Controls.Button();
            this.dgvMegalacEUR = new General.Controls.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Field = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelHeader1 = new General.Controls.PanelHeader();
            this.headerLabel3 = new General.Controls.HeaderLabel();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMegalacEUR)).BeginInit();
            this.panelHeader1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.ApplyMySettings = true;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.CtrlProperty = "";
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 730);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelHeader1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dgvMegalacEUR, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.ApplyMySettings = true;
            this.panel7.Controls.Add(this.btnMegalacExFactory);
            this.panel7.CtrlProperty = "";
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel7.Size = new System.Drawing.Size(1008, 30);
            this.panel7.TabIndex = 132;
            // 
            // btnMegalacExFactory
            // 
            this.btnMegalacExFactory.ApplyMySettings = true;
            this.btnMegalacExFactory.CtrlProperty = "Save";
            this.btnMegalacExFactory.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMegalacExFactory.Location = new System.Drawing.Point(930, 0);
            this.btnMegalacExFactory.Name = "btnMegalacExFactory";
            this.btnMegalacExFactory.Size = new System.Drawing.Size(75, 30);
            this.btnMegalacExFactory.TabIndex = 23;
            this.btnMegalacExFactory.Text = "Save";
            this.btnMegalacExFactory.UseVisualStyleBackColor = true;
            this.btnMegalacExFactory.Click += new System.EventHandler(this.btnMegalacExFactory_Click);
            // 
            // dgvMegalacEUR
            // 
            this.dgvMegalacEUR.AllowUpDownKey = false;
            this.dgvMegalacEUR.AllowUserToAddRows = false;
            this.dgvMegalacEUR.AllowUserToDeleteRows = false;
            this.dgvMegalacEUR.AllowUserToResizeRows = false;
            this.dgvMegalacEUR.ApplyMySettings = false;
            this.dgvMegalacEUR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMegalacEUR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Field,
            this.Description,
            this.Amount,
            this.Currency});
            this.dgvMegalacEUR.ctlInfoMessage = null;
            this.dgvMegalacEUR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMegalacEUR.Location = new System.Drawing.Point(3, 63);
            this.dgvMegalacEUR.MyAutoGenerateColumn = false;
            this.dgvMegalacEUR.MyEditGrid = true;
            this.dgvMegalacEUR.Name = "dgvMegalacEUR";
            this.dgvMegalacEUR.Size = new System.Drawing.Size(1002, 664);
            this.dgvMegalacEUR.strInfoMsgArr = null;
            this.dgvMegalacEUR.TabIndex = 0;
            this.dgvMegalacEUR.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvMegalacEUR_CellFormatting);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Field
            // 
            this.Field.DataPropertyName = "Field";
            this.Field.HeaderText = "Field";
            this.Field.Name = "Field";
            this.Field.ReadOnly = true;
            this.Field.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Field.Width = 150;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Description.Width = 400;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Amount.Width = 150;
            // 
            // Currency
            // 
            this.Currency.DataPropertyName = "Currency";
            this.Currency.HeaderText = "Currency";
            this.Currency.Name = "Currency";
            this.Currency.ReadOnly = true;
            this.Currency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panelHeader1
            // 
            this.panelHeader1.ApplyMySettings = true;
            this.panelHeader1.Controls.Add(this.headerLabel3);
            this.panelHeader1.CtrlProperty = "";
            this.panelHeader1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader1.Location = new System.Drawing.Point(3, 3);
            this.panelHeader1.Name = "panelHeader1";
            this.panelHeader1.Size = new System.Drawing.Size(1002, 24);
            this.panelHeader1.TabIndex = 141;
            // 
            // headerLabel3
            // 
            this.headerLabel3.ApplyMySettings = true;
            this.headerLabel3.AutoSize = true;
            this.headerLabel3.CtrlProperty = "Megalac Ex-Factory";
            this.headerLabel3.Location = new System.Drawing.Point(8, 6);
            this.headerLabel3.Name = "headerLabel3";
            this.headerLabel3.Size = new System.Drawing.Size(101, 13);
            this.headerLabel3.TabIndex = 0;
            this.headerLabel3.Text = "Megalac Ex-Factory";
            // 
            // frmMegalacEUR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMegalacEUR";
            this.Text = "Megalac Ex-Factory";
            this.Load += new System.EventHandler(this.frmMegalacEUR_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMegalacEUR)).EndInit();
            this.panelHeader1.ResumeLayout(false);
            this.panelHeader1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private General.Controls.Panel panel1;
        private General.Controls.DataGridView dgvMegalacEUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private General.Controls.Panel panel7;
        private General.Controls.Button btnMegalacExFactory;
        private General.Controls.PanelHeader panelHeader1;
        private General.Controls.HeaderLabel headerLabel3;
    }
}