﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VWPriceMatrix
{
    public partial class Login : Form
    {
        #region Variable
        clsSqlLayer objclsSqlLayer = new clsSqlLayer();
        #endregion

        public Login()
        {
            InitializeComponent();
        }

        #region Button Event        
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string strPassword = txtPassword.Text.Trim();
            string strUserName = txtUserName.Text.Trim();

            if (strUserName.Length == 0)
            {
                MessageBox.Show("Please enter user name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserName.Focus();
                return;
            }

            if (strPassword.Length == 0)
            {
                MessageBox.Show("Please enter pasword.", "Error",  MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPassword.Focus();
                return;
            }           

            

            //objclsSqlLayer.MakeDBConnection();

            string strQuery = "select UserId, UserName, Password, Role from Users where IsActive = 1";

            DataTable dtContactDate = objclsSqlLayer.GetDataTable(strQuery);

            //objclsSqlLayer.CloseDBConnection();           

            foreach (DataRow drTemp in dtContactDate.Rows)
            {
                if (drTemp["UserName"].ToString().Equals(strUserName, StringComparison.CurrentCultureIgnoreCase) && drTemp["Password"].ToString().Equals(strPassword, StringComparison.CurrentCultureIgnoreCase))
                {
                    Support.IsAuthenticated = true;
                    Support.Role = drTemp["Role"].ToString();
                    Support.UserId = drTemp["UserId"].ToString();
                    break;                    
                }
            }

            if (Support.IsAuthenticated == false)
            {
                MessageBox.Show("User name or pasword incorrect.\r\nPlease try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.Close();
        }

        #endregion

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(null, null);
            }
        }
    }
}
